<?php

	require($_SERVER['DOCUMENT_ROOT'] . 'admin/php/ctd.php');

	if ($ctd::exec('user.logout')) {
		Header("Location: news.php");
	} else {
		echo 'Something went wrong';
	}

?>