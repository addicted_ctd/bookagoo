<?php

require 'smarty_init.php';

$smarty->clearCompiledTemplate('preview.tpl');

$child_info = $ctd::exec('constructor.get_child_info', (object) array('uid' => $_GET['u']));
$child_info = $child_info[0];

$month = ['Января', 'Февраля', 'Марта', 'Апреля', 'Мая', 'Июня', 'Июля', 'Августа', 'Сентября', 'Октября', 'Ноября', 'Декабря'];

$birth = explode('.', $child_info['birth_date']);
$child_info['birth_full'] = $birth[0] . ' ' . $month[(int) $birth[1]] . ' ' . $birth[2];
$additional = $ctd::exec('constructor.get_additional', (object) array('uid' => $_GET['u']));
$constructor = $ctd::exec('constructor.get_vars', (object) array('card_id' => $_GET['card'], 'uid' => $_GET['u']));


$smarty->assign('additional', $additional);
$smarty->assign('constructor', $constructor);
$smarty->assign('child_info', $child_info);
$smarty->display('preview.tpl');