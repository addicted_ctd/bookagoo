<?php

define('SMARTY_RESOURCE_CHAR_SET', 'UTF-8');
require('/usr/local/lib/php/Smarty/Smarty.class.php');
require('/home/quantum/webpoetry.org/bookagoo/docs/app/php/ctd.php');
$smarty = new Smarty();

$path = $_SERVER['DOCUMENT_ROOT'] . 'smarty/';

$smarty->setTemplateDir($path . 'templates');
$smarty->setCompileDir($path . 'templates_c');
$smarty->setCacheDir($path . 'cache');
$smarty->setConfigDir($path . 'configs');

$login_status = $ctd::exec('user.check_auth');
$general = $ctd::exec('root.get_general');

if ($login_status) {
	$uid = $ctd::exec('user.uid');
	$smarty->assign('uid', $uid);
}
$smarty->assign('general', $general); 
$smarty->assign('login_status', $login_status);