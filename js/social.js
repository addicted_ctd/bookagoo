$(document).ready(function(){  
  var vk = new Vkontakte();
  var fb = new Facebook();
  var ins = new Instagram();
  var result = $(".socials_scroll_popup_result");
  var socials = $(".socials");
  var close = $(".socials_scroll_close");
  var vkontakte = $(".vk");
  var facebook = $(".fb");
  var instagram = $(".in");
  var scroll_popup = $(".socials_scroll_popup");
  var scroll_flag = true;
  var remote_img;
  var remote_upload_target;

  // ALL HELPERS
  result.on("click",".image", function(){
    remote_img = $(this).data("url");
    remote_upload_target = $('.remote_upload_target');
    remote_upload_target.addClass('uploading');
    $('.socials').removeClass('active');
    Global.test_ajax({
      mod: 'root.download_image',
      data: {
        remote_img: remote_img
      }
    }, function(data) {
      remote_upload_target.removeClass('uploading');
      Photo.upload_callback(data, remote_upload_target, remote_upload_target.find("figure").css("background-image"));
    });
  });

  function text(text){
    result.text(text);
  }

  function images(objects, callback){  
    result.html("");  
    scroll_popup.unbind("scroll",scroll_instagram);
    for(var i=0; i<objects.length; i++){
      if (callback){
        callback(objects[i]);
      }   
    }
  }

  function soc_back(social){
    var back = $('<div />', {
        class: "socials_scroll_popup_result_back "+social
      });
    back.prependTo(result);
  }

  function image(url){      
    var img = $('<div />', {
        class: "image", 
        "data-url": url,
        style: "background-image: url('" + url +"')"
      });
    img.appendTo(result);
  }

  function album(hash){
    var span = "<span>"+hash.name+"</span>"; 
    var album = $('<div />', {
        class: "album "+hash.class, 
        "data-user": hash.user_id,
        "data-album": hash.album_id,
        style: "background-image: url('" + hash.picture +"')"
      }).filter(".album").html(span);
    album.appendTo(result);
    result.addClass('no_bg');
  }

  function active(element){
    $(".socials_scroll .active").removeClass("active");
    $(element).addClass("active");
  }

  close.click(function(){
    socials.removeClass("socials_show");
  });

  socials.click(function(event){
    if ($(event.target).closest(".socials_scroll").length) return;
    socials.removeClass("socials_show");
  });

  // VKONTAKTE METHODS

  function vk_click(){
    var self = this;
    active(vkontakte);
    vk.login(function(result){
      vk.getAlbums(function(albums){
        images(albums, function(one_album){
          vk.lastPhoto(one_album.aid, one_album.owner_id, function(photo){
            var hash = {
              album_id: one_album.aid,
              user_id: one_album.owner_id,
              picture: vk.bigUrl(photo),
              name: one_album.title,
              class: "album_vk"
            }
            album(hash);
          });
        });        
      });
    });
  }

  result.on("click", ".album_vk",function(){
    vk.getPhotosForAlbumId($(this).data("album"),$(this).data("user"),function(photos){
      images(photos, function(photo){
        image(vk.bigUrl(photo));
      });
      soc_back("social_back_vk");
    });
  });
  
  vkontakte.click(vk_click);
  result.on("click", ".social_back_vk",vk_click);

  // FACEBOOK METHODS

  function fb_click(){
    active(facebook);
    fb.login(function(login_response){
      var accessToken = login_response.authResponse.accessToken || '';
      fb.getAlbums(function(albums){
        images(albums.data, function(one_album){
          if(one_album.cover_photo){
            var hash = {
                album_id: one_album.id,
                picture: fb.makeFacebookPhotoURL( one_album.cover_photo.id, accessToken ),
                name: one_album.name,
                class: "album_fb"
              }
            album(hash);            
          }
        });

      });
    });  
  }

  result.on("click", ".album_fb",function(){
    var self = this;
    fb.login(function(login_response){
      var accessToken = login_response.authResponse.accessToken || '';
      fb.getPhotosForAlbumId($(self).data("album"),function(album_id, photos){
        images(photos.data, function(photo){
          var url = fb.makeFacebookPhotoURL( photo.id, accessToken );
          image(url);
        });
        soc_back("social_back_fb");
      });
    });    
  });

  facebook.click(fb_click);
  result.on("click", ".social_back_fb",fb_click);


  // INSTAGRAM METHODS
  function inst_load_image(){
    if (scroll_flag){
      var self = this;
      scroll_flag = false;
      ins.getUserImages(12,function(photos){
        for(var i=0; i<photos.length; i++){
          image(photos[i].images.standard_resolution.url);        
        }  
        scroll_flag = true;
      });       
    }
  }

  function scroll_instagram(){   
    if(scroll_popup.scrollTop() + scroll_popup.innerHeight() >= scroll_popup[0].scrollHeight - 50) {
      inst_load_image();
    }   
  }

   
  instagram.click(function(){
    result.html("");
    active(this);
    ins.setIdNull();
    scroll_popup.bind("scroll",scroll_instagram);
    inst_load_image();
  });
  result.on("click", ".social_back_in",inst_load_image);
 
});