var Overlay = {

	box: '.alert',

	init: function() {
		this.bind();
	},

	bind: function() {
		$(document).on('click', this.box + ' .close', function() {
			Overlay.hide();
		});
	},

	show: function(c) {
		$(this.box).hide();
		$(c).show();
		$('body').addClass('ov');
		console.log(c);
	},

	hide: function() {
		$('body').removeClass('ov');
	}

};

Overlay.init();