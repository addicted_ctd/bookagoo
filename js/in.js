// CLASS
var Instagram = (function() {
  function Instagram() {
    this.events = $({});
    this.access_token = null;
    this.client_id = "60ae45b0f6bc42e9adcb43694127b4da";
    this.last_id = null;
  }

  Instagram.prototype.login = function(callback){
    var popupWidth = 700,
      popupHeight = 500,
      popupLeft = (window.screen.width - popupWidth) / 2,
      popupTop = (window.screen.height - popupHeight) / 2,
      self = this;
      var url = 'https://instagram.com/oauth/authorize/?client_id='+self.client_id+'&redirect_uri='+window.location.href+'&response_type=token'; 
      var popup = window.open(url, '', 'width='+popupWidth+',height='+popupHeight+',left='+popupLeft+',top='+popupTop+'');
      popup.onload = function() {
        // if(window.location.hash.length == 0) {
        //   popup.open('https://instagram.com/oauth/authorize/?client_id='+self.client_id+'&redirect_uri='+window.location.href+'&response_type=token', '_self');
        // }
        var interval = setInterval(function() {
          try {
            if(popup.location.hash.length) {
              clearInterval(interval);
              self.access_token = popup.location.hash.slice(14); //slice #access_token= from string
              popup.close();
              if(callback) {
                callback(self.access_token);
              };
            }
          }
          catch(evt) {
            //permission denied
          }
        }, 100);
      }  
  }
  
  Instagram.prototype.getImages = function(count,callback){
    var url = null;
    var self = this;
    if(this.last_id){
      url = "https://api.instagram.com/v1/users/self/media/recent?count="+count+"&access_token="+this.access_token+"&max_id="+this.last_id;      
    }else{
      url = "https://api.instagram.com/v1/users/self/media/recent?count="+count+"&access_token="+this.access_token;
    }
    $.ajax({
      type: "POST",
      url: url,
      dataType: 'jsonp',
      cache: false,
      success: function (response) {
        if(callback && response.data){
          if(response.data.length > 0){
            self.last_id = response.data[response.data.length - 1].id
          }
          callback(response.data);
        }
      }        
    });
  }

  Instagram.prototype.getUserImages = function(count,callback){
    var self = this;
    if (self.access_token){
      self.getImages(count,function(data){
        if(callback){
          callback(data);
        }
      });
    }else{
      self.login(function(){
        self.getImages(count,function(data){
          if(callback){
            callback(data);
          }
        });
      });
    }
  }
  
  Instagram.prototype.setIdNull = function(){
    this.last_id = null;
  }

 return Instagram;
})();

window.Instagram = Instagram;