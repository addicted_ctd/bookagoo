﻿

// CLASS
var Vkontakte = (function() {
  function Vkontakte() {
    this.events = $({});
  }

  Vkontakte.prototype.login = function(callback){
    var self = this;
    VK.Auth.login(function(response){
    if (response.session) { 
      self.hasPermision(4,response.session.user.id, function(){
        if(callback){
          callback(response);
        }        
      })
    }
    },1028)
  };

  Vkontakte.prototype.hasPermision = function(permision, user_id, callback){
    VK.Api.call("account.getAppPermissions",{user_id: user_id},function(response){
      if((response.response&permision) == permision){
        if (callback){
          callback();
        }
      }
    })
  }

  Vkontakte.prototype.getAlbums = function(callback){
    this.getIdUser(function(ids){
      VK.Api.call("photos.getAlbums",{owner_id: ids},function(response){
        if(callback){
          callback(response.response);
        }
      })
    });      
  }

  Vkontakte.prototype.getPhotosForAlbumId = function(albumId, ownerId, callback){
    VK.Api.call("photos.get",{owner_id: ownerId, album_id: albumId},function(response){
      if(callback){
        callback(response.response);
      }
    });
  }

  Vkontakte.prototype.getIdUser = function(callback){
    var self = this;
    VK.Auth.getLoginStatus(function(response){
      if (response.session) {
        if (callback){
          callback(response.session.mid);
        }
      }else{
        self.login(function(){
          if (callback){
            callback(response.session.mid);
          }
        })
      }
    });
  }

  Vkontakte.prototype.getPhotos = function(callback){
    var self = this;
    self.getAlbums(function(albomus){
       for(i=0; i<albomus.length; i++){
        self.getPhotosForAlbumId(albomus[i].aid, albomus[i].owner_id, function(photos){
          if (callback){
            callback(photos);
          }
        })
      }
    })
  }

  Vkontakte.prototype.lastPhoto = function(albumId, ownerId,callback){
    this.getPhotosForAlbumId(albumId, ownerId, function(photos){
      if (photos && callback){
        callback(photos[photos.length -1]);
      }
    });
  }

  Vkontakte.prototype.bigUrl = function(photo){
    var url = photo.src_xxbig || photo.src_xbig || photo.src_big || photo.src || photo.src_small;
    return url;
  }

  Vkontakte.prototype.appPermision = function(callback){
    var self = this;
    self.getIdUser(function(ids){
      VK.Api.call("account.getAppPermissions",{user_id: ids},function(response){
        if((response.response&4) == 4){
          if(callback){
            callback();
          }
        }else{
          self.login(function(){
            if(callback){
              callback();
            }
          });
        }
        
      });  
    });    
  }

 return Vkontakte;
})();

window.Vkontakte = Vkontakte;
