

// CLASS
var Facebook = (function() {
  function Facebook() {
    this.events = $({});
    this.accessToken = null;
  }

  Facebook.prototype.makeFacebookPhotoURL = function (id, accessToken) {
    return 'https://graph.facebook.com/' + id + '/picture?access_token=' + accessToken;
  }  

  Facebook.prototype.login = function (callback){
    FB.login(function(response){
      console.log(response);
      if (response.authResponse){
        if (callback){
          callback(response);
        }
      }else{
        console.log('User cancelled login or did not fully authorize.');
      }
    },{scope: 'user_photos', return_scopes: true} );
  }

  Facebook.prototype.getAlbums = function (callback){
    FB.api(
      '/me/albums',
      {fields: 'id,name,cover_photo'},
      function(albumResponse){
        if (callback){
          console.log(albumResponse);
          callback(albumResponse);
        }
      }
    );
  }

  Facebook.prototype.addPermission = function(callback){
    var self = this;
    if(self.accessToken){
      if(callback){
        callback(self.accessToken);
      }
    }else{
      self.login(function(loginResponse){
        self.accessToken = loginResponse.authResponse.accessToken || '';
        if(callback){
          callback(self.accessToken);
        }
      });
    }
  }

  Facebook.prototype.getPhotosForAlbumId = function (albumId, callback){
    FB.api(
      '/'+albumId+'/photos',
      {fields: 'id'},
      function(albumPhotosResponse){
        if (callback){
          callback(albumId, albumPhotosResponse);
        }
      }
    );
  }

  Facebook.prototype.getLikesForPhotoId = function (photoId, callback){
    FB.api(
      '/'+albumId+'/photos/'+photoId+'/likes',
      {},
      function(photoLikesResponse){
        if (callback){
          callback(photoId, photoLikesResponse);
        }
      }
    );
  }

  Facebook.prototype.getPhotos = function (callback){
    var allPhotos = [];
    var accessToken = '';
    self = this;
    self.login(function(loginResponse){
      accessToken = loginResponse.authResponse.accessToken || '';
      self.getAlbums(function(albumResponse) {
          var i, album, deferreds = {}, listOfDeferreds = [];
          for (i = 0; i < albumResponse.data.length; i++) {
            album = albumResponse.data[i];
            deferreds[album.id] = $.Deferred();
            listOfDeferreds.push( deferreds[album.id] );
            self.getPhotosForAlbumId( album.id, function( albumId, albumPhotosResponse ) {
                var i, facebookPhoto;
                for (i = 0; i < albumPhotosResponse.data.length; i++) {
                  facebookPhoto = albumPhotosResponse.data[i];
                  allPhotos.push({
                    'id'  : facebookPhoto.id,
                    'added' : facebookPhoto.created_time,
                    'url' : self.makeFacebookPhotoURL( facebookPhoto.id, accessToken )
                  });
                }
                deferreds[albumId].resolve();
              });
          }
          $.when.apply($, listOfDeferreds ).then( function() {
            if (callback) {
              callback( allPhotos );
            }
          }, function( error ) {
            if (callback) {
              callback( allPhotos, error );
            }
          });
        });
    });
  }

 return Facebook;
})();

window.Facebook = Facebook;
