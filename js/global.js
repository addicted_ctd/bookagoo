var Global = {

    ajax: function(params, callback) {
        if (!params.hasOwnProperty('data')) params.data = {};
        $.ajax({type:'POST',url:'php/router.php',dataType:'JSON',data:{mod:params.mod,data:JSON.stringify(params.data)},success:function(data){if(typeof callback!= "undefined")callback(data);}});
    },

    test_ajax: function(params, callback) {
        if (!params.hasOwnProperty('data')) params.data = {};
        $.ajax({type:'POST',url:'php/router.php',data:{mod:params.mod,data:JSON.stringify(params.data)},success:function(data){if(typeof callback!= "undefined")callback(data);}});
    },

    check_inputs: function(req_arr) {
        var len = req_arr.length;
        var check = true;
        for (var i = 0; i < len; i++) {
            if (req_arr[i].val() === "") {
                check = false;
                this.input_highlight(req_arr[i]);
            }
        }
        return check;
    },

    input_highlight: function(block) {
        block.addClass('wrong');
        setTimeout(function() {
            block.removeClass('wrong');
        }, 600);
    },

    check_mail: function(input) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(input.val());
    },

    fields_match: function(field_1, field_2) {
        if (field_1.val() === field_2.val())
            return true;
        return false;
    },

    value_length: function(el, len) {
        return (el.val().length >= len);
    },

    vk_login: function(response) {
        if (response.session) {
            Global.ajax({
                mod: 'user.vk_login',
                data: response
            }, function(data) {
                console.log(data);
                window.location.reload();
            });
        }
        console.log(response);
    },

    fb_login: function() {
        var data = {};
        FB.login(function(response) {
            if (response.authResponse) {
                data.signature = response.authResponse.signedRequest;
                FB.api('/me', function(res) {
                    var split_name = res.name.split(' ');
                    data.f_name = split_name[0];
                    data.l_name = split_name[2];
                    if (res.hasOwnProperty('email'))
                        data.email = res.email;
                    data.fb_id = res.id;
                    Global.ajax({
                        mod: 'user.fb_login',
                        data: data
                    }, function(data) {
                        window.location.reload();
                    });
                });
            }
        }, {scope: 'email,public_profile,user_photos', return_scopes: true});
    }

}