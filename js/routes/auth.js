var Auth = {
    init: function() {
        this.bind();
    },

    bind: function() {
        this.bind_pwd();
        this.bind_login();
    },

    bind_pwd: function() {
        var pwd_input = $('#user_password');
        $("#show_pwd").change(function() {
            if($(this).is(':checked')) {
                pwd_input.attr('type', 'text');
            } else {
                pwd_input.attr('type', 'password');
            }
        });
    },

    bind_login: function() {
        var input_fields = {
            email: $("#user_name"),
            password: $("#user_password")
        };

        $(document).keypress(function(e) {
            if(e.which == 13) {
                $("#login").click();
            }
        });

        $('#login').click(function() {

            if (!Global.check_inputs([
                input_fields.email, 
                input_fields.password])) return;

            if (!Global.check_mail(input_fields.email)) 
                return Global.input_highlight(input_fields.email);

            Global.ajax({
                mod: 'user.login',
                data: {
                    email: input_fields.email.val(),
                    password: input_fields.password.val()
                }
            }, function(data) {
                console.log(data);
                if (data.data.error === 1) {
                    return alert('Пользователь не существует');
                } else if (data.data.error === 2) {
                    return alert('Неправильный email или пароль');
                }
                window.location.reload();
            });
        });
    }
};

Auth.init();