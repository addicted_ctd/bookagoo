var Reg = {
	init: function() {
		this.bind();
	},

	bind: function() {

		this.bind_gender();
		this.bind_create();
		this.bind_pwd();

	},

	bind_create: function() {

		var input_fields = {
			email: $("[name='email']"),
			password: $("[name='pwd']"),
			kid_birth: $("[name='kid_birth']"),
			kid_name: $("[name='kid_name']")
		};

		input_fields.kid_birth.mask('99.99.9999');

		var gender, subscription;

		$('.create_account').click(function() {

			if (!Global.check_inputs([
				input_fields.email, 
				input_fields.password,
				input_fields.kid_birth,
				input_fields.kid_name])) return;

			if (!Global.check_mail(input_fields.email)) 
				return Global.input_highlight(input_fields.email);

			if (!Global.value_length(input_fields.password, 6))
				return alert('Пароль должен содержать минимум 6 символов');

			gender = $(".gender").filter('.active');
			if (gender.length === 0) return alert('Выберите пол ребенка');

			subscription = $("#subscription").is(':checked');
			console.log(subscription);

			Global.ajax({
				mod: 'user.register',
				data: {
					email: input_fields.email.val(),
					password: input_fields.password.val(),
					kid_birth_date: input_fields.kid_birth.val(),
					kid_name: input_fields.kid_name.val(),
					kid_gender: gender.data('gender'),
					subscription: subscription ? 1 : 0
				}
			}, function(data) {
				console.log(data);
				if (data.data.error === 1) {
					return alert('Email уже используется');
				}
				window.location.reload();
				console.log(data);
			});
		});

	},

	bind_pwd: function() {
		var pwd_input = $("[name='pwd']");
		$("#show_pwd").change(function() {
			$(this).is(':checked') ? 
				pwd_input.attr('type', 'text') : 
				pwd_input.attr('type', 'password');
		});
	},

	bind_gender: function() {
		var gender = $(".gender");
		gender.click(function() {
			gender.removeClass('active');
			$(this).addClass('active');
		});
	}
};

Reg.init();