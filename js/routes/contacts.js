var Contacts = {

	init: function() {
		this.bind();
	},

	bind: function() {
		
		var send_msg = $('#contacts').find('.send_message');
		var name = $('#contacts').find('[name="name"]');
		var email = $('#contacts').find('[name="email"]');
		var topic = $('#contacts').find('[name="topic"]');
		var msg = $('#contacts').find('[name="msg"]');

		send_msg.click(function() {
			if (!Global.check_inputs([name, email, topic, msg])) return;
			if (!Global.check_mail(email)) return Global.input_highlight(email);
			Global.ajax({
				mod: 'user.feedback',
				data: {
					name: name.val(),
					email: email.val(),
					topic: topic.val(),
					msg: msg.val()
				}
			}, function(data) {
				console.log(data);
			});
			name.val(''); email.val(''); topic.val(''); msg.val('');
			alert('Спасибо, сообщение отправлено');
		});	

	}

};

Contacts.init();