var Constructor = {
	init: function() {
		console.log(this.card);
		this.bind_inputs();
		this.mask_dates();
		this.init_dropdowns();
		this.init_notes();
	},

	init_notes: function() {
		if ($(".notes").length === 0) return;
		var txt_area = $(".notes").find("textarea");
		var old_val;
		var self = this;

		txt_area.focus(function() {
			old_val = $(this).val();
		});

		txt_area.blur(function() {
			if ($(this).val() !== old_val)
				self.write($(this).val(), $(this).data('constructor_var'), $(this).closest('.card').data('side'));
		});
	},

	init_dropdowns: function() {
		var self = this;
		var dropdown = $('ul[data-constructor_var]');
		dropdown.each(function() {
			$(this).find('li:contains("'+ $(this).data("selected") +'")').prependTo($(this));
		});

		dropdown.click(function() {
			if ( $(this).hasClass('opened') ){
				process_close($(this));
			}else{
				process_close($('.dropdown'));
				process_open($(this));
			}
		});

		function process_close(ul) {
			ul.find('.select_dropdown').remove();
			ul.removeClass('opened');
		}

		var target_ul;

		$(document).on('click', '.select_dropdown span', function() {
			target_ul.find('li:contains("'+ $(this).text() +'")').prependTo(target_ul);
			self.write($(this).text(), target_ul.data('constructor_var'), target_ul.closest('.card').data('side'));
		});

		var select_el = $('<div class="select_dropdown">');
		var select_el_cont = $('<section class="ins_drop_block_cont">');
		var select_el_ins = $('<section class="ins_drop_block">');
		var select_el_clone;
		var c;

		function process_open(ul) {
			target_ul = ul;
			select_el_clone_main = select_el.clone();
			select_el_clone_cont = select_el_cont.clone();
			select_el_clone = select_el_ins.clone();
			c = ul.children();
			for (var i = 1; i < c.length; i++) {
				if (!c.eq(i).hasClass('default'))
					select_el_clone.append('<span>'+c.eq(i).text()+'</span>');
			}
			// select_el
			ul.find('.select_dropdown').remove();
			ul.append(select_el_clone_main);
			ul.find('.select_dropdown').append(select_el_clone_cont);
			ul.find('.ins_drop_block_cont').append(select_el_clone);
			ul.addClass('opened');
		}
	},

	mask_dates: function() {
		$('.date').each(function() {
			$(this).mask('99.99.9999');
		});
		$(".comment").each(function(){
			$(this).attr("maxlength", 30);
		});
	},

	bind_inputs: function() {
		var self = this;
		var inputs = $("input[data-constructor_var][type='text']");
		var old_val;

		inputs.focus(function() {
			old_val = $(this).val();
			$(this).off('blur').blur(function() {
				if ($(this).val() !== old_val)
					self.write($(this).val(), $(this).data('constructor_var'), $(this).closest('.card').data('side'));
			});
		});

		console.log(inputs);
	},

	init_nav: function() {
		var nav = $('.card_nav');
		var menu_items = $('.constructor_menu').find('.items').find('a');
		var current_card = $('.constructor_menu').find('.active').index();
		console.log(current_card);
		if (current_card === 0) {
			nav.find('.left').hide();
			nav.find('.right').html(menu_items.eq(current_card + 1).text()).attr('href', menu_items.eq(current_card + 1).attr('href'));
		} else if (current_card === menu_items.length - 1) {
			nav.find('.right').hide();
			nav.find('.left').html(menu_items.eq(current_card - 1).text()).attr('href', menu_items.eq(current_card - 1).attr('href'));
		} else {
			nav.find('.right').html(menu_items.eq(current_card + 1).text()).attr('href', menu_items.eq(current_card + 1).attr('href'));
			nav.find('.left').html(menu_items.eq(current_card - 1).text()).attr('href', menu_items.eq(current_card - 1).attr('href'));
		}
	},

	write: function(val, var_id, side) {
		console.log({
				val: val,
				var_id: var_id,
				card_id: this.card
			});
		var self = this;
		Global.ajax({
			mod: 'constructor.write',
			data: {
				val: val,
				var_id: var_id,
				card_id: self.card,
				side: side
			}
		}, function(data) {
			console.log(data);
		});
	}
};