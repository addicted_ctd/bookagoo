var Tree = {
	init: function() {
		this.track();
		this.circlify();
		this.circlify_focus();
	},

	circlify: function() {
		var self = this;
		$('.circle_text').lettering().each(function() {
			self.degreefy($(this));
		});
	},

	circlify_focus: function() {
		var self = this;
		$('.circle_text').on('focus', function(){
			if ( $(this).text() == "Введите имя" || $(this).text() == "Введите&nbsp;имя" || $(this).text().indexOf("Введите") > -1 ){
				$(this).text("");
			}
			$(this).trigger('input');
		});
		$('.circle_text').on('click', function(){
			// $(this).trigger('input');
			self.placeCaretAtEnd($(this).get(0));
		});
	},

	track: function() {
		var self = this;
		$('.circle_text').on('keydown', function(event) {
			var keyC = event.keyCode;
			if ( $(this).text().length > 50 && keyC != 8){
				return false;
			}
		});
		$('.circle_text').on('input', function() {
			if( $(this).text().trim().length <= 0 ){
				$(this).html("&nbsp;");
			} else {
				// $(this).text( $(this).text().trim() );
			}
			$(this).lettering();
			self.degreefy($(this));
			self.placeCaretAtEnd($(this).get(0));
		});

		var old_val;

		$('.circle_text').on('focus', function() {
			old_val = $(this).text().trim();
			$(this).off('blur').on('blur', function() {
				if ( $(this).text().trim().length <= 0 ){
					$(this).text("Введите имя");
					$(this).lettering();
					// self.placeCaretAtEnd($(this).get(0));
					self.degreefy($(this));
				}
				if ($(this).text().trim() !== old_val) {
					Constructor.write($(this).text().trim(), $(this).data('constructor_var'), $(this).closest('.card').data('side'));
				}
			});
		});

		$('.br_s').click(function() {
			$(this).text($(this).text() === 'Брат' ? 'Сестра' : 'Брат');
			$(this).lettering();
			self.degreefy($(this));
			Constructor.write($(this).text().trim(), $(this).data('constructor_var'), $(this).closest('.card').data('side'));
		});

	},
	degreefy: function(el) {
		var chars = el.find('span');
		var len = chars.length;
		var deg = 0;
		var step = parseFloat(el.attr('deg'));
		var dir = parseInt(el.attr('dir'));
		var self = this;
		chars.each(function() {
			$(this).css(self.return_rotate(deg));
			deg += step * dir;
		});
		el.css(self.return_rotate(-(len-1) * dir * step / 2));
	},
	placeCaretAtEnd: function(el) {
	    // el.focus();
	    if (typeof window.getSelection != "undefined"
	            && typeof document.createRange != "undefined") {
	        var range = document.createRange();
	        range.selectNodeContents(el);
	        range.collapse(false);
	        var sel = window.getSelection();
	        sel.removeAllRanges();
	        sel.addRange(range);
	    } else if (typeof document.body.createTextRange != "undefined") {
	        var textRange = document.body.createTextRange();
	        textRange.moveToElementText(el);
	        textRange.collapse(false);
	        textRange.select();
	    }
	},
	return_rotate: function(deg) {
		return {
			'transform': 'rotate('+deg+'deg)',
			'-webkit-transform': 'rotate('+deg+'deg)',
			'-moz-transform': 'rotate('+deg+'deg)',
			'-o-transform': 'rotate('+deg+'deg)'
		}
	}
};

Tree.init();