var Photo = {
	el: '.photo',
    remote_uplaod_target: null,
	init: function() {
		this.make_final();
		this.get_templates();
		this.listen_events();
		this.add_del_photo();
		this.drop.check();
	},

	drop: {
        check: function() {
            if (typeof (window.FileReader) === 'undefined') {
                console.log('error');
            } else {
                this.init();
            }
        },
        init: function() {
            this.listen();
            this.block_unwanted();
        },
        block_unwanted: function() {
            $(document).on("drop", function(e) {
                e.preventDefault();
            });
            $(document).on("dragover", function(e) {
                e.preventDefault();
            });
        },
        listen: function() {
            var self = this;
            var photos = $(".photo");
            photos.on("drop", function(e) {
                e.preventDefault();
                e.stopPropagation();
                $(this).removeClass('file_over');
                var file = e.originalEvent.dataTransfer.files[0];
                Photo.upload_file(file, $(this), $(this).find("figure").css("background-image"));
            });
            photos.on("dragover", function(e) {
                e.preventDefault();
                e.stopPropagation();
                $(this).addClass("file_over");
            });
            photos.on("dragleave", function(e) {
                e.preventDefault();
                e.stopPropagation();
                $(this).removeClass("file_over");
            });
        }
    },

	make_final: function() {
		$('.photo').each(function() {
			if ($(this).find('figure').length > 0) {
				$(this).addClass('final');
                $(this).find('figure').wrap('<div class="figure_wrap"></div>');
			}
		});
	},

	bind_hover: function() {
		var self = this;
		var clone = self.hover_tpl.clone();

		$('.photo').append(clone);

		$('.photo > .inner > p').on("mouseenter", function() {
			$(this).closest('.photo').find('.photo_hover').addClass('active');
		});

		$('.photo').on("mouseleave", function() {
			$(this).find('.photo_hover').removeClass('active');
		});
	},

	add_del_photo: function(){
		var delBtn = $("<div class='del_this_photo'>");
		$('.photo').append(delBtn);
	},

	// bind_hover: function() {
	// 	var self = this;
	// 	var clone;
	// 	$(this.el).hover(function() {
	// 		if ($(this).find('.photo_editor').length !== 0) return;
	// 		clone = self.hover_tpl.clone();
	// 		$(this).find('.photo_hover').remove();
	// 		$(this).append(clone);
	// 		$(this).find('.photo_hover').fadeIn(200);
	// 	}, function() {
	// 		$(this).find('.photo_hover').stop().fadeOut(200, function() {$(this).remove();});
	// 	});
	// },

	listen_events: function() {

		var self = this;

		$(document).on('click', '.load_image, .photo_editor, .comment', function(e) {
			e.stopPropagation();
		});

		$(document).on('change', '.load_image', function(e) {
			self.change_action($(this));
		});

		$(document).on('click', '.photo_editor .confirm', function() {
			self.editor_confirm($(this));
		});

		$(document).on('click', '.photo_editor .cancel', function() {
			self.editor_cancel($(this));
		});

		$(document).on('click', '.photo.final .del_this_photo', function(){
			$(this).closest('.photo').removeClass('final');
			$(this).closest('.photo').find('figure').remove();
			Constructor.write('', $(this).closest('.photo').data('constructor_var'), $(this).closest('.photo').closest('.card').data('side'));
		});

		$(document).on('mousedown', '.photo_editor figure', function(e) {
			var f = $(this);
			self.enable_drag({
				init_pos: f.position(),
				init_click: [e.pageX, e.pageY],
				max_shift: self.drag_limits(f),
				el: f
			});
		});

		$(document).on('change, input', '.photo_editor .zoom', function() {
			self.zoom_pic($(this));
		});

		$(document).on('change, input', '.photo_editor .rotate', function() {
			self.rotate_pic($(this));
		});

		$(window).on('mouseup', function(e) {
			$(this).off('mousemove');
		});

		$(document).on('click', '.photo .upload_local', function() {
			$(this).closest('.photo').find('input[type="file"]').click();
		});

        $(document).on('click', '.photo .upload_social', function() {
            $('.socials').addClass('active');
            $('.remote_upload_target').removeClass('remote_upload_target');
            $(this).closest('.photo').addClass('remote_upload_target');
        });
	},

	zoom_pic: function(range_input) {
		var self = this;
		var editor = range_input.closest('.photo_editor');
		var figure = editor.find('figure');
		var val = range_input.val();
		var init_val = range_input.data('init');
		var init_w = parseInt(figure.attr('init_w'));
		var init_h = parseInt(figure.attr('init_h'));
		var diff = init_val - val;
		var new_w = init_w + init_w * diff / 67;
		var new_h = new_w * init_h / init_w;
		figure.css({
			height: new_h + 'px',
			width: new_w + 'px'
		});
		var pos = this.snap_to_limits({
			left: figure.position().left,
			top: figure.position().top
		}, self.drag_limits(figure));
		figure.css({
			left: pos.left + 'px',
			top: pos.top + 'px'
		});
		console.log(self.drag_limits(figure));
	},

	rotate_pic: function(range_input) {
		var self = this;
		var editor = range_input.closest('.photo_editor');
		var figure = editor.find('figure');
		var val = range_input.val() / 2;
		figure.css({
			'transform': 'rotate('+val+'deg)',
			'-webkit-transform': 'rotate('+val+'deg)',
			'-moz-transform': 'rotate('+val+'deg)',
			'-o-transform': 'rotate('+val+'deg)'
		});
		var pos = this.snap_to_limits({
			left: figure.position().left,
			top: figure.position().top
		}, self.drag_limits(figure));
		figure.css({
			left: pos.left + 'px',
			top: pos.top + 'px'
		});
	},

	enable_drag: function(opt) {
		console.log(opt);
		var self = this;
		var pos = {};
		$(window).on("mousemove", function(e) {
            pos.left = opt.init_pos.left - opt.init_click[0] + e.pageX;
            pos.top = opt.init_pos.top - opt.init_click[1] + e.pageY;
            pos = self.snap_to_limits(pos, opt.max_shift);
            opt.el.css({
                'left': pos.left + 'px',
                'top': pos.top + 'px'
            });
        });
	},

	snap_to_limits: function(pos, limits) {
        if (pos.left < 0 && pos.left < -limits.left) {
            pos.left = -limits.left;
        }
        if (pos.left > 0) {
            pos.left = 0;
        }
        if (pos.top < 0 && pos.top < -limits.top) {
            pos.top = -limits.top;
        }
        if (pos.top > 0) {
            pos.top = 0;
        }
        return pos;
    },

	drag_limits: function(figure) {
		var parent = figure.closest(".photo_wrapper");
        return {
            'left': figure.width() > parent.width() ? Math.abs((parent.width() - figure.width() + 10)) : 0,
            'top': figure.height() > parent.height() ? Math.abs((parent.height() - figure.height() + 10)) : 0
        };
	},

    change_action: function(up) {
        var self = this;
        var photo = up.closest(".photo");
        var photo_bg = photo.find("figure").css("background-image");
        if (up.get(0).files.length > 0) 
        	this.upload_file(up.get(0).files[0], photo, photo_bg);
        up.replaceWith(up.clone());
    },

    upload_callback: function(data, photo, photo_bg) {
        console.log(data, photo);
        data = JSON.parse(data);
        data = data.data;
        if (typeof data.error === "undefined") {
            if (data === "") {
                return;
            } else {
				if (photo.data('editor') === true) {
					console.log(data);
					Photo.show_editor(data, photo);
				} else {
					Photo.append_figure(data, photo);
				}
            }
        } else {
            alert(data.error);
        }
    },

    editor_confirm: function(confirm) {
    	var tmp_editor = confirm.closest('.photo_editor');
    	var figure = tmp_editor.find('figure');
    	var photo = confirm.closest('.photo');
    	tmp_editor.remove();
    	photo.find('figure').remove();
    	photo.addClass('final').append(figure);
        photo.find('figure').wrap('<div class="figure_wrap"></div>');
    	Constructor.write(figure.get(0).outerHTML, photo.data('constructor_var'), photo.closest('.card').data('side'));
    	console.log(figure);
    },

    editor_cancel: function(cancel) {
    	var tmp_editor = cancel.closest('.photo_editor');
    	var photo = cancel.closest('.photo');
    	tmp_editor.remove();
    },

    show_editor: function(data, photo) {
    	// photo.find('.photo_hover').remove();
        console.log(data, photo);
    	photo.find('.photo_editor').remove();
    	photo.removeClass('final');
    	var tmp_editor = this.editor_tpl.clone();
    	var figure = tmp_editor.find('.photo_wrapper').find('figure');
    	console.log(figure);
    	figure.css({
            "background-image": "url(uploads/" + data.file + ")",
            "width": data.size.width + 'px',
            "height": data.size.height + 'px'
        }).attr({
            "init_w": data.size.width,
            "init_h": data.size.height
        });
    	photo.append(tmp_editor);
        this.scale_to_fit(photo);
    },

    scale_to_fit: function(photo) {
        console.log(photo);
        var self = this;
        var zoom = photo.find('.zoom');
        var figure = photo.find('figure');
        console.log(zoom.val());
        while ((zoom.val() > 0) && (figure.width() < photo.width() || figure.height() < photo.height())) {
            zoom.val(parseInt(zoom.val()) - 1);
            self.zoom_pic(zoom);
            console.log(zoom.val(), figure.width(), photo.width(), figure.height(), photo.height());
        }
    },

    append_figure: function(data, photo) {
    	var figure = $("<figure></figure>");
    	figure.css({
            "background-image": "url(uploads/" + data.file + ")",
            "width": data.size.width + 'px',
            "height": data.size.height + 'px'
        }).attr({
            "init_w": data.size.width,
            "init_h": data.size.height
        });
        photo.addClass('final');
        photo.find('figure').remove();
        photo.append(figure);
        photo.find('figure').wrap('<div class="figure_wrap"></div>');
        Constructor.write(figure.get(0).outerHTML, photo.data('constructor_var'), photo.closest('.card').data('side'));
    	console.log(figure);
    },

	get_templates: function() {
		var self = this;
		$.get('/smarty/templates/photo_editor.tpl', function(data) {
			self.editor_tpl = $(data);
		});

		$.get('/smarty/templates/photo_hover.tpl', function(data) {
			self.hover_tpl = $(data);
			self.bind_hover();
		});
	},

	upload_file: function(file, fixed, fixed_bg) {
        var xhr = new XMLHttpRequest();
        var fd = new FormData();
        fixed.addClass('uploading');
        fd.append('photo', file);
        fd.append('request', 'upload_image');
        fd.append('mod', 'root.upload_image');
        xhr.onreadystatechange = stateChange;
        xhr.open('POST', 'php/router.php');
        xhr.send(fd);

        function stateChange(e) {
            if (e.target.readyState === 4 && e.target.status === 200) {
                fixed.removeClass('uploading');
                Photo.upload_callback(xhr.responseText, fixed, fixed_bg);
            }
        }
    }
};

Photo.init();