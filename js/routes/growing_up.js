var Growing_up = {
	init: function() {
		this.init_tooth_select();
		this.init_ruler();
	},

	position_tooth: function(init_left, t_w) {
		$('.tooth').each(function() {
			$(this).css('left', init_left + 'px');
			init_left += t_w;
		});
	},

	init_tooth_select: function() {

		this.position_tooth(30, 43);

		var tooth_select = $(".tooth_num");

		tooth_select.click(function() {
			$(this).parent().hasClass('opened') ? close_select($(this)) : open_select($(this));
		});

		tooth_select.find('li').click(function(e) {
			var listitems = tooth_select.children("li");
			listitems.sort(function(a, b) {
			   var compA = parseInt($(a).text());
			   var compB = parseInt($(b).text());
			   return (compA < compB) ? -1 : (compA > compB) ? 1 : 0;
			})
			tooth_select.append(listitems);
			$(this).prependTo($(this).parent());
		});

		function close_select(ul) {
			ul.parent().removeClass('opened');
		}

		function open_select(ul) {
			ul.parent().addClass('opened');
		}

		var tooth_num;
		var tooth = $(".tooth");
		var month_num = $(".tooth_month_num");
		var reg = /^\d+$/;

		var teeth_wrapper = $(".teeth_wrapper");

		$('.add_tooth').click(function() {
			if (!reg.test(month_num.val())) {
				return Global.input_highlight(month_num);
			}
			tooth_num = parseInt(tooth_select.find('li').eq(0).text()) - 1;
			console.log(tooth_num);
			tooth.eq(tooth_num).show().find('p').html(month_num.val()+' м'+'<span>'+(tooth_num + 1)+'й</span>');
			Constructor.write(teeth_wrapper.html(), teeth_wrapper.data('constructor_var'));
		});

		$(document).on('click', '.tooth .del', function() {
			$(this).closest('.tooth').hide();
			Constructor.write(teeth_wrapper.html(), teeth_wrapper.data('constructor_var'));
		});

		var label_array = new Array('месяц', 'месяцев', 'месяца');

		function month_word_form(value) {
		    var num_length = value.length;
		    var last = num_length - 1;
		    var check = value.charAt(last);
		    var first = value.charAt(0);
			if ((check === '1' && first !== '1' && value.length !== 1) || (check === '1' && first === '1' && value.length === 1)) {
		        return label_array[0];
		    }
		    else if ((value.length === 2 && first === '1') || (check === '5' || check === '6' || check === '7' || check === '8' || check === '9') || check === '0') {
		        return label_array[1];
		    }
		    else if ((check === '2' || check === '3' || check === '4')) {
		        return label_array[2];
		    }
		}
	},

	init_ruler: function() {
		var x_dim = $(".x").find('ul');
		var y_dim = $('.y').find('ul');
		var x_li_width = x_dim.find('li').width();
		var y_li_height = y_dim.find('li').height();

		var add_point = $(".add_point");
		var reg = /^\d+$/;
		var inputs = {
			age: $(".form").find('.age'),
			growth: $(".form").find('.growth'),
			weight: $(".form").find('.weight')
		}

		var chart_body = $(".chart_body").find('.chart_inner');
		var svg_body = $('.chart_body').find('svg');
		var point_tpl = $("<div class='point'><p></p><a class='del'></a></div>");
		var new_point, sorted;

		$(document).on('click', '.point .del', function() {
			$(this).closest('.point').remove();
			draw_lines();
			Constructor.write($(".chart_body").html(), $('.chart_body').data('constructor_var'));
		});

		add_point.click(function() {
			if (!Global.check_inputs([inputs.age, inputs.growth, inputs.weight])) return;
			if (!reg.test(inputs.age.val())) {
				return Global.input_highlight(inputs.age);
			}
			if (!reg.test(inputs.growth.val())) {
				return Global.input_highlight(inputs.growth);
			}
			if (!reg.test(inputs.weight.val())) {
				return Global.input_highlight(inputs.weight);
			}
			if (inputs.age.val() > 24 || inputs.age.val() < 1) {
				return Global.input_highlight(inputs.age);
			}
			if (inputs.growth.val() > 100 || inputs.growth.val() < 50) {
				return Global.input_highlight(inputs.growth);
			}
			var pos = {
				x: (inputs.age.val() - 1) * x_li_width / 2 + x_li_width / 2,
				y: (inputs.growth.val() / 5 - 10) * y_li_height + y_li_height / 2
			};
			new_point = point_tpl.clone();
			new_point.find('p').html(inputs.weight.val() + 'кг');
			new_point.css({
				left: pos.x + 'px',
				bottom: pos.y + 'px'
			});
			new_point.attr({
				'age': inputs.age.val(),
				'growth': inputs.growth.val()
			});
			chart_body.append(new_point);
			sorted = getSorted('.point', 'age');
			chart_body.empty();
			for (var i = 0; i < sorted.length; i++) {
				chart_body.append(sorted[i]);
			}
			draw_lines();
			Constructor.write($(".chart_body").html(), $('.chart_body').data('constructor_var'));
		});

		function getSorted(selector, attrName) {
		    return $($(selector).toArray().sort(function(a, b){
		        var aVal = parseInt(a.getAttribute(attrName)),
		            bVal = parseInt(b.getAttribute(attrName));
		        return aVal - bVal;
		    }));
		}

		function draw_lines() {
			$("#mySVG").empty();
			var points = $(".point");
			console.log(points);
			var len = points.length;
			if (len < 2) return;
			for (var i = 0; i < len; i++) {
				if (i + 1 < len) {
					connect(points.eq(i), points.eq(i+1));
				}
			}
		}

		function position_points() {
			var points = $('.point');
			var len = points.length;
			var growth, age, pos;
			for (var i = 0; i < len; i++) {
				growth = points.eq(i).attr('growth');
				age = points.eq(i).attr('age');
				pos = {
					x: (age - 1) * x_li_width / 2 + x_li_width / 2,
					y: (growth / 5 - 10) * y_li_height + y_li_height / 2
				};
				points.eq(i).css({
					left: pos.x + 'px',
					bottom: pos.y + 'px'
				});
			}
		}

		var svgNS = "http://www.w3.org/2000/svg"; 

		function connect(div_1, div_2) {
			var line = document.createElementNS(svgNS, "line");
		    line.setAttributeNS(null, "x1", div_1.position().left);
		    line.setAttributeNS(null, "y1", div_1.position().top + div_1.height() / 2);
		    line.setAttributeNS(null, "x2", div_2.position().left);
		    line.setAttributeNS(null, "y2", div_2.position().top + div_2.height() / 2);
		    line.setAttributeNS(null, "stroke", "#ecf0e8");
		    line.setAttributeNS(null, "stroke-width", "2");
		    document.getElementById("mySVG").appendChild(line);
		}

		position_points();
		draw_lines();
		$('.card_5').addClass('rotated');
	}
};

Growing_up.init();