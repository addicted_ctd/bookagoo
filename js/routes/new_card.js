var New_card = {
	init: function() {
		this.bind_title();
	},

	bind_title: function() {
		var old_val;

		$('h1.card_title').find('span').on('focus', function() {
			old_val = $(this).text().trim();
			if ( $(this).text() == "Введите название раздела" ){
				$(this).text("");
			}
			$(this).off('blur').on('blur', function() {
				if ( ($(this).text().trim() !== old_val) && $(this).text().trim() != "" ) {
					Constructor.write($(this).text().trim(), $(this).closest('h1').data('constructor_var'), $(this).closest('.card').data('side'));
					$('h1.card_title').find('span').text($(this).text());
				}
				if ( $(this).text().trim() == "" ){
					$(this).text("Введите название раздела");
				}
			});
		});
		$('h1.card_title').find('span').on('keydown', function(event) {
			var key = event.keyCode;
			console.log(key);
			if (key === 13) {
				$(this).blur();
			}
			if ( ( $(this).text().length > 40 || key === 13 ) && key != 8)
				return false;
		});
	}
};

New_card.init();