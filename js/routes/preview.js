var Preview = {
  init: function() {
    this.hiddePlaceholder();
  },

  hiddePlaceholder: function(){
    if ( $('input').length ) {
      $('input').removeAttr('placeholder');

      if ( $('.circle_text').length) {
        $('.circle_text').each(function(){
          if ( $(this).text() === "Введите имя" ||
               $(this).attr("aria-label") == "Введите имя" ){
            $(this).remove();
          }
        });
      }
    }
  }

};

Preview.init();