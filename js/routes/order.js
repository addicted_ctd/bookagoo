var Order = {

	init: function() {
		this.count_cards();
	},

	count_cards: function() {
		var cards_ordered = $('[data-cards_ordered]');
		var label_array = new Array('карточка', 'карточек', 'карточки');
		var self = this;
		var num;
		cards_ordered.each(function() {
			var cards = $(this).data('cards_ordered').split(',');
			num = cards.indexOf('0') > -1 ? cards.length - 1 : cards.length;
			$(this).html('Книга ('+num+' '+self.make_word(num + '', label_array)+')');
		});
	},

	make_word: function(value, label_array) {
		var num_length = value.length;
		var last = num_length - 1;
		var check = value.charAt(last);
		var first = value.charAt(0);
		if ((check === '1' && first !== '1' && value.length !== 1) || (check === '1' && first === '1' && value.length === 1)) {
		    return label_array[0];
		}
		else if ((value.length === 2 && first === '1') || (check === '5' || check === '6' || check === '7' || check === '8' || check === '9') || check === '0') {
		    return label_array[1];
		}
		else if ((check === '2' || check === '3' || check === '4')) {
		    return label_array[2];
		}
	}

};

Order.init();