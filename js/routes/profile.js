var Profile = {
	init: function() {
		this.bind();
	},

	bind: function() {
		this.bind_social();
		this.bind_save();
		this.bind_gender();
		this.bind_delete();
	},

	bind_gender: function() {
		var gender = $(".gender").find("a");
		gender.click(function() {
			gender.removeClass('active');
			$(this).addClass('active');
		});
	},

	bind_delete: function() {
		$('.delete_account').click(function() {
			Overlay.show('.acc_del');
		});
	},

	process_delete: function() {
		Global.ajax({
			mod: 'user.delete_account'
		}, function(data) {
			window.location.reload();
			console.log(data);
		});
	},

	pass_check: function() {
		if (this.input_fields.new_pass.val() !== '' ||
			this.input_fields.new_pass_repeat.val() !== '') {
			if (!Global.value_length(this.input_fields.new_pass, 6)) {
				Global.input_highlight(this.input_fields.new_pass);
				return 'Пароль должен содержать минимум 6 символов';
			}
			if (!Global.fields_match(this.input_fields.new_pass, this.input_fields.new_pass_repeat)) {
				Global.input_highlight(this.input_fields.new_pass);
				Global.input_highlight(this.input_fields.new_pass_repeat);
				return 'Пароли не совпадают';
			}
			if (this.input_fields.old_pass.val().trim().length === 0)
				return 'Введите старый пароль';
		}
		return 'ok';
	},

	check_fio: function() {
		var fio = this.input_fields.fio.val().trim();
		fio = fio.split(' ');
		if (fio.length !== 3) {
			return 'Укажите полное ФИО';
		}
		return {
			f_name: fio[1],
			l_name: fio[0],
			m_name: fio[2]
		};
	},

	check_email: function() {
		return Global.check_mail(this.input_fields.email);
	},

	bind_save: function() {

		var self = this;

		this.input_fields = {
			fio: $("[name='fio']"),
			email: $("[name='email']"),
			old_pass: $("[name='old_pass']"),
			new_pass: $("[name='new_pass']"),
			new_pass_repeat: $("[name='new_pass_repeat']"),
			child_birth: $("[name='child_birth']"),
			child_name: $("[name='child_name']"),
			newsletter: $("[name='subscription']")
		};

		this.input_fields.child_birth.mask('99.99.9999');

		var check_pass, check_fio, fio;

		$(".save_changes").click(function() {
			check_pass = self.pass_check();
			if (check_pass !== 'ok') return alert(check_pass);
			check_fio = self.check_fio();
			if (typeof check_fio === 'string') return alert(check_fio);
			if (!self.check_email()) return alert('Неправильный формат E-mail');
			var data = {
				email: self.input_fields.email.val(),
				f_name: check_fio.f_name,
				l_name: check_fio.l_name,
				m_name: check_fio.m_name,
				child_birth: self.input_fields.child_birth.val(),
				child_name: self.input_fields.child_name.val(),
				old_pass: self.input_fields.old_pass.val(),
				new_pass: self.input_fields.new_pass.val(),
				new_pass_repeat: self.input_fields.new_pass_repeat.val(),
				newsletter: self.input_fields.newsletter.is(':checked') ? 1 : 0,
				child_gender: $('.gender').find('.active').data('gender')
			};
			Global.ajax({
				mod: 'user.update_profile',
				data: data
			}, function(data) {
				console.log(data);
				if (data.data.error === 1) {
					return alert('Email уже используется');
				} else if (data.data.error === 2) {
					return alert("Неправильный старый пароль");
				}
				alert('Изменения сохранены');
				window.location.reload();
			});
		});
	},

	bind_social: function() {
		var network;
		$(".disconnect").click(function() {
			network = $(this).data('network');
			Global.ajax({
				mod: 'user.social_disconnect',
				data: {
					network: network
				}
			}, function(data) {
				console.log(data);
				window.location.reload();
			});
		});
	}
};

Profile.init();