var Cart = {

	cards_quantity: 1,
	box_quantity: 1,
	total: {
		box: 0,
		cards: 0,
		all: 0,
		discount: 0
	},
	coupon: {},
	delivery: 0,

	init: function() {
		this.render_list();
		this.count_total();
		this.collect_pdf();
		this.bind();
		this.mask_phones();
	},

	mask_phones: function() {
		$('.phone').each(function() {
			$(this).mask('+9-999-999-99-99');
		});
	},

	collect_pdf: function() {
		var cards = $('.card_side');
		var card_id, side;
		cards.each(function() {
			card_id = $(this).closest('[data-card_id]').data('card_id');
			if (card_id !== 0) {
				side = $(this).data('side');
				Global.ajax({
					mod: 'order.generate_preview',
					data: {
						card_id: card_id,
						side: side
					}
				}, function(data) {
					data = data.data;
					$('[data-card_id="'+data.card_id+'"]').find('[data-side="'+data.side+'"]').find('figure').css('background-image', 'url(/preview/'+data.filename+')');
				});
			}
		});
	},

	return_user: function(el, alert_msg) {
		alert(alert_msg);
		$('body, html').animate({
			scrollTop: el.offset().top - 100
		}, 500);
	},

	bind: function() {

		var self = this;

		var cards = $('.cart_item');
		var cards_total = cards.length;
		var cards_ordered, cards_quantity_details, card_id;
		var name = $('.name');
		var phone = $('.phone'); 
		var city = $('.city');
		var street = $('.street');
		var house = $('.house');
		var appartment = $('.appartment');
		var comment = $('.comment');

		$("#place_order").click(function() {
			if (!Global.check_inputs([name, phone])) return self.return_user(name, 'Заполните все поля');
			cards_ordered = [];
			cards_quantity_details = {};
			for (var i = 0; i < cards_total; i++) {
				if (cards.eq(i).find('input[type="checkbox"]').is(':checked')) {
					card_id = cards.eq(i).data('card_id');
					cards_ordered.push(card_id);
					cards_quantity_details[card_id] = self.return_quantity(card_id);
				}
			}
			console.log(cards_quantity_details);
			if (cards_ordered.indexOf(0) > -1) {
				if (cards_ordered.length < 6)
					return self.return_user(cards.eq(0), 'Выберите минимум 5 карточек');
			} else {
				if (cards_ordered.length < 5)
					return self.return_user(cards.eq(0), 'Выберите минимум 5 карточек');
			}
			if (self.delivery === 1) {
				if (!Global.check_inputs([city, street, house, appartment])) return self.return_user(city, 'Заполните все поля');
			}
			$(this).off('click');
			Global.ajax({
				mod: 'order.place_order',
				data: {
					cards: cards_ordered,
					total: self.total,
					name: name.val(),
					phone: phone.val(),
					coupon: self.coupon,
					cards_quantity: self.cards_quantity,
					cards_quantity_details: cards_quantity_details,
					box_quantity: self.box_quantity,
					delivery: self.delivery,
					delivery_address: self.delivery === 1 ? (city.val() + ', Ул. ' + street.val() + ' дом ' + house.val() + ', кв. ' + appartment.val()) : '',
					comments: comment.val()
				}
			}, function(data) {
				console.log(data);
			});
			self.say_thanks();
			console.log(cards_ordered);
		});

		this.bind_checkboxes();
		this.bind_quantity();
		this.bind_delivery();
		this.bind_details();
		this.bind_coupon();
		this.bind_region();

	},

	say_thanks: function() {
		$('#cart').addClass('thanks_vis');
	},

	bind_region: function() {
		var region_id;
		var city = $('.city');
		$('.region').on('change', function() {
			region_id = $(this).val();
			console.log(region_id);
			city.empty();
			city.append('<option selected disabled>Выберите город</option>');
			city.show();
			Global.ajax({
				mod: 'order.list_cities',
				data: {
					region_id: region_id
				}
			}, function(data) {
				console.log(data);
				data = data.data;
				for (var i = 0; i < data.length; i++) {
					city.append('<option value="'+data[i].name+'">'+data[i].name+'</option>');
				}
			});
		});
	},

	bind_coupon: function() {
		var self = this;
		var coupon_code = $('.coupon_code');
		var activate_button = $('.activate_coupon');
		activate_button.click(function() {
			if (!Global.check_inputs([coupon_code])) return;
			Global.ajax({
				mod: 'order.get_coupon',
				data: {
					code: coupon_code.val()
				}
			}, function(data) {
				if (typeof data.data.error !== 'undefined') {
					return alert('Купон недействителен');
				}
				self.coupon.code = coupon_code.val();
				self.coupon.discount = parseInt(data.data.discount);
				self.coupon.type = data.data.type;
				self.count_total();
			});
		});
	},

	apply_coupon: function() {
		if (typeof this.coupon.type === 'undefined') return;
		var self = this;
		if (self.coupon.type === '1') {
			self.total.discount = - (self.total.cards) * (self.coupon.discount / 100);
		} else {
			self.total.discount = - self.coupon.discount;
		}
		$('.discount_amount').html(self.total.discount);
		console.log(self.total.discount);
	},

	bind_details: function() {
		var details = $('.cards_details');
		var to_height;
		$('.cart_purchase_info').parent().click(function() {
			$(this).toggleClass('opened');
			to_height = $(this).hasClass('opened') ? $('.cards_details').find('ul').height() : 0;
			details.height(to_height);
		});
	},

	change_element_text: function(element){
		var text = element.text();
		element.text(element.data("text"));
		element.data("text", text);
	},

	change_delivery_type: function(){		
		var delivery_type = $(".delivery_type");
		var delivery_type_span = $(".delivery_type_span");
		this.change_element_text(delivery_type);
		this.change_element_text(delivery_type_span);
		delivery_type_span.parent().toggleClass("delivery_type_bigger");
	},

	bind_delivery: function() {
		var self = this;
		var tabs = $('.delivery').find('.tabs').find('div');
		$(document).on('click', '.delivery ul li', function() {
			$(this).parent().toggleClass('opened');
			if ($(this).index() !== 0) {
				self.delivery = parseInt($(this).data('type'));
				$(this).prependTo($(this).parent());
				tabs.hide();
				tabs.eq(self.delivery).show();
				self.change_delivery_type();
			}
		});

	},

	make_word: function(value, label_array) {
		var num_length = value.length;
		var last = num_length - 1;
		var check = value.charAt(last);
		var first = value.charAt(0);
		if ((check === '1' && first !== '1' && value.length !== 1) || (check === '1' && first === '1' && value.length === 1)) {
		    return label_array[0];
		}
		else if ((value.length === 2 && first === '1') || (check === '5' || check === '6' || check === '7' || check === '8' || check === '9') || check === '0') {
		    return label_array[1];
		}
		else if ((check === '2' || check === '3' || check === '4')) {
		    return label_array[2];
		}
	},

	bind_quantity: function() {
		var self = this;
		$('.cards_quantity').change(function() {
			self.cards_quantity = parseInt($(this).val());
			$('.single_quantity').val(self.cards_quantity);
			self.count_total();
		});

		$('.box_quantity').change(function() {
			self.box_quantity = parseInt($(this).val());
			self.count_total();
		});

		$(document).on('change', '.single_quantity', function() {
			console.log($(this).val());
			self.count_total();
		});

	},

	count_total: function() {
		var total_info = $('.total_info');
		var checked, checked_len, quantity, price, card_id;
		var self = this;
		var label_array = new Array('товар', 'товаров', 'товара');
		var label_array_2 = new Array('карточка', 'карточек', 'карточки');
		var cards_total = $('.cards_total').find('span');
		var box_total = $('.box_total').find('span');
		var cart_purchase_info = $('.cart_purchase_info');
		var box = $('.cart_item[data-card_id="0"]').find('input[type="checkbox"]');
		var total_final = $('.total_final');
		var cards_quantity = 0;

		self.total.box = 0;
		self.total.cards = 0;
		checked = $('.cart_item').find('input[type="checkbox"]:checked');
		checked_len = checked.length;
		for (var i = 0; i < checked_len; i++) {
			card_id = checked.eq(i).closest('.cart_item').data('card_id');
			price = parseInt(checked.eq(i).closest('.cart_item').data('price'));
			if (card_id === 0) {
				self.total.box += price * self.box_quantity; 
			} else {
				self.total.cards += price * self.return_quantity(card_id);
				cards_quantity += parseInt(self.return_quantity(card_id));
			}
		}
		self.apply_coupon();
		self.total.all = self.total.box + self.total.cards + self.total.discount;
		total_info.html('В корзине '+checked_len+' '+self.make_word(checked_len+'', label_array)+' на сумму '+self.total.all+' рублей');
		cards_total.html(self.total.cards);
		box_total.html(self.total.box);
		cart_purchase_info.html('Книга ('+cards_quantity+' '+self.make_word(cards_quantity+'', label_array_2)+')');
		total_final.html(self.total.all);
	},

	return_quantity: function(card_id) {
		return $('.cards_details').find('[data-card_id="'+card_id+'"]').find('.single_quantity').val();
	},

	bind_checkboxes: function() {
		var self = this;
		var checkbox = $('.cart_item').find('input[type="checkbox"]');
		
		checkbox.click(function() {
			if ($(this).closest('.cart_item').data('card_id') === 0 && $(this).is(':checked')) {
				$('.box_cart').show();
				$('.box_quantity').val(1);
			} else {
				$('.box_cart').hide();
				$('.box_quantity').val(0);
			}
			self.render_list();
			self.count_total();
		});
	},

	render_list: function() {
		console.log('rendering list');
		var checked = $('.cart_item:not([data-card_id="0"])').find('input[type="checkbox"]:checked');
		var checked_len = checked.length;
		var card_name, card_id, card;
		var list = $('.cards_details').find('ul');
		list.empty();
		for (var i = 0; i < checked_len; i++) {
			card = checked.eq(i).closest('.cart_item');
			card_name = card.find('h1').text();
			card_id = card.data('card_id');
			list.append("<li class='clearfix' data-card_id='"+card_id+"'><div><h3>"+card_name+"</h3></div><div><input type='number' value='1' min='1' class='b g single_quantity'></div><div></div></li>");
		}
	}
};

Cart.init();