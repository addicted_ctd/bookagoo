var Landing = {
	init: function() {
		this.bind_scroll();
	},

	bind_scroll: function() {
		var el = $("#index");

		el.find(".header").addClass('vis');

		$(window).scroll(function() {
			scr_check();
		});

		var scr_check = function() {
			if ($(this).scrollTop() > $(".one").offset().top - 100) {
				$(".one").addClass("vis");
			}
			if ($(this).scrollTop() > $(".two").offset().top - 100) {
				$(".two").addClass("vis");
			}
		}

		scr_check();
	}
};

Landing.init();