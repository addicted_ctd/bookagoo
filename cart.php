<?php

require 'smarty_init.php';

if (!$login_status) {
	Header("Location: auth.php");
}

$smarty->clearCompiledTemplate('cart.tpl');

$additional = $ctd::exec('constructor.get_additional');
$prices = $ctd::exec('order.get_prices');
$profile = $ctd::exec('user.get_profile');
$regions = $ctd::exec('order.get_regions');

$smarty->assign('prices', $prices);
$smarty->assign('additional', $additional);
$smarty->assign('profile', $profile);
$smarty->assign('regions', $regions);
$smarty->display('cart.tpl');

