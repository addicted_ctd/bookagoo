<?php

require '../smarty_init.php';

$child_info = $ctd::exec('constructor.get_child_info', (object) array('uid' => $_GET['uid']));
$child_info = $child_info[0];

$month = ['Января', 'Февраля', 'Марта', 'Апреля', 'Мая', 'Июня', 'Июля', 'Августа', 'Сентября', 'Октября', 'Ноября', 'Декабря'];

$birth = explode('.', $child_info['birth_date']);
$child_info['birth_full'] = $birth[0] . ' ' . $month[(int) $birth[1]] . ' ' . $birth[2];

$constructor = $ctd::exec('constructor.get_vars', (object) array('card_id' => $_GET['card'], 'uid' => $_GET['uid']));

$smarty->assign('constructor', $constructor);
$smarty->assign('child_info', $child_info);
$smarty->display('print.tpl');