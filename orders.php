<?php

require 'smarty_init.php';

if (!$login_status) {
	Header("Location: auth.php");
}

$smarty->clearCompiledTemplate('orders.tpl');

$orders = $ctd::exec('order.get_orders');

$smarty->assign('orders', $orders);
$smarty->display('orders.tpl');

