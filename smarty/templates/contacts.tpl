{include file='header.tpl' title='Контакты'}
<section id="contacts" class='page'>
	<section class="page_title">
		<h1 class="content">Контакты</h1>
	</section>
	<section class="one">
		<section class="wrapper content clearfix">
			<div>
				<p class="g">
					Спасибо за ваш интерес к BookAgoo!
					<br><br>
					Пожалуйста, заполните форму ниже, и мы свяжемся с<br> вами в ближайшее время.
				</p>
				<h3 class="g b">Присоединяйтесь к нам</h3>
				<a href="{$general.fb_link}" class="fb" target="_blank"></a>
				<a href="{$general.vk_link}" class="vk" target="_blank"></a>
			</div>
			<div>
				<input type='text' placeholder='Имя' name='name'>
				<input type='text' placeholder='E-mail' name='email'>
				<input type='text' placeholder='Тема сообщения' name='topic'>
				<textarea placeholder='Текст' name='msg'></textarea>
				<a class='send_message'>Отправить</a>
			</div>
		</section>
	</section>
</section>
<script src="/js/routes/contacts.js"></script>
{include file='footer.tpl'}