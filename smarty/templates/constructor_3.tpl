<!-- Card 3 -->
<section class="card card_3 front" data-side='front'>
	<h1 class="card_title">
		Моя семья
	</h1>
	<section class="card_content">
		<div class="leaf_wrapper" style="left: 406px; top: 363px;">
			<div class="leaf me">
				<h1 class='circle_text kitty' deg='6' dir='1'>Я</h1>
				<h2 class='circle_text kitty' deg='9' dir='-1' style='font-size: 17px;'>{$child_info.f_name}</h2>
				<div class="leaf_inner">
					<div class='photo' data-constructor_var="0">
						<div class='inner'>
							<p>
								<span>+</span>
							</p>
						</div>
						{if isset($constructor.0)}{$constructor.0}{/if}
						<input type="file" name="photo" class="load_image hidden">
					</div>
				</div>
			</div>
		</div>
		<div class="leaf_wrapper" style="left: 224px; top: 480px;">
			<div class="leaf">
				<h1 class='circle_text br_s' deg='6' dir='1' data-constructor_var="29">{if isset($constructor.29)}{$constructor.29}{else}Брат{/if}</h1>
				<h2 class='circle_text' contenteditable='true' deg='5' dir='-1' data-constructor_var="1">{if isset($constructor.1)}{$constructor.1}{else}Введите имя{/if}</h2>
				<div class="leaf_inner">
					<div class='photo' data-constructor_var="2">
						<div class='inner'>
							<p>
								<span>+</span>
							</p>
						</div>
						{if isset($constructor.2)}{$constructor.2}{/if}
						<input type="file" name="photo" class="load_image hidden">
					</div>
				</div>
			</div>
		</div>
		<div class="leaf_wrapper" style="left: 590px; top: 480px;">
			<div class="leaf">
				<h1 class='circle_text br_s' deg='6' dir='1' data-constructor_var="30">{if isset($constructor.30)}{$constructor.30}{else}Сестра{/if}</h1>
				<h2 class='circle_text' contenteditable='true' deg='5' dir='-1' data-constructor_var="3">{if isset($constructor.3)}{$constructor.3}{else}Введите имя{/if}</h2>
				<div class="leaf_inner">
					<div class='photo' data-constructor_var="4">
						<div class='inner'>
							<p>
								<span>+</span>
							</p>
						</div>
						{if isset($constructor.4)}{$constructor.4}{/if}
						<input type="file" name="photo" class="load_image hidden">
					</div>
				</div>
			</div>
		</div>
		<div class="leaf_wrapper" style="left: 301px; top: 687px;">
			<div class="leaf">
				<h1 class='circle_text' deg='6' dir='1'>Папа</h1>
				<h2 class='circle_text' contenteditable='true' deg='5' dir='-1' data-constructor_var="5">{if isset($constructor.5)}{$constructor.5}{else}Введите имя{/if}</h2>
				<div class="leaf_inner">
					<div class='photo' data-constructor_var="6">
						<div class='inner'>
							<p>
								<span>+</span>
							</p>
						</div>
						{if isset($constructor.6)}{$constructor.6}{/if}
						<input type="file" name="photo" class="load_image hidden">
					</div>
				</div>
			</div>
		</div>
		<div class="leaf_wrapper" style="left: 517px; top: 687px;">
			<div class="leaf">
				<h1 class='circle_text' deg='6' dir='1'>Мама</h1>
				<h2 class='circle_text' contenteditable='true' deg='5' dir='-1' data-constructor_var="7">{if isset($constructor.7)}{$constructor.7}{else}Введите имя{/if}</h2>
				<div class="leaf_inner">
					<div class='photo' data-constructor_var="8">
						<div class='inner'>
							<p>
								<span>+</span>
							</p>
						</div>
						{if isset($constructor.8)}{$constructor.8}{/if}
						<input type="file" name="photo" class="load_image hidden">
					</div>
				</div>
			</div>
		</div>
		<div class="leaf_wrapper" style="left: 96px; top: 777px;">
			<div class="leaf">
				<h1 class='circle_text' deg='6' dir='1'>Дедушка</h1>
				<h2 class='circle_text' contenteditable='true' deg='5' dir='-1' data-constructor_var="9">{if isset($constructor.9)}{$constructor.9}{else}Введите имя{/if}</h2>
				<div class="leaf_inner">
					<div class='photo' data-constructor_var="10">
						<div class='inner'>
							<p>
								<span>+</span>
							</p>
						</div>
						{if isset($constructor.10)}{$constructor.10}{/if}
						<input type="file" name="photo" class="load_image hidden">
					</div>
				</div>
			</div>
		</div>
		<div class="leaf_wrapper" style="left: 720px; top: 777px;">
			<div class="leaf">
				<h1 class='circle_text' deg='6' dir='1'>Дедушка</h1>
				<h2 class='circle_text' contenteditable='true' deg='5' dir='-1' data-constructor_var="11">{if isset($constructor.11)}{$constructor.11}{else}Введите имя{/if}</h2>
				<div class="leaf_inner">
					<div class='photo' data-constructor_var="12">
						<div class='inner'>
							<p>
								<span>+</span>
							</p>
						</div>
						{if isset($constructor.12)}{$constructor.12}{/if}
						<input type="file" name="photo" class="load_image hidden">
					</div>
				</div>
			</div>
		</div>
		<div class="leaf_wrapper" style="left: 275px; top: 906px;">
			<div class="leaf">
				<h1 class='circle_text' deg='6' dir='1'>Бабушка</h1>
				<h2 class='circle_text' contenteditable='true' deg='5' dir='-1' data-constructor_var="13">{if isset($constructor.13)}{$constructor.13}{else}Введите имя{/if}</h2>
				<div class="leaf_inner">
					<div class='photo' data-constructor_var="14">
						<div class='inner'>
							<p>
								<span>+</span>
							</p>
						</div>
						{if isset($constructor.14)}{$constructor.14}{/if}
						<input type="file" name="photo" class="load_image hidden">
					</div>
				</div>
			</div>
		</div>
		<div class="leaf_wrapper" style="left: 542px; top: 906px;">
			<div class="leaf">
				<h1 class='circle_text' deg='6' dir='1'>Бабушка</h1>
				<h2 class='circle_text' contenteditable='true' deg='5' dir='-1' data-constructor_var="15">{if isset($constructor.15)}{$constructor.15}{else}Введите имя{/if}</h2>
				<div class="leaf_inner">
					<div class='photo' data-constructor_var="16">
						<div class='inner'>
							<p>
								<span>+</span>
							</p>
						</div>
						{if isset($constructor.16)}{$constructor.16}{/if}
						<input type="file" name="photo" class="load_image hidden">
					</div>
				</div>
			</div>
		</div>
	</section>
</section>

<section class="card card_3 back" data-side='back'>
	<h1 class="card_title">
		Моя семья
	</h1>
	<section class="card_content clearfix">
		<section class="col">
			<div class='photo white regular' data-editor=true data-constructor_var="17">
				<div class='inner'>
					<p>
						<span>+</span><br>
						Добавить фото
					</p>
					<input type='text' class='blue comment border_input' placeholder='Добавьте комментарий' data-constructor_var="18" value="{if isset($constructor.18)}{$constructor.18}{/if}">
				</div>
				{if isset($constructor.17)}{$constructor.17}{/if}
				<input type="file" name="photo" class="load_image hidden">
			</div>
		</section>
		<section class="col col_2">
			<h1>Мою маму зовут</h1>
			<input type='text' class='blue border_input' placeholder='укажите имя' style='width: 320px;' data-constructor_var="19" value="{if isset($constructor.19)}{$constructor.19}{/if}">
			<h1>Она родилась</h1>
			<div class='calendar'><input type='text' class='blue border_input date' placeholder='укажите дату' style='width: 175px;' data-constructor_var="20" value="{if isset($constructor.20)}{$constructor.20}{/if}"></div>
			<h1>Ее профессия</h1>
			<input type='text' class='blue border_input' placeholder='укажите профессию' style='width: 320px;' data-constructor_var="21" value="{if isset($constructor.21)}{$constructor.21}{/if}">
			<h1>Она любит заниматься</h1>
			<input type='text' class='blue border_input' placeholder='чем любит заниматься' style='width: 320px;' data-constructor_var="22" value="{if isset($constructor.22)}{$constructor.22}{/if}">
		</section>
		<section class="col col_3">
			<h1>Моего папу зовут</h1>
			<input type='text' class='blue border_input' placeholder='укажите имя' style='width: 320px;' data-constructor_var="23" value="{if isset($constructor.23)}{$constructor.23}{/if}">
			<h1>Он родился</h1>
			<div class='calendar'><input type='text' class='blue border_input date' placeholder='укажите дату' style='width: 175px;' data-constructor_var="24" value="{if isset($constructor.24)}{$constructor.24}{/if}"></div>
			<h1>Его профессия</h1>
			<input type='text' class='blue border_input' placeholder='укажите профессию' style='width: 320px;' data-constructor_var="25" value="{if isset($constructor.25)}{$constructor.25}{/if}">
			<h1>Он любит заниматься</h1>
			<input type='text' class='blue border_input' placeholder='чем любит заниматься' style='width: 320px;' data-constructor_var="26" value="{if isset($constructor.26)}{$constructor.26}{/if}">
		</section>
		<section class="col col_4">
			<div class='photo white regular' data-editor=true data-constructor_var="27">
				<div class='inner'>
					<p>
						<span>+</span><br>
						Добавить фото
					</p>
					<input type='text' class='blue comment border_input' placeholder='Добавьте комментарий' data-constructor_var="28" value="{if isset($constructor.28)}{$constructor.28}{/if}">
				</div>
				{if isset($constructor.27)}{$constructor.27}{/if}
				<input type="file" name="photo" class="load_image hidden">
			</div>
		</section>
	</section>
</section>

<script src="/js/routes/tree.js"></script>

<!-- End of Card 3 -->