		<footer>
			<section class="content clearfix">
				<div>
					<section class="links clearfix">
						<a class="under_hover2" href="news.php">Новости</a>
						<a class="under_hover2" href="about.php">О проекте</a>
						<a class="under_hover2" href="contacts.php">Контакты</a>
					</section>
					<p>© 2015  OOO «Букагу»</p>
				</div>
				<div>
					<section class="social clearfix">
						<a href="{$general.fb_link}" class="fb" target="_blank"></a>
						<a href="{$general.vk_link}" class="vk" target="_blank"></a>
					</section>
					<aside class="clearfix">
						<a href="">Доставка и оплата</a>
						<a href="agreement.php">Пользовательское соглашение</a>
					</aside>
				</div>
			</section>
		</footer>
		<section id="alert_overlay">
			<section class='ov_content'>
				<div class='alert acc_del'>
					<p>
						Вы уверены, что хотите<br>
						удалить аккаунт Bookagoo?
					</p>
					<section class='clearfix'>
						<a onclick='Overlay.hide();'>Нет, спасибо</a>
						<a onclick='Profile.process_delete();'>Да, уверен</a>
					</section>
					<a class='close'></a>
				</div>
				<div class='alert first_login'>
					<p>
						Вы впервые зашли к нам!<br>
						Первым делом заполните профиль<br>
						Книжечка автоматически добавит из него<br>
						информацию о вашем ребенке 
					</p>
					<section class='clearfix'>
						<a href='profile.php'>Перейти в профиль</a>
					</section>
					<a class='close'></a>
				</div>
			</section>
		</section>
		{if isset($first_login)}
		<script>Overlay.show('.first_login');</script>
		{/if}
		<script src="js/main.js"></script>
        <script src="js/vendor/css3finalize/jquery.css3finalize.min.js" type="text/javascript"></script>
        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            var _gaq = [['_setAccount', 'UA-XXXXX-X'], ['_trackPageview']];
            (function(d, t) {
                var g = d.createElement(t), s = d.getElementsByTagName(t)[0];
                g.src = '//www.google-analytics.com/ga.js';
                s.parentNode.insertBefore(g, s)
            }(document, 'script'));
        </script>
    </body>
</html>