{include file='header.tpl' title='Мои заказы'}
<section id="orders" class='page'>
	<section class="page_title">
		<h1 class="content">Мои заказы</h1>
	</section>
	<section class="one">
		<section class="content clearfix">
			<section class="left">
				{if $orders|@count neq 0}
					{foreach from=$orders item=order}
					<section class='order wrapper'>
						<div class='top clearfix'>
							<hgroup>
								<h2 class="b g">Заказ № {$order.id} от {$order.date|date_format:"%d/%m/%Y"}</h2>
								<h3><span class="b g">Адрес доставки:</span> {$order.delivery_address}</h3>
							</hgroup>
							<h4 status="1" class="b">{if $order.status_id neq 3}{$order.status}{else}Заказ оплачен{/if}</h4>
						</div>
						<div class='middle'>
							<h2 class="b g">Состав заказа:</h2>
							<div class='order_content clearfix'>
								{if $order.purchase_ids|count_characters > 2}
								<section class='clearfix'>
									<figure style='background-image: url(img/profile/book_preview.png);'></figure>
									<p class='b' data-cards_ordered="{$order.purchase_ids}">Книга ({$order.cards_quantity} карточек)</p>
									<p class='b'>{$order.cards_quantity} шт</p>
									<p class='b'>{$order.cards_total} рублей</p>
								</section>
								{/if}
								{assign var=cards value=","|explode:$order.purchase_ids}
								{if in_array('0', $cards)}
								<section class='clearfix'>
									<figure style='background-image: url(img/profile/book_preview.png);'></figure>
									<p class='b'>Коробка</p>
									<p class='b'>{$order.box_quantity} шт</p>
									<p class='b'>{$order.box_total} рублей</p>
								</section>
								{/if}
							</div>
						</div>
						<div class='bottom clearfix'>
							<p class='b'>Итого : {$order.total} рублей</p>
							{if $order.status_id eq 1}
							<a class='green_button_arrow' href='payment/pay.php?order={$order.id}'>Оплатить заказ</a>
							{/if}
						</div>
					</section>
					{/foreach}
				{else}
					<section class='wrapper'>
						<h2 class='notif'>У вас нет активных заказов. Пожалуйста, <a href='cart.php'>создайте заказ</a></h2>
					</section>
				{/if}
			</section>
			{include file='profile_menu.tpl'}
		</section>
	</section>
</section>
<script src="js/routes/order.js"></script>
{include file='footer.tpl'}