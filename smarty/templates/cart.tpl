{include file='header.tpl' title='Мои заказы'}
<section id="cart" class='page'>
	<section class="page_title">
		<section class="content clearfix">
			<h1>Подтвердите выбор печати</h1>
			<p class='total_info'>В корзине 0 товаров на сумму 0 рублей</p>
		</section>
	</section>
	<section class="one">
		<section class="content clearfix">
			<section class='cart_item wrapper single' data-card_id='0' data-price='{$prices.box}'>
				<hgroup class="clearfix">
					<h1 class="b">Коробка</h1>
				</hgroup>
				<div class='item_content clearfix'>
					<div class='card_side'>
						<p></p>
						<figure style='background-image: url(img/cart/design_box.jpg); background-position: center center;'></figure>
					</div>
				</div>
				<aside class='clearfix'>
					<input type='checkbox' id='card_0_order' checked>
					<label class='g' for="card_0_order">Добавить в корзину</label>
				</aside>
			</section>
			<section class='cart_item wrapper single' data-card_id='1' data-price='{$prices.card}'>
				<hgroup class="clearfix">
					<h1 class="b">Обложка</h1>
					<a class="edit" href='constructor.php?card=1'>Изменить</a>
				</hgroup>
				<div class='item_content clearfix'>
					<div data-side='front' class='card_side'>
						<p></p>
						<figure></figure>
					</div>
				</div>
				<aside class='clearfix'>
					<input type='checkbox' id='card_1_order' checked>
					<label class='g' for="card_1_order">Добавить в корзину</label>
				</aside>
			</section>
			<section class='cart_item wrapper' data-card_id='2' data-price='{$prices.card}'>
				<hgroup class="clearfix">
					<h1 class="b">Привет, а вот и я</h1>
					<a class="edit" href='constructor.php?card=2'>Изменить</a>
				</hgroup>
				<div class='item_content clearfix'>
					<div data-side='front' class='card_side'>
						<p>Лицевая сторона</p>
						<figure></figure>
					</div>
					<div data-side='back' class='card_side'>
						<p>Обратная сторона</p>
						<figure></figure>
					</div>
				</div>
				<aside class='clearfix'>
					<input type='checkbox' id='card_2_order' checked>
					<label class='g' for="card_2_order">Добавить в корзину</label>
				</aside>
			</section>
			<section class='cart_item wrapper' data-card_id='3' data-price='{$prices.card}'>
				<hgroup class="clearfix">
					<h1 class="b">Моя семья</h1>
					<a class="edit" href='constructor.php?card=3'>Изменить</a>
				</hgroup>
				<div class='item_content clearfix'>
					<div data-side='front' class='card_side'>
						<p>Лицевая сторона</p>
						<figure></figure>
					</div>
					<div data-side='back' class='card_side'>
						<p>Обратная сторона</p>
						<figure></figure>
					</div>
				</div>
				<aside class='clearfix'>
					<input type='checkbox' id='card_3_order' checked>
					<label class='g' for="card_3_order">Добавить в корзину</label>
				</aside>
			</section>
			<section class='cart_item wrapper' data-card_id='4' data-price='{$prices.card}'>
				<hgroup class="clearfix">
					<h1 class="b">Моя ручка и ножка</h1>
					<a class="edit" href='constructor.php?card=4'>Изменить</a>
				</hgroup>
				<div class='item_content clearfix'>
					<div data-side='front' class='card_side'>
						<p>Лицевая сторона</p>
						<figure></figure>
					</div>
					<div data-side='back' class='card_side'>
						<p>Обратная сторона</p>
						<figure></figure>
					</div>
				</div>
				<aside class='clearfix'>
					<input type='checkbox' id='card_4_order' checked>
					<label class='g' for="card_4_order">Добавить в корзину</label>
				</aside>
			</section>
			<section class='cart_item wrapper' data-card_id='5' data-price='{$prices.card}'>
				<hgroup class="clearfix">
					<h1 class="b">Я расту</h1>
					<a class="edit" href='constructor.php?card=5'>Изменить</a>
				</hgroup>
				<div class='item_content clearfix'>
					<div data-side='front' class='card_side'>
						<p>Лицевая сторона</p>
						<figure></figure>
					</div>
					<div data-side='back' class='card_side'>
						<p>Обратная сторона</p>
						<figure></figure>
					</div>
				</div>
				<aside class='clearfix'>
					<input type='checkbox' id='card_5_order' checked>
					<label class='g' for="card_5_order">Добавить в корзину</label>
				</aside>
			</section>
			<section class='cart_item wrapper' data-card_id='6' data-price='{$prices.card}'>
				<hgroup class="clearfix">
					<h1 class="b">Любимое</h1>
					<a class="edit" href='constructor.php?card=6'>Изменить</a>
				</hgroup>
				<div class='item_content clearfix'>
					<div data-side='front' class='card_side'>
						<p>Лицевая сторона</p>
						<figure></figure>
					</div>
					<div data-side='back' class='card_side'>
						<p>Обратная сторона</p>
						<figure></figure>
					</div>
				</div>
				<aside class='clearfix'>
					<input type='checkbox' id='card_6_order' checked>
					<label class='g' for="card_6_order">Добавить в корзину</label>
				</aside>
			</section>
			<section class='cart_item wrapper' data-card_id='7' data-price='{$prices.card}'>
				<hgroup class="clearfix">
					<h1 class="b">Путешествую</h1>
					<a class="edit" href='constructor.php?card=7'>Изменить</a>
				</hgroup>
				<div class='item_content clearfix'>
					<div data-side='front' class='card_side'>
						<p>Лицевая сторона</p>
						<figure></figure>
					</div>
					<div data-side='back' class='card_side'>
						<p>Обратная сторона</p>
						<figure></figure>
					</div>
				</div>
				<aside class='clearfix'>
					<input type='checkbox' id='card_7_order' checked>
					<label class='g' for="card_7_order">Добавить в корзину</label>
				</aside>
			</section>
			<section class='cart_item wrapper' data-card_id='8' data-price='{$prices.card}'>
				<hgroup class="clearfix">
					<h1 class="b">Вот и год прошел</h1>
					<a class="edit" href='constructor.php?card=8'>Изменить</a>
				</hgroup>
				<div class='item_content clearfix'>
					<div data-side='front' class='card_side'>
						<p>Лицевая сторона</p>
						<figure></figure>
					</div>
					<div data-side='back' class='card_side'>
						<p>Обратная сторона</p>
						<figure></figure>
					</div>
				</div>
				<aside class='clearfix'>
					<input type='checkbox' id="card_8_order" checked>
					<label class='g' for="card_8_order">Добавить в корзину</label>
				</aside>
			</section>
			{foreach from=$additional item=card_title key=card_id}
			<section class='cart_item wrapper' data-card_id='{$card_id}' data-price='{$prices.card}'>
				<hgroup class="clearfix">
					<h1 class="b">{$card_title}</h1>
					<a class="edit" href='constructor.php?card={$card_id}'>Изменить</a>
				</hgroup>
				<div class='item_content clearfix'>
					<div data-side='front' class='card_side'>
						<p>Лицевая сторона</p>
						<figure></figure>
					</div>
					<div data-side='back' class='card_side'>
						<p>Обратная сторона</p>
						<figure></figure>
					</div>
				</div>
				<aside class='clearfix'>
					<input type='checkbox' id="card_{$card_id}_order" checked>
					<label class='g' for="card_{$card_id}_order">Добавить в корзину</label>
				</aside>
			</section>
			{/foreach}
		</section>
	</section>
	<section class="page_title two">
		<section class="content clearfix">
			<h1>Оформление заказа</h1>
		</section>
	</section>
	<section class="three">
		<section class="content wrapper clearfix">
			<section class='contacts clearfix'>
				<h2 class="b g">Контактное лицо</h2>
				<input type="text" placeholder="Имя фамилия" class="gray name" value="{if !empty($profile.me.f_name)}{$profile.me.f_name}{/if}{if !empty($profile.me.l_name)} {$profile.me.l_name}{/if}">
				<input type="text" placeholder="Номер телефона" class="gray phone">
			</section>
			<section class='delivery'>
				<h2 class="b g">Доставочка<span>Мы доставляем книги по всей России </span></h2>
				<ul>
					<li class="b" data-type='0'>Самовывоз в Москве</li>
					<li class="b" data-type='1'>Доставка курьером</li>
				</ul>
				<section class='tabs'>
					<div>
						<p>
							Книгу можно будет забрать по адресу<br>
							Ул. {$general.delivery_street} дом {$general.delivery_house}, оф. {$general.delivery_appartment}
						</p>
					</div>
					<div class='clearfix'>
						<select class='region'>
							<option selected disabled>Выберите регион</option>
							{foreach from=$regions item=region}
							<option value="{$region.region_id}">{$region.name}</option>
							{/foreach}
						</select>
						<select class='city' style='display: none;'>

						</select>
						<input type='text' class='street gray' placeholder='Улица'>
						<input type='text' class='house gray' placeholder='Дом'>
						<input type='text' class='appartment gray' placeholder='Кварт.'>
						<textarea placeholder='Комментарии к заказу' class='order_comments'></textarea>
					</div>
				</section>
			</section>
		</section>
	</section>
	<section class="page_title">
		<section class="content clearfix">
			<h1>Корзина</h1>
		</section>
	</section>
	<section class="five">
		<section class="content wrapper clearfix">
			<section class='row title clearfix'>
				<div>
					<p>Наименование товара</p>
				</div>
				<div>
					<p>Количество</p>
				</div>
				<div>
					<p>Цена</p>
				</div>
			</section>
			<section class='row clearfix box_cart'>
				<div>
					<h2 class="b g">Коробка</h2>
				</div>
				<div>
					<input type='number' value="1" min="1" class="b g box_quantity">
				</div>
				<div>
					<h2 class="b g box_total"><span>0</span> ₽</h2>
				</div>
			</section>
			<section class='row clearfix'>
				<div>
					<h2 class="b g cart_purchase_info">Книга (0 карточек)</h2>
				</div>
				<div>
					<input type='number' value="1" min="1" class="b g cards_quantity">
				</div>
				<div>
					<h2 class="b g cards_total"><span>0</span> ₽</h2>
				</div>
			</section>
			<section class='cards_details row clearfix' style='border-bottom: 0;'>
				<ul>
					
				</ul>
			</section>
			<section class='row clearfix'>
				<div>
					<h2 class="b g delivery_type" data-text="Курьером">Самовывоз</h2>
				</div>
				<div>
					
				</div>
				<div>
					<h2 class="b g"><span class="delivery_type_span" data-text="Уточняется менеджером">0</span> <span class="currency">₽</span></h2>
				</div>
			</section>
			<section class='coupon clearfix'>
				<a class='b activate_coupon'>Применить</a>
				<input type='text' class='gray coupon_code'>
				<p>Активация купона:</p>			
				<p>Сумма скидок по заказу:<span class='spacing'><a class='discount_amount'>0</a> ₽</span></p>
				<p>Итого:<span class='spacing b g'><a class='total_final'>0</a> ₽</span></p>
			</section>
		</section>
	</section>
	<section class="six">
		<a class="green_button_arrow b" id='place_order'>Перейти к оплате</a>
	</section>
	<section class='thanks'>
		<section class='thanks_content'>
			<article>
				<p>
					Поздравляем, о том что вы заказали книжечку<br>
					уже знает наш менеджер, он обязательно<br>
					перезвонит вам в ближайшее время
				</p>
				<a href='orders.php'>Перейти к моим заказам</a>
			</article>
		</section>
	</section>
</section>
<script src="js/routes/cart.js"></script>
{include file='footer.tpl'}