{include file='header.tpl' title='Контакты'}
<section id="profile" class='page'>
	<section class="page_title">
		<h1 class="content">Аккаунт</h1>
	</section>
	<section class="one">
		<section class="content clearfix">
			<section class="left">
				<section class="wrapper">
					<div class="row">
						<h2 class="b g">Настройки аккаунта</h2>
						<input type='text' placeholder='ФИО' class='gray' value="{if !empty($profile.me.l_name)}{$profile.me.l_name} {/if}{if !empty($profile.me.f_name)}{$profile.me.f_name} {/if}{if !empty($profile.me.m_name)}{$profile.me.m_name}{/if}" name='fio'>
						<input type='text' placeholder='Электронная почта' class='gray' value="{$profile.me.email}" name='email'>
					</div>
					<div class="row clearfix">
						<h2 class="b g">Данные ребенка</h2>
						<input type='text' placeholder='Имя ребенка' class='gray' value="{$profile.child.f_name}" name='child_name'>
						<div class="clearfix" style="margin-top: 18px;">
							<div class="gender">
								<p class="b g">Пол:</p>
								<a class='boy {if $profile.child.gender eq 'male'}active{/if}' data-gender='male'></a>
								<a class='girl {if $profile.child.gender eq 'female'}active{/if}' data-gender='female'></a>
							</div>
							<div class='birth_date'>
								<input type='text' class='gray' placeholder='Дата рождения' value="{$profile.child.birth_date}" name='child_birth'>
								<a class='calendar'></a>
							</div>
						</div>
					</div>
					<div class="row password_change">
						<h2 class="b g">Изменить пароль</h2>
						<input type='password' placeholder='Старый пароль' class='gray' name='old_pass'>
						<input type='password' placeholder='Новый пароль' class='gray' name='new_pass'>
						<input type='password' placeholder='Повторите новый пароль' class='gray' name='new_pass_repeat'>
					</div>
					<div class="row clearfix">
						<button class='save_changes'>Сохранить</button>
						<a class='delete_account'>Удалить аккаунт</a>
					</div>
				</section>
			</section>
			<section class="right">
				<section class="wrapper">
					{if empty($profile.social.vk_id) or empty($profile.social.fb_id)}
					<div class="row" style='margin-bottom: 40px;'>
						<h2 class="b g">Свяжите социальные сети с аккаунтом</h2>
						{if empty($profile.social.vk_id)}
						<a class='connect_to_vk' onclick="VK.Auth.login(Global.vk_login);">Соеденить со Вконтакте</a>
						{/if}
						{if empty($profile.social.fb_id)}
						<a class='connect_to_fb' onclick="Global.fb_login();">Соеденить с Facebook</a>
						{/if}
					</div>
					{/if}
					{if !empty($profile.social.vk_id) or !empty($profile.social.fb_id)}
					<div class="row" style="padding-bottom: 40px;">
						<h2 class="b g">Связанные аккаунты</h2>	
						{if !empty($profile.social.fb_id)}
						<div class="account fb clearfix">
							<p>{$profile.social.fb_f_name} {$profile.social.fb_l_name}</p>
							<a class='disconnect' data-network="fb">Отвязать</a>
						</div>
						{/if}
						{if !empty($profile.social.vk_id)}
						<div class="account vk clearfix" style='margin-top: 20px;'>
							<p>{$profile.social.vk_f_name} {$profile.social.vk_l_name}</p>
							<a class='disconnect' data-network="vk">Отвязать</a>
						</div>
						{/if}
					</div>
					{/if}
				</section>
				<section class="wrapper" style="margin-top: 20px;">
					<div class="row">
						<h2 class="b g">Рассылка</h2>
						<div class="clearfix news_subscription">
							<input id="have_mess" type="checkbox" {if $profile.newsletter.subscription eq 1}checked{/if} name='subscription'>
							<label for="have_mess">
								Получать уведомления<br>
								по электронной почте
							</label>
						</div>
					</div>
				</section>
				<section class="wrapper" style="margin-top: 20px;">
					<div class="row">
						<h2 class="b g">Книга онлайн</h2>
						<p class="book_online">
							Нажмите на кнопку ниже, чтобы опубликовать<br>
							вашу книгу в интернете. Она будет доступна<br>
							для просмотра по прямой уникальной ссылке.<br>
							Рекомендуем добавить ее в избранное<br>
							вашего браузера.
						</p>
						<a class='publish_book' href="preview.php?u={$uid}&card=1" target="_blank">Опубликовать книгу онлайн</a>
					</div>
				</section>
			</section>
			{include file='profile_menu.tpl'}
		</section>
	</section>
</section>
<script src="js/routes/profile.js"></script>
{include file='footer.tpl'}