{include file='header.tpl' title='Главная страница'}
<section id="news" class='page'> 
    <section class="one page_title"> 
        <section class="content">
            <h1>Новости</h1>
        </section> 
    </section> 
    <section class="two clearfix"> 
        <section class="content clearfix"> 
            <section class="left">
                <section id="article">
                    <article class="clearfix">
                        {$post.tpl}
                    </article>
                    <aside class='social_shares clearfix'>
                        <a href="https://twitter.com/share" class="twitter-share-button" data-url="http://webpoetry.org/bookagoo/docs/app/article.php" data-text="yey">Tweet</a>
                        {literal}
                        <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
                        {/literal}
                        <div class="fb-like" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div>
                    </aside>
                    <section class='comment'>
                        <div id="disqus_thread"></div>
                        {literal}
                        <script type="text/javascript">
                            var disqus_shortname = 'bookagooweb';
                            (function() {
                                var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
                                dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
                                (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
                            })();
                        </script>
                        {/literal}
                    </section>
                </section>
            </section> 
            <section class="right"> 
                {include file='sidebar.tpl'}
            </section> 
        </section>
    </section>
</section>
{include file='footer.tpl'}