{include file='header.tpl' title='Новости'}
<section id="news" class='page'> 
    <section class="one page_title"> 
        <h1 class="content">Новости</h1>
    </section> 
    <section class="two clearfix"> 
        <section class="content clearfix"> 
            <section class="left">
                {foreach from=$news item=item}
                <article class="news_preview clearfix">
                    <figure style="background-image: url({$item.thumb});">
                        <span class="date b">{$item.date}</span>
                    </figure>
                    <div>
                        <h2 class="title b">{$item.title}</h2>
                        <p class="lead">
                            {$item.lead}      
                        </p>
                        <a href="article.php?read={$item.slug}" class="b">read more</a>
                    </div>
                </article>
                {/foreach}
            </section> 
            <section class="right"> 
                {include file='sidebar.tpl'}
            </section> 
        </section>
    </section>
</section>
{include file='footer.tpl'}