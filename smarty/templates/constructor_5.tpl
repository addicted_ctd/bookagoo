<!-- Card 5 -->
<section class="card card_5 front" data-side='front'>
	<h1 class="card_title">
		Я расту
	</h1>
	<section class="card_content">
		<div class='teeth_wrapper' data-constructor_var="0">
			{if isset($constructor.0)}
				{$constructor.0}
			{else}
				<div class='tooth tooth_1'><p></p><a class='del'></a></div>
				<div class='tooth tooth_1'><p></p><a class='del'></a></div>
				<div class='tooth tooth_1'><p></p><a class='del'></a></div>
				<div class='tooth tooth_1'><p></p><a class='del'></a></div>
				<div class='tooth tooth_1'><p></p><a class='del'></a></div>
				<div class='tooth tooth_1'><p></p><a class='del'></a></div>
				<div class='tooth tooth_1'><p></p><a class='del'></a></div>
				<div class='tooth tooth_1'><p></p><a class='del'></a></div>
				<div class='tooth tooth_1'><p></p><a class='del'></a></div>
				<div class='tooth tooth_1'><p></p><a class='del'></a></div>
				<div class='tooth tooth_1'><p></p><a class='del'></a></div>
				<div class='tooth tooth_1'><p></p><a class='del'></a></div>
				<div class='tooth tooth_1'><p></p><a class='del'></a></div>
				<div class='tooth tooth_1'><p></p><a class='del'></a></div>
				<div class='tooth tooth_1'><p></p><a class='del'></a></div>
				<div class='tooth tooth_1'><p></p><a class='del'></a></div>
				<div class='tooth tooth_1'><p></p><a class='del'></a></div>
				<div class='tooth tooth_1'><p></p><a class='del'></a></div>
				<div class='tooth tooth_1'><p></p><a class='del'></a></div>
				<div class='tooth tooth_1'><p></p><a class='del'></a></div>
			{/if}
		</div>
		<div class='form'>
			<div class='clearfix'>
				<p>Возраст</p>
				<input type='text' placeholder='в месяцах' class='tooth_month_num'>
			</div>
			<div class='clearfix'>
				<p>Зуб</p>
				<aside>
					<div class='ul_wrapper'>
						<ul class='tooth_num'>
							<li>1</li>
							<li>2</li>
							<li>3</li>
							<li>4</li>
							<li>5</li>
							<li>6</li>
							<li>7</li>
							<li>8</li>
							<li>9</li>
							<li>10</li>
							<li>11</li>
							<li>12</li>
							<li>13</li>
							<li>14</li>
							<li>15</li>
							<li>16</li>
							<li>17</li>
							<li>18</li>
							<li>19</li>
							<li>20</li>
						</ul>
					</div>
				</aside>
			</div>
			<div class='clearfix'>
				<button class='add_tooth'>Добавить зуб</button>
			</div>
		</div>
	</section>
</section>

<section class="card card_5 back" data-side='back'>
	<h1 class="card_title">
		Я расту
	</h1>
	<section class="card_content">
		<div class='ruler'>
			<div class='y'>
				<p class='title b'>рост</p>
				<ul>
					<li>100 см</li>
					<li></li>
					<li>90 см</li>
					<li></li>
					<li>80 см</li>
					<li></li>
					<li>70 см</li>
					<li></li>
					<li>60 см</li>
					<li></li>
					<li>50 см</li>
				</ul>
			</div>
			<div class='x'>
				<p class='title b'>возраст</p>
				<ul class='clearfix'>
					<li><span>2 месяца</span></li>
					<li></li>
					<li><span>6 месяцев</span></li>
					<li></li>
					<li><span>10 месяцев</span></li>
					<li></li>
					<li><span>14 месяцев</span></li>
					<li></li>
					<li><span>18 месяцев</span></li>
					<li></li>
					<li></li>
					<li><span>24 месяца</span></li>
				</ul>
			</div>
		</div>
		<div class='chart_body' data-constructor_var="1">
			{if isset($constructor.1)}
				{$constructor.1}
			{else}
				<div class='chart_inner'></div>
				<svg id="mySVG" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"></svg>
			{/if}
		</div>
		<div class='form clearfix'>
			<div class='clearfix'>
				<p>Возраст</p>
				<input type='text' placeholder='в месяцах' class='age'>
			</div>
			<div class='clearfix'>
				<p>Рост</p>
				<input type='text' placeholder='в см' class='growth'>
			</div>
			<div class='clearfix'>
				<p>Вес</p>
				<input type='text' placeholder='в кг' class='weight'>
			</div>
			<div class='clearfix'>
				<button class='add_point'>Добавить точку</button>
			</div>
		</div>
	</section>
</section>
<script src="/js/routes/growing_up.js"></script>
<!-- End of Card 5 -->