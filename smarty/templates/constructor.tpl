{include file='header.tpl' title='Мои заказы'}
<section id="constructor" class='page'>

	<div class="socials"> 
      <div class="socials_scroll">        
        <ul>
          <li class="vk">Vkontakte</li>
          <li class="in">Instagram</li>
          <li class="fb">Facebook</li>          
        </ul>
        <div class="socials_scroll_close" onclick="$('.socials').removeClass('active');"></div>
        <div class="socials_scroll_popup">
          <div class="socials_scroll_popup_result"></div>
        </div><!-- socials_scroll_popup -->
      </div><!-- socials_scroll -->   
  </div> <!-- socials -->

	<section class="page_title">
		<section class="content clearfix">
			<h1>Создать книгу</h1>
		</section>
	</section>

	<section class="clearfix content">
		<section class="constructor_content">

		{if $smarty.get.card eq 1}

			{include file='constructor_1.tpl'}

		{elseif $smarty.get.card eq 2}

			{include file='constructor_2.tpl'}

		{elseif $smarty.get.card eq 3}

			{include file='constructor_3.tpl'}

		{elseif $smarty.get.card eq 4}

			{include file='constructor_4.tpl'}

		{elseif $smarty.get.card eq 5}

			{include file='constructor_5.tpl'}

		{elseif $smarty.get.card eq 6}

			{include file='constructor_6.tpl'}

		{elseif $smarty.get.card eq 7}

			{include file='constructor_7.tpl'}

		{elseif $smarty.get.card eq 8}

			{include file='constructor_8.tpl'}

		{elseif $smarty.get.card > 8}

			{include file='constructor_new.tpl'}

		{/if}

		</section>

		<section class="constructor_menu">
			<section class="items">
				<a href='constructor.php?card=1'>
					Обложка
				</a>
				<a href='constructor.php?card=2'>
					Привет,
					а вот и я!
				</a>
				<a href='constructor.php?card=3'>
					Моя семья
				</a>
				<a href='constructor.php?card=4'>
					Моя ручка
					и ножка
				</a>
				<a href='constructor.php?card=5'>
					Я расту
				</a>
				<a href='constructor.php?card=6'>
					Любимое
				</a>
				<a href='constructor.php?card=7'>
					Путешествую
				</a>
				<a href='constructor.php?card=8'>
					Вот и год
					прошел
				</a>
				{foreach from=$additional item=card_title key=card_id}
				<a href='constructor.php?card={$card_id}'>
					{$card_title}
				</a>
				{/foreach}
			</section>
			<aside>
				<span class='add'>
					<a href='constructor.php?card={$new_card_id}'>Добавить<br>
					раздел</a>
				</span>
			</aside>
		</section>
	</section>
	<section class='card_nav clearfix'>
		<a class='left'></a>
		<a class='right'></a>
	</section>
</section>

<div id="vk_api_transport"></div>

<script src="js/routes/constructor.js"></script>
<script> Constructor.card = {$smarty.get.card}; Constructor.init(); $('.items').find('a').eq(Constructor.card - 1).addClass('active');</script>
<script>Constructor.init_nav();</script>
<script src="js/vendor/jquery.form.js"></script>
<script src="js/fb.js" type="text/javascript"></script>
<script src="js/vk.js" type="text/javascript"></script>
<script src="js/in.js" type="text/javascript"></script>
<script src="js/social.js" type="text/javascript"></script>
<script src="js/routes/photo.js"></script>
{include file='footer.tpl'}