<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>{$title}</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
        <meta name="interkassa-verification" content="9ef9cd47240e57ea66a1b0ac96a0f500" />
        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

        <link rel="stylesheet" href="/css/normalize.css">
        <link rel="stylesheet" href="/css/main.css">
        <link rel="stylesheet" href="/css/print.css">
        <link rel="stylesheet" href="/css/preview.css">
        <script src="/js/vendor/jquery-1.9.1.min.js"></script>
        <script src="/js/vendor/modernizr-2.6.2.min.js"></script>
        <script src="/js/plugins.js"></script>
        <script src="/js/vendor/lettering.js"></script>
        <script src="/js/vendor/jquery.maskedinput.js"></script>
        <script src="http://vkontakte.ru/js/api/openapi.js" type="text/javascript"></script>
        <script src="/js/global.js"></script>
    </head>
    <body>
        <!--[if lt IE 7]>
            <script src="js/vendor/css3pie/PIE.js" type="text/javascript"></script>
            <script src="js/vendor/html5shiv/html5shiv.js" type="text/javascript"></script>
        <![endif]-->