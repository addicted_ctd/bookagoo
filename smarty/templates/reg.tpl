{include file='header.tpl' title='Регистрация'}
<section id="reg" class='page'>
	<section class="page_title">
		<h1 class="content">Регистрация</h1>
	</section>
	<section class="one">
		<section class="wrapper content">
			<h1 class="g b">Вход через социальные сети</h1>
			<div class="buttons">
				<a class="fb" onclick="Global.fb_login();">Войти при помощи facebook</a>
				<a class="vk" onclick="VK.Auth.login(Global.vk_login);">Войти при помощи vkontakte</a>
			</div>
		</section>
	</section>
	<section class="two">
		<section class="wrapper content clearfix">
			<section class="clearfix">
				<div>
					<h1 class="g b">Укажите данные ниже</h1>
					<input type="text" placeholder="E-mail" name='email'>
					<input type="password" placeholder="Пароль" name='pwd'>
					<aside class="clearfix">
						<input type="checkbox" id='show_pwd'>
						<label class='g' for='show_pwd'>Показать пароль</label>
					</aside>
				</div>
				<div>
					<h1 class="g b">Укажите данные вашего ребенка</h1>
					<section class="clearfix">
						<section class="col">
							<input type="text" placeholder="Дата рождения" name='kid_birth'>
							<input type="text" placeholder="Имя" name='kid_name'>
						</section>
						<section class="col clearfix">
							<p>Пол</p>
							<a class='gender boy' data-gender='male'></a>
							<a class='gender girl' data-gender='female'></a>
						</section>
					</section>
				</div>
			</section>
			<a class='create_account'>Создать аккаунт</a>
			<section class="feed_wrapper">
				<section class="inner clearfix">
					<input type="checkbox" id='subscription' checked>
					<label class='g' for='subscription'>Подписаться на рассылку</label>
				</section>
			</section>
			<h3 class="g">Нажимая на кнопку «Создать аккаунт», вы подтверждаете свое согласие с условиями пользовательского соглашения</h3>
		</section>
	</section>
</section>

<script src="js/routes/reg.js"></script>
{include file='footer.tpl'}