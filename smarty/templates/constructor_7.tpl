<!-- Card 5 -->
<section class="card card_7 front" data-side='front'>
	<h1 class="card_title">
		Путешествую
	</h1>
	<section class="card_content">
		<div class='photo blue_light big' data-editor=true data-constructor_var="0">
			<div class='inner'>
				<p>
					<span>+</span><br>
					Добавить фото
				</p>
				<input type='text' class='blue_light comment border_input' placeholder='Добавьте комментарий' data-constructor_var="1" value="{if isset($constructor.1)}{$constructor.1}{/if}">
			</div>
			{if isset($constructor.0)}{$constructor.0}{/if}
			<input type="file" name="photo" class="load_image hidden">
		</div>
		<section class='clearfix'>
			<section class='col'>
				<div class='photo blue_light regular' data-editor=true data-constructor_var="2">
					<div class='inner'>
						<p>
							<span>+</span><br>
							Добавить фото
						</p>
						<input type='text' class='blue_light comment border_input' placeholder='Добавьте комментарий' data-constructor_var="3" value="{if isset($constructor.3)}{$constructor.3}{/if}">
					</div>
					{if isset($constructor.2)}{$constructor.2}{/if}
					<input type="file" name="photo" class="load_image hidden">
				</div>
			</section>
			<section class='col'>
				<div class='photo blue_light regular' data-editor=true data-constructor_var="4">
					<div class='inner'>
						<p>
							<span>+</span><br>
							Добавить фото
						</p>
						<input type='text' class='blue_light comment border_input' placeholder='Добавьте комментарий' data-constructor_var="5" value="{if isset($constructor.5)}{$constructor.5}{/if}">
					</div>
					{if isset($constructor.4)}{$constructor.4}{/if}
					<input type="file" name="photo" class="load_image hidden">
				</div>
			</section>
		</section>
	</section>
</section>

<section class="card card_7 back" data-side='back'>
	<h1 class="card_title">
		Путешествую
	</h1>
	<section class="card_content">
		<section class='clearfix'>
			<section class='col'>
				<div class='photo blue_light regular' data-editor=true data-constructor_var="6">
					<div class='inner'>
						<p>
							<span>+</span><br>
							Добавить фото
						</p>
						<input type='text' class='blue_light comment border_input' placeholder='Добавьте комментарий' data-constructor_var="7" value="{if isset($constructor.7)}{$constructor.7}{/if}">
					</div>
					{if isset($constructor.6)}{$constructor.6}{/if}
					<input type="file" name="photo" class="load_image hidden">
				</div>
			</section>
			<section class='col'>
				<div class='photo blue_light regular' data-editor=true data-constructor_var="8">
					<div class='inner'>
						<p>
							<span>+</span><br>
							Добавить фото
						</p>
						<input type='text' class='blue_light comment border_input' placeholder='Добавьте комментарий' data-constructor_var="9" value="{if isset($constructor.9)}{$constructor.9}{/if}">
					</div>
					{if isset($constructor.8)}{$constructor.8}{/if}
					<input type="file" name="photo" class="load_image hidden">
				</div>
			</section>
		</section>
		<div class='photo blue_light big' data-editor=true data-constructor_var="10">
			<div class='inner'>
				<p>
					<span>+</span><br>
					Добавить фото
				</p>
				<input type='text' class='blue_light comment border_input' placeholder='Добавьте комментарий' data-constructor_var="11" value="{if isset($constructor.11)}{$constructor.11}{/if}">
			</div>
			{if isset($constructor.10)}{$constructor.10}{/if}
			<input type="file" name="photo" class="load_image hidden">
		</div>
	</section>
</section>

<!-- End of Card 5 -->