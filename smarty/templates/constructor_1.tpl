<!-- Card 1 -->
<section class="card card_1 front" data-side='front'>
	<h1 class="card_title">
		Обложка
	</h1>
	<section class="card_content">
		<div class='book'>
			<div class='book_inner'>
				<div class='photo' data-editor=true data-final=false data-constructor_var="0">
					<div class='inner'>
						<p>
							<span>+</span><br>
							Добавить фото
						</p>
					</div>
					{if isset($constructor.0)}
						{$constructor.0}
					{/if}
					<input type="file" name="photo" class="load_image hidden">
				</div>
				<article>
					<input type='text' value='Меня зовут {$child_info.f_name}' disabled='true'>
					{if $child_info.gender eq 'female'}
						{assign var='word' value='родилась'}
					{else}
						{assign var='word' value='родился'}
					{/if}
					<input type='text' value='Я {$word} {$child_info.birth_full}' disabled="true">
				</article>
			</div>
		</div>
	</section>
</section>

<!-- End of Card 1 -->