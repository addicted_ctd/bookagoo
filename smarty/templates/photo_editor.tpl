<section class='photo_editor'>
	<div class='photo_wrapper'>
		<figure class='photo_preview'></figure>
		<input type="range" min="0" max="100" value="50" class='zoom' data-init="50">
		<!-- <input type="range" min="0" max="360" value="0" class='rotate'> -->
	</div>
	<div class='photo_controls clearfix'>
		<button class='confirm'>Загрузить</button>
		<button class='cancel'>Отмена</button>
		<aside class='share clearfix'>
			<p>Поделиться:</p>
			<a class="fb"></a>
			<a class="inst"></a>
		</aside>
	</div>
</section>