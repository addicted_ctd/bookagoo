{include file='header.tpl' title='Мои заказы'}
<section id="about" class='page'>
	<section class="one">
		<section class="content clearfix">
			<article>
				<h1 class="b">Кто мы?</h1>
				<p>
					Компания основана в 2013 году в Москве.<br>
					BookAgoo - это сплоченная команда единомышленников,<br>
					друзей, а также родителей. Собственный опыт, знания и<br>
					одержимость современными цифровыми технологиями<br>
					помогают нам двигаться и совершенствоваться каждый<br>
					день. Мы работаем, чтобы сделать жизнь вашей семьи<br>
					ярче и интереснее.
				</p>
			</article>
		</section>
	</section>
	<section class='two'>
		<section class="content clearfix">
			<section class='left'>
				<figure></figure>
				<figure></figure>
			</section>
			<section class='right'>
				<article>
					<h1 class="g b">Наш продукт</h1>
					<p>
						Книга первых лет жизни малыша в коробке – оригинальное, простое и красивое 
						решение для любого родителя, желающего сохранить незабываемые моменты жизни всей семьи.<br>
						Мы используем только высококачественные материалы, работаем с проверенными
						поставщиками и отвечаем за финальный
						результат.
					</p>
				</article>
			</section>
		</section>
	</section>
</section>
{include file='footer.tpl'}