{include file='header.tpl' title='Мои заказы'}
<section id="agreement" class='page'>
	<section class="page_title">
		<section class="content clearfix">
			<h1>Пользовательское соглашение</h1>
		</section>
	</section>
	<section class="one" style="padding-bottom: 70px;">
		<section class="content clearfix wrapper">
			<p class='g' style='padding: 45px; font-size: 18px'>
				Thank you for using BookAgoo! These terms of service (the “Terms”) govern your access to and use of BookAgoo (“we” or “our”) website, mobile application and services (the “Services”), so please carefully read them before using the Services. By using the Services you agree to be bound by these Terms. You may use the Services only in compliance with these Terms. The Services may continue to change over time as we refine and add more features. We may stop, suspend, or modify the Services at any time with or without prior notice to you. We may also remove any content from our Services at our discretion.
				<br><br>
				Your Content & Privacy<br>
				By using our Services you provide us with information and files (texts, images, video and audio files) that you submit to BookAgoo (together, “your content”). You retain full ownership to your content. We don’t claim any ownership to any of it. These Terms do not grant us any rights to your content or intellectual property except for the limited rights that are needed to run the Services, as explained below.<br>
				We may need your permission to do things you ask us to do with your content, for example, hosting your files, or sharing them at your direction. This includes product features visible to you, for example, image thumbnails or video previews. It also includes design choices we make to technically administer our Services, for example, how we redundantly backup data to keep it safe. You give us the permissions we need to do those things solely to provide the Services. This permission also extends to trusted third parties we work with to provide the Services.<br>
				To be clear, aside from the rare exceptions we identify in our Privacy Policy, no matter how the Services change, we won’t share your content with others, including law enforcement, for any purpose unless you direct us to. How we collect and use your information generally is also explained in our Privacy Policy.<br>
				You are solely responsible for your conduct, content, and your communications with others while using the Services. We may choose to review public content for compliance with our guidelines, but you acknowledge that BookAgoo has no obligation to monitor any information on the Services. We are not responsible for the accuracy, completeness, appropriateness, or legality of files, user posts, or any other information you may be able to access using the Services.
				<br><br>
				Sharing Your Content<br>
				The Services provide features that allow you to share your content with others or to make it public. There are many things that users may do with that content (for example, copy it, modify it, re-share it). Please consider carefully what you choose to share or make public. BookAgoo has no responsibility for that activity.
				<br><br>
				Your Responsibilities<br>
				Files and other content in the Services may be protected by intellectual property rights of others. Please do not copy, upload, download, or share files unless you have the right to do so. You will be fully responsible and liable for what you copy, share, upload, download or otherwise use while using the Services. You must not upload spyware or any other malicious software to the Service.
				<br><br>
				You are responsible for maintaining and protecting all of your content. BookAgoo will not be liable for any loss or corruption of your content, or for any costs or expenses associated with backing up or restoring any of your content.<br>
				If your contact information, or other information related to your account, changes, you must notify us promptly and keep your information current. The Services are not intended for use by you if you are under years of age. By agreeing to these Terms, you are representing to us that you are over.
			</p>
		</section>
	</section>
</section>
{include file='footer.tpl'}