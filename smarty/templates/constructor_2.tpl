<!-- Card 2 -->
<section class="card card_2 front" data-side='front'>
	<h1 class="card_title">
		Привет, а вот и я
	</h1>
	<section class="card_content">
		<section class="clearfix">
			<section class="col">
				<div class='photo white regular' data-editor='true' data-constructor_var="0">
					<div class='inner'>
						<p>
							<span>+</span><br>
							Добавить фото
						</p>
						<input type='text' class='violet comment border_input' placeholder='Добавьте комментарий' data-constructor_var="1" value="{if isset($constructor.1)}{$constructor.1}{/if}">
						<h3 class='title'>В ожидании меня</h3>
					</div>
					{if isset($constructor.0)}{$constructor.0}{/if}
					<input type="file" name="photo" class="load_image hidden">
				</div>
			</section>
			<section class="col col_2">
				<h1>Маме сказали, что я появлюсь</h1>
				<div class='calendar'>
					<input type='text' class='violet border_input date' placeholder='укажите дату' style='width: 120px;' data-constructor_var="2" value="{if isset($constructor.2)}{$constructor.2}{/if}">
				</div>
				<h1>Меня хотели назвать</h1>
				<input type='text' class='violet border_input' placeholder='укажите имя' style='width: 195px;' data-constructor_var="3" value="{if isset($constructor.3)}{$constructor.3}{/if}">
				<h1>Мое имя означает</h1>
				<input type='text' class='violet border_input' placeholder='что означает имя' style='width: 355px;' data-constructor_var="4" value="{if isset($constructor.4)}{$constructor.4}{/if}">
			</section>
		</section>
		<section class="clearfix">
			<section class="col col_3">
				<div class="clearfix">
					{if $child_info.gender eq 'female'}
						{assign var='word' value='родилась'}
					{else}
						{assign var='word' value='родился'}
					{/if}
					<h1>Я {$word} 01.06.1999</h1>
				</div>
				<div class="clearfix">
					<h1>Мой вес</h1>
					<input type='text' class='violet border_input' placeholder='укажите вес' data-constructor_var="5" value="{if isset($constructor.5)}{$constructor.5}{/if}">
				</div>
				<div class="clearfix">
					<h1>Мой рост</h1>
					<input type='text' class='violet border_input' placeholder='укажите рост' data-constructor_var="6" value="{if isset($constructor.6)}{$constructor.6}{/if}">
				</div>
				<div class="clearfix">
					<h1>Цвет глаз</h1>
					<ul data-constructor_var="7" data-selected="{if isset($constructor.7)}{$constructor.7}{/if}" class='dropdown'>
						<li class='default'>выберите цвет</li>
						<li>Синий</li>
						<li>Голубой</li>
						<li>Серый</li>
						<li>Зелёный</li>
						<li>Янтарный</li>
						<li>Болотный</li>
						<li>Карий</li>
						<li>Чёрный</li>
						<li>Жёлтый</li>
					</ul>
				</div>
				<div class="clearfix">
					<h1>Цвет волос</h1>
					<ul data-constructor_var="8" data-selected="{if isset($constructor.8)}{$constructor.8}{/if}" class='dropdown'>
						<li class='default'>выберите цвет</li>
						<li>Брюнет</li>
						<li>Рыжий</li>
						<li>Русый</li>
						<li>Блондин</li>
						<li>Шатен</li>
					</ul>
				</div>
				<div class="clearfix">
					<h1>Мой знак зодиака</h1>
					<ul data-constructor_var="9" data-selected="{if isset($constructor.9)}{$constructor.9}{/if}" class='dropdown'>
						<li class='default'>выберите знак зодиака</li>
						<li>Овен</li>
						<li>Телец</li>
						<li>Близнецы</li>
						<li>Рак</li>
						<li>Лев</li>
						<li>Дева</li>
						<li>Весы</li>
						<li>Скорпион</li>
						<li>Стрелец</li>
						<li>Козерог</li>
						<li>Водолей</li>
						<li>Рыбы</li>
					</ul>
				</div>
			</section>
			<section class="col" style="padding-top: 24px;">
				<div class='photo white regular' data-editor=true data-constructor_var="10">
					<div class='inner'>
						<p>
							<span>+</span><br>
							Добавить фото
						</p>
						<input type='text' class='violet comment border_input' placeholder='Добавьте комментарий' data-constructor_var="11" value="{if isset($constructor.11)}{$constructor.11}{/if}">
						<h3 class='title'>А вот и я</h3>
					</div>
					{if isset($constructor.10)}{$constructor.10}{/if}
					<input type="file" name="photo" class="load_image hidden">
				</div>
			</section>
		</section>
	</section>
</section>

<section class="card card_2 back" data-side='back'>
	<h1 class="card_title">
		Привет, а вот и я
	</h1>
	<section class="card_content">
		<section class="clearfix">
			<section class="col">
				<div class='photo white regular' data-editor=true data-constructor_var="12">
					<div class='inner'>
						<p>
							<span>+</span><br>
							Добавить фото
						</p>
						<input type='text' class='violet comment border_input' placeholder='Добавьте комментарий' data-constructor_var="13" value="{if isset($constructor.13)}{$constructor.13}{/if}">
					</div>
					{if isset($constructor.12)}{$constructor.12}{/if}
					<input type="file" name="photo" class="load_image hidden">
				</div>
			</section>
			<section class="col col_2">
				<div class='photo white regular' data-editor=true data-constructor_var="14">
					<div class='inner'>
						<p>
							<span>+</span><br>
							Добавить фото
						</p>
						<input type='text' class='violet comment border_input' placeholder='Добавьте комментарий' data-constructor_var="15" value="{if isset($constructor.15)}{$constructor.15}{/if}">
						<h3 class='title'>В ожидании меня</h3>
					</div>
					{if isset($constructor.14)}{$constructor.14}{/if}
					<input type="file" name="photo" class="load_image hidden">
				</div>
			</section>
		</section>
		<section class="clearfix">
			<section class="col col_3">
				<div class='photo white big' data-editor=true data-constructor_var="16">
					<div class='inner'>
						<p>
							<span>+</span><br>
							Добавить фото
						</p>
						<input type='text' class='violet comment border_input' placeholder='Добавьте комментарий' data-constructor_var="17" value="{if isset($constructor.17)}{$constructor.17}{/if}">
					</div>
					{if isset($constructor.16)}{$constructor.16}{/if}
					<input type="file" name="photo" class="load_image hidden">
				</div>
			</section>
		</section>
	</section>
</section>

<!-- End of Card 2 -->