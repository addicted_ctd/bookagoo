{include file='header.tpl' title='Главная страница'}
<section id="index">
	<script>
	$("header").hide();
	</script>
	<section class="header">
		<a id="logo"></a>
		<a id="login" href='auth.php'>Войти</a>
		<section class="content">
			<h1>
				<span>Книга “Первых лет жизни малыша”</span> 
				<span>в коробочке - это просто и красиво!</span>
			</h1>
			<div id="video">
				<a id="play_video"></a>
			</div>
			<a href="/constructor.php?card=1" id="create_book">Создать книгу</a>
			<a id="follow"></a>
		</section>
		<section class="content arrows">
			<section class="wrap">
				<figure>
					<p>Загрузи фото<br>
						в шаблон книги </p>
				</figure>
				<figure>
					<p>Закажи<br>
						печать книги </p>
				</figure>
				<figure>
					<p>Запиши только<br>
						самое важное</p>
				</figure>
				<figure>
					<p>Пополняй книгу
						новыми страницами</p>
				</figure>
			</section>
		</section>
	</section>
	<section class="one">
		<section class="content clearfix">
			<article>
				<h1>
					Несколько минут,<br>
					и книга готова!
				</h1>
				<p data-num="1.">Загрузи фото в шаблон книги<br>
				из vkontakte, facebook,<br>
				Instagram и локального диска</p>
				<p data-num="2.">Дополни фото своими комментариями.<br>
				Внеси данные в графики роста и веса,<br>
				первых зубов</p>
				<p data-num="3.">Закажи печать, и мы изготовим<br>
				книгу за 3 дня!</p>
			</article>
			<aside>
				<figure></figure>
				<figure></figure>
			</aside>
		</section>
	</section>
	<section class="two">
		<section class="content">
			<article>
				<h1>Книга растет вместе с ребенком<br>
					и радует каждый день!</h1>
				<ul>
					<li>Пополняй существующую книгу новыми страницами</li>
					<li>Поставь открытую коробочку на полку</li>
					<li>Наслаждайся воспоминаниями в любой момент!</li>
				</ul>
			</article>
		</section>
	</section>
	<section class="three">
		<section class="content">
			<h1>Стоимость</h1>
			<article class="clearfix">
				<h3>Стоимость книги</h3>
				<div>
					<p>
						Минимальный заказ<br>
						включает 10 страниц и коробку
					</p>
					<p class="rub">1600 ₽</p>
				</div>
				<div>
					<p>
						Стоимость<br>
						дополнительной карточки
					</p>
					<p class="rub">50 ₽</p>
				</div>
			</article>
			<a href="/constructor.php?card=1" id="create_book_2">Создать книгу</a>
		</section>
	</section>
</section>
<script src="js/routes/landing.js"></script>
{include file='footer.tpl'}