<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Bookagoo</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
        <meta name="interkassa-verification" content="9ef9cd47240e57ea66a1b0ac96a0f500" />
        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/preview.css">
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script src="js/vendor/modernizr-2.6.2.min.js"></script>
        <script src="js/plugins.js"></script>
        <script src="js/vendor/lettering.js"></script>
        <script src="js/vendor/jquery.maskedinput.js"></script>
        <script src="http://vkontakte.ru/js/api/openapi.js" type="text/javascript"></script>
        <script src="js/global.js"></script>
    </head>
    <body>
        <!--[if lt IE 7]>
            <script src="js/vendor/css3pie/PIE.js" type="text/javascript"></script>
            <script src="js/vendor/html5shiv/html5shiv.js" type="text/javascript"></script>
        <![endif]-->

        {literal}
        <script>
            window.fbAsyncInit = function() {
                FB.init({
                    appId: '497937203715073',
                    xfbml: true,
                    version: 'v2.4'
                });
            };

            (function(d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) {
                    return;
                }
                js = d.createElement(s);
                js.id = id;
                js.src = "//connect.facebook.net/en_US/sdk.js";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));


            VK.init({
                apiId: 5097470
            });

        </script>
        {/literal}

        <header>
            <section class="content clearfix">
                <a class="logo" href="index.php"></a>
            </section>
        </header>
        <section id="constructor" class='page preview' style="padding-top: 100px;">

            <section class="clearfix content">
                <section class="constructor_content">

                    {if $smarty.get.card eq 1}

                    {include file='constructor_1.tpl'}

                    {elseif $smarty.get.card eq 2}

                    {include file='constructor_2.tpl'}

                    {elseif $smarty.get.card eq 3}

                    {include file='constructor_3.tpl'}

                    {elseif $smarty.get.card eq 4}

                    {include file='constructor_4.tpl'}

                    {elseif $smarty.get.card eq 5}

                    {include file='constructor_5.tpl'}

                    {elseif $smarty.get.card eq 6}

                    {include file='constructor_6.tpl'}

                    {elseif $smarty.get.card eq 7}

                    {include file='constructor_7.tpl'}

                    {elseif $smarty.get.card eq 8}

                    {include file='constructor_8.tpl'}

                    {elseif $smarty.get.card > 8}

                    {include file='constructor_new.tpl'}

                    {/if}

                </section>

                <section class="constructor_menu">
                    <section class="items">
                        <a href='preview.php?u={$smarty.get.u}&card=1'>
                            Обложка
                        </a>
                        <a href='preview.php?u={$smarty.get.u}&card=2'>
                            Привет,<br>
                            а вот и я!
                        </a>
                        <a href='preview.php?u={$smarty.get.u}&card=3'>
                            Моя семья
                        </a>
                        <a href='preview.php?u={$smarty.get.u}&card=4'>
                            Моя ручка<br>
                            и ножка
                        </a>
                        <a href='preview.php?u={$smarty.get.u}&card=5'>
                            Я расту
                        </a>
                        <a href='preview.php?u={$smarty.get.u}&card=6'>
                            Любимое
                        </a>
                        <a href='preview.php?u={$smarty.get.u}&card=7'>
                            Путешествую
                        </a>
                        <a href='preview.php?u={$smarty.get.u}&card=8'>
                            Вот и год<br>
                            прошел
                        </a>
                        {foreach from=$additional item=card_title key=card_id}
                        <a href='preview.php?u={$smarty.get.u}&card={$card_id}'>
                            {$card_title}
                        </a>
                        {/foreach}
                    </section>
                </section>
            </section>

        </section>

        <script> var Constructor = {}; Constructor.card = {$smarty.get.card};
            
            $('.items').find('a').eq(Constructor.card - 1).addClass('active'); $('input, textarea').prop('disabled', true); $('[contenteditable]').removeAttr('contenteditable'); $('.photo').addClass('final');</script>

        <script src="js/vendor/jquery.form.js"></script>
        <script src="js/main.js"></script>
        <script src="js/routes/preview.js"></script>
        <script src="js/vendor/css3finalize/jquery.css3finalize.min.js" type="text/javascript"></script>
        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            var _gaq = [['_setAccount', 'UA-XXXXX-X'], ['_trackPageview']];
            (function(d, t) {
                var g = d.createElement(t), s = d.getElementsByTagName(t)[0];
                g.src = '//www.google-analytics.com/ga.js';
                s.parentNode.insertBefore(g, s)
            }(document, 'script'));
        </script>
    </body>
</html>