{include file='print_header.tpl' title='Мои заказы'}
<section id="constructor" class='page print'>
	<section class="clearfix content">
		<section class="constructor_content">

		{if $smarty.get.card eq 1}

			{include file='constructor_1.tpl'}

		{elseif $smarty.get.card eq 2}

			{include file='constructor_2.tpl'}

		{elseif $smarty.get.card eq 3}

			{include file='constructor_3.tpl'}

		{elseif $smarty.get.card eq 4}

			{include file='constructor_4.tpl'}

		{elseif $smarty.get.card eq 5}

			{include file='constructor_5_print.tpl'}

		{elseif $smarty.get.card eq 6}

			{include file='constructor_6.tpl'}

		{elseif $smarty.get.card eq 7}

			{include file='constructor_7.tpl'}

		{elseif $smarty.get.card eq 8}

			{include file='constructor_8.tpl'}

		{elseif $smarty.get.card > 8}

			{include file='constructor_new.tpl'}

		{/if}

		</section>
	</section>
</section>

<script src="/js/routes/constructor.js"></script>
<script> Constructor.card = {$smarty.get.card}; Constructor.init();</script>

{if isset($smarty.get.side)}
	<script> var to_show = '{$smarty.get.side}'; $(".card").hide(); $('.'+to_show).show();</script>
{/if}

{literal}
	<script>
		if ($('.card_8.back').length !== 0) {
			var input = $('.first').find('input');
			$('.first').html('<p>Мой первый день рождения отмечали '+input.eq(0).val()+' в '+input.eq(1).val()+'</p>');
		}
		$('.photo').addClass('final');

		//hide empty leafs
		$('.leaf').each(function() {
			if ($(this).text().indexOf('Введите имя') > -1 && $(this).find('figure').length === 0) {
				$(this).remove();
			}
		});
	</script>
{/literal}

<script src="/js/vendor/jquery.form.js"></script>
<script src="/js/routes/photo.js"></script>
{include file='print_footer.tpl'}