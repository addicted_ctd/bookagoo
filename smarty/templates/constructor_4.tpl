<!-- Card 4 -->
<section class="card card_4 front" data-side='front'>
	<h1 class="card_title">
		Моя ручка и ножка
	</h1>
	<section class="card_content">
		<p class='slim_title'>
			Моя ручка
		</p>
		<p class='notice'>
			Если вы хотите добавить отпечаток<br>
			или рисунок ручки малыша<br>
			в печатную версию книги,<br>
			просто оставьте эту страницу пустой.
		</p>
		<div class='photo' data-editor=true data-constructor_var="0">
			<div class='inner'>
				<p>
					<span>+</span><br>
					Добавить фото
				</p>
			</div>
			{if isset($constructor.0)}{$constructor.0}{/if}
			<input type="file" name="photo" class="load_image hidden">
		</div>
	</section>
</section>

<section class="card card_4 back" data-side='back'>
	<h1 class="card_title">
		Моя ручка и ножка
	</h1>
	<section class="card_content">
		<p class='slim_title'>
			Моя ножка
		</p>
		<p class='notice'>
			Если вы хотите добавить отпечаток<br>
			или рисунок ножки малыша<br>
			в печатную версию книги,<br>
			просто оставьте эту страницу пустой.
		</p>
		<div class='photo' data-editor=true data-constructor_var="1">
			<div class='inner'>
				<p>
					<span>+</span><br>
					Добавить фото
				</p>
			</div>
			{if isset($constructor.1)}{$constructor.1}{/if}
			<input type="file" name="photo" class="load_image hidden">
		</div>
	</section>
</section>

<!-- End of Card 4 -->