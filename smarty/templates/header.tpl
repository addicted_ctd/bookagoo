<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>{$title}</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
        <meta name="interkassa-verification" content="9ef9cd47240e57ea66a1b0ac96a0f500" />
        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="admin/css/construction_sight.css">
        <script src="js/vendor/jquery-1.9.1.min.js"></script>
        <script src="js/vendor/modernizr-2.6.2.min.js"></script>
        <script src="js/plugins.js"></script>
        <script src="js/vendor/lettering.js"></script>
        <script src="js/vendor/jquery.maskedinput.js"></script>
        <script src="http://vkontakte.ru/js/api/openapi.js" type="text/javascript"></script>
        <script src="js/global.js"></script>
        <script src="js/overlay.js"></script>
    </head>
    <body>
        <!--[if lt IE 7]>
            <script src="js/vendor/css3pie/PIE.js" type="text/javascript"></script>
            <script src="js/vendor/html5shiv/html5shiv.js" type="text/javascript"></script>
        <![endif]-->

        <div id='vk_api_transport'></div>

        {literal}
        <script>
            window.fbAsyncInit = function() {
                FB.init({
                    appId: '497937203715073',
                    xfbml: true,
                    version: 'v2.5'
                });
            };

            (function(d, s, id){
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) {return;}
                js = d.createElement(s); js.id = id;
                js.src = "//connect.facebook.net/en_US/sdk.js";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));

            window.vkAsyncInit = function() {
                VK.init({
                    apiId: 5097470
                });
            };

            setTimeout(function() {
              var el = document.createElement("script");
              el.type = "text/javascript";
              el.src = "//vk.com/js/api/openapi.js";
              el.async = true;
              document.getElementById("vk_api_transport").appendChild(el);  }, 0);

        </script>
        {/literal}

        <div id='fb-root'></div>

        <header>
            <section class="content clearfix">
                <a class="logo" href="constructor.php?card=1"></a>
                <ul class="clearfix">
                    {if $login_status eq true}
                        <li>
                            <a class="under_hover" href="preview.php?u={$uid}&card=1" target="_blank">Поделиться</a>
                        </li>
                        <li>
                            <a class="under_hover" href="constructor.php?card=1">Книга</a>
                        </li>
                        <li>
                            <a href="cart.php" class="order_print">Заказать печать</a>
                        </li>
                        <li>
                            <a class="under_hover" href="profile.php">Профиль</a>
                        </li>
                        <li>
                            <a href="logout.php" class="logout">Выход</a>
                        </li>
                    {else}
                        <li>
                            <a class="under_hover" href="reg.php">Регистрация</a>
                        </li>
                        <li>
                            <a class="under_hover" href="auth.php">Авторизация</a>
                        </li>
                    {/if}
                </ul>
            </section>
        </header>