{include file='header.tpl' title='Вход'}
<section id="auth" class='page'>
	<section class="page_title">
		<h1 class="content">Вход</h1>
	</section>
	<section class="content wrapper soc_enter_block">
		<h2 class="b g">Вход через социальные сети</h2>
		<div class="soc_enter">
			<div class="soc_fb" onclick="Global.fb_login();">Войти при помощи facebook</div>
			<div class="soc_vk" onclick="VK.Auth.login(Global.vk_login);">Войти при помощи vkontakte</div>
		</div>
	</section>
	<section class="content wrapper enter_block">
		<h2 class="b g">Либо войдите через E-mail</h2>
		<div class="form_enter_block">
			<input type="text" placeholder="E-mail" class="gray" name="email" id="user_name">
			<input type="password" placeholder="Пароль" class="gray" name="user_password" id="user_password">
			<div class="checkbox_block">
				<input type="checkbox" name="show_pwd" id="show_pwd">
				<label for="show_pwd" class="g">Показать пароль</label>
			</div>
			<div class="botton_block">
				<button id='login'>Вход</button>
			</div>
		</div>
	</section>
</section>
<script src="js/routes/auth.js"></script>
{include file='footer.tpl'}