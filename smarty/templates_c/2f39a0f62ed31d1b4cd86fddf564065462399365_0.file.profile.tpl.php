<?php /* Smarty version 3.1.27, created on 2015-11-07 18:17:09
         compiled from "/var/www/quantum/webpoetry.org/bookagoo/docs/app/smarty/templates/profile.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:1299399874563e8675912695_47306816%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2f39a0f62ed31d1b4cd86fddf564065462399365' => 
    array (
      0 => '/var/www/quantum/webpoetry.org/bookagoo/docs/app/smarty/templates/profile.tpl',
      1 => 1446912951,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1299399874563e8675912695_47306816',
  'variables' => 
  array (
    'profile' => 0,
    'uid' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_563e8675993625_34024340',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_563e8675993625_34024340')) {
function content_563e8675993625_34024340 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '1299399874563e8675912695_47306816';
echo $_smarty_tpl->getSubTemplate ('header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>'Контакты'), 0);
?>

<section id="profile" class='page'>
	<section class="page_title">
		<h1 class="content">Аккаунт</h1>
	</section>
	<section class="one">
		<section class="content clearfix">
			<section class="left">
				<section class="wrapper">
					<div class="row">
						<h2 class="b g">Настройки аккаунта</h2>
						<input type='text' placeholder='ФИО' class='gray' value="<?php if (!empty($_smarty_tpl->tpl_vars['profile']->value['me']['l_name'])) {
echo $_smarty_tpl->tpl_vars['profile']->value['me']['l_name'];?>
 <?php }
if (!empty($_smarty_tpl->tpl_vars['profile']->value['me']['f_name'])) {
echo $_smarty_tpl->tpl_vars['profile']->value['me']['f_name'];?>
 <?php }
if (!empty($_smarty_tpl->tpl_vars['profile']->value['me']['m_name'])) {
echo $_smarty_tpl->tpl_vars['profile']->value['me']['m_name'];
}?>" name='fio'>
						<input type='text' placeholder='Электронная почта' class='gray' value="<?php echo $_smarty_tpl->tpl_vars['profile']->value['me']['email'];?>
" name='email'>
					</div>
					<div class="row clearfix">
						<h2 class="b g">Данные ребенка</h2>
						<input type='text' placeholder='Имя ребенка' class='gray' value="<?php echo $_smarty_tpl->tpl_vars['profile']->value['child']['f_name'];?>
" name='child_name'>
						<div class="clearfix" style="margin-top: 18px;">
							<div class="gender">
								<p class="b g">Пол:</p>
								<a class='boy <?php if ($_smarty_tpl->tpl_vars['profile']->value['child']['gender'] == 'male') {?>active<?php }?>' data-gender='male'></a>
								<a class='girl <?php if ($_smarty_tpl->tpl_vars['profile']->value['child']['gender'] == 'female') {?>active<?php }?>' data-gender='female'></a>
							</div>
							<div class='birth_date'>
								<input type='text' class='gray' placeholder='Дата рождения' value="<?php echo $_smarty_tpl->tpl_vars['profile']->value['child']['birth_date'];?>
" name='child_birth'>
								<a class='calendar'></a>
							</div>
						</div>
					</div>
					<div class="row password_change">
						<h2 class="b g">Изменить пароль</h2>
						<input type='password' placeholder='Старый пароль' class='gray' name='old_pass'>
						<input type='password' placeholder='Новый пароль' class='gray' name='new_pass'>
						<input type='password' placeholder='Повторите новый пароль' class='gray' name='new_pass_repeat'>
					</div>
					<div class="row clearfix">
						<button class='save_changes'>Сохранить</button>
						<a class='delete_account'>Удалить аккаунт</a>
					</div>
				</section>
			</section>
			<section class="right">
				<section class="wrapper">
					<?php if (empty($_smarty_tpl->tpl_vars['profile']->value['social']['vk_id']) || empty($_smarty_tpl->tpl_vars['profile']->value['social']['fb_id'])) {?>
					<div class="row" style='margin-bottom: 40px;'>
						<h2 class="b g">Свяжите социальные сети с аккаунтом</h2>
						<?php if (empty($_smarty_tpl->tpl_vars['profile']->value['social']['vk_id'])) {?>
						<a class='connect_to_vk' onclick="VK.Auth.login(Global.vk_login);">Соеденить со Вконтакте</a>
						<?php }?>
						<?php if (empty($_smarty_tpl->tpl_vars['profile']->value['social']['fb_id'])) {?>
						<a class='connect_to_fb' onclick="Global.fb_login();">Соеденить с Facebook</a>
						<?php }?>
					</div>
					<?php }?>
					<?php if (!empty($_smarty_tpl->tpl_vars['profile']->value['social']['vk_id']) || !empty($_smarty_tpl->tpl_vars['profile']->value['social']['fb_id'])) {?>
					<div class="row" style="padding-bottom: 40px;">
						<h2 class="b g">Связанные аккаунты</h2>	
						<?php if (!empty($_smarty_tpl->tpl_vars['profile']->value['social']['fb_id'])) {?>
						<div class="account fb clearfix">
							<p><?php echo $_smarty_tpl->tpl_vars['profile']->value['social']['fb_f_name'];?>
 <?php echo $_smarty_tpl->tpl_vars['profile']->value['social']['fb_l_name'];?>
</p>
							<a class='disconnect' data-network="fb">Отвязать</a>
						</div>
						<?php }?>
						<?php if (!empty($_smarty_tpl->tpl_vars['profile']->value['social']['vk_id'])) {?>
						<div class="account vk clearfix" style='margin-top: 20px;'>
							<p><?php echo $_smarty_tpl->tpl_vars['profile']->value['social']['vk_f_name'];?>
 <?php echo $_smarty_tpl->tpl_vars['profile']->value['social']['vk_l_name'];?>
</p>
							<a class='disconnect' data-network="vk">Отвязать</a>
						</div>
						<?php }?>
					</div>
					<?php }?>
				</section>
				<section class="wrapper" style="margin-top: 20px;">
					<div class="row">
						<h2 class="b g">Рассылка</h2>
						<div class="clearfix news_subscription">
							<input id="have_mess" type="checkbox" <?php if ($_smarty_tpl->tpl_vars['profile']->value['newsletter']['subscription'] == 1) {?>checked<?php }?> name='subscription'>
							<label for="have_mess">
								Получать уведомления<br>
								по электронной почте
							</label>
						</div>
					</div>
				</section>
				<section class="wrapper" style="margin-top: 20px;">
					<div class="row">
						<h2 class="b g">Книга онлайн</h2>
						<p class="book_online">
							Нажмите на кнопку ниже, чтобы опубликовать<br>
							вашу книгу в интернете. Она будет доступна<br>
							для просмотра по прямой уникальной ссылке.<br>
							Рекомендуем добавить ее в избранное<br>
							вашего браузера.
						</p>
						<a class='publish_book' href="preview.php?u=<?php echo $_smarty_tpl->tpl_vars['uid']->value;?>
&card=1" target="_blank">Опубликовать книгу онлайн</a>
					</div>
				</section>
			</section>
			<?php echo $_smarty_tpl->getSubTemplate ('profile_menu.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

		</section>
	</section>
</section>
<?php echo '<script'; ?>
 src="js/routes/profile.js"><?php echo '</script'; ?>
>
<?php echo $_smarty_tpl->getSubTemplate ('footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);

}
}
?>