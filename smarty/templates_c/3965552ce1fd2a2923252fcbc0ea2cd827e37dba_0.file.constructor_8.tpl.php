<?php /* Smarty version 3.1.27, created on 2015-11-02 19:08:12
         compiled from "/var/www/quantum/webpoetry.org/bookagoo/docs/app/smarty/templates/constructor_8.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:9290801555637faec74e2e4_17542715%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3965552ce1fd2a2923252fcbc0ea2cd827e37dba' => 
    array (
      0 => '/var/www/quantum/webpoetry.org/bookagoo/docs/app/smarty/templates/constructor_8.tpl',
      1 => 1446484081,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '9290801555637faec74e2e4_17542715',
  'variables' => 
  array (
    'constructor' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_5637faec7bb787_30711241',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_5637faec7bb787_30711241')) {
function content_5637faec7bb787_30711241 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '9290801555637faec74e2e4_17542715';
?>
<!-- Card 5 -->
<section class="card card_8 front" data-side='front'>
	<h1 class="card_title">
		Вот и год прошел
	</h1>
	<section class="card_content">
		<section class='clearfix'>
			<section class='col'>
				<div class='photo blue_light regular' data-editor=true data-constructor_var="0">
					<div class='inner'>
						<p>
							<span>+</span><br>
							Добавить фото
						</p>
						<input type='text' class='blue_light comment border_input' placeholder='Добавьте комментарий' data-constructor_var="1" value="<?php if (isset($_smarty_tpl->tpl_vars['constructor']->value[1])) {
echo $_smarty_tpl->tpl_vars['constructor']->value[1];
}?>">
					</div>
					<?php if (isset($_smarty_tpl->tpl_vars['constructor']->value[0])) {
echo $_smarty_tpl->tpl_vars['constructor']->value[0];
}?>
					<input type="file" name="photo" class="load_image hidden">
				</div>
			</section>
			<section class='col'>
				<div class='notes'>
					<h1>Заметки</h1>
					<textarea data-constructor_var="2"><?php if (isset($_smarty_tpl->tpl_vars['constructor']->value[2])) {
echo $_smarty_tpl->tpl_vars['constructor']->value[2];
}?></textarea>
				</div>
			</section>
		</section>
		<div class='photo blue_light big' data-editor=true data-constructor_var="3">
			<div class='inner'>
				<p>
					<span>+</span><br>
					Добавить фото
				</p>
				<input type='text' class='blue_light comment border_input' placeholder='Добавьте комментарий' data-constructor_var="4" value="<?php if (isset($_smarty_tpl->tpl_vars['constructor']->value[4])) {
echo $_smarty_tpl->tpl_vars['constructor']->value[4];
}?>">
			</div>
			<?php if (isset($_smarty_tpl->tpl_vars['constructor']->value[3])) {
echo $_smarty_tpl->tpl_vars['constructor']->value[3];
}?>
			<input type="file" name="photo" class="load_image hidden">
		</div>
	</section>
</section>

<section class="card card_8 back" data-side='back'>
	<h1 class="card_title">
		Вот и год прошел
	</h1>
	<section class="card_content">
		<section class="first clearfix">
			<p>
				Мой первый день рождения отмечали 
			</p>
			<div class='calendar'>
				<input type='text' class='blue_light border_input date' placeholder='укажите дату' style='width: 197px;' data-constructor_var="5" value="<?php if (isset($_smarty_tpl->tpl_vars['constructor']->value[5])) {
echo $_smarty_tpl->tpl_vars['constructor']->value[5];
}?>">
			</div>
			<p>
				в
			</p>
			<input type='text' class='blue_light border_input' placeholder='укажите место' style='width: 250px;' data-constructor_var="6" value="<?php if (isset($_smarty_tpl->tpl_vars['constructor']->value[6])) {
echo $_smarty_tpl->tpl_vars['constructor']->value[6];
}?>">
		</section>
		<div class='photo blue_light big' data-editor=true data-constructor_var="7">
			<div class='inner'>
				<p>
					<span>+</span><br>
					Добавить фото
				</p>
				<input type='text' class='blue_light comment border_input' placeholder='Добавьте комментарий' data-constructor_var="8" value="<?php if (isset($_smarty_tpl->tpl_vars['constructor']->value[8])) {
echo $_smarty_tpl->tpl_vars['constructor']->value[8];
}?>">
			</div>
			<?php if (isset($_smarty_tpl->tpl_vars['constructor']->value[7])) {
echo $_smarty_tpl->tpl_vars['constructor']->value[7];
}?>
			<input type="file" name="photo" class="load_image hidden">
		</div>
		<section class='clearfix second'>
			<section class='col'>
				<div class='photo blue_light regular' data-editor=true data-constructor_var="9">
					<div class='inner'>
						<p>
							<span>+</span><br>
							Добавить фото
						</p>
						<input type='text' class='blue_light comment border_input' placeholder='Добавьте комментарий' data-constructor_var="10" value="<?php if (isset($_smarty_tpl->tpl_vars['constructor']->value[10])) {
echo $_smarty_tpl->tpl_vars['constructor']->value[10];
}?>">
					</div>
					<?php if (isset($_smarty_tpl->tpl_vars['constructor']->value[9])) {
echo $_smarty_tpl->tpl_vars['constructor']->value[9];
}?>
					<input type="file" name="photo" class="load_image hidden">
				</div>
			</section>
			<section class='col'>
				<div class='photo blue_light regular' data-editor=true data-constructor_var="11">
					<div class='inner'>
						<p>
							<span>+</span><br>
							Добавить фото
						</p>
						<input type='text' class='blue_light comment border_input' placeholder='Добавьте комментарий' data-constructor_var="12" value="<?php if (isset($_smarty_tpl->tpl_vars['constructor']->value[12])) {
echo $_smarty_tpl->tpl_vars['constructor']->value[12];
}?>">
					</div>
					<?php if (isset($_smarty_tpl->tpl_vars['constructor']->value[11])) {
echo $_smarty_tpl->tpl_vars['constructor']->value[11];
}?>
					<input type="file" name="photo" class="load_image hidden">
				</div>
			</section>
		</section>
	</section>
</section>

<!-- End of Card 5 --><?php }
}
?>