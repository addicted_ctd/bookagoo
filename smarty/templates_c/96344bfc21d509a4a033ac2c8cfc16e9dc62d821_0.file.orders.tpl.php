<?php /* Smarty version 3.1.27, created on 2015-10-02 08:36:33
         compiled from "/home/quantum/webpoetry.org/bookagoo/smarty/templates/orders.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:1856359435560e7a51071142_36159822%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '96344bfc21d509a4a033ac2c8cfc16e9dc62d821' => 
    array (
      0 => '/home/quantum/webpoetry.org/bookagoo/smarty/templates/orders.tpl',
      1 => 1443785584,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1856359435560e7a51071142_36159822',
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_560e7a51083f92_66084181',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_560e7a51083f92_66084181')) {
function content_560e7a51083f92_66084181 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '1856359435560e7a51071142_36159822';
echo $_smarty_tpl->getSubTemplate ('header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>'Мои заказы'), 0);
?>

<section id="orders" class='page'>
	<section class="page_title">
		<h1 class="content">Мои заказы</h1>
	</section>
	<section class="one">
		<section class="content clearfix">
			<section class="left">
				<section class='order wrapper'>
					<div class='top clearfix'>
						<hgroup>
							<h2 class="b g">Заказ № 4 от 00/00/2045</h2>
							<h3><span class="b g">Адрес доставки:</span> Москва, ул. Гарибальди, д. 3, квартира 168</h3>
						</hgroup>
						<h4 status="1" class="b">Заказ печатается</h4>
					</div>
					<div class='middle'>
						<h2 class="b g">Состав заказа:</h2>
						<div class='order_content clearfix'>
							<figure style='background-image: url(img/profile/book_preview.png);'></figure>
							<p class='b'>Книга (коробка и 5 карточек)</p>
							<p class='b'>1 шт</p>
							<p class='b'>3500 рублей</p>
						</div>
					</div>
					<div class='bottom clearfix'>
						<p class='b'>Итого : 3500 рублей</p>
						<a class='green_button_arrow'>Отменить заказ</a>
					</div>
				</section>
				<section class='order wrapper'>
					<div class='top clearfix'>
						<hgroup>
							<h2 class="b g">Заказ № 4 от 00/00/2045</h2>
							<h3><span class="b g">Адрес доставки:</span> Москва, ул. Гарибальди, д. 3, квартира 168</h3>
						</hgroup>
						<h4 status="2" class="b">Заказ готов</h4>
					</div>
					<div class='middle'>
						<h2>Состав заказа:</h2>
						<div class='order_content clearfix'>
							<figure style='background-image: url(img/profile/book_preview.png);'></figure>
							<p class='b'>Книга (коробка и 5 карточек)</p>
							<p class='b'>1 шт</p>
							<p class='b'>3500 рублей</p>
						</div>
					</div>
					<div class='bottom clearfix'>
						<p class='b'>Итого : 3500 рублей</p>
						<a class='green_button_arrow'>Отменить заказ</a>
					</div>
				</section>
			</section>
			<?php echo $_smarty_tpl->getSubTemplate ('profile_menu.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

		</section>
	</section>
</section>
<?php echo $_smarty_tpl->getSubTemplate ('footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);

}
}
?>