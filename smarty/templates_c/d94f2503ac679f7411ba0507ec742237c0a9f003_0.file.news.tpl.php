<?php /* Smarty version 3.1.27, created on 2015-10-02 07:34:04
         compiled from "/home/quantum/webpoetry.org/bookagoo/smarty/templates/news.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:354407914560e6bac872ef8_71427704%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd94f2503ac679f7411ba0507ec742237c0a9f003' => 
    array (
      0 => '/home/quantum/webpoetry.org/bookagoo/smarty/templates/news.tpl',
      1 => 1443785597,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '354407914560e6bac872ef8_71427704',
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_560e6bac8891d0_82562621',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_560e6bac8891d0_82562621')) {
function content_560e6bac8891d0_82562621 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '354407914560e6bac872ef8_71427704';
echo $_smarty_tpl->getSubTemplate ('header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>'Новости'), 0);
?>

<section id="news" class='page'> 
    <section class="one page_title"> 
        <h1 class="content">Новости</h1>
    </section> 
    <section class="two clearfix"> 
        <section class="content clearfix"> 
            <section class="left">         
                <article class="news_preview clearfix">
                    <figure style="background-image: url(img/news_2.jpg);">
                        <span class="date b">25.09.2015</span>
                    </figure>
                    <div>
                        <h2 class="title b">Aenean sollicitudin, lorem quis bibendum  auctor.</h2>
                        <p class="lead">
                            Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec 
                            sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. <br>
                            Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit.             
                        </p>
                        <a href="article.php" class="b">read more</a>
                    </div>
                </article>
                <article class="news_preview clearfix no_image">
                    <figure style="background-image: url();">
                        <span class="date b">24.09.2015</span>
                    </figure>
                    <div>
                        <h2 class="title b">Proin gravida nibh vel velit auctor aliquet sollicitudin, lorem quis.</h2>
                        <p class="lead">
                            Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. 
                            Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. <br>
                            Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. 
                            Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.
                        </p>
                        <a href="article.php" class="b">read more</a>
                    </div>
                </article>
                <article class="news_preview clearfix">
                    <figure style="background-image: url(img/news_1.jpg);">
                        <span class="date b">25.09.2015</span>
                    </figure>
                    <div>
                        <h2 class="title b">Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat</h2>
                        <p class="lead">
                            Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec 
                            sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. <br>
                            Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit.             
                        </p>
                        <a href="article.php" class="b">read more</a>
                    </div>
                </article>
            </section> 
            <section class="right"> 
                <?php echo $_smarty_tpl->getSubTemplate ('sidebar.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

            </section> 
        </section>
    </section>
</section>
<?php echo $_smarty_tpl->getSubTemplate ('footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);

}
}
?>