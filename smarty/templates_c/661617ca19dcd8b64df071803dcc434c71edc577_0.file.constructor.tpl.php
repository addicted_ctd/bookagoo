<?php /* Smarty version 3.1.27, created on 2015-10-08 12:53:58
         compiled from "/home/quantum/webpoetry.org/bookagoo/smarty/templates/constructor.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:214061780056169fa6b64ac0_84522929%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '661617ca19dcd8b64df071803dcc434c71edc577' => 
    array (
      0 => '/home/quantum/webpoetry.org/bookagoo/smarty/templates/constructor.tpl',
      1 => 1444323234,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '214061780056169fa6b64ac0_84522929',
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_56169fa6ba8cb1_07976673',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_56169fa6ba8cb1_07976673')) {
function content_56169fa6ba8cb1_07976673 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '214061780056169fa6b64ac0_84522929';
echo $_smarty_tpl->getSubTemplate ('header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>'Мои заказы'), 0);
?>

<section id="constructor" class='page'>
	<section class="page_title">
		<section class="content clearfix">
			<h1>Создать книгу</h1>
		</section>
	</section>

	<section class="clearfix content">
		<section class="constructor_content">

		<?php if ($_GET['card'] == 1) {?>

			<?php echo $_smarty_tpl->getSubTemplate ('constructor_1.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>


		<?php } elseif ($_GET['card'] == 2) {?>

			<?php echo $_smarty_tpl->getSubTemplate ('constructor_2.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>


		<?php } elseif ($_GET['card'] == 3) {?>

			<?php echo $_smarty_tpl->getSubTemplate ('constructor_3.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>


		<?php } elseif ($_GET['card'] == 4) {?>

			<?php echo $_smarty_tpl->getSubTemplate ('constructor_4.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>


		<?php } elseif ($_GET['card'] == 5) {?>

			<?php echo $_smarty_tpl->getSubTemplate ('constructor_5.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>


		<?php } elseif ($_GET['card'] == 6) {?>

			<?php echo $_smarty_tpl->getSubTemplate ('constructor_6.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>


		<?php } elseif ($_GET['card'] == 7) {?>

			<?php echo $_smarty_tpl->getSubTemplate ('constructor_7.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>


		<?php } elseif ($_GET['card'] == 8) {?>

			<?php echo $_smarty_tpl->getSubTemplate ('constructor_8.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>


		<?php } elseif ($_GET['card'] == 'new') {?>

			<?php echo $_smarty_tpl->getSubTemplate ('constructor_new.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>


		<?php }?>

		</section>

		<section class="constructor_menu">
			<section class="items">
				<a href='constructor.php?card=1'>
					Обложка
				</a>
				<a href='constructor.php?card=2'>
					Привет,<br>
					а вот и я!
				</a>
				<a href='constructor.php?card=3'>
					Моя семья
				</a>
				<a href='constructor.php?card=4'>
					Моя ручка<br>
					и ножка
				</a>
				<a href='constructor.php?card=5'>
					Я расту
				</a>
				<a href='constructor.php?card=6'>
					Любимое
				</a>
				<a href='constructor.php?card=7'>
					Путешествую
				</a>
				<a href='constructor.php?card=8'>
					Вот и год<br>
					прошел
				</a>
			</section>
			<aside>
				<span class='add'>
					<a href='constructor.php?card=new'>Добавить<br>
					раздел</a>
				</span>
			</aside>
		</section>
	</section>

</section>
<?php echo '<script'; ?>
 src="js/vendor/jquery.form.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="js/routes/photo.js"><?php echo '</script'; ?>
>
<?php echo $_smarty_tpl->getSubTemplate ('footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);

}
}
?>