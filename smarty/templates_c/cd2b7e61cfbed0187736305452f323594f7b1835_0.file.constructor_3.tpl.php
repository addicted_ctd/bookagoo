<?php /* Smarty version 3.1.27, created on 2015-11-06 15:36:17
         compiled from "/var/www/quantum/webpoetry.org/bookagoo/docs/app/smarty/templates/constructor_3.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:1654707644563d0f41f156f6_75579011%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'cd2b7e61cfbed0187736305452f323594f7b1835' => 
    array (
      0 => '/var/www/quantum/webpoetry.org/bookagoo/docs/app/smarty/templates/constructor_3.tpl',
      1 => 1446816989,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1654707644563d0f41f156f6_75579011',
  'variables' => 
  array (
    'child_info' => 0,
    'constructor' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_563d0f420c50d2_07082064',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_563d0f420c50d2_07082064')) {
function content_563d0f420c50d2_07082064 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '1654707644563d0f41f156f6_75579011';
?>
<!-- Card 3 -->
<section class="card card_3 front" data-side='front'>
	<h1 class="card_title">
		Моя семья
	</h1>
	<section class="card_content">
		<div class="leaf_wrapper" style="left: 406px; top: 363px;">
			<div class="leaf me">
				<h1 class='circle_text kitty' deg='6' dir='1'>Я</h1>
				<h2 class='circle_text kitty' deg='9' dir='-1' style='font-size: 17px;'><?php echo $_smarty_tpl->tpl_vars['child_info']->value['f_name'];?>
</h2>
				<div class="leaf_inner">
					<div class='photo' data-constructor_var="0">
						<div class='inner'>
							<p>
								<span>+</span>
							</p>
						</div>
						<?php if (isset($_smarty_tpl->tpl_vars['constructor']->value[0])) {
echo $_smarty_tpl->tpl_vars['constructor']->value[0];
}?>
						<input type="file" name="photo" class="load_image hidden">
					</div>
				</div>
			</div>
		</div>
		<div class="leaf_wrapper" style="left: 224px; top: 480px;">
			<div class="leaf">
				<h1 class='circle_text br_s' deg='6' dir='1' data-constructor_var="29"><?php if (isset($_smarty_tpl->tpl_vars['constructor']->value[29])) {
echo $_smarty_tpl->tpl_vars['constructor']->value[29];
} else { ?>Брат<?php }?></h1>
				<h2 class='circle_text' contenteditable='true' deg='5' dir='-1' data-constructor_var="1"><?php if (isset($_smarty_tpl->tpl_vars['constructor']->value[1])) {
echo $_smarty_tpl->tpl_vars['constructor']->value[1];
} else { ?>Введите имя<?php }?></h2>
				<div class="leaf_inner">
					<div class='photo' data-constructor_var="2">
						<div class='inner'>
							<p>
								<span>+</span>
							</p>
						</div>
						<?php if (isset($_smarty_tpl->tpl_vars['constructor']->value[2])) {
echo $_smarty_tpl->tpl_vars['constructor']->value[2];
}?>
						<input type="file" name="photo" class="load_image hidden">
					</div>
				</div>
			</div>
		</div>
		<div class="leaf_wrapper" style="left: 590px; top: 480px;">
			<div class="leaf">
				<h1 class='circle_text br_s' deg='6' dir='1' data-constructor_var="30"><?php if (isset($_smarty_tpl->tpl_vars['constructor']->value[30])) {
echo $_smarty_tpl->tpl_vars['constructor']->value[30];
} else { ?>Сестра<?php }?></h1>
				<h2 class='circle_text' contenteditable='true' deg='5' dir='-1' data-constructor_var="3"><?php if (isset($_smarty_tpl->tpl_vars['constructor']->value[3])) {
echo $_smarty_tpl->tpl_vars['constructor']->value[3];
} else { ?>Введите имя<?php }?></h2>
				<div class="leaf_inner">
					<div class='photo' data-constructor_var="4">
						<div class='inner'>
							<p>
								<span>+</span>
							</p>
						</div>
						<?php if (isset($_smarty_tpl->tpl_vars['constructor']->value[4])) {
echo $_smarty_tpl->tpl_vars['constructor']->value[4];
}?>
						<input type="file" name="photo" class="load_image hidden">
					</div>
				</div>
			</div>
		</div>
		<div class="leaf_wrapper" style="left: 301px; top: 687px;">
			<div class="leaf">
				<h1 class='circle_text' deg='6' dir='1'>Папа</h1>
				<h2 class='circle_text' contenteditable='true' deg='5' dir='-1' data-constructor_var="5"><?php if (isset($_smarty_tpl->tpl_vars['constructor']->value[5])) {
echo $_smarty_tpl->tpl_vars['constructor']->value[5];
} else { ?>Введите имя<?php }?></h2>
				<div class="leaf_inner">
					<div class='photo' data-constructor_var="6">
						<div class='inner'>
							<p>
								<span>+</span>
							</p>
						</div>
						<?php if (isset($_smarty_tpl->tpl_vars['constructor']->value[6])) {
echo $_smarty_tpl->tpl_vars['constructor']->value[6];
}?>
						<input type="file" name="photo" class="load_image hidden">
					</div>
				</div>
			</div>
		</div>
		<div class="leaf_wrapper" style="left: 517px; top: 687px;">
			<div class="leaf">
				<h1 class='circle_text' deg='6' dir='1'>Мама</h1>
				<h2 class='circle_text' contenteditable='true' deg='5' dir='-1' data-constructor_var="7"><?php if (isset($_smarty_tpl->tpl_vars['constructor']->value[7])) {
echo $_smarty_tpl->tpl_vars['constructor']->value[7];
} else { ?>Введите имя<?php }?></h2>
				<div class="leaf_inner">
					<div class='photo' data-constructor_var="8">
						<div class='inner'>
							<p>
								<span>+</span>
							</p>
						</div>
						<?php if (isset($_smarty_tpl->tpl_vars['constructor']->value[8])) {
echo $_smarty_tpl->tpl_vars['constructor']->value[8];
}?>
						<input type="file" name="photo" class="load_image hidden">
					</div>
				</div>
			</div>
		</div>
		<div class="leaf_wrapper" style="left: 96px; top: 777px;">
			<div class="leaf">
				<h1 class='circle_text' deg='6' dir='1'>Дедушка</h1>
				<h2 class='circle_text' contenteditable='true' deg='5' dir='-1' data-constructor_var="9"><?php if (isset($_smarty_tpl->tpl_vars['constructor']->value[9])) {
echo $_smarty_tpl->tpl_vars['constructor']->value[9];
} else { ?>Введите имя<?php }?></h2>
				<div class="leaf_inner">
					<div class='photo' data-constructor_var="10">
						<div class='inner'>
							<p>
								<span>+</span>
							</p>
						</div>
						<?php if (isset($_smarty_tpl->tpl_vars['constructor']->value[10])) {
echo $_smarty_tpl->tpl_vars['constructor']->value[10];
}?>
						<input type="file" name="photo" class="load_image hidden">
					</div>
				</div>
			</div>
		</div>
		<div class="leaf_wrapper" style="left: 720px; top: 777px;">
			<div class="leaf">
				<h1 class='circle_text' deg='6' dir='1'>Дедушка</h1>
				<h2 class='circle_text' contenteditable='true' deg='5' dir='-1' data-constructor_var="11"><?php if (isset($_smarty_tpl->tpl_vars['constructor']->value[11])) {
echo $_smarty_tpl->tpl_vars['constructor']->value[11];
} else { ?>Введите имя<?php }?></h2>
				<div class="leaf_inner">
					<div class='photo' data-constructor_var="12">
						<div class='inner'>
							<p>
								<span>+</span>
							</p>
						</div>
						<?php if (isset($_smarty_tpl->tpl_vars['constructor']->value[12])) {
echo $_smarty_tpl->tpl_vars['constructor']->value[12];
}?>
						<input type="file" name="photo" class="load_image hidden">
					</div>
				</div>
			</div>
		</div>
		<div class="leaf_wrapper" style="left: 275px; top: 906px;">
			<div class="leaf">
				<h1 class='circle_text' deg='6' dir='1'>Бабушка</h1>
				<h2 class='circle_text' contenteditable='true' deg='5' dir='-1' data-constructor_var="13"><?php if (isset($_smarty_tpl->tpl_vars['constructor']->value[13])) {
echo $_smarty_tpl->tpl_vars['constructor']->value[13];
} else { ?>Введите имя<?php }?></h2>
				<div class="leaf_inner">
					<div class='photo' data-constructor_var="14">
						<div class='inner'>
							<p>
								<span>+</span>
							</p>
						</div>
						<?php if (isset($_smarty_tpl->tpl_vars['constructor']->value[14])) {
echo $_smarty_tpl->tpl_vars['constructor']->value[14];
}?>
						<input type="file" name="photo" class="load_image hidden">
					</div>
				</div>
			</div>
		</div>
		<div class="leaf_wrapper" style="left: 542px; top: 906px;">
			<div class="leaf">
				<h1 class='circle_text' deg='6' dir='1'>Бабушка</h1>
				<h2 class='circle_text' contenteditable='true' deg='5' dir='-1' data-constructor_var="15"><?php if (isset($_smarty_tpl->tpl_vars['constructor']->value[15])) {
echo $_smarty_tpl->tpl_vars['constructor']->value[15];
} else { ?>Введите имя<?php }?></h2>
				<div class="leaf_inner">
					<div class='photo' data-constructor_var="16">
						<div class='inner'>
							<p>
								<span>+</span>
							</p>
						</div>
						<?php if (isset($_smarty_tpl->tpl_vars['constructor']->value[16])) {
echo $_smarty_tpl->tpl_vars['constructor']->value[16];
}?>
						<input type="file" name="photo" class="load_image hidden">
					</div>
				</div>
			</div>
		</div>
	</section>
</section>

<section class="card card_3 back" data-side='back'>
	<h1 class="card_title">
		Моя семья
	</h1>
	<section class="card_content clearfix">
		<section class="col">
			<div class='photo white regular' data-editor=true data-constructor_var="17">
				<div class='inner'>
					<p>
						<span>+</span><br>
						Добавить фото
					</p>
					<input type='text' class='blue comment border_input' placeholder='Добавьте комментарий' data-constructor_var="18" value="<?php if (isset($_smarty_tpl->tpl_vars['constructor']->value[18])) {
echo $_smarty_tpl->tpl_vars['constructor']->value[18];
}?>">
				</div>
				<?php if (isset($_smarty_tpl->tpl_vars['constructor']->value[17])) {
echo $_smarty_tpl->tpl_vars['constructor']->value[17];
}?>
				<input type="file" name="photo" class="load_image hidden">
			</div>
		</section>
		<section class="col col_2">
			<h1>Мою маму зовут</h1>
			<input type='text' class='blue border_input' placeholder='укажите имя' style='width: 320px;' data-constructor_var="19" value="<?php if (isset($_smarty_tpl->tpl_vars['constructor']->value[19])) {
echo $_smarty_tpl->tpl_vars['constructor']->value[19];
}?>">
			<h1>Она родилась</h1>
			<div class='calendar'><input type='text' class='blue border_input date' placeholder='укажите дату' style='width: 175px;' data-constructor_var="20" value="<?php if (isset($_smarty_tpl->tpl_vars['constructor']->value[20])) {
echo $_smarty_tpl->tpl_vars['constructor']->value[20];
}?>"></div>
			<h1>Ее профессия</h1>
			<input type='text' class='blue border_input' placeholder='укажите профессию' style='width: 320px;' data-constructor_var="21" value="<?php if (isset($_smarty_tpl->tpl_vars['constructor']->value[21])) {
echo $_smarty_tpl->tpl_vars['constructor']->value[21];
}?>">
			<h1>Она любит заниматься</h1>
			<input type='text' class='blue border_input' placeholder='чем любит заниматься' style='width: 320px;' data-constructor_var="22" value="<?php if (isset($_smarty_tpl->tpl_vars['constructor']->value[22])) {
echo $_smarty_tpl->tpl_vars['constructor']->value[22];
}?>">
		</section>
		<section class="col col_3">
			<h1>Моего папу зовут</h1>
			<input type='text' class='blue border_input' placeholder='укажите имя' style='width: 320px;' data-constructor_var="23" value="<?php if (isset($_smarty_tpl->tpl_vars['constructor']->value[23])) {
echo $_smarty_tpl->tpl_vars['constructor']->value[23];
}?>">
			<h1>Он родился</h1>
			<div class='calendar'><input type='text' class='blue border_input date' placeholder='укажите дату' style='width: 175px;' data-constructor_var="24" value="<?php if (isset($_smarty_tpl->tpl_vars['constructor']->value[24])) {
echo $_smarty_tpl->tpl_vars['constructor']->value[24];
}?>"></div>
			<h1>Его профессия</h1>
			<input type='text' class='blue border_input' placeholder='укажите профессию' style='width: 320px;' data-constructor_var="25" value="<?php if (isset($_smarty_tpl->tpl_vars['constructor']->value[25])) {
echo $_smarty_tpl->tpl_vars['constructor']->value[25];
}?>">
			<h1>Он любит заниматься</h1>
			<input type='text' class='blue border_input' placeholder='чем любит заниматься' style='width: 320px;' data-constructor_var="26" value="<?php if (isset($_smarty_tpl->tpl_vars['constructor']->value[26])) {
echo $_smarty_tpl->tpl_vars['constructor']->value[26];
}?>">
		</section>
		<section class="col col_4">
			<div class='photo white regular' data-editor=true data-constructor_var="27">
				<div class='inner'>
					<p>
						<span>+</span><br>
						Добавить фото
					</p>
					<input type='text' class='blue comment border_input' placeholder='Добавьте комментарий' data-constructor_var="28" value="<?php if (isset($_smarty_tpl->tpl_vars['constructor']->value[28])) {
echo $_smarty_tpl->tpl_vars['constructor']->value[28];
}?>">
				</div>
				<?php if (isset($_smarty_tpl->tpl_vars['constructor']->value[27])) {
echo $_smarty_tpl->tpl_vars['constructor']->value[27];
}?>
				<input type="file" name="photo" class="load_image hidden">
			</div>
		</section>
	</section>
</section>

<?php echo '<script'; ?>
 src="/js/routes/tree.js"><?php echo '</script'; ?>
>

<!-- End of Card 3 --><?php }
}
?>