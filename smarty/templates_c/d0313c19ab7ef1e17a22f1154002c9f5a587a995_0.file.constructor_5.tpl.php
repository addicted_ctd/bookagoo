<?php /* Smarty version 3.1.27, created on 2015-11-09 17:55:16
         compiled from "/var/www/quantum/webpoetry.org/bookagoo/docs/app/smarty/templates/constructor_5.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:192447014156412454101b77_88495606%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd0313c19ab7ef1e17a22f1154002c9f5a587a995' => 
    array (
      0 => '/var/www/quantum/webpoetry.org/bookagoo/docs/app/smarty/templates/constructor_5.tpl',
      1 => 1447084492,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '192447014156412454101b77_88495606',
  'variables' => 
  array (
    'constructor' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_56412454133299_46743808',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_56412454133299_46743808')) {
function content_56412454133299_46743808 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '192447014156412454101b77_88495606';
?>
<!-- Card 5 -->
<section class="card card_5 front" data-side='front'>
	<h1 class="card_title">
		Я расту
	</h1>
	<section class="card_content">
		<div class='teeth_wrapper' data-constructor_var="0">
			<?php if (isset($_smarty_tpl->tpl_vars['constructor']->value[0])) {?>
				<?php echo $_smarty_tpl->tpl_vars['constructor']->value[0];?>

			<?php } else { ?>
				<div class='tooth tooth_1'><p></p><a class='del'></a></div>
				<div class='tooth tooth_1'><p></p><a class='del'></a></div>
				<div class='tooth tooth_1'><p></p><a class='del'></a></div>
				<div class='tooth tooth_1'><p></p><a class='del'></a></div>
				<div class='tooth tooth_1'><p></p><a class='del'></a></div>
				<div class='tooth tooth_1'><p></p><a class='del'></a></div>
				<div class='tooth tooth_1'><p></p><a class='del'></a></div>
				<div class='tooth tooth_1'><p></p><a class='del'></a></div>
				<div class='tooth tooth_1'><p></p><a class='del'></a></div>
				<div class='tooth tooth_1'><p></p><a class='del'></a></div>
				<div class='tooth tooth_1'><p></p><a class='del'></a></div>
				<div class='tooth tooth_1'><p></p><a class='del'></a></div>
				<div class='tooth tooth_1'><p></p><a class='del'></a></div>
				<div class='tooth tooth_1'><p></p><a class='del'></a></div>
				<div class='tooth tooth_1'><p></p><a class='del'></a></div>
				<div class='tooth tooth_1'><p></p><a class='del'></a></div>
				<div class='tooth tooth_1'><p></p><a class='del'></a></div>
				<div class='tooth tooth_1'><p></p><a class='del'></a></div>
				<div class='tooth tooth_1'><p></p><a class='del'></a></div>
				<div class='tooth tooth_1'><p></p><a class='del'></a></div>
			<?php }?>
		</div>
		<div class='form'>
			<div class='clearfix'>
				<p>Возраст</p>
				<input type='text' placeholder='в месяцах' class='tooth_month_num'>
			</div>
			<div class='clearfix'>
				<p>Зуб</p>
				<aside>
					<div class='ul_wrapper'>
						<ul class='tooth_num'>
							<li>1</li>
							<li>2</li>
							<li>3</li>
							<li>4</li>
							<li>5</li>
							<li>6</li>
							<li>7</li>
							<li>8</li>
							<li>9</li>
							<li>10</li>
							<li>11</li>
							<li>12</li>
							<li>13</li>
							<li>14</li>
							<li>15</li>
							<li>16</li>
							<li>17</li>
							<li>18</li>
							<li>19</li>
							<li>20</li>
						</ul>
					</div>
				</aside>
			</div>
			<div class='clearfix'>
				<button class='add_tooth'>Добавить зуб</button>
			</div>
		</div>
	</section>
</section>

<section class="card card_5 back" data-side='back'>
	<h1 class="card_title">
		Я расту
	</h1>
	<section class="card_content">
		<div class='ruler'>
			<div class='y'>
				<p class='title b'>рост</p>
				<ul>
					<li>100 см</li>
					<li></li>
					<li>90 см</li>
					<li></li>
					<li>80 см</li>
					<li></li>
					<li>70 см</li>
					<li></li>
					<li>60 см</li>
					<li></li>
					<li>50 см</li>
				</ul>
			</div>
			<div class='x'>
				<p class='title b'>возраст</p>
				<ul class='clearfix'>
					<li><span>2 месяца</span></li>
					<li></li>
					<li><span>6 месяцев</span></li>
					<li></li>
					<li><span>10 месяцев</span></li>
					<li></li>
					<li><span>14 месяцев</span></li>
					<li></li>
					<li><span>18 месяцев</span></li>
					<li></li>
					<li></li>
					<li><span>24 месяца</span></li>
				</ul>
			</div>
		</div>
		<div class='chart_body' data-constructor_var="1">
			<?php if (isset($_smarty_tpl->tpl_vars['constructor']->value[1])) {?>
				<?php echo $_smarty_tpl->tpl_vars['constructor']->value[1];?>

			<?php } else { ?>
				<div class='chart_inner'></div>
				<svg id="mySVG" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"></svg>
			<?php }?>
		</div>
		<div class='form clearfix'>
			<div class='clearfix'>
				<p>Возраст</p>
				<input type='text' placeholder='в месяцах' class='age'>
			</div>
			<div class='clearfix'>
				<p>Рост</p>
				<input type='text' placeholder='в см' class='growth'>
			</div>
			<div class='clearfix'>
				<p>Вес</p>
				<input type='text' placeholder='в кг' class='weight'>
			</div>
			<div class='clearfix'>
				<button class='add_point'>Добавить точку</button>
			</div>
		</div>
	</section>
</section>
<?php echo '<script'; ?>
 src="/js/routes/growing_up.js"><?php echo '</script'; ?>
>
<!-- End of Card 5 --><?php }
}
?>