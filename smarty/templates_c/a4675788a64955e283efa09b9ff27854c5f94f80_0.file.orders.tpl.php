<?php /* Smarty version 3.1.27, created on 2015-11-10 15:31:23
         compiled from "/var/www/quantum/webpoetry.org/bookagoo/docs/app/smarty/templates/orders.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:13226522185642541ba6ba79_90184944%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a4675788a64955e283efa09b9ff27854c5f94f80' => 
    array (
      0 => '/var/www/quantum/webpoetry.org/bookagoo/docs/app/smarty/templates/orders.tpl',
      1 => 1446728962,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '13226522185642541ba6ba79_90184944',
  'variables' => 
  array (
    'orders' => 0,
    'order' => 0,
    'cards' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_5642541bb05de8_37693953',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_5642541bb05de8_37693953')) {
function content_5642541bb05de8_37693953 ($_smarty_tpl) {
if (!is_callable('smarty_modifier_date_format')) require_once '/usr/local/lib/php/Smarty/plugins/modifier.date_format.php';

$_smarty_tpl->properties['nocache_hash'] = '13226522185642541ba6ba79_90184944';
echo $_smarty_tpl->getSubTemplate ('header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>'Мои заказы'), 0);
?>

<section id="orders" class='page'>
	<section class="page_title">
		<h1 class="content">Мои заказы</h1>
	</section>
	<section class="one">
		<section class="content clearfix">
			<section class="left">
				<?php if (count($_smarty_tpl->tpl_vars['orders']->value) != 0) {?>
					<?php
$_from = $_smarty_tpl->tpl_vars['orders']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['order'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['order']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['order']->value) {
$_smarty_tpl->tpl_vars['order']->_loop = true;
$foreach_order_Sav = $_smarty_tpl->tpl_vars['order'];
?>
					<section class='order wrapper'>
						<div class='top clearfix'>
							<hgroup>
								<h2 class="b g">Заказ № <?php echo $_smarty_tpl->tpl_vars['order']->value['id'];?>
 от <?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['order']->value['date'],"%d/%m/%Y");?>
</h2>
								<h3><span class="b g">Адрес доставки:</span> <?php echo $_smarty_tpl->tpl_vars['order']->value['delivery_address'];?>
</h3>
							</hgroup>
							<h4 status="1" class="b"><?php if ($_smarty_tpl->tpl_vars['order']->value['status_id'] != 3) {
echo $_smarty_tpl->tpl_vars['order']->value['status'];
} else { ?>Заказ оплачен<?php }?></h4>
						</div>
						<div class='middle'>
							<h2 class="b g">Состав заказа:</h2>
							<div class='order_content clearfix'>
								<?php if (preg_match_all('/[^\s]/u',$_smarty_tpl->tpl_vars['order']->value['purchase_ids'], $tmp) > 2) {?>
								<section class='clearfix'>
									<figure style='background-image: url(img/profile/book_preview.png);'></figure>
									<p class='b' data-cards_ordered="<?php echo $_smarty_tpl->tpl_vars['order']->value['purchase_ids'];?>
">Книга (<?php echo $_smarty_tpl->tpl_vars['order']->value['cards_quantity'];?>
 карточек)</p>
									<p class='b'><?php echo $_smarty_tpl->tpl_vars['order']->value['cards_quantity'];?>
 шт</p>
									<p class='b'><?php echo $_smarty_tpl->tpl_vars['order']->value['cards_total'];?>
 рублей</p>
								</section>
								<?php }?>
								<?php $_smarty_tpl->tpl_vars['cards'] = new Smarty_Variable(explode(",",$_smarty_tpl->tpl_vars['order']->value['purchase_ids']), null, 0);?>
								<?php if (in_array('0',$_smarty_tpl->tpl_vars['cards']->value)) {?>
								<section class='clearfix'>
									<figure style='background-image: url(img/profile/book_preview.png);'></figure>
									<p class='b'>Коробка</p>
									<p class='b'><?php echo $_smarty_tpl->tpl_vars['order']->value['box_quantity'];?>
 шт</p>
									<p class='b'><?php echo $_smarty_tpl->tpl_vars['order']->value['box_total'];?>
 рублей</p>
								</section>
								<?php }?>
							</div>
						</div>
						<div class='bottom clearfix'>
							<p class='b'>Итого : <?php echo $_smarty_tpl->tpl_vars['order']->value['total'];?>
 рублей</p>
							<?php if ($_smarty_tpl->tpl_vars['order']->value['status_id'] == 1) {?>
							<a class='green_button_arrow' href='payment/pay.php?order=<?php echo $_smarty_tpl->tpl_vars['order']->value['id'];?>
'>Оплатить заказ</a>
							<?php }?>
						</div>
					</section>
					<?php
$_smarty_tpl->tpl_vars['order'] = $foreach_order_Sav;
}
?>
				<?php } else { ?>
					<section class='wrapper'>
						<h2 class='notif'>У вас нет активных заказов. Пожалуйста, <a href='cart.php'>создайте заказ</a></h2>
					</section>
				<?php }?>
			</section>
			<?php echo $_smarty_tpl->getSubTemplate ('profile_menu.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

		</section>
	</section>
</section>
<?php echo '<script'; ?>
 src="js/routes/order.js"><?php echo '</script'; ?>
>
<?php echo $_smarty_tpl->getSubTemplate ('footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);

}
}
?>