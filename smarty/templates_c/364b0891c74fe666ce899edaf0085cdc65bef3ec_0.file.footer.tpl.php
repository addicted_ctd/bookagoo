<?php /* Smarty version 3.1.27, created on 2015-11-07 19:11:55
         compiled from "/var/www/quantum/webpoetry.org/bookagoo/docs/app/smarty/templates/footer.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:1599172217563e934b43d631_88269692%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '364b0891c74fe666ce899edaf0085cdc65bef3ec' => 
    array (
      0 => '/var/www/quantum/webpoetry.org/bookagoo/docs/app/smarty/templates/footer.tpl',
      1 => 1446916330,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1599172217563e934b43d631_88269692',
  'variables' => 
  array (
    'general' => 0,
    'first_login' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_563e934b464435_53162434',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_563e934b464435_53162434')) {
function content_563e934b464435_53162434 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '1599172217563e934b43d631_88269692';
?>
		<footer>
			<section class="content clearfix">
				<div>
					<section class="links clearfix">
						<a class="under_hover2" href="news.php">Новости</a>
						<a class="under_hover2" href="about.php">О проекте</a>
						<a class="under_hover2" href="contacts.php">Контакты</a>
					</section>
					<p>© 2015  OOO «Букагу»</p>
				</div>
				<div>
					<section class="social clearfix">
						<a href="<?php echo $_smarty_tpl->tpl_vars['general']->value['fb_link'];?>
" class="fb" target="_blank"></a>
						<a href="<?php echo $_smarty_tpl->tpl_vars['general']->value['vk_link'];?>
" class="vk" target="_blank"></a>
					</section>
					<aside class="clearfix">
						<a href="">Доставка и оплата</a>
						<a href="agreement.php">Пользовательское соглашение</a>
					</aside>
				</div>
			</section>
		</footer>
		<section id="alert_overlay">
			<section class='ov_content'>
				<div class='alert acc_del'>
					<p>
						Вы уверены, что хотите<br>
						удалить аккаунт Bookagoo?
					</p>
					<section class='clearfix'>
						<a onclick='Overlay.hide();'>Нет, спасибо</a>
						<a onclick='Profile.process_delete();'>Да, уверен</a>
					</section>
					<a class='close'></a>
				</div>
				<div class='alert first_login'>
					<p>
						Вы впервые зашли к нам!<br>
						Первым делом заполните профиль<br>
						Книжечка автоматически добавит из него<br>
						информацию о вашем ребенке 
					</p>
					<section class='clearfix'>
						<a href='profile.php'>Перейти в профиль</a>
					</section>
					<a class='close'></a>
				</div>
			</section>
		</section>
		<?php if (isset($_smarty_tpl->tpl_vars['first_login']->value)) {?>
		<?php echo '<script'; ?>
>Overlay.show('.first_login');<?php echo '</script'; ?>
>
		<?php }?>
		<?php echo '<script'; ?>
 src="js/main.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="js/vendor/css3finalize/jquery.css3finalize.min.js" type="text/javascript"><?php echo '</script'; ?>
>
        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <?php echo '<script'; ?>
>
            var _gaq = [['_setAccount', 'UA-XXXXX-X'], ['_trackPageview']];
            (function(d, t) {
                var g = d.createElement(t), s = d.getElementsByTagName(t)[0];
                g.src = '//www.google-analytics.com/ga.js';
                s.parentNode.insertBefore(g, s)
            }(document, 'script'));
        <?php echo '</script'; ?>
>
    </body>
</html><?php }
}
?>