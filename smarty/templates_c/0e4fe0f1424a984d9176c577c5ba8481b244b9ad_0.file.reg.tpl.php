<?php /* Smarty version 3.1.27, created on 2015-10-07 09:58:59
         compiled from "/home/quantum/webpoetry.org/bookagoo/smarty/templates/reg.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:17452293225615252364aff1_13158689%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0e4fe0f1424a984d9176c577c5ba8481b244b9ad' => 
    array (
      0 => '/home/quantum/webpoetry.org/bookagoo/smarty/templates/reg.tpl',
      1 => 1444226336,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '17452293225615252364aff1_13158689',
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_56152523663b34_84841418',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_56152523663b34_84841418')) {
function content_56152523663b34_84841418 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '17452293225615252364aff1_13158689';
echo $_smarty_tpl->getSubTemplate ('header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>'Регистрация'), 0);
?>

<section id="reg" class='page'>
	<section class="page_title">
		<h1 class="content">Регистрация</h1>
	</section>
	<section class="one">
		<section class="wrapper content">
			<h1 class="g b">Вход через социальные сети</h1>
			<div class="buttons">
				<a class="fb" onclick="Global.fb_login();">Войти при помощи facebook</a>
				<a class="vk" onclick="VK.Auth.login(Global.vk_login);">Войти при помощи vkontakte</a>
			</div>
		</section>
	</section>
	<section class="two">
		<section class="wrapper content clearfix">
			<section class="clearfix">
				<div>
					<h1 class="g b">Укажите данные ниже</h1>
					<input type="text" placeholder="E-mail" name='email'>
					<input type="password" placeholder="Пароль" name='pwd'>
					<aside class="clearfix">
						<input type="checkbox" id='show_pwd'>
						<label class='g' for='show_pwd'>Показать пароль</label>
					</aside>
				</div>
				<div>
					<h1 class="g b">Укажите данные вашего ребенка</h1>
					<section class="clearfix">
						<section class="col">
							<input type="text" placeholder="Дата рождения" name='kid_birth'>
							<input type="text" placeholder="Имя" name='kid_name'>
						</section>
						<section class="col clearfix">
							<p>Пол</p>
							<a class='gender boy' data-gender='male'></a>
							<a class='gender girl' data-gender='female'></a>
						</section>
					</section>
				</div>
			</section>
			<a class='create_account'>Создать аккаунт</a>
			<section class="feed_wrapper">
				<section class="inner clearfix">
					<input type="checkbox" id='subscription' checked>
					<label class='g' for='subscription'>Подписаться на рассылку</label>
				</section>
			</section>
			<h3 class="g">Нажимая на кнопку «Создать аккаунт», вы подтверждаете свое согласие с условиями пользовательского соглашения</h3>
		</section>
	</section>
</section>

<?php echo '<script'; ?>
 src="js/routes/reg.js"><?php echo '</script'; ?>
>
<?php echo $_smarty_tpl->getSubTemplate ('footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);

}
}
?>