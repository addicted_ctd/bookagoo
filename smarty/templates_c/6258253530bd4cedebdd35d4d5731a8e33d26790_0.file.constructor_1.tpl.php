<?php /* Smarty version 3.1.27, created on 2015-10-09 06:11:34
         compiled from "/home/quantum/webpoetry.org/bookagoo/smarty/templates/constructor_1.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:1172391537561792d620b215_92954668%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6258253530bd4cedebdd35d4d5731a8e33d26790' => 
    array (
      0 => '/home/quantum/webpoetry.org/bookagoo/smarty/templates/constructor_1.tpl',
      1 => 1444385490,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1172391537561792d620b215_92954668',
  'variables' => 
  array (
    'child_info' => 0,
    'word' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_561792d6233a00_87463347',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_561792d6233a00_87463347')) {
function content_561792d6233a00_87463347 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '1172391537561792d620b215_92954668';
?>
<!-- Card 1 -->
<section class="card card_1">
	<h1 class="card_title">
		Обложка
	</h1>
	<section class="card_content">
		<div class='book'>
			<div class='book_inner'>
				<div class='photo' data-editor=true>
					<div class='inner'>
						<p>
							<span>+</span><br>
							Добавить фото
						</p>
					</div>
					<figure></figure>
					<input type="file" name="photo" class="load_image hidden">
				</div>
				<article>
					<input type='text' value='Меня зовут <?php echo $_smarty_tpl->tpl_vars['child_info']->value['f_name'];?>
' disabled='true'>
					<?php if ($_smarty_tpl->tpl_vars['child_info']->value['gender'] == 'female') {?>
						<?php $_smarty_tpl->tpl_vars['word'] = new Smarty_Variable('родилась', null, 0);?>
					<?php } else { ?>
						<?php $_smarty_tpl->tpl_vars['word'] = new Smarty_Variable('родился', null, 0);?>
					<?php }?>
					<input type='text' value='Я <?php echo $_smarty_tpl->tpl_vars['word']->value;?>
 <?php echo $_smarty_tpl->tpl_vars['child_info']->value['birth_full'];?>
' disabled="true">
				</article>
			</div>
		</div>
	</section>
</section>

<!-- End of Card 1 --><?php }
}
?>