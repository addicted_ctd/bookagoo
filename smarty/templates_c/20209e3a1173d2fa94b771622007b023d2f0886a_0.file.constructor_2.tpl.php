<?php /* Smarty version 3.1.27, created on 2015-11-02 17:43:09
         compiled from "/var/www/quantum/webpoetry.org/bookagoo/docs/app/smarty/templates/constructor_2.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:19205541565637e6fdd59f25_61488250%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '20209e3a1173d2fa94b771622007b023d2f0886a' => 
    array (
      0 => '/var/www/quantum/webpoetry.org/bookagoo/docs/app/smarty/templates/constructor_2.tpl',
      1 => 1446479004,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '19205541565637e6fdd59f25_61488250',
  'variables' => 
  array (
    'constructor' => 0,
    'child_info' => 0,
    'word' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_5637e6fddf5a57_13865700',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_5637e6fddf5a57_13865700')) {
function content_5637e6fddf5a57_13865700 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '19205541565637e6fdd59f25_61488250';
?>
<!-- Card 2 -->
<section class="card card_2 front" data-side='front'>
	<h1 class="card_title">
		Привет, а вот и я
	</h1>
	<section class="card_content">
		<section class="clearfix">
			<section class="col">
				<div class='photo white regular' data-editor='true' data-constructor_var="0">
					<div class='inner'>
						<p>
							<span>+</span><br>
							Добавить фото
						</p>
						<input type='text' class='violet comment border_input' placeholder='Добавьте комментарий' data-constructor_var="1" value="<?php if (isset($_smarty_tpl->tpl_vars['constructor']->value[1])) {
echo $_smarty_tpl->tpl_vars['constructor']->value[1];
}?>">
						<h3 class='title'>В ожидании меня</h3>
					</div>
					<?php if (isset($_smarty_tpl->tpl_vars['constructor']->value[0])) {
echo $_smarty_tpl->tpl_vars['constructor']->value[0];
}?>
					<input type="file" name="photo" class="load_image hidden">
				</div>
			</section>
			<section class="col col_2">
				<h1>Маме сказали, что я появлюсь</h1>
				<div class='calendar'>
					<input type='text' class='violet border_input date' placeholder='укажите дату' style='width: 120px;' data-constructor_var="2" value="<?php if (isset($_smarty_tpl->tpl_vars['constructor']->value[2])) {
echo $_smarty_tpl->tpl_vars['constructor']->value[2];
}?>">
				</div>
				<h1>Меня хотели назвать</h1>
				<input type='text' class='violet border_input' placeholder='укажите имя' style='width: 195px;' data-constructor_var="3" value="<?php if (isset($_smarty_tpl->tpl_vars['constructor']->value[3])) {
echo $_smarty_tpl->tpl_vars['constructor']->value[3];
}?>">
				<h1>Мое имя означает</h1>
				<input type='text' class='violet border_input' placeholder='что означает имя' style='width: 355px;' data-constructor_var="4" value="<?php if (isset($_smarty_tpl->tpl_vars['constructor']->value[4])) {
echo $_smarty_tpl->tpl_vars['constructor']->value[4];
}?>">
			</section>
		</section>
		<section class="clearfix">
			<section class="col col_3">
				<div class="clearfix">
					<?php if ($_smarty_tpl->tpl_vars['child_info']->value['gender'] == 'female') {?>
						<?php $_smarty_tpl->tpl_vars['word'] = new Smarty_Variable('родилась', null, 0);?>
					<?php } else { ?>
						<?php $_smarty_tpl->tpl_vars['word'] = new Smarty_Variable('родился', null, 0);?>
					<?php }?>
					<h1>Я <?php echo $_smarty_tpl->tpl_vars['word']->value;?>
 01.06.1999</h1>
				</div>
				<div class="clearfix">
					<h1>Мой вес</h1>
					<input type='text' class='violet border_input' placeholder='укажите вес' data-constructor_var="5" value="<?php if (isset($_smarty_tpl->tpl_vars['constructor']->value[5])) {
echo $_smarty_tpl->tpl_vars['constructor']->value[5];
}?>">
				</div>
				<div class="clearfix">
					<h1>Мой рост</h1>
					<input type='text' class='violet border_input' placeholder='укажите рост' data-constructor_var="6" value="<?php if (isset($_smarty_tpl->tpl_vars['constructor']->value[6])) {
echo $_smarty_tpl->tpl_vars['constructor']->value[6];
}?>">
				</div>
				<div class="clearfix">
					<h1>Цвет глаз</h1>
					<ul data-constructor_var="7" data-selected="<?php if (isset($_smarty_tpl->tpl_vars['constructor']->value[7])) {
echo $_smarty_tpl->tpl_vars['constructor']->value[7];
}?>" class='dropdown'>
						<li class='default'>выберите цвет</li>
						<li>Синий</li>
						<li>Голубой</li>
						<li>Серый</li>
						<li>Зелёный</li>
						<li>Янтарный</li>
						<li>Болотный</li>
						<li>Карий</li>
						<li>Чёрный</li>
						<li>Жёлтый</li>
					</ul>
				</div>
				<div class="clearfix">
					<h1>Цвет волос</h1>
					<ul data-constructor_var="8" data-selected="<?php if (isset($_smarty_tpl->tpl_vars['constructor']->value[8])) {
echo $_smarty_tpl->tpl_vars['constructor']->value[8];
}?>" class='dropdown'>
						<li class='default'>выберите цвет</li>
						<li>Брюнет</li>
						<li>Рыжий</li>
						<li>Русый</li>
						<li>Блондин</li>
						<li>Шатен</li>
					</ul>
				</div>
				<div class="clearfix">
					<h1>Мой знак зодиака</h1>
					<ul data-constructor_var="9" data-selected="<?php if (isset($_smarty_tpl->tpl_vars['constructor']->value[9])) {
echo $_smarty_tpl->tpl_vars['constructor']->value[9];
}?>" class='dropdown'>
						<li class='default'>выберите знак зодиака</li>
						<li>Овен</li>
						<li>Телец</li>
						<li>Близнецы</li>
						<li>Рак</li>
						<li>Лев</li>
						<li>Дева</li>
						<li>Весы</li>
						<li>Скорпион</li>
						<li>Стрелец</li>
						<li>Козерог</li>
						<li>Водолей</li>
						<li>Рыбы</li>
					</ul>
				</div>
			</section>
			<section class="col" style="padding-top: 24px;">
				<div class='photo white regular' data-editor=true data-constructor_var="10">
					<div class='inner'>
						<p>
							<span>+</span><br>
							Добавить фото
						</p>
						<input type='text' class='violet comment border_input' placeholder='Добавьте комментарий' data-constructor_var="11" value="<?php if (isset($_smarty_tpl->tpl_vars['constructor']->value[11])) {
echo $_smarty_tpl->tpl_vars['constructor']->value[11];
}?>">
						<h3 class='title'>А вот и я</h3>
					</div>
					<?php if (isset($_smarty_tpl->tpl_vars['constructor']->value[10])) {
echo $_smarty_tpl->tpl_vars['constructor']->value[10];
}?>
					<input type="file" name="photo" class="load_image hidden">
				</div>
			</section>
		</section>
	</section>
</section>

<section class="card card_2 back" data-side='back'>
	<h1 class="card_title">
		Привет, а вот и я
	</h1>
	<section class="card_content">
		<section class="clearfix">
			<section class="col">
				<div class='photo white regular' data-editor=true data-constructor_var="12">
					<div class='inner'>
						<p>
							<span>+</span><br>
							Добавить фото
						</p>
						<input type='text' class='violet comment border_input' placeholder='Добавьте комментарий' data-constructor_var="13" value="<?php if (isset($_smarty_tpl->tpl_vars['constructor']->value[13])) {
echo $_smarty_tpl->tpl_vars['constructor']->value[13];
}?>">
					</div>
					<?php if (isset($_smarty_tpl->tpl_vars['constructor']->value[12])) {
echo $_smarty_tpl->tpl_vars['constructor']->value[12];
}?>
					<input type="file" name="photo" class="load_image hidden">
				</div>
			</section>
			<section class="col col_2">
				<div class='photo white regular' data-editor=true data-constructor_var="14">
					<div class='inner'>
						<p>
							<span>+</span><br>
							Добавить фото
						</p>
						<input type='text' class='violet comment border_input' placeholder='Добавьте комментарий' data-constructor_var="15" value="<?php if (isset($_smarty_tpl->tpl_vars['constructor']->value[15])) {
echo $_smarty_tpl->tpl_vars['constructor']->value[15];
}?>">
						<h3 class='title'>В ожидании меня</h3>
					</div>
					<?php if (isset($_smarty_tpl->tpl_vars['constructor']->value[14])) {
echo $_smarty_tpl->tpl_vars['constructor']->value[14];
}?>
					<input type="file" name="photo" class="load_image hidden">
				</div>
			</section>
		</section>
		<section class="clearfix">
			<section class="col col_3">
				<div class='photo white big' data-editor=true data-constructor_var="16">
					<div class='inner'>
						<p>
							<span>+</span><br>
							Добавить фото
						</p>
						<input type='text' class='violet comment border_input' placeholder='Добавьте комментарий' data-constructor_var="17" value="<?php if (isset($_smarty_tpl->tpl_vars['constructor']->value[17])) {
echo $_smarty_tpl->tpl_vars['constructor']->value[17];
}?>">
					</div>
					<?php if (isset($_smarty_tpl->tpl_vars['constructor']->value[16])) {
echo $_smarty_tpl->tpl_vars['constructor']->value[16];
}?>
					<input type="file" name="photo" class="load_image hidden">
				</div>
			</section>
		</section>
	</section>
</section>

<!-- End of Card 2 --><?php }
}
?>