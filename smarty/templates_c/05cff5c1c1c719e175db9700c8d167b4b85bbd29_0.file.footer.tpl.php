<?php /* Smarty version 3.1.27, created on 2015-10-02 07:37:29
         compiled from "/home/quantum/webpoetry.org/bookagoo/smarty/templates/footer.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:1770963409560e6c790f4354_83934873%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '05cff5c1c1c719e175db9700c8d167b4b85bbd29' => 
    array (
      0 => '/home/quantum/webpoetry.org/bookagoo/smarty/templates/footer.tpl',
      1 => 1443785846,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1770963409560e6c790f4354_83934873',
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_560e6c79116747_00134778',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_560e6c79116747_00134778')) {
function content_560e6c79116747_00134778 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '1770963409560e6c790f4354_83934873';
?>
		<footer>
			<section class="content clearfix">
				<div>
					<section class="links clearfix">
						<a href="news.php">Новости</a>
						<a href="about.php">О проекте</a>
						<a href="contacts.php">Контакты</a>
					</section>
					<p>© 2015  OOO «Букагу»</p>
				</div>
				<div>
					<section class="social clearfix">
						<a href="" class="fb"></a>
						<a href="" class="tw"></a>
						<a href="" class="vk"></a>
						<a href="" class="yt"></a>
					</section>
					<aside class="clearfix">
						<a href="">Доставка и оплата</a>
						<a href="agreement.php">Пользовательское соглашение</a>
					</aside>
				</div>
			</section>
		</footer>
		<?php echo '<script'; ?>
 src="js/main.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="js/vendor/css3finalize/jquery.css3finalize.min.js" type="text/javascript"><?php echo '</script'; ?>
>
        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <?php echo '<script'; ?>
>
            var _gaq = [['_setAccount', 'UA-XXXXX-X'], ['_trackPageview']];
            (function(d, t) {
                var g = d.createElement(t), s = d.getElementsByTagName(t)[0];
                g.src = '//www.google-analytics.com/ga.js';
                s.parentNode.insertBefore(g, s)
            }(document, 'script'));
        <?php echo '</script'; ?>
>
    </body>
</html><?php }
}
?>