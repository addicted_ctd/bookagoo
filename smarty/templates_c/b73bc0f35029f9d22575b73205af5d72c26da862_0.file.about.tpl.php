<?php /* Smarty version 3.1.27, created on 2015-10-19 13:27:37
         compiled from "/var/www/quantum/webpoetry.org/bookagoo/docs/app/smarty/templates/about.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:101713812656252809e13f13_68642573%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b73bc0f35029f9d22575b73205af5d72c26da862' => 
    array (
      0 => '/var/www/quantum/webpoetry.org/bookagoo/docs/app/smarty/templates/about.tpl',
      1 => 1443788499,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '101713812656252809e13f13_68642573',
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_56252809e3f5e3_67839660',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_56252809e3f5e3_67839660')) {
function content_56252809e3f5e3_67839660 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '101713812656252809e13f13_68642573';
echo $_smarty_tpl->getSubTemplate ('header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>'Мои заказы'), 0);
?>

<section id="about" class='page'>
	<section class="one">
		<section class="content clearfix">
			<article>
				<h1 class="b">Кто мы?</h1>
				<p>
					Компания основана в 2013 году в Москве.<br>
					BookAgoo - это сплоченная команда единомышленников,<br>
					друзей, а также родителей. Собственный опыт, знания и<br>
					одержимость современными цифровыми технологиями<br>
					помогают нам двигаться и совершенствоваться каждый<br>
					день. Мы работаем, чтобы сделать жизнь вашей семьи<br>
					ярче и интереснее.
				</p>
			</article>
		</section>
	</section>
	<section class='two'>
		<section class="content clearfix">
			<section class='left'>
				<figure></figure>
				<figure></figure>
			</section>
			<section class='right'>
				<article>
					<h1 class="g b">Наш продукт</h1>
					<p>
						Книга первых лет жизни малыша в коробке – оригинальное, простое и красивое 
						решение для любого родителя, желающего сохранить незабываемые моменты жизни всей семьи.<br>
						Мы используем только высококачественные материалы, работаем с проверенными
						поставщиками и отвечаем за финальный
						результат.
					</p>
				</article>
			</section>
		</section>
	</section>
</section>
<?php echo $_smarty_tpl->getSubTemplate ('footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);

}
}
?>