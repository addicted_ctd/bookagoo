<?php /* Smarty version 3.1.27, created on 2015-10-21 12:22:41
         compiled from "/var/www/quantum/webpoetry.org/bookagoo/docs/app/smarty/templates/index.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:11683289685627bbd1241b98_55658902%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '8c8d2254f908952d44677aa0ffc72ccb9970fa18' => 
    array (
      0 => '/var/www/quantum/webpoetry.org/bookagoo/docs/app/smarty/templates/index.tpl',
      1 => 1445444559,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '11683289685627bbd1241b98_55658902',
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_5627bbd1278b52_72723739',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_5627bbd1278b52_72723739')) {
function content_5627bbd1278b52_72723739 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '11683289685627bbd1241b98_55658902';
echo $_smarty_tpl->getSubTemplate ('header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>'Главная страница'), 0);
?>

<section id="index">
	<?php echo '<script'; ?>
>
	$("header").hide();
	<?php echo '</script'; ?>
>
	<section class="header">
		<a id="logo"></a>
		<a id="login" href='auth.php'>Войти</a>
		<section class="content">
			<h1>
				<span>Книга “Первых лет жизни малыша”</span> 
				<span>в коробочке - это просто и красиво!</span>
			</h1>
			<div id="video">
				<a id="play_video"></a>
			</div>
			<a href="/constructor.php?card=1" id="create_book">Создать книгу</a>
			<a id="follow"></a>
		</section>
		<section class="content arrows">
			<section class="wrap">
				<figure>
					<p>Загрузи фото<br>
						в шаблон книги </p>
				</figure>
				<figure>
					<p>Закажи<br>
						печать книги </p>
				</figure>
				<figure>
					<p>Запиши только<br>
						самое важное</p>
				</figure>
				<figure>
					<p>Пополняй книгу
						новыми страницами</p>
				</figure>
			</section>
		</section>
	</section>
	<section class="one">
		<section class="content clearfix">
			<article>
				<h1>
					Несколько минут,<br>
					и книга готова!
				</h1>
				<p data-num="1.">Загрузи фото в шаблон книги<br>
				из vkontakte, facebook,<br>
				Instagram и локального диска</p>
				<p data-num="2.">Дополни фото своими комментариями.<br>
				Внеси данные в графики роста и веса,<br>
				первых зубов</p>
				<p data-num="3.">Закажи печать, и мы изготовим<br>
				книгу за 3 дня!</p>
			</article>
			<aside>
				<figure></figure>
				<figure></figure>
			</aside>
		</section>
	</section>
	<section class="two">
		<section class="content">
			<article>
				<h1>Книга растет вместе с ребенком<br>
					и радует каждый день!</h1>
				<ul>
					<li>Пополняй существующую книгу новыми страницами</li>
					<li>Поставь открытую коробочку на полку</li>
					<li>Наслаждайся воспоминаниями в любой момент!</li>
				</ul>
			</article>
		</section>
	</section>
	<section class="three">
		<section class="content">
			<h1>Стоимость</h1>
			<article class="clearfix">
				<h3>Стоимость книги</h3>
				<div>
					<p>
						Минимальный заказ<br>
						включает 10 страниц и коробку
					</p>
					<p class="rub">1600 ₽</p>
				</div>
				<div>
					<p>
						Стоимость<br>
						дополнительной карточки
					</p>
					<p class="rub">50 ₽</p>
				</div>
			</article>
			<a href="/constructor.php?card=1" id="create_book_2">Создать книгу</a>
		</section>
	</section>
</section>
<?php echo '<script'; ?>
 src="js/routes/landing.js"><?php echo '</script'; ?>
>
<?php echo $_smarty_tpl->getSubTemplate ('footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);

}
}
?>