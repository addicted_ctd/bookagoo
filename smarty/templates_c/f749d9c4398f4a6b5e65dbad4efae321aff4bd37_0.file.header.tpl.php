<?php /* Smarty version 3.1.27, created on 2015-10-07 09:55:05
         compiled from "/home/quantum/webpoetry.org/bookagoo/smarty/templates/header.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:1134193567561524396cf4e8_73113785%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f749d9c4398f4a6b5e65dbad4efae321aff4bd37' => 
    array (
      0 => '/home/quantum/webpoetry.org/bookagoo/smarty/templates/header.tpl',
      1 => 1444226028,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1134193567561524396cf4e8_73113785',
  'variables' => 
  array (
    'title' => 0,
    'login_status' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_561524396ed1d0_60250857',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_561524396ed1d0_60250857')) {
function content_561524396ed1d0_60250857 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '1134193567561524396cf4e8_73113785';
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
        <meta name="interkassa-verification" content="9ef9cd47240e57ea66a1b0ac96a0f500" />
        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css">
        <?php echo '<script'; ?>
 src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="js/vendor/modernizr-2.6.2.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="js/plugins.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="js/vendor/lettering.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="js/vendor/jquery.maskedinput.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="http://vkontakte.ru/js/api/openapi.js" type="text/javascript"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="js/global.js"><?php echo '</script'; ?>
>
    </head>
    <body>
        <!--[if lt IE 7]>
            <?php echo '<script'; ?>
 src="js/vendor/css3pie/PIE.js" type="text/javascript"><?php echo '</script'; ?>
>
            <?php echo '<script'; ?>
 src="js/vendor/html5shiv/html5shiv.js" type="text/javascript"><?php echo '</script'; ?>
>
        <![endif]-->

        
        <?php echo '<script'; ?>
>
            window.fbAsyncInit = function() {
                FB.init({
                    appId: '497937203715073',
                    xfbml: true,
                    version: 'v2.4'
                });
            };

            (function(d, s, id){
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) {return;}
                js = d.createElement(s); js.id = id;
                js.src = "//connect.facebook.net/en_US/sdk.js";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));


            VK.init({
                apiId: 5097470
            });

        <?php echo '</script'; ?>
>
        

        <header>
            <section class="content clearfix">
                <a class="logo" href="index.php"></a>
                <ul class="clearfix">
                    <?php if ($_smarty_tpl->tpl_vars['login_status']->value == true) {?>
                        <li>
                            <a href="">Поделиться</a>
                        </li>
                        <li>
                            <a href="constructor.php" class="order_print">Заказать печать</a>
                        </li>
                        <li>
                            <a href="profile.php">Профиль</a>
                        </li>
                        <li>
                            <a href="logout.php" class="logout">Выход</a>
                        </li>
                    <?php } else { ?>
                        <li>
                            <a href="reg.php">Регистрация</a>
                        </li>
                        <li>
                            <a href="auth.php">Авторизация</a>
                        </li>
                    <?php }?>
                </ul>
            </section>
        </header><?php }
}
?>