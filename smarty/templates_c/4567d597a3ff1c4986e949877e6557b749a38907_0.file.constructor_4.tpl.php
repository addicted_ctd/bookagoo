<?php /* Smarty version 3.1.27, created on 2015-10-05 11:27:18
         compiled from "/home/quantum/webpoetry.org/bookagoo/smarty/templates/constructor_4.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:528708256561296d61489a3_10147991%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4567d597a3ff1c4986e949877e6557b749a38907' => 
    array (
      0 => '/home/quantum/webpoetry.org/bookagoo/smarty/templates/constructor_4.tpl',
      1 => 1444058816,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '528708256561296d61489a3_10147991',
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_561296d6164651_57917316',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_561296d6164651_57917316')) {
function content_561296d6164651_57917316 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '528708256561296d61489a3_10147991';
?>
<!-- Card 4 -->
<section class="card card_4 front">
	<h1 class="card_title">
		Моя ручка и ножка
	</h1>
	<section class="card_content">
		<p class='slim_title'>
			Моя ручка
		</p>
		<p class='notice'>
			Если вы хотите добавить отпечаток<br>
			или рисунок ручки малыша<br>
			в печатную версию книги,<br>
			просто оставьте эту страницу пустой.
		</p>
		<div class='photo'>
			<div class='inner'>
				<p>
					<span>+</span><br>
					Добавить фото
				</p>
			</div>
		</div>
	</section>
</section>

<section class="card card_4 back">
	<h1 class="card_title">
		Моя ручка и ножка
	</h1>
	<section class="card_content">
		<p class='slim_title'>
			Моя ножка
		</p>
		<p class='notice'>
			Если вы хотите добавить отпечаток<br>
			или рисунок ножки малыша<br>
			в печатную версию книги,<br>
			просто оставьте эту страницу пустой.
		</p>
		<div class='photo'>
			<div class='inner'>
				<p>
					<span>+</span><br>
					Добавить фото
				</p>
			</div>
		</div>
	</section>
</section>

<!-- End of Card 4 --><?php }
}
?>