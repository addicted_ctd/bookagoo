<?php /* Smarty version 3.1.27, created on 2015-10-06 05:52:46
         compiled from "/home/quantum/webpoetry.org/bookagoo/smarty/templates/constructor_5.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:253062191561399ee08ff00_28585346%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7b362a3e0156139b0ac14d27efc78f57c96a51bc' => 
    array (
      0 => '/home/quantum/webpoetry.org/bookagoo/smarty/templates/constructor_5.tpl',
      1 => 1444125031,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '253062191561399ee08ff00_28585346',
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_561399ee0aaf18_72263738',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_561399ee0aaf18_72263738')) {
function content_561399ee0aaf18_72263738 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '253062191561399ee08ff00_28585346';
?>
<!-- Card 5 -->
<section class="card card_5 front">
	<h1 class="card_title">
		Я расту
	</h1>
	<section class="card_content">
		<div class='teeth_wrapper'>
			<div class='tooth tooth_1'>
				<p>
					6 месяцев
					<span>1й</span>
				</p>
			</div>
		</div>
		<div class='form'>
			<div class='clearfix'>
				<p>Возраст</p>
				<input type='text' placeholder='в месяцах'>
			</div>
			<div class='clearfix'>
				<p>Зуб</p>
				<aside>
					<ul>
						<li></li>
					</ul>
				</aside>
			</div>
			<div class='clearfix'>
				<button>Добавить зуб</button>
			</div>
		</div>
	</section>
</section>

<section class="card card_5 back">
	<h1 class="card_title">
		Я расту
	</h1>
	<section class="card_content">
		<div class='ruler'>
			<div class='y'>
				<p class='title b'>рост</p>
				<ul>
					<li>100 см</li>
					<li></li>
					<li>90 см</li>
					<li></li>
					<li>80 см</li>
					<li></li>
					<li>70 см</li>
					<li></li>
					<li>60 см</li>
					<li></li>
					<li>50 см</li>
				</ul>
			</div>
			<div class='x'>
				<p class='title b'>возраст</p>
				<ul class='clearfix'>
					<li>1 месяц</li>
					<li></li>
					<li>3 месяца</li>
					<li></li>
					<li>6 месяцев</li>
					<li></li>
					<li>9 месяцев</li>
					<li></li>
					<li>12 месяцев</li>
				</ul>
			</div>
		</div>
		<div class='form'>
			<div class='clearfix'>
				<p>Возраст</p>
				<input type='text' placeholder='в месяцах'>
			</div>
			<div class='clearfix'>
				<p>Зуб</p>
				<input type='text' placeholder='в см'>
			</div>
			<div class='clearfix'>
				<p>Вес</p>
				<input type='text' placeholder='в кг'>
			</div>
			<div class='clearfix'>
				<button>Добавить зуб</button>
			</div>
		</div>
	</section>
</section>

<!-- End of Card 5 --><?php }
}
?>