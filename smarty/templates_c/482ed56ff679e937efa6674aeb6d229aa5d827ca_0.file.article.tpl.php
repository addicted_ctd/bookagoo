<?php /* Smarty version 3.1.27, created on 2015-10-02 08:36:30
         compiled from "/home/quantum/webpoetry.org/bookagoo/smarty/templates/article.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:518840699560e7a4eb497a4_14725515%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '482ed56ff679e937efa6674aeb6d229aa5d827ca' => 
    array (
      0 => '/home/quantum/webpoetry.org/bookagoo/smarty/templates/article.tpl',
      1 => 1443785593,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '518840699560e7a4eb497a4_14725515',
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_560e7a4eb834f8_07584701',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_560e7a4eb834f8_07584701')) {
function content_560e7a4eb834f8_07584701 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '518840699560e7a4eb497a4_14725515';
echo $_smarty_tpl->getSubTemplate ('header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>'Главная страница'), 0);
?>

<section id="news" class='page'> 
    <section class="one page_title"> 
        <section class="content">
            <h1>Новости</h1>
        </section> 
    </section> 
    <section class="two clearfix"> 
        <section class="content clearfix"> 
            <section class="left">
                <section id="article">
                    <article class="news_preview clearfix">
                        <figure style="background-image: url(img/news_2.jpg);">
                            <span class="date b">25.09.2015</span>
                        </figure>
                        <div>
                            <h2 class="title b">Aenean sollicitudin, lorem quis bibendum  auctor.</h2>
                            <p class="lead">
                                Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit.<br>
                                Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit.
                                <br><br>
                                Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo. Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, velit mauris egestas quam, ut aliquam massa nisl quis neque. Suspendisse in orci enim.
                                <br><br>
                                Lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. 
                                <br><br>
                                Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit.  Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.<br>
                                Mauris in erat justo. Nullam ac urna eu felis dapibus condimentum sit amet a augue. 
                                <br><br>
                                Sed non neque elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, velit mauris egestas quam, ut aliquam massa nisl quis neque. Suspendisse in orci enim.
                            </p>
                        </div>
                    </article>
                    <aside class='social_shares clearfix'>
                        <a href="https://twitter.com/share" class="twitter-share-button" data-url="http://webpoetry.org/bookagoo/docs/app/article.php" data-text="yey">Tweet</a>
                        
                        <?php echo '<script'; ?>
>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');<?php echo '</script'; ?>
>
                        
                        <div class="fb-like" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div>
                    </aside>
                    <section class='comment'>
                        <div id="disqus_thread"></div>
                        
                        <?php echo '<script'; ?>
 type="text/javascript">
                            var disqus_shortname = 'bookagooweb';
                            (function() {
                                var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
                                dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
                                (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
                            })();
                        <?php echo '</script'; ?>
>
                        
                    </section>
                </section>
            </section> 
            <section class="right"> 
                <?php echo $_smarty_tpl->getSubTemplate ('sidebar.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

            </section> 
        </section>
    </section>
</section>
<?php echo $_smarty_tpl->getSubTemplate ('footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);

}
}
?>