<?php /* Smarty version 3.1.27, created on 2015-11-06 16:32:26
         compiled from "/var/www/quantum/webpoetry.org/bookagoo/docs/app/smarty/templates/contacts.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:1718585190563d1c6acb4d49_59399297%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b9587f54bd508ca0de133a8b42e616acc334ac62' => 
    array (
      0 => '/var/www/quantum/webpoetry.org/bookagoo/docs/app/smarty/templates/contacts.tpl',
      1 => 1446820360,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1718585190563d1c6acb4d49_59399297',
  'variables' => 
  array (
    'general' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_563d1c6aced325_81413754',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_563d1c6aced325_81413754')) {
function content_563d1c6aced325_81413754 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '1718585190563d1c6acb4d49_59399297';
echo $_smarty_tpl->getSubTemplate ('header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>'Контакты'), 0);
?>

<section id="contacts" class='page'>
	<section class="page_title">
		<h1 class="content">Контакты</h1>
	</section>
	<section class="one">
		<section class="wrapper content clearfix">
			<div>
				<p class="g">
					Спасибо за ваш интерес к BookAgoo!
					<br><br>
					Пожалуйста, заполните форму ниже, и мы свяжемся с<br> вами в ближайшее время.
				</p>
				<h3 class="g b">Присоединяйтесь к нам</h3>
				<a href="<?php echo $_smarty_tpl->tpl_vars['general']->value['fb_link'];?>
" class="fb" target="_blank"></a>
				<a href="<?php echo $_smarty_tpl->tpl_vars['general']->value['vk_link'];?>
" class="vk" target="_blank"></a>
			</div>
			<div>
				<input type='text' placeholder='Имя' name='name'>
				<input type='text' placeholder='E-mail' name='email'>
				<input type='text' placeholder='Тема сообщения' name='topic'>
				<textarea placeholder='Текст' name='msg'></textarea>
				<a class='send_message'>Отправить</a>
			</div>
		</section>
	</section>
</section>
<?php echo '<script'; ?>
 src="/js/routes/contacts.js"><?php echo '</script'; ?>
>
<?php echo $_smarty_tpl->getSubTemplate ('footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);

}
}
?>