<?php /* Smarty version 3.1.27, created on 2015-11-07 20:40:42
         compiled from "/var/www/quantum/webpoetry.org/bookagoo/docs/app/smarty/templates/constructor.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:1851737596563ea81a1e9a60_08864517%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f62b53c1361974a0ee626769d90e49157d8027d3' => 
    array (
      0 => '/var/www/quantum/webpoetry.org/bookagoo/docs/app/smarty/templates/constructor.tpl',
      1 => 1446921540,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1851737596563ea81a1e9a60_08864517',
  'variables' => 
  array (
    'additional' => 0,
    'card_id' => 0,
    'card_title' => 0,
    'new_card_id' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_563ea81a296ac4_77805026',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_563ea81a296ac4_77805026')) {
function content_563ea81a296ac4_77805026 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '1851737596563ea81a1e9a60_08864517';
echo $_smarty_tpl->getSubTemplate ('header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>'Мои заказы'), 0);
?>

<section id="constructor" class='page'>

	<div class="socials"> 
      <div class="socials_scroll">        
        <ul>
          <li class="vk">Vkontakte</li>
          <li class="in">Instagram</li>
          <li class="fb">Facebook</li>          
        </ul>
        <div class="socials_scroll_close" onclick="$('.socials').removeClass('active');"></div>
        <div class="socials_scroll_popup">
          <div class="socials_scroll_popup_result"></div>
        </div><!-- socials_scroll_popup -->
      </div><!-- socials_scroll -->   
  </div> <!-- socials -->

	<section class="page_title">
		<section class="content clearfix">
			<h1>Создать книгу</h1>
		</section>
	</section>

	<section class="clearfix content">
		<section class="constructor_content">

		<?php if ($_GET['card'] == 1) {?>

			<?php echo $_smarty_tpl->getSubTemplate ('constructor_1.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>


		<?php } elseif ($_GET['card'] == 2) {?>

			<?php echo $_smarty_tpl->getSubTemplate ('constructor_2.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>


		<?php } elseif ($_GET['card'] == 3) {?>

			<?php echo $_smarty_tpl->getSubTemplate ('constructor_3.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>


		<?php } elseif ($_GET['card'] == 4) {?>

			<?php echo $_smarty_tpl->getSubTemplate ('constructor_4.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>


		<?php } elseif ($_GET['card'] == 5) {?>

			<?php echo $_smarty_tpl->getSubTemplate ('constructor_5.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>


		<?php } elseif ($_GET['card'] == 6) {?>

			<?php echo $_smarty_tpl->getSubTemplate ('constructor_6.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>


		<?php } elseif ($_GET['card'] == 7) {?>

			<?php echo $_smarty_tpl->getSubTemplate ('constructor_7.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>


		<?php } elseif ($_GET['card'] == 8) {?>

			<?php echo $_smarty_tpl->getSubTemplate ('constructor_8.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>


		<?php } elseif ($_GET['card'] > 8) {?>

			<?php echo $_smarty_tpl->getSubTemplate ('constructor_new.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>


		<?php }?>

		</section>

		<section class="constructor_menu">
			<section class="items">
				<a href='constructor.php?card=1'>
					Обложка
				</a>
				<a href='constructor.php?card=2'>
					Привет,
					а вот и я!
				</a>
				<a href='constructor.php?card=3'>
					Моя семья
				</a>
				<a href='constructor.php?card=4'>
					Моя ручка
					и ножка
				</a>
				<a href='constructor.php?card=5'>
					Я расту
				</a>
				<a href='constructor.php?card=6'>
					Любимое
				</a>
				<a href='constructor.php?card=7'>
					Путешествую
				</a>
				<a href='constructor.php?card=8'>
					Вот и год
					прошел
				</a>
				<?php
$_from = $_smarty_tpl->tpl_vars['additional']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['card_title'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['card_title']->_loop = false;
$_smarty_tpl->tpl_vars['card_id'] = new Smarty_Variable;
foreach ($_from as $_smarty_tpl->tpl_vars['card_id']->value => $_smarty_tpl->tpl_vars['card_title']->value) {
$_smarty_tpl->tpl_vars['card_title']->_loop = true;
$foreach_card_title_Sav = $_smarty_tpl->tpl_vars['card_title'];
?>
				<a href='constructor.php?card=<?php echo $_smarty_tpl->tpl_vars['card_id']->value;?>
'>
					<?php echo $_smarty_tpl->tpl_vars['card_title']->value;?>

				</a>
				<?php
$_smarty_tpl->tpl_vars['card_title'] = $foreach_card_title_Sav;
}
?>
			</section>
			<aside>
				<span class='add'>
					<a href='constructor.php?card=<?php echo $_smarty_tpl->tpl_vars['new_card_id']->value;?>
'>Добавить<br>
					раздел</a>
				</span>
			</aside>
		</section>
	</section>
	<section class='card_nav clearfix'>
		<a class='left'></a>
		<a class='right'></a>
	</section>
</section>

<div id="vk_api_transport"></div>

<?php echo '<script'; ?>
 src="js/routes/constructor.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
> Constructor.card = <?php echo $_GET['card'];?>
; Constructor.init(); $('.items').find('a').eq(Constructor.card - 1).addClass('active');<?php echo '</script'; ?>
>
<?php echo '<script'; ?>
>Constructor.init_nav();<?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="js/vendor/jquery.form.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="js/fb.js" type="text/javascript"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="js/vk.js" type="text/javascript"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="js/in.js" type="text/javascript"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="js/social.js" type="text/javascript"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="js/routes/photo.js"><?php echo '</script'; ?>
>
<?php echo $_smarty_tpl->getSubTemplate ('footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);

}
}
?>