<?php /* Smarty version 3.1.27, created on 2015-10-07 11:45:07
         compiled from "/home/quantum/webpoetry.org/bookagoo/smarty/templates/auth.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:159177938056153e03217c32_93429008%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5c9789a30acdde5eb40ddb721e54af3ea354a80b' => 
    array (
      0 => '/home/quantum/webpoetry.org/bookagoo/smarty/templates/auth.tpl',
      1 => 1444232698,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '159177938056153e03217c32_93429008',
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_56153e032394d9_28213357',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_56153e032394d9_28213357')) {
function content_56153e032394d9_28213357 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '159177938056153e03217c32_93429008';
echo $_smarty_tpl->getSubTemplate ('header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>'Вход'), 0);
?>

<section id="auth" class='page'>
	<section class="page_title">
		<h1 class="content">Вход</h1>
	</section>
	<section class="content wrapper soc_enter_block">
		<h2 class="b g">Вход через социальные сети</h2>
		<div class="soc_enter">
			<div class="soc_fb" onclick="Global.fb_login();">Войти при помощи facebook</div>
			<div class="soc_vk" onclick="VK.Auth.login(Global.vk_login);">Войти при помощи vkontakte</div>
		</div>
	</section>
	<section class="content wrapper enter_block">
		<h2 class="b g">Либо войдите через E-mail</h2>
		<div class="form_enter_block">
			<input type="text" placeholder="E-mail" class="gray" name="email" id="user_name">
			<input type="password" placeholder="Пароль" class="gray" name="user_password" id="user_password">
			<div class="checkbox_block">
				<input type="checkbox" name="show_pwd" id="show_pwd">
				<label for="show_pwd" class="g">Показать пароль</label>
			</div>
			<div class="botton_block">
				<button id='login'>Вход</button>
			</div>
		</div>
	</section>
</section>
<?php echo '<script'; ?>
 src="js/routes/auth.js"><?php echo '</script'; ?>
>
<?php echo $_smarty_tpl->getSubTemplate ('footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);

}
}
?>