<?php /* Smarty version 3.1.27, created on 2015-10-05 11:05:57
         compiled from "/home/quantum/webpoetry.org/bookagoo/smarty/templates/constructor_3.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:172264958561291d5749247_42739528%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b0fe280758f5f6c06bbff050d6bc59a3a60bef3e' => 
    array (
      0 => '/home/quantum/webpoetry.org/bookagoo/smarty/templates/constructor_3.tpl',
      1 => 1444057555,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '172264958561291d5749247_42739528',
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_561291d5775895_48761022',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_561291d5775895_48761022')) {
function content_561291d5775895_48761022 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '172264958561291d5749247_42739528';
?>
<!-- Card 3 -->
<section class="card card_3 front">
	<h1 class="card_title">
		Моя семья
	</h1>
	<section class="card_content">
		<div class="leaf_wrapper" style="left: 423px; top: 363px;">
			<div class="leaf me">
				<h1 class='circle_text kitty' deg='6' dir='1'>Я</h1>
				<h2 class='circle_text kitty' deg='9' dir='-1'>Константин Иванович</h2>
				<div class="leaf_inner">
					<div class='photo'>
						<div class='inner'>
							<p>
								<span>+</span>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="leaf_wrapper" style="left: 241px; top: 480px;">
			<div class="leaf">
				<h1 class='circle_text' deg='6' dir='1'>Брат</h1>
				<h2 class='circle_text' contenteditable='true' deg='5' dir='-1'>Введите имя</h2>
				<div class="leaf_inner">
					<div class='photo'>
						<div class='inner'>
							<p>
								<span>+</span>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="leaf_wrapper" style="left: 607px; top: 480px;">
			<div class="leaf">
				<h1 class='circle_text' deg='6' dir='1'>Сестра</h1>
				<h2 class='circle_text' contenteditable='true' deg='5' dir='-1'>Введите имя</h2>
				<div class="leaf_inner">
					<div class='photo'>
						<div class='inner'>
							<p>
								<span>+</span>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="leaf_wrapper" style="left: 318px; top: 687px;">
			<div class="leaf">
				<h1 class='circle_text' deg='6' dir='1'>Папа</h1>
				<h2 class='circle_text' contenteditable='true' deg='5' dir='-1'>Введите имя</h2>
				<div class="leaf_inner">
					<div class='photo'>
						<div class='inner'>
							<p>
								<span>+</span>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="leaf_wrapper" style="left: 534px; top: 687px;">
			<div class="leaf">
				<h1 class='circle_text' deg='6' dir='1'>Мама</h1>
				<h2 class='circle_text' contenteditable='true' deg='5' dir='-1'>Введите имя</h2>
				<div class="leaf_inner">
					<div class='photo'>
						<div class='inner'>
							<p>
								<span>+</span>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="leaf_wrapper" style="left: 113px; top: 777px;">
			<div class="leaf">
				<h1 class='circle_text' deg='6' dir='1'>Дедушка</h1>
				<h2 class='circle_text' contenteditable='true' deg='5' dir='-1'>Введите имя</h2>
				<div class="leaf_inner">
					<div class='photo'>
						<div class='inner'>
							<p>
								<span>+</span>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="leaf_wrapper" style="left: 737px; top: 777px;">
			<div class="leaf">
				<h1 class='circle_text' deg='6' dir='1'>Дедушка</h1>
				<h2 class='circle_text' contenteditable='true' deg='5' dir='-1'>Введите имя</h2>
				<div class="leaf_inner">
					<div class='photo'>
						<div class='inner'>
							<p>
								<span>+</span>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="leaf_wrapper" style="left: 292px; top: 906px;">
			<div class="leaf">
				<h1 class='circle_text' deg='6' dir='1'>Бабушка</h1>
				<h2 class='circle_text' contenteditable='true' deg='5' dir='-1'>Введите имя</h2>
				<div class="leaf_inner">
					<div class='photo'>
						<div class='inner'>
							<p>
								<span>+</span>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="leaf_wrapper" style="left: 559px; top: 906px;">
			<div class="leaf">
				<h1 class='circle_text' deg='6' dir='1'>Бабушка</h1>
				<h2 class='circle_text' contenteditable='true' deg='5' dir='-1'>Введите имя</h2>
				<div class="leaf_inner">
					<div class='photo'>
						<div class='inner'>
							<p>
								<span>+</span>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</section>

<section class="card card_3 back">
	<h1 class="card_title">
		Моя семья
	</h1>
	<section class="card_content clearfix">
		<section class="col">
			<div class='photo white regular'>
				<div class='inner'>
					<p>
						<span>+</span><br>
						Добавить фото
					</p>
					<input type='text' class='blue comment border_input' placeholder='Добавьте комментарий'>
				</div>
			</div>
		</section>
		<section class="col col_2">
			<h1>Мою маму зовут</h1>
			<input type='text' class='blue border_input' placeholder='укажите имя' style='width: 320px;'>
			<h1>Она родилась</h1>
			<div class='calendar'><input type='text' class='blue border_input' placeholder='укажите дату' style='width: 175px;'></div>
			<h1>Ее профессия</h1>
			<input type='text' class='blue border_input' placeholder='укажите профессию' style='width: 320px;'>
			<h1>Она любит заниматься</h1>
			<input type='text' class='blue border_input' placeholder='чем любит заниматься' style='width: 320px;'>
		</section>
		<section class="col col_3">
			<h1>Моего папу зовут</h1>
			<input type='text' class='blue border_input' placeholder='укажите имя' style='width: 320px;'>
			<h1>Он родился</h1>
			<div class='calendar'><input type='text' class='blue border_input' placeholder='укажите дату' style='width: 175px;'></div>
			<h1>Его профессия</h1>
			<input type='text' class='blue border_input' placeholder='укажите профессию' style='width: 320px;'>
			<h1>Она любит заниматься</h1>
			<input type='text' class='blue border_input' placeholder='чем любит заниматься' style='width: 320px;'>
		</section>
		<section class="col_4">
			<div class='photo white regular'>
				<div class='inner'>
					<p>
						<span>+</span><br>
						Добавить фото
					</p>
					<input type='text' class='blue comment border_input' placeholder='Добавьте комментарий'>
				</div>
			</div>
		</section>
	</section>
</section>

<?php echo '<script'; ?>
 src="js/routes/tree.js"><?php echo '</script'; ?>
>

<!-- End of Card 3 --><?php }
}
?>