<?php /* Smarty version 3.1.27, created on 2015-10-09 06:18:37
         compiled from "/var/www/quantum/webpoetry.org/bookagoo/docs/app/smarty/templates/auth.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:8999142435617947d894322_13300279%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6c21449732551d70e6fd16cbac7e56716830282d' => 
    array (
      0 => '/var/www/quantum/webpoetry.org/bookagoo/docs/app/smarty/templates/auth.tpl',
      1 => 1444232698,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '8999142435617947d894322_13300279',
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_5617947d8af272_70081716',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_5617947d8af272_70081716')) {
function content_5617947d8af272_70081716 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '8999142435617947d894322_13300279';
echo $_smarty_tpl->getSubTemplate ('header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>'Вход'), 0);
?>

<section id="auth" class='page'>
	<section class="page_title">
		<h1 class="content">Вход</h1>
	</section>
	<section class="content wrapper soc_enter_block">
		<h2 class="b g">Вход через социальные сети</h2>
		<div class="soc_enter">
			<div class="soc_fb" onclick="Global.fb_login();">Войти при помощи facebook</div>
			<div class="soc_vk" onclick="VK.Auth.login(Global.vk_login);">Войти при помощи vkontakte</div>
		</div>
	</section>
	<section class="content wrapper enter_block">
		<h2 class="b g">Либо войдите через E-mail</h2>
		<div class="form_enter_block">
			<input type="text" placeholder="E-mail" class="gray" name="email" id="user_name">
			<input type="password" placeholder="Пароль" class="gray" name="user_password" id="user_password">
			<div class="checkbox_block">
				<input type="checkbox" name="show_pwd" id="show_pwd">
				<label for="show_pwd" class="g">Показать пароль</label>
			</div>
			<div class="botton_block">
				<button id='login'>Вход</button>
			</div>
		</div>
	</section>
</section>
<?php echo '<script'; ?>
 src="js/routes/auth.js"><?php echo '</script'; ?>
>
<?php echo $_smarty_tpl->getSubTemplate ('footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);

}
}
?>