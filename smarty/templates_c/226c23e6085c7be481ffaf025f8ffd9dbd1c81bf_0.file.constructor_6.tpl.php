<?php /* Smarty version 3.1.27, created on 2015-11-05 14:39:23
         compiled from "/var/www/quantum/webpoetry.org/bookagoo/docs/app/smarty/templates/constructor_6.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:594174280563bb06bb631f7_08852026%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '226c23e6085c7be481ffaf025f8ffd9dbd1c81bf' => 
    array (
      0 => '/var/www/quantum/webpoetry.org/bookagoo/docs/app/smarty/templates/constructor_6.tpl',
      1 => 1446727191,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '594174280563bb06bb631f7_08852026',
  'variables' => 
  array (
    'constructor' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_563bb06bd08ff0_59256416',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_563bb06bd08ff0_59256416')) {
function content_563bb06bd08ff0_59256416 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '594174280563bb06bb631f7_08852026';
?>
<!-- Card 6 -->
<section class="card card_6 front" data-side='front'>
	<h1 class="card_title">
		Любимое
	</h1>
	<section class="card_content clearfix">
		<section class='clearfix first'>
			<section class='col'>
				<div class='photo blue_light regular' data-editor=true data-constructor_var="0">
					<div class='inner'>
						<p>
							<span>+</span><br>
							Добавить фото
						</p>
						<input type='text' class='blue_light comment border_input' placeholder='Добавьте комментарий' data-constructor_var="1" value="<?php if (isset($_smarty_tpl->tpl_vars['constructor']->value[1])) {
echo $_smarty_tpl->tpl_vars['constructor']->value[1];
}?>">
						<h3 class='title'>Улыбаюсь</h3>
					</div>
					<?php if (isset($_smarty_tpl->tpl_vars['constructor']->value[0])) {
echo $_smarty_tpl->tpl_vars['constructor']->value[0];
}?>
					<input type="file" name="photo" class="load_image hidden">
				</div>
			</section>
			<section class='col'>
				<div class='photo blue_light regular' data-editor=true data-constructor_var="2">
					<div class='inner'>
						<p>
							<span>+</span><br>
							Добавить фото
						</p>
						<input type='text' class='blue_light comment border_input' placeholder='Добавьте комментарий' data-constructor_var="3" value="<?php if (isset($_smarty_tpl->tpl_vars['constructor']->value[3])) {
echo $_smarty_tpl->tpl_vars['constructor']->value[3];
}?>">
						<h3 class='title'>Играю</h3>
					</div>
					<?php if (isset($_smarty_tpl->tpl_vars['constructor']->value[2])) {
echo $_smarty_tpl->tpl_vars['constructor']->value[2];
}?>
					<input type="file" name="photo" class="load_image hidden">
				</div>
			</section>
		</section>
		<section class='clearfix second'>
			<h1>Я наряжаюсь</h1>
			<section class='dressing_up clearfix' data-editor=true>
				<div class='photo' data-constructor_var="4">
					<div class='inner'>
						<p>
							<span>+</span><br>
							Добавить фото
						</p>
					</div>
					<?php if (isset($_smarty_tpl->tpl_vars['constructor']->value[4])) {
echo $_smarty_tpl->tpl_vars['constructor']->value[4];
}?>
					<input type="file" name="photo" class="load_image hidden">
				</div>
				<div class='photo' data-constructor_var="5">
					<div class='inner' data-editor=true>
						<p>
							<span>+</span><br>
							Добавить фото
						</p>
					</div>
					<?php if (isset($_smarty_tpl->tpl_vars['constructor']->value[5])) {
echo $_smarty_tpl->tpl_vars['constructor']->value[5];
}?>
					<input type="file" name="photo" class="load_image hidden">
				</div>
			</section>
		</section>
	</section>
</section>

<section class="card card_6 back" data-side='back'>
	<h1 class="card_title">
		Любимое
	</h1>
	<section class="card_content">
		<section class='clearfix'>
			<section class='col'>
				<div class='photo blue_light regular' data-editor=true data-constructor_var="6">
					<div class='inner'>
						<p>
							<span>+</span><br>
							Добавить фото
						</p>
						<input type='text' class='blue_light comment border_input' placeholder='Добавьте комментарий' data-constructor_var="7" value="<?php if (isset($_smarty_tpl->tpl_vars['constructor']->value[7])) {
echo $_smarty_tpl->tpl_vars['constructor']->value[7];
}?>">
					</div>
					<?php if (isset($_smarty_tpl->tpl_vars['constructor']->value[6])) {
echo $_smarty_tpl->tpl_vars['constructor']->value[6];
}?>
					<input type="file" name="photo" class="load_image hidden">
				</div>
			</section>
			<section class='col'>
				<div class='photo blue_light regular' data-editor=true data-constructor_var="8">
					<div class='inner'>
						<p>
							<span>+</span><br>
							Добавить фото
						</p>
						<input type='text' class='blue_light comment border_input' placeholder='Добавьте комментарий' data-constructor_var="9" value="<?php if (isset($_smarty_tpl->tpl_vars['constructor']->value[9])) {
echo $_smarty_tpl->tpl_vars['constructor']->value[9];
}?>">
					</div>
					<?php if (isset($_smarty_tpl->tpl_vars['constructor']->value[8])) {
echo $_smarty_tpl->tpl_vars['constructor']->value[8];
}?>
					<input type="file" name="photo" class="load_image hidden">
				</div>
			</section>
		</section>
		<div class='photo blue_light big' data-editor=true data-constructor_var="10">
			<div class='inner'>
				<p>
					<span>+</span><br>
					Добавить фото
				</p>
				<input type='text' class='blue_light comment border_input' placeholder='Добавьте комментарий' data-constructor_var="11" value="<?php if (isset($_smarty_tpl->tpl_vars['constructor']->value[11])) {
echo $_smarty_tpl->tpl_vars['constructor']->value[11];
}?>">
			</div>
			<?php if (isset($_smarty_tpl->tpl_vars['constructor']->value[10])) {
echo $_smarty_tpl->tpl_vars['constructor']->value[10];
}?>
			<input type="file" name="photo" class="load_image hidden">
		</div>
	</section>
</section>

<!-- End of Card 6 --><?php }
}
?>