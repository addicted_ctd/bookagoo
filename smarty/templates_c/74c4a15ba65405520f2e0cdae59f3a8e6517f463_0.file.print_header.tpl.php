<?php /* Smarty version 3.1.27, created on 2015-10-29 15:48:00
         compiled from "/var/www/quantum/webpoetry.org/bookagoo/docs/app/smarty/templates/print_header.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:562035871563277f05e19c2_93190406%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '74c4a15ba65405520f2e0cdae59f3a8e6517f463' => 
    array (
      0 => '/var/www/quantum/webpoetry.org/bookagoo/docs/app/smarty/templates/print_header.tpl',
      1 => 1446122890,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '562035871563277f05e19c2_93190406',
  'variables' => 
  array (
    'title' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_563277f060adc8_29511062',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_563277f060adc8_29511062')) {
function content_563277f060adc8_29511062 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '562035871563277f05e19c2_93190406';
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
        <meta name="interkassa-verification" content="9ef9cd47240e57ea66a1b0ac96a0f500" />
        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

        <link rel="stylesheet" href="/css/normalize.css">
        <link rel="stylesheet" href="/css/main.css">
        <link rel="stylesheet" href="/css/print.css">
        <link rel="stylesheet" href="/css/preview.css">
        <?php echo '<script'; ?>
 src="/js/vendor/jquery-1.9.1.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="/js/vendor/modernizr-2.6.2.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="/js/plugins.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="/js/vendor/lettering.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="/js/vendor/jquery.maskedinput.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="http://vkontakte.ru/js/api/openapi.js" type="text/javascript"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="/js/global.js"><?php echo '</script'; ?>
>
    </head>
    <body>
        <!--[if lt IE 7]>
            <?php echo '<script'; ?>
 src="js/vendor/css3pie/PIE.js" type="text/javascript"><?php echo '</script'; ?>
>
            <?php echo '<script'; ?>
 src="js/vendor/html5shiv/html5shiv.js" type="text/javascript"><?php echo '</script'; ?>
>
        <![endif]--><?php }
}
?>