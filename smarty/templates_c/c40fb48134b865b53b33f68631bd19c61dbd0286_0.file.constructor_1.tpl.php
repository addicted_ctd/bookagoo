<?php /* Smarty version 3.1.27, created on 2015-10-29 18:51:29
         compiled from "/var/www/quantum/webpoetry.org/bookagoo/docs/app/smarty/templates/constructor_1.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:1700243235632a2f1d818b7_44857951%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c40fb48134b865b53b33f68631bd19c61dbd0286' => 
    array (
      0 => '/var/www/quantum/webpoetry.org/bookagoo/docs/app/smarty/templates/constructor_1.tpl',
      1 => 1446159087,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1700243235632a2f1d818b7_44857951',
  'variables' => 
  array (
    'constructor' => 0,
    'child_info' => 0,
    'word' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_5632a2f1da73b1_77499314',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_5632a2f1da73b1_77499314')) {
function content_5632a2f1da73b1_77499314 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '1700243235632a2f1d818b7_44857951';
?>
<!-- Card 1 -->
<section class="card card_1 front" data-side='front'>
	<h1 class="card_title">
		Обложка
	</h1>
	<section class="card_content">
		<div class='book'>
			<div class='book_inner'>
				<div class='photo' data-editor=true data-final=false data-constructor_var="0">
					<div class='inner'>
						<p>
							<span>+</span><br>
							Добавить фото
						</p>
					</div>
					<?php if (isset($_smarty_tpl->tpl_vars['constructor']->value[0])) {?>
						<?php echo $_smarty_tpl->tpl_vars['constructor']->value[0];?>

					<?php }?>
					<input type="file" name="photo" class="load_image hidden">
				</div>
				<article>
					<input type='text' value='Меня зовут <?php echo $_smarty_tpl->tpl_vars['child_info']->value['f_name'];?>
' disabled='true'>
					<?php if ($_smarty_tpl->tpl_vars['child_info']->value['gender'] == 'female') {?>
						<?php $_smarty_tpl->tpl_vars['word'] = new Smarty_Variable('родилась', null, 0);?>
					<?php } else { ?>
						<?php $_smarty_tpl->tpl_vars['word'] = new Smarty_Variable('родился', null, 0);?>
					<?php }?>
					<input type='text' value='Я <?php echo $_smarty_tpl->tpl_vars['word']->value;?>
 <?php echo $_smarty_tpl->tpl_vars['child_info']->value['birth_full'];?>
' disabled="true">
				</article>
			</div>
		</div>
	</section>
</section>

<!-- End of Card 1 --><?php }
}
?>