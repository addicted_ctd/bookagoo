<?php /* Smarty version 3.1.27, created on 2015-10-02 07:34:12
         compiled from "/home/quantum/webpoetry.org/bookagoo/smarty/templates/contacts.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:775281259560e6bb4970c31_58918046%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'fafa8b1ceaf5157de6426f683feb870c3a042e1b' => 
    array (
      0 => '/home/quantum/webpoetry.org/bookagoo/smarty/templates/contacts.tpl',
      1 => 1443785606,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '775281259560e6bb4970c31_58918046',
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_560e6bb498e329_42330713',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_560e6bb498e329_42330713')) {
function content_560e6bb498e329_42330713 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '775281259560e6bb4970c31_58918046';
echo $_smarty_tpl->getSubTemplate ('header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>'Контакты'), 0);
?>

<section id="contacts" class='page'>
	<section class="page_title">
		<h1 class="content">Контакты</h1>
	</section>
	<section class="one">
		<section class="wrapper content clearfix">
			<div>
				<p class="g">
					Спасибо за ваш интерес к BookAgoo!
					<br><br>
					Пожалуйста, заполните форму ниже, и мы свяжемся с<br> вами в ближайшее время.
				</p>
				<h3 class="g b">Присоединяйтесь к нам</h3>
				<a href='' class='fb'></a>
				<a href='' class='tw'></a>
				<a href='' class='blog'></a>
				<a href='' class='yt'></a>
			</div>
			<div>
				<input type='text' placeholder='Имя'>
				<input type='text' placeholder='E-mail'>
				<input type='text' placeholder='Тема сообщения'>
				<textarea placeholder='Текст'></textarea>
				<a class='send_message'>Отправить</a>
			</div>
		</section>
	</section>
</section>
<?php echo $_smarty_tpl->getSubTemplate ('footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);

}
}
?>