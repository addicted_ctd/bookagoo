<?php /* Smarty version 3.1.27, created on 2015-11-07 18:13:00
         compiled from "/var/www/quantum/webpoetry.org/bookagoo/docs/app/smarty/templates/header.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:1516021630563e857cade656_72317899%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f12bb06fc891f077916b1c7dac4ec3cf23470552' => 
    array (
      0 => '/var/www/quantum/webpoetry.org/bookagoo/docs/app/smarty/templates/header.tpl',
      1 => 1446912672,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1516021630563e857cade656_72317899',
  'variables' => 
  array (
    'title' => 0,
    'login_status' => 0,
    'uid' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_563e857cb03844_70925201',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_563e857cb03844_70925201')) {
function content_563e857cb03844_70925201 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '1516021630563e857cade656_72317899';
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
        <meta name="interkassa-verification" content="9ef9cd47240e57ea66a1b0ac96a0f500" />
        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="admin/css/construction_sight.css">
        <?php echo '<script'; ?>
 src="js/vendor/jquery-1.9.1.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="js/vendor/modernizr-2.6.2.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="js/plugins.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="js/vendor/lettering.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="js/vendor/jquery.maskedinput.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="http://vkontakte.ru/js/api/openapi.js" type="text/javascript"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="js/global.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="js/overlay.js"><?php echo '</script'; ?>
>
    </head>
    <body>
        <!--[if lt IE 7]>
            <?php echo '<script'; ?>
 src="js/vendor/css3pie/PIE.js" type="text/javascript"><?php echo '</script'; ?>
>
            <?php echo '<script'; ?>
 src="js/vendor/html5shiv/html5shiv.js" type="text/javascript"><?php echo '</script'; ?>
>
        <![endif]-->

        <div id='vk_api_transport'></div>

        
        <?php echo '<script'; ?>
>
            window.fbAsyncInit = function() {
                FB.init({
                    appId: '497937203715073',
                    xfbml: true,
                    version: 'v2.5'
                });
            };

            (function(d, s, id){
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) {return;}
                js = d.createElement(s); js.id = id;
                js.src = "//connect.facebook.net/en_US/sdk.js";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));

            window.vkAsyncInit = function() {
                VK.init({
                    apiId: 5097470
                });
            };

            setTimeout(function() {
              var el = document.createElement("script");
              el.type = "text/javascript";
              el.src = "//vk.com/js/api/openapi.js";
              el.async = true;
              document.getElementById("vk_api_transport").appendChild(el);  }, 0);

        <?php echo '</script'; ?>
>
        

        <div id='fb-root'></div>

        <header>
            <section class="content clearfix">
                <a class="logo" href="constructor.php?card=1"></a>
                <ul class="clearfix">
                    <?php if ($_smarty_tpl->tpl_vars['login_status']->value == true) {?>
                        <li>
                            <a class="under_hover" href="preview.php?u=<?php echo $_smarty_tpl->tpl_vars['uid']->value;?>
&card=1" target="_blank">Поделиться</a>
                        </li>
                        <li>
                            <a class="under_hover" href="constructor.php?card=1">Книга</a>
                        </li>
                        <li>
                            <a href="cart.php" class="order_print">Заказать печать</a>
                        </li>
                        <li>
                            <a class="under_hover" href="profile.php">Профиль</a>
                        </li>
                        <li>
                            <a href="logout.php" class="logout">Выход</a>
                        </li>
                    <?php } else { ?>
                        <li>
                            <a class="under_hover" href="reg.php">Регистрация</a>
                        </li>
                        <li>
                            <a class="under_hover" href="auth.php">Авторизация</a>
                        </li>
                    <?php }?>
                </ul>
            </section>
        </header><?php }
}
?>