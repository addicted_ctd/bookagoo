<?php /* Smarty version 3.1.27, created on 2015-11-09 21:30:54
         compiled from "/var/www/quantum/webpoetry.org/bookagoo/docs/app/smarty/templates/cart.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:212848032564156de190819_86178614%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c62a7bbcef05b35f05deb2f4d8b3b3ae6a15bc4e' => 
    array (
      0 => '/var/www/quantum/webpoetry.org/bookagoo/docs/app/smarty/templates/cart.tpl',
      1 => 1446827692,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '212848032564156de190819_86178614',
  'variables' => 
  array (
    'prices' => 0,
    'additional' => 0,
    'card_id' => 0,
    'card_title' => 0,
    'profile' => 0,
    'general' => 0,
    'regions' => 0,
    'region' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_564156de266b23_47106531',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_564156de266b23_47106531')) {
function content_564156de266b23_47106531 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '212848032564156de190819_86178614';
echo $_smarty_tpl->getSubTemplate ('header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>'Мои заказы'), 0);
?>

<section id="cart" class='page'>
	<section class="page_title">
		<section class="content clearfix">
			<h1>Подтвердите выбор печати</h1>
			<p class='total_info'>В корзине 0 товаров на сумму 0 рублей</p>
		</section>
	</section>
	<section class="one">
		<section class="content clearfix">
			<section class='cart_item wrapper single' data-card_id='0' data-price='<?php echo $_smarty_tpl->tpl_vars['prices']->value['box'];?>
'>
				<hgroup class="clearfix">
					<h1 class="b">Коробка</h1>
				</hgroup>
				<div class='item_content clearfix'>
					<div class='card_side'>
						<p></p>
						<figure style='background-image: url(img/cart/design_box.jpg); background-position: center center;'></figure>
					</div>
				</div>
				<aside class='clearfix'>
					<input type='checkbox' id='card_0_order' checked>
					<label class='g' for="card_0_order">Добавить в корзину</label>
				</aside>
			</section>
			<section class='cart_item wrapper single' data-card_id='1' data-price='<?php echo $_smarty_tpl->tpl_vars['prices']->value['card'];?>
'>
				<hgroup class="clearfix">
					<h1 class="b">Обложка</h1>
					<a class="edit" href='constructor.php?card=1'>Изменить</a>
				</hgroup>
				<div class='item_content clearfix'>
					<div data-side='front' class='card_side'>
						<p></p>
						<figure></figure>
					</div>
				</div>
				<aside class='clearfix'>
					<input type='checkbox' id='card_1_order' checked>
					<label class='g' for="card_1_order">Добавить в корзину</label>
				</aside>
			</section>
			<section class='cart_item wrapper' data-card_id='2' data-price='<?php echo $_smarty_tpl->tpl_vars['prices']->value['card'];?>
'>
				<hgroup class="clearfix">
					<h1 class="b">Привет, а вот и я</h1>
					<a class="edit" href='constructor.php?card=2'>Изменить</a>
				</hgroup>
				<div class='item_content clearfix'>
					<div data-side='front' class='card_side'>
						<p>Лицевая сторона</p>
						<figure></figure>
					</div>
					<div data-side='back' class='card_side'>
						<p>Обратная сторона</p>
						<figure></figure>
					</div>
				</div>
				<aside class='clearfix'>
					<input type='checkbox' id='card_2_order' checked>
					<label class='g' for="card_2_order">Добавить в корзину</label>
				</aside>
			</section>
			<section class='cart_item wrapper' data-card_id='3' data-price='<?php echo $_smarty_tpl->tpl_vars['prices']->value['card'];?>
'>
				<hgroup class="clearfix">
					<h1 class="b">Моя семья</h1>
					<a class="edit" href='constructor.php?card=3'>Изменить</a>
				</hgroup>
				<div class='item_content clearfix'>
					<div data-side='front' class='card_side'>
						<p>Лицевая сторона</p>
						<figure></figure>
					</div>
					<div data-side='back' class='card_side'>
						<p>Обратная сторона</p>
						<figure></figure>
					</div>
				</div>
				<aside class='clearfix'>
					<input type='checkbox' id='card_3_order' checked>
					<label class='g' for="card_3_order">Добавить в корзину</label>
				</aside>
			</section>
			<section class='cart_item wrapper' data-card_id='4' data-price='<?php echo $_smarty_tpl->tpl_vars['prices']->value['card'];?>
'>
				<hgroup class="clearfix">
					<h1 class="b">Моя ручка и ножка</h1>
					<a class="edit" href='constructor.php?card=4'>Изменить</a>
				</hgroup>
				<div class='item_content clearfix'>
					<div data-side='front' class='card_side'>
						<p>Лицевая сторона</p>
						<figure></figure>
					</div>
					<div data-side='back' class='card_side'>
						<p>Обратная сторона</p>
						<figure></figure>
					</div>
				</div>
				<aside class='clearfix'>
					<input type='checkbox' id='card_4_order' checked>
					<label class='g' for="card_4_order">Добавить в корзину</label>
				</aside>
			</section>
			<section class='cart_item wrapper' data-card_id='5' data-price='<?php echo $_smarty_tpl->tpl_vars['prices']->value['card'];?>
'>
				<hgroup class="clearfix">
					<h1 class="b">Я расту</h1>
					<a class="edit" href='constructor.php?card=5'>Изменить</a>
				</hgroup>
				<div class='item_content clearfix'>
					<div data-side='front' class='card_side'>
						<p>Лицевая сторона</p>
						<figure></figure>
					</div>
					<div data-side='back' class='card_side'>
						<p>Обратная сторона</p>
						<figure></figure>
					</div>
				</div>
				<aside class='clearfix'>
					<input type='checkbox' id='card_5_order' checked>
					<label class='g' for="card_5_order">Добавить в корзину</label>
				</aside>
			</section>
			<section class='cart_item wrapper' data-card_id='6' data-price='<?php echo $_smarty_tpl->tpl_vars['prices']->value['card'];?>
'>
				<hgroup class="clearfix">
					<h1 class="b">Любимое</h1>
					<a class="edit" href='constructor.php?card=6'>Изменить</a>
				</hgroup>
				<div class='item_content clearfix'>
					<div data-side='front' class='card_side'>
						<p>Лицевая сторона</p>
						<figure></figure>
					</div>
					<div data-side='back' class='card_side'>
						<p>Обратная сторона</p>
						<figure></figure>
					</div>
				</div>
				<aside class='clearfix'>
					<input type='checkbox' id='card_6_order' checked>
					<label class='g' for="card_6_order">Добавить в корзину</label>
				</aside>
			</section>
			<section class='cart_item wrapper' data-card_id='7' data-price='<?php echo $_smarty_tpl->tpl_vars['prices']->value['card'];?>
'>
				<hgroup class="clearfix">
					<h1 class="b">Путешествую</h1>
					<a class="edit" href='constructor.php?card=7'>Изменить</a>
				</hgroup>
				<div class='item_content clearfix'>
					<div data-side='front' class='card_side'>
						<p>Лицевая сторона</p>
						<figure></figure>
					</div>
					<div data-side='back' class='card_side'>
						<p>Обратная сторона</p>
						<figure></figure>
					</div>
				</div>
				<aside class='clearfix'>
					<input type='checkbox' id='card_7_order' checked>
					<label class='g' for="card_7_order">Добавить в корзину</label>
				</aside>
			</section>
			<section class='cart_item wrapper' data-card_id='8' data-price='<?php echo $_smarty_tpl->tpl_vars['prices']->value['card'];?>
'>
				<hgroup class="clearfix">
					<h1 class="b">Вот и год прошел</h1>
					<a class="edit" href='constructor.php?card=8'>Изменить</a>
				</hgroup>
				<div class='item_content clearfix'>
					<div data-side='front' class='card_side'>
						<p>Лицевая сторона</p>
						<figure></figure>
					</div>
					<div data-side='back' class='card_side'>
						<p>Обратная сторона</p>
						<figure></figure>
					</div>
				</div>
				<aside class='clearfix'>
					<input type='checkbox' id="card_8_order" checked>
					<label class='g' for="card_8_order">Добавить в корзину</label>
				</aside>
			</section>
			<?php
$_from = $_smarty_tpl->tpl_vars['additional']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['card_title'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['card_title']->_loop = false;
$_smarty_tpl->tpl_vars['card_id'] = new Smarty_Variable;
foreach ($_from as $_smarty_tpl->tpl_vars['card_id']->value => $_smarty_tpl->tpl_vars['card_title']->value) {
$_smarty_tpl->tpl_vars['card_title']->_loop = true;
$foreach_card_title_Sav = $_smarty_tpl->tpl_vars['card_title'];
?>
			<section class='cart_item wrapper' data-card_id='<?php echo $_smarty_tpl->tpl_vars['card_id']->value;?>
' data-price='<?php echo $_smarty_tpl->tpl_vars['prices']->value['card'];?>
'>
				<hgroup class="clearfix">
					<h1 class="b"><?php echo $_smarty_tpl->tpl_vars['card_title']->value;?>
</h1>
					<a class="edit" href='constructor.php?card=<?php echo $_smarty_tpl->tpl_vars['card_id']->value;?>
'>Изменить</a>
				</hgroup>
				<div class='item_content clearfix'>
					<div data-side='front' class='card_side'>
						<p>Лицевая сторона</p>
						<figure></figure>
					</div>
					<div data-side='back' class='card_side'>
						<p>Обратная сторона</p>
						<figure></figure>
					</div>
				</div>
				<aside class='clearfix'>
					<input type='checkbox' id="card_<?php echo $_smarty_tpl->tpl_vars['card_id']->value;?>
_order" checked>
					<label class='g' for="card_<?php echo $_smarty_tpl->tpl_vars['card_id']->value;?>
_order">Добавить в корзину</label>
				</aside>
			</section>
			<?php
$_smarty_tpl->tpl_vars['card_title'] = $foreach_card_title_Sav;
}
?>
		</section>
	</section>
	<section class="page_title two">
		<section class="content clearfix">
			<h1>Оформление заказа</h1>
		</section>
	</section>
	<section class="three">
		<section class="content wrapper clearfix">
			<section class='contacts clearfix'>
				<h2 class="b g">Контактное лицо</h2>
				<input type="text" placeholder="Имя фамилия" class="gray name" value="<?php if (!empty($_smarty_tpl->tpl_vars['profile']->value['me']['f_name'])) {
echo $_smarty_tpl->tpl_vars['profile']->value['me']['f_name'];
}
if (!empty($_smarty_tpl->tpl_vars['profile']->value['me']['l_name'])) {?> <?php echo $_smarty_tpl->tpl_vars['profile']->value['me']['l_name'];
}?>">
				<input type="text" placeholder="Номер телефона" class="gray phone">
			</section>
			<section class='delivery'>
				<h2 class="b g">Доставочка<span>Мы доставляем книги по всей России </span></h2>
				<ul>
					<li class="b" data-type='0'>Самовывоз в Москве</li>
					<li class="b" data-type='1'>Доставка курьером</li>
				</ul>
				<section class='tabs'>
					<div>
						<p>
							Книгу можно будет забрать по адресу<br>
							Ул. <?php echo $_smarty_tpl->tpl_vars['general']->value['delivery_street'];?>
 дом <?php echo $_smarty_tpl->tpl_vars['general']->value['delivery_house'];?>
, оф. <?php echo $_smarty_tpl->tpl_vars['general']->value['delivery_appartment'];?>

						</p>
					</div>
					<div class='clearfix'>
						<select class='region'>
							<option selected disabled>Выберите регион</option>
							<?php
$_from = $_smarty_tpl->tpl_vars['regions']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['region'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['region']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['region']->value) {
$_smarty_tpl->tpl_vars['region']->_loop = true;
$foreach_region_Sav = $_smarty_tpl->tpl_vars['region'];
?>
							<option value="<?php echo $_smarty_tpl->tpl_vars['region']->value['region_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['region']->value['name'];?>
</option>
							<?php
$_smarty_tpl->tpl_vars['region'] = $foreach_region_Sav;
}
?>
						</select>
						<select class='city' style='display: none;'>

						</select>
						<input type='text' class='street gray' placeholder='Улица'>
						<input type='text' class='house gray' placeholder='Дом'>
						<input type='text' class='appartment gray' placeholder='Кварт.'>
						<textarea placeholder='Комментарии к заказу' class='order_comments'></textarea>
					</div>
				</section>
			</section>
		</section>
	</section>
	<section class="page_title">
		<section class="content clearfix">
			<h1>Корзина</h1>
		</section>
	</section>
	<section class="five">
		<section class="content wrapper clearfix">
			<section class='row title clearfix'>
				<div>
					<p>Наименование товара</p>
				</div>
				<div>
					<p>Количество</p>
				</div>
				<div>
					<p>Цена</p>
				</div>
			</section>
			<section class='row clearfix box_cart'>
				<div>
					<h2 class="b g">Коробка</h2>
				</div>
				<div>
					<input type='number' value="1" min="1" class="b g box_quantity">
				</div>
				<div>
					<h2 class="b g box_total"><span>0</span> ₽</h2>
				</div>
			</section>
			<section class='row clearfix'>
				<div>
					<h2 class="b g cart_purchase_info">Книга (0 карточек)</h2>
				</div>
				<div>
					<input type='number' value="1" min="1" class="b g cards_quantity">
				</div>
				<div>
					<h2 class="b g cards_total"><span>0</span> ₽</h2>
				</div>
			</section>
			<section class='cards_details row clearfix' style='border-bottom: 0;'>
				<ul>
					
				</ul>
			</section>
			<section class='row clearfix'>
				<div>
					<h2 class="b g delivery_type" data-text="Курьером">Самовывоз</h2>
				</div>
				<div>
					
				</div>
				<div>
					<h2 class="b g"><span class="delivery_type_span" data-text="Уточняется менеджером">0</span> <span class="currency">₽</span></h2>
				</div>
			</section>
			<section class='coupon clearfix'>
				<a class='b activate_coupon'>Применить</a>
				<input type='text' class='gray coupon_code'>
				<p>Активация купона:</p>			
				<p>Сумма скидок по заказу:<span class='spacing'><a class='discount_amount'>0</a> ₽</span></p>
				<p>Итого:<span class='spacing b g'><a class='total_final'>0</a> ₽</span></p>
			</section>
		</section>
	</section>
	<section class="six">
		<a class="green_button_arrow b" id='place_order'>Перейти к оплате</a>
	</section>
	<section class='thanks'>
		<section class='thanks_content'>
			<article>
				<p>
					Поздравляем, о том что вы заказали книжечку<br>
					уже знает наш менеджер, он обязательно<br>
					перезвонит вам в ближайшее время
				</p>
				<a href='orders.php'>Перейти к моим заказам</a>
			</article>
		</section>
	</section>
</section>
<?php echo '<script'; ?>
 src="js/routes/cart.js"><?php echo '</script'; ?>
>
<?php echo $_smarty_tpl->getSubTemplate ('footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);

}
}
?>