<?php /* Smarty version 3.1.27, created on 2015-10-15 08:26:39
         compiled from "/var/www/quantum/webpoetry.org/bookagoo/docs/app/smarty/templates/constructor_4.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:880221036561f9b7fce15e1_91788158%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'bff672ab96a3c66050cfa12258d3282476a65b27' => 
    array (
      0 => '/var/www/quantum/webpoetry.org/bookagoo/docs/app/smarty/templates/constructor_4.tpl',
      1 => 1444911858,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '880221036561f9b7fce15e1_91788158',
  'variables' => 
  array (
    'constructor' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_561f9b7fcfa071_36638325',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_561f9b7fcfa071_36638325')) {
function content_561f9b7fcfa071_36638325 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '880221036561f9b7fce15e1_91788158';
?>
<!-- Card 4 -->
<section class="card card_4 front" data-side='front'>
	<h1 class="card_title">
		Моя ручка и ножка
	</h1>
	<section class="card_content">
		<p class='slim_title'>
			Моя ручка
		</p>
		<p class='notice'>
			Если вы хотите добавить отпечаток<br>
			или рисунок ручки малыша<br>
			в печатную версию книги,<br>
			просто оставьте эту страницу пустой.
		</p>
		<div class='photo' data-editor=true data-constructor_var="0">
			<div class='inner'>
				<p>
					<span>+</span><br>
					Добавить фото
				</p>
			</div>
			<?php if (isset($_smarty_tpl->tpl_vars['constructor']->value[0])) {
echo $_smarty_tpl->tpl_vars['constructor']->value[0];
}?>
			<input type="file" name="photo" class="load_image hidden">
		</div>
	</section>
</section>

<section class="card card_4 back" data-side='back'>
	<h1 class="card_title">
		Моя ручка и ножка
	</h1>
	<section class="card_content">
		<p class='slim_title'>
			Моя ножка
		</p>
		<p class='notice'>
			Если вы хотите добавить отпечаток<br>
			или рисунок ножки малыша<br>
			в печатную версию книги,<br>
			просто оставьте эту страницу пустой.
		</p>
		<div class='photo' data-editor=true data-constructor_var="1">
			<div class='inner'>
				<p>
					<span>+</span><br>
					Добавить фото
				</p>
			</div>
			<?php if (isset($_smarty_tpl->tpl_vars['constructor']->value[1])) {
echo $_smarty_tpl->tpl_vars['constructor']->value[1];
}?>
			<input type="file" name="photo" class="load_image hidden">
		</div>
	</section>
</section>

<!-- End of Card 4 --><?php }
}
?>