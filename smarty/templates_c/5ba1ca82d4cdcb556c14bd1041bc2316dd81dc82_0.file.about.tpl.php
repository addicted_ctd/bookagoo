<?php /* Smarty version 3.1.27, created on 2015-10-02 08:23:01
         compiled from "/home/quantum/webpoetry.org/bookagoo/smarty/templates/about.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:556141249560e7725065027_12414519%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5ba1ca82d4cdcb556c14bd1041bc2316dd81dc82' => 
    array (
      0 => '/home/quantum/webpoetry.org/bookagoo/smarty/templates/about.tpl',
      1 => 1443788499,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '556141249560e7725065027_12414519',
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_560e77250752c6_58979187',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_560e77250752c6_58979187')) {
function content_560e77250752c6_58979187 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '556141249560e7725065027_12414519';
echo $_smarty_tpl->getSubTemplate ('header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>'Мои заказы'), 0);
?>

<section id="about" class='page'>
	<section class="one">
		<section class="content clearfix">
			<article>
				<h1 class="b">Кто мы?</h1>
				<p>
					Компания основана в 2013 году в Москве.<br>
					BookAgoo - это сплоченная команда единомышленников,<br>
					друзей, а также родителей. Собственный опыт, знания и<br>
					одержимость современными цифровыми технологиями<br>
					помогают нам двигаться и совершенствоваться каждый<br>
					день. Мы работаем, чтобы сделать жизнь вашей семьи<br>
					ярче и интереснее.
				</p>
			</article>
		</section>
	</section>
	<section class='two'>
		<section class="content clearfix">
			<section class='left'>
				<figure></figure>
				<figure></figure>
			</section>
			<section class='right'>
				<article>
					<h1 class="g b">Наш продукт</h1>
					<p>
						Книга первых лет жизни малыша в коробке – оригинальное, простое и красивое 
						решение для любого родителя, желающего сохранить незабываемые моменты жизни всей семьи.<br>
						Мы используем только высококачественные материалы, работаем с проверенными
						поставщиками и отвечаем за финальный
						результат.
					</p>
				</article>
			</section>
		</section>
	</section>
</section>
<?php echo $_smarty_tpl->getSubTemplate ('footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);

}
}
?>