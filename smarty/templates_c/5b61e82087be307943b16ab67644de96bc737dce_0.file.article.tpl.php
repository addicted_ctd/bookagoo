<?php /* Smarty version 3.1.27, created on 2015-11-07 17:50:51
         compiled from "/var/www/quantum/webpoetry.org/bookagoo/docs/app/smarty/templates/article.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:144738939563e804bb52e06_81091771%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5b61e82087be307943b16ab67644de96bc737dce' => 
    array (
      0 => '/var/www/quantum/webpoetry.org/bookagoo/docs/app/smarty/templates/article.tpl',
      1 => 1445265086,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '144738939563e804bb52e06_81091771',
  'variables' => 
  array (
    'post' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_563e804bb95dc0_55456854',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_563e804bb95dc0_55456854')) {
function content_563e804bb95dc0_55456854 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '144738939563e804bb52e06_81091771';
echo $_smarty_tpl->getSubTemplate ('header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>'Главная страница'), 0);
?>

<section id="news" class='page'> 
    <section class="one page_title"> 
        <section class="content">
            <h1>Новости</h1>
        </section> 
    </section> 
    <section class="two clearfix"> 
        <section class="content clearfix"> 
            <section class="left">
                <section id="article">
                    <article class="clearfix">
                        <?php echo $_smarty_tpl->tpl_vars['post']->value['tpl'];?>

                    </article>
                    <aside class='social_shares clearfix'>
                        <a href="https://twitter.com/share" class="twitter-share-button" data-url="http://webpoetry.org/bookagoo/docs/app/article.php" data-text="yey">Tweet</a>
                        
                        <?php echo '<script'; ?>
>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');<?php echo '</script'; ?>
>
                        
                        <div class="fb-like" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div>
                    </aside>
                    <section class='comment'>
                        <div id="disqus_thread"></div>
                        
                        <?php echo '<script'; ?>
 type="text/javascript">
                            var disqus_shortname = 'bookagooweb';
                            (function() {
                                var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
                                dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
                                (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
                            })();
                        <?php echo '</script'; ?>
>
                        
                    </section>
                </section>
            </section> 
            <section class="right"> 
                <?php echo $_smarty_tpl->getSubTemplate ('sidebar.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

            </section> 
        </section>
    </section>
</section>
<?php echo $_smarty_tpl->getSubTemplate ('footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);

}
}
?>