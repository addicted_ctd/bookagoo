<?php /* Smarty version 3.1.27, created on 2015-11-09 15:18:59
         compiled from "/var/www/quantum/webpoetry.org/bookagoo/docs/app/smarty/templates/preview.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:12299883485640ffb33d4b96_28444768%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '15429be146cdc03de67d943786f4a0fc5339b77a' => 
    array (
      0 => '/var/www/quantum/webpoetry.org/bookagoo/docs/app/smarty/templates/preview.tpl',
      1 => 1446064289,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '12299883485640ffb33d4b96_28444768',
  'variables' => 
  array (
    'additional' => 0,
    'card_id' => 0,
    'card_title' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_5640ffb3442bc7_27879721',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_5640ffb3442bc7_27879721')) {
function content_5640ffb3442bc7_27879721 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '12299883485640ffb33d4b96_28444768';
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Bookagoo</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
        <meta name="interkassa-verification" content="9ef9cd47240e57ea66a1b0ac96a0f500" />
        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/preview.css">
        <?php echo '<script'; ?>
 src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="js/vendor/modernizr-2.6.2.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="js/plugins.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="js/vendor/lettering.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="js/vendor/jquery.maskedinput.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="http://vkontakte.ru/js/api/openapi.js" type="text/javascript"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="js/global.js"><?php echo '</script'; ?>
>
    </head>
    <body>
        <!--[if lt IE 7]>
            <?php echo '<script'; ?>
 src="js/vendor/css3pie/PIE.js" type="text/javascript"><?php echo '</script'; ?>
>
            <?php echo '<script'; ?>
 src="js/vendor/html5shiv/html5shiv.js" type="text/javascript"><?php echo '</script'; ?>
>
        <![endif]-->

        
        <?php echo '<script'; ?>
>
            window.fbAsyncInit = function() {
                FB.init({
                    appId: '497937203715073',
                    xfbml: true,
                    version: 'v2.4'
                });
            };

            (function(d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) {
                    return;
                }
                js = d.createElement(s);
                js.id = id;
                js.src = "//connect.facebook.net/en_US/sdk.js";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));


            VK.init({
                apiId: 5097470
            });

        <?php echo '</script'; ?>
>
        

        <header>
            <section class="content clearfix">
                <a class="logo" href="index.php"></a>
            </section>
        </header>
        <section id="constructor" class='page preview' style="padding-top: 100px;">

            <section class="clearfix content">
                <section class="constructor_content">

                    <?php if ($_GET['card'] == 1) {?>

                    <?php echo $_smarty_tpl->getSubTemplate ('constructor_1.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>


                    <?php } elseif ($_GET['card'] == 2) {?>

                    <?php echo $_smarty_tpl->getSubTemplate ('constructor_2.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>


                    <?php } elseif ($_GET['card'] == 3) {?>

                    <?php echo $_smarty_tpl->getSubTemplate ('constructor_3.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>


                    <?php } elseif ($_GET['card'] == 4) {?>

                    <?php echo $_smarty_tpl->getSubTemplate ('constructor_4.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>


                    <?php } elseif ($_GET['card'] == 5) {?>

                    <?php echo $_smarty_tpl->getSubTemplate ('constructor_5.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>


                    <?php } elseif ($_GET['card'] == 6) {?>

                    <?php echo $_smarty_tpl->getSubTemplate ('constructor_6.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>


                    <?php } elseif ($_GET['card'] == 7) {?>

                    <?php echo $_smarty_tpl->getSubTemplate ('constructor_7.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>


                    <?php } elseif ($_GET['card'] == 8) {?>

                    <?php echo $_smarty_tpl->getSubTemplate ('constructor_8.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>


                    <?php } elseif ($_GET['card'] > 8) {?>

                    <?php echo $_smarty_tpl->getSubTemplate ('constructor_new.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>


                    <?php }?>

                </section>

                <section class="constructor_menu">
                    <section class="items">
                        <a href='preview.php?u=<?php echo $_GET['u'];?>
&card=1'>
                            Обложка
                        </a>
                        <a href='preview.php?u=<?php echo $_GET['u'];?>
&card=2'>
                            Привет,<br>
                            а вот и я!
                        </a>
                        <a href='preview.php?u=<?php echo $_GET['u'];?>
&card=3'>
                            Моя семья
                        </a>
                        <a href='preview.php?u=<?php echo $_GET['u'];?>
&card=4'>
                            Моя ручка<br>
                            и ножка
                        </a>
                        <a href='preview.php?u=<?php echo $_GET['u'];?>
&card=5'>
                            Я расту
                        </a>
                        <a href='preview.php?u=<?php echo $_GET['u'];?>
&card=6'>
                            Любимое
                        </a>
                        <a href='preview.php?u=<?php echo $_GET['u'];?>
&card=7'>
                            Путешествую
                        </a>
                        <a href='preview.php?u=<?php echo $_GET['u'];?>
&card=8'>
                            Вот и год<br>
                            прошел
                        </a>
                        <?php
$_from = $_smarty_tpl->tpl_vars['additional']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['card_title'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['card_title']->_loop = false;
$_smarty_tpl->tpl_vars['card_id'] = new Smarty_Variable;
foreach ($_from as $_smarty_tpl->tpl_vars['card_id']->value => $_smarty_tpl->tpl_vars['card_title']->value) {
$_smarty_tpl->tpl_vars['card_title']->_loop = true;
$foreach_card_title_Sav = $_smarty_tpl->tpl_vars['card_title'];
?>
                        <a href='preview.php?u=<?php echo $_GET['u'];?>
&card=<?php echo $_smarty_tpl->tpl_vars['card_id']->value;?>
'>
                            <?php echo $_smarty_tpl->tpl_vars['card_title']->value;?>

                        </a>
                        <?php
$_smarty_tpl->tpl_vars['card_title'] = $foreach_card_title_Sav;
}
?>
                    </section>
                </section>
            </section>

        </section>

        <?php echo '<script'; ?>
> var Constructor = {}; Constructor.card = <?php echo $_GET['card'];?>
;
            
            $('.items').find('a').eq(Constructor.card - 1).addClass('active'); $('input, textarea').prop('disabled', true); $('[contenteditable]').removeAttr('contenteditable'); $('.photo').addClass('final');<?php echo '</script'; ?>
>

        <?php echo '<script'; ?>
 src="js/vendor/jquery.form.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="js/main.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="js/routes/preview.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="js/vendor/css3finalize/jquery.css3finalize.min.js" type="text/javascript"><?php echo '</script'; ?>
>
        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <?php echo '<script'; ?>
>
            var _gaq = [['_setAccount', 'UA-XXXXX-X'], ['_trackPageview']];
            (function(d, t) {
                var g = d.createElement(t), s = d.getElementsByTagName(t)[0];
                g.src = '//www.google-analytics.com/ga.js';
                s.parentNode.insertBefore(g, s)
            }(document, 'script'));
        <?php echo '</script'; ?>
>
    </body>
</html><?php }
}
?>