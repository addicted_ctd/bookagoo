<?php /* Smarty version 3.1.27, created on 2015-10-19 17:16:11
         compiled from "/var/www/quantum/webpoetry.org/bookagoo/docs/app/smarty/templates/news.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:121477957156255d9b4d48d5_55374160%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3a097daab323ba5b1ab434be4bd396c0a062fc91' => 
    array (
      0 => '/var/www/quantum/webpoetry.org/bookagoo/docs/app/smarty/templates/news.tpl',
      1 => 1445264185,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '121477957156255d9b4d48d5_55374160',
  'variables' => 
  array (
    'news' => 0,
    'item' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_56255d9b4f50b0_46708516',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_56255d9b4f50b0_46708516')) {
function content_56255d9b4f50b0_46708516 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '121477957156255d9b4d48d5_55374160';
echo $_smarty_tpl->getSubTemplate ('header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>'Новости'), 0);
?>

<section id="news" class='page'> 
    <section class="one page_title"> 
        <h1 class="content">Новости</h1>
    </section> 
    <section class="two clearfix"> 
        <section class="content clearfix"> 
            <section class="left">
                <?php
$_from = $_smarty_tpl->tpl_vars['news']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['item']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
$foreach_item_Sav = $_smarty_tpl->tpl_vars['item'];
?>
                <article class="news_preview clearfix">
                    <figure style="background-image: url(<?php echo $_smarty_tpl->tpl_vars['item']->value['thumb'];?>
);">
                        <span class="date b"><?php echo $_smarty_tpl->tpl_vars['item']->value['date'];?>
</span>
                    </figure>
                    <div>
                        <h2 class="title b"><?php echo $_smarty_tpl->tpl_vars['item']->value['title'];?>
</h2>
                        <p class="lead">
                            <?php echo $_smarty_tpl->tpl_vars['item']->value['lead'];?>
      
                        </p>
                        <a href="article.php?read=<?php echo $_smarty_tpl->tpl_vars['item']->value['slug'];?>
" class="b">read more</a>
                    </div>
                </article>
                <?php
$_smarty_tpl->tpl_vars['item'] = $foreach_item_Sav;
}
?>
            </section> 
            <section class="right"> 
                <?php echo $_smarty_tpl->getSubTemplate ('sidebar.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);
?>

            </section> 
        </section>
    </section>
</section>
<?php echo $_smarty_tpl->getSubTemplate ('footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0);

}
}
?>