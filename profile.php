<?php

require 'smarty_init.php';

if (!$login_status) {
	Header("Location: auth.php");
}
$profile = $ctd::exec('user.get_profile');

$smarty->assign('profile', $profile);

$smarty->display('profile.tpl');
