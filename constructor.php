<?php

require 'smarty_init.php';

if (!$login_status) {
	Header("Location: auth.php");
}

$child_info = $ctd::exec('constructor.get_child_info');
$child_info = $child_info[0];

$month = ['Января', 'Февраля', 'Марта', 'Апреля', 'Мая', 'Июня', 'Июля', 'Августа', 'Сентября', 'Октября', 'Ноября', 'Декабря'];

$birth = explode('.', $child_info['birth_date']);
if ($birth[1] !== '00') {
	$child_info['birth_full'] = $birth[0] . ' ' . $month[(int) $birth[1] - 1] . ' ' . $birth[2];
} else {
	$child_info['birth_full'] = '1 января 2014';
}

if (isset($_COOKIE['first_login']) && $_COOKIE['first_login']) {
	unset($_COOKIE['first_login']);
	setcookie('first_login', null, -1, '/');
	$smarty->assign('first_login', true);
}

$additional = $ctd::exec('constructor.get_additional');
end($additional);
$offset = (int) key($additional);
if ($offset === 0) {
	$offset = 8;
}
$new_card_id = $offset + 1;

$constructor = $ctd::exec('constructor.get_vars', (object) array('card_id' => $_GET['card']));

$smarty->assign('new_card_id', $new_card_id);
$smarty->assign('additional', $additional);
$smarty->assign('constructor', $constructor);
$smarty->assign('child_info', $child_info);
$smarty->display('constructor.tpl');