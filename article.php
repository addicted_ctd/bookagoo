<?php

require 'smarty_init.php';
$smarty->clearCompiledTemplate('article.tpl');
if (isset($_GET['read'])) {
	$post = $ctd::exec('posts.get_post', (object) array('slug' => $_GET['read']));
	$smarty->assign('post', $post);
	$smarty->display('article.tpl');
}