<?php

    

    $order_id = $_GET['order_id'];

    $images = 'order/' . $order_id . '/';
    $zip_name = 'order_' . $order_id . '.tar';
    $zip_file = 'order/' . $zip_name;
    $gz_name = 'order_' . $order_id . '.tar.gz';

    $files = array_diff(scandir($images), array('..', '.'));

    $archive = new PharData($zip_file);
    foreach ($files as $file) {
        $archive->addFile($images . $file);
    }

    header("Pragma: public");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Cache-Control: public");
    header("Content-Description: File Transfer"); 
    header("Content-type: application/octet-stream");
    header("Content-Disposition: attachment; filename='$zip_name'");
    header("Content-Transfer-Encoding: binary");
    header("Content-Length: ".filesize($zip_file));
    while (ob_get_level()) {
        ob_end_clean();
    } 
    @readfile($zip_file);

    unset($archive);
    unlink($zip_file);
    echo "<script>window.close();</script>";
    exit;
?>