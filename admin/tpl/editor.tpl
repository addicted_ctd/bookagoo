<template id="editor" type="text/html">
    <section id="editor">
        <section class="editor_body">
            <section class="top_menu">
                <h1 class="section_title">
                    <span onclick="router.navigate('posts/' + editor.id, {trigger: true});">Post info</span>
                    <span class="active">Content</span>
                </h1>
                <button onclick="router.navigate('posts', {trigger: true});" style="opacity: 1;"><span class="entypo-left-open" style="margin-right: 15px;"></span>Back</li>
                <button id="publish" class="button_tpl">Publish</button>
                <button id="save" class="button_tpl">Save</button>
                <button id="delete">Delete</button>
                <aside class="close" data-parent="index"></aside>
            </section>
            <section class="editor_wrapper clearfix">
                <section class="sight_wrapper">
                    <div class="construction_sight">
                    {{#tpl}}
                        {{{dev_file}}}
                    {{/tpl}}
                    {{^tpl}}
                        <header class="top_header clearfix">
                            <h4 id='date'><span>18</span> <span>апреля</span> <span>2014</span></h4>
                            <div class="photo premade fixed block_drag">
                                <upload class="pre_hidden upload_photo helper">Upload</upload>
                                <img class="photo_image_seo" itemprop="image">
                                <figure class="photo_image pre_hidden" data-zoom="1" id="top_pic"></figure>
                                <hint class="pre_hidden helper"></hint>
                                <form class="secret helper" enctype="multipart/form-data" method="post" action="php/router.php">
                                    <input type="file" name="photo" class="load_image">
                                    <input type="hidden" name="mod" value="root.upload_image">
                                </form>
                            </div>
                            <aside>
                                <h1 contenteditable="true" id="title" itemprop="name">Article
                                title</h1>
                                <div class="text premade block_drag fixed lead">
                                    <p itemprop="text" contenteditable="true" class="fixed_child">Regular text</p>
                                </div>
                            </aside>
                        </header>
                        <section class="main" id="premade_box">
                            <section class="content main_content clearfix" itemprop="articleBody"></section>
                        </section>
                    {{/tpl}}
                    </div>
                </section>
                <div class="premade_blocks">
                    <div class="header premade drag_me">
                        <p itemprop="text">Heading</p>
                    </div>
                    <div class="emphasis premade drag_me">
                        <p itemprop="text">Emphasis</p>
                    </div>
                    <div class="text premade drag_me">
                        <p itemprop="text">Regular text</p>
                    </div>
                    <div class="quote premade drag_me">
                        <p itemprop="text">Quote</p>
                    </div>
                    <div class="photo premade drag_me">
                        <p class="remove_in_fixed">Photo</p>
                        <upload class="pre_hidden upload_photo helper">Upload</upload>
                        <figure class="photo_image pre_hidden" zoom="1"></figure>
                        <img class="photo_image_seo" itemprop="image">
                        <hint class="pre_hidden helper"></hint>
                        <form class="secret helper" enctype="multipart/form-data" method="post" action="php/router.php">
                            <input type="hidden" name="mod" value="root.upload_image">
                            <input type="file" name="photo" class="load_image">
                        </form>
                    </div>
                    <div class="gallery premade drag_me">
                        <p class="remove_in_fixed">Gallery</p>
                        <gallery_wrapper>
                            <gallery class='clearfix'>
                                <div class="photo premade fixed block_drag exception pre_hidden gallery_item block_resize">
                                    <upload class="pre_hidden upload_photo helper">Uplaod</upload>
                                    <figure class="photo_image pre_hidden" zoom="1"></figure>
                                    <img class="photo_image_seo" itemprop="image">
                                    <hint class="pre_hidden helper"></hint>
                                    <form class="secret helper" enctype="multipart/form-data" method="post" action="php/router.php">
                                        <input type="file" name="photo" class="load_image">
                                        <input type="hidden" name="mod" value="root.upload_image">
                                    </form>
                                </div>
                            </gallery>
                        </gallery_wrapper>
                        <thumbs class='clearfix'>
                            <thumb></thumb>
                        </thumbs>
                    </div>
                    <div class="video premade drag_me" itemprop="video">
                        <p class="remove_in_fixed">Video</p>
                        <outer_wrapper class="pre_hidden helper">
                            <wrapper class="pre_hidden helper">
                                <input type="text" placeholder="Видео URL" class="video_url">
                                <button class="submit_url">ok</button>
                            </wrapper>
                        </outer_wrapper>
                    </div>
                </div>
            </section>
        </section>
    </section>
</template>

<template id="resize_box" type="text/html">
    <aside id="resize_box" class="helper">
        <div>
            <span class="resizer"></span>
        </div>
    </aside>
</template>

<template id="selection_tooltip" type="text/html">
    <aside id="selection_tooltip" data-init_w="84" class='helper'>
        <wrapper>
            <div class="first_level">
                <tools class="clearfix">
                    <span data-apply="bold">B</span>
                    <span data-apply="italic">I</span>
                    <span data-apply="link"></span>
                </tools>
            </div>
            <div class="second_level">
                <add_link>
                    <input type="text" value="http://" data-hint="http://">
                    <button>ok</button>
                </add_link>
            </div>
        </wrapper>
    </aside>
</template>

<template id="video_frame" type="text/html">
    <iframe width="100%" height="100%" src="//www.youtube.com/embed/{{url}}?wmode=opaque" frameborder="0" allowfullscreen></iframe>
</template>

<template id="side_tooltip" type="text/html">
    <side_tt id="side_tooltip" class='helper'>
        <wrapper>
            <span data-action="remove"></span>
            <span data-action="settings"></span>
        </wrapper>
    </side_tt>  
</template>

<template id="gallery_item" type="text/html">
    <div class="photo premade fixed block_drag exception pre_hidden gallery_item helper block_resize">
        <upload class="pre_hidden upload_photo helper">Upload</upload>
        <figure class="photo_image pre_hidden" zoom="1"></figure>
        <img class="photo_image_seo" itemprop="image">
        <hint class="pre_hidden helper"></hint>
        <form class="secret helper" enctype="multipart/form-data" method="post" action="php/router.php">
            <input type="hidden" name="mod" value="root.upload_image">
            <input type="file" name="photo" class="load_image">
        </form>
    </div>
</template>