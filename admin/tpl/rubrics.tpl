<template id="rubrics" type="text/html">
<section id="rubrics">
	<section class="content clearfix">

	</section>
</section>
</template>

<template id="rubrics_list" type="text/html">
<section class="rubrics_list">
	<ul>
	{{#data}}
	<li data-id="{{id}}" class="clearfix">
		{{#image}}
		<figure style="background-image: url({{image}});"></figure>
		{{/image}}
		{{^image}}
		<figure><span class="entypo-picture"></span></figure>
		{{/image}}
		<hgroup>
			<h3>{{name}}</h3>
		</hgroup>
		<p>Pages: {{children}}</p>
		<div onclick="router.navigate('rubrics/editor/{{id}}', {trigger: true});"><span class="entypo-feather"></span><p>edit</p></div>
	</li>
	{{/data}}
	<li id="add_rubric" onclick="router.navigate('rubrics/editor/new', {trigger: true});"></li>
	</ul>
</section>
</template>

<template id="rubrics_editor" type="text/html">
<section id="rubrics">
	<header class="clearfix">
		<h1 class="hover" onclick="router.navigate('rubrics', {trigger: true});"><span class="entypo-left-open"></span>Back</h1>
		<h1 class="hover" id="save">Save</h1>
		{{#data.id}}<h1 class="hover" id="delete" style="color: #bb4036">Delete</h1>{{/data.id}}
	</header>
	<section class="content clearfix">
		<section class="rubric_editor clearfix {{^data.id}}new{{/data.id}}">
			<section class="info">
				<h1><span class="entypo-docs"></span>Info</h1>
				<div class="clearfix">
					<p>Name:</p>
					<input type="text" value="{{data.name}}" class="name">
				</div>
				<div class="clearfix">
					<p>Image:</p>
					{{#data.image}}
					<figure class="image" data-image="{{data.image}}" style="background-image: url({{data.image}})"></figure>
					{{/data.image}}
					{{^data.image}}
					<figure class="image" data-image=""><span class="entypo-picture"></span></figure>
					{{/data.image}}
					<aside><button class="entypo-upload upload hover"></button>
						<form enctype="multipart/form-data" method="post" action="php/router.php">
			                <input type="file" name="photo" class="load_image">
			                <input type="hidden" name="mod" value="root.upload_image">
			            </form>
					</aside>
				</div>
				<div class="clearfix">
					<p>Description:</p>
					<textarea rows="5" class="description">{{data.description}}</textarea>
				</div>
			</section>
			<section class="custom_fields">
				<h1><span class="entypo-list"></span>Custom fields</h1>
				<ul>
					
				</ul>
			</section>
		</section>
	</section>
</section>
</template>

<template id="custom_fields" type="text/html">
<aside>
	<custom_select value="0" class="type">
		<li value="0" selected>Short text</li>
		<li value="1">Text</li>
		<li value="2">Number</li>
		<li value="3">File</li>
	</custom_select>
	<input type="text" class="name" placeholder="Name">
	<input type="text" class="title" placeholder="Title">
	<input type="text" class="default" placeholder="Default value">
	<button class="entypo-upload upload_default hover"></button>
	<form enctype="multipart/form-data" method="post" action="php/router.php">
	    <input type="file" name="photo" class="default_file">
	    <input type="hidden" name="mod" value="root.upload_image">
	</form>
	<button class="hover" id="add_custom_field"><span class="entypo-plus-circled"></span>Add</button>
</aside>
{{#data}}
<aside data-id="{{id}}">
	<custom_select value="0" class="type">
		<li value="0" selected>Short text</li>
		<li value="1">Text</li>
		<li value="2">Number</li>
		<li value="3">File</li>
	</custom_select>
	<input type="text" class="name" placeholder="Name" value="{{name}}">
	<input type="text" class="title" placeholder="Title" value="{{title}}">
	<input type="text" class="default" placeholder="Default value" value="{{default_val}}">
	<button class="entypo-upload upload_default hover"></button>
	<form enctype="multipart/form-data" method="post" action="php/router.php">
	    <input type="file" name="photo" class="default_file">
	    <input type="hidden" name="mod" value="root.upload_image">
	</form>
	<button class="hover" onclick="Rubrics.remove_custom_field({{id}}, '{{name}}')"><span class="entypo-cancel"></span>Remove</button>
	<script>
		$("aside[data-id={{id}}]").find("custom_select").find("li[value={{type}}]").click();
		$("aside[data-id={{id}}]").find("custom_select").click();
	</script>
</aside>
{{/data}}
</template>