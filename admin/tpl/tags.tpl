<template id="tags" type="text/html">
<section id="tags">
	<header class="clearfix">
		<h1 class="hover" id="add_tag"><span class="entypo-plus"></span>Add</h1>
	</header>
	<section class="content clearfix">

	</section>
</section>
</template>

<template id="tags_list" type="text/html">
<section class="tags_list">
	<aside class="switch">
		<span class="entypo-menu hover active"></span>
		<span class="entypo-cloud hover"></span>
	</aside>
	<ul>
	{{#data}}
	<li data-id="{{id}}"><span class="hover">{{name}}</span></li>
	{{/data}}
	</ul>
</section>
</template>

<template id="tag_editor" type="text/html">
	<section class="tag_editor {{#data.new}}new{{/data.new}}">
		<h1>{{#data.new}}<span class="entypo-plus"></span>Create new tag{{/data.new}}{{^data.new}}<span class="entypo-pencil"></span>Tag Editor{{/data.new}}</h1>
		<div class="clearfix">
			<p>Name:</p>
			<input type="text" value="{{data.name}}" class="name">
		</div>
		<div class="clearfix">
			<p>Image:</p>
			{{#data.image}}
			<figure class="image" data-image="{{data.image}}" style="background-image: url({{data.image}})"></figure>
			{{/data.image}}
			{{^data.image}}
			<figure class="image" data-image=""><span class="entypo-picture"></span></figure>
			{{/data.image}}
			<aside><button class="entypo-upload upload hover"></button>
				<form enctype="multipart/form-data" method="post" action="php/router.php">
	                <input type="file" name="photo" class="load_image">
	                <input type="hidden" name="mod" value="root.upload_image">
	            </form>
			</aside>
		</div>
		<div class="clearfix">
			<p>Description:</p>
			<textarea rows="5" class="description">{{data.description}}</textarea>
		</div>
		<div class="clearfix">
			<p>Related:<span>*Drag'n'drop<br>from the list</span></p>
			<aside>
				<ul class="related">
					{{#data.related}}
						<li data-id="{{id}}">{{name}}<span class="entypo-cancel hover"></span></li>
					{{/data.related}}
				</ul>
			</aside>
		</div>
		<aside>
			<span id="save_tag">Save</span>
			<span id="discard_changes">Close</span>
		</aside>
	</section>
</template>