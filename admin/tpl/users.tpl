<template id="users" type="text/html">
<section id="users">
	<header class="clearfix">
		<aside>
			<input type="text" class="search" placeholder="Начните искать...">
		</aside>
	</header>
	<section class="all">
		<ul class="clearfix">
			<li style='width: 220px;'>Имя</li>
			<li style='width: 120px;'>Регистрация</li>
			<li style='width: 200px;'>Email</li>
			<li style='width: 200px;'>Последний вход</li>
			<li style='width: 80px;'>Заказы</li>
			<li style='width: 150px;'>Общая сумма</li>
		</ul>
		<section class='scroll_wrapper'>
			<section class='scroll_content'>
			</section>
		</section>
		<section class='pagination clearfix'>
			<div class='clearfix'>
				<p>1</p>
				<p>2</p>
				<p>3</p>
				<p class='sep'></p>
				<p>12</p>
				<a class='hover'>Следующая страница<span class="entypo-right-open"></span></a>
			</div>
		</section>
	</section>
</section>
</template>

<template id="user_list">
{{#data}}
<article class="clearfix" onclick="router.navigate('orders/person/{{id}}', {trigger: true});">
	<div style='width: 220px;'><p>{{f_name}} {{l_name}}</p></div>
	<div style='width: 120px;'><p>{{reg_date}}</p></div>
	<div style='width: 200px;'><p>{{email}}</p></div>
	<div style='width: 200px;'><p>{{last_login}}</p></div>
	<div style='width: 80px;'><p>{{#orders}}{{orders}}{{/orders}}{{^orders}}–{{/orders}}</p></div>
	<div style='width: 150px;'><p>{{#total}}{{total}}{{/total}}{{^total}}–{{/total}}</p></div>
</article>
{{/data}}
</template>

<template id="user_editor" type="text/html">
<section id="user_editor" class="clearfix">
	<header class="clearfix">
		<ul>
			<li class="hover" style="opacity: 1;" onclick="router.navigate('users', {trigger: true});"><span class="entypo-left-open" style="margin-right: 15px;"></span>Back</li>
			<li class="hover" id="save">Save</li>
			<li class="hover">Discard</li>
		</ul>
	</header>
	<section class="profile">
		<h1><span class="entypo-book-open"></span>General info</h1>
		<article>
			<p>Name:</p>
			<div><input type="text" class="name" value="{{data.name}}"></div>
		</article>
		<article>
			<p>Image:</p>
			{{#data.image}}
			<figure class="image" data-image="{{data.image}}" style="background-image: url({{data.image}});"></figure>
			{{/data.image}}
			{{^data.image}}
			<figure class="image" data-image="null"><span class="entypo-picture"></span></figure>
			{{/data.image}}
			<aside><button class="entypo-upload upload hover"></button>
				<form enctype="multipart/form-data" method="post" action="php/router.php">
	                <input type="file" name="photo" class="load_image">
	                <input type="hidden" name="mod" value="root.upload_image">
	            </form>
			</aside>
		</article>
		<article>
			<p>Role:</p>
			<div><input type="text" class="role" value="{{data.role}}"></div>
		</article>
		<h1><span class="entypo-github"></span>Contact info</h1>
		<article>
			<p>E-mail:</p>
			<div><input type="text" class="email" value="{{data.email}}"></div>
		</article>
		<article>
			<p>Phone:</p>
			<input type="text" class="phone" value="{{data.phone}}">
		</article>
	</section>
</section>
</template>

<template id="user_preview" type="text/html">
<section id="user_editor" class="clearfix">
	<header class="clearfix">
		<ul>
			<li class="hover" style="opacity: 1;" onclick="router.navigate('users', {trigger: true});"><span class="entypo-left-open" style="margin-right: 15px;"></span>Back</li>
		</ul>
	</header>
	<section class="profile">
		<h1><span class="entypo-book-open"></span>General info</h1>
		<article>
			<p>Name:</p>
			<div><input type="text" value="{{data.name}}" disabled="true"></div>
		</article>
		<article>
			<p>Image:</p>
			{{#data.image}}
			<figure style="background-image: url({{data.image}});"></figure>
			{{/data.image}}
			{{^data.image}}
			<figure><span class="entypo-picture"></span></figure>
			{{/data.image}}
		</article>
		<article>
			<p>Role:</p>
			<div><input type="text" value="{{data.role}}" disabled="true"></div>
		</article>
		<h1><span class="entypo-github"></span>Contact info</h1>
		<article>
			<p>E-mail:</p>
			<div><input type="text" value="{{data.email}}" disabled="true"></div>
		</article>
		<article>
			<p>Phone:</p>
			<input type="text" value="{{data.phone}}" disabled="true">
		</article>
	</section>
	<section class="logs">
		
	</section>
</section>
</template>

<template id="logs" type="text/html">
	<h1><span class="entypo-shareable"></span>Activity Logs</h1>
	<ul>
		{{#logs}}
		<li>
			<p>{{time}}</p>
			<h1>{{event}}</h1>
		</li>
		{{/logs}}
	</ul>
</template>