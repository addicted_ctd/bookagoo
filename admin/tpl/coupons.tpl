<template id="coupons" type="text/html">
<section id="coupons">
	<header>
		<h1>Купоны</h1>
		<button>Cоздать купон</button>
		<ul class='clearfix'>
			<li style="width: 90px;">Формат скидки</li>
			<li style="width: 70px;">Активность</li>
			<li style="width: 100px;">Купон</li>
			<li style="width: 110px;">Дата применения</li>
			<li style="width: 110px;">Неограниченый</li>
			<li style="width: 110px;">Действителен до</li>
		</ul>
	</header>
	<section class="all">
		<section class="scroll_wrapper">
			<setion class="scroll_content">
			</setion>
		</section>
		<section class='pagination clearfix'>
			<div class='clearfix'>
				<p>1</p>
				<p>2</p>
				<p>3</p>
				<p class='sep'></p>
				<p>12</p>
				<a class="hover">Следующая страница<span class="entypo-right-open"></span></a>
			</div>
		</section>
	</section>
	<section id="overlay">
		<section class="ov_content">
			<section class='cards popup'>
				<div class='back clearfix'><a></a></div>
				<div class='clearfix type'><p>Тип</p><span class='selected' data-type="0">В рублях</span><span data-type="1">В процентах</span></div>
				<div class='clearfix'><p>Скидка</p><input type='text' class='discount_amount' placeholder='Значение'></div>
				<div class='clearfix'><p>Количество купонов</p><input type='number' min="1" value="1" class='coupon_quantity'></div>
				<div class='clearfix'><p>Действителен до</p><input type='text' class='expire_date' value='30.01.2016'></div>
				<div class='clearfix'>
					<p>Неограниченое применение</p>
					<aside class='clearfix'>
						<input type='radio' id='endless_no' name='endless' checked value="0"><label for='endless_no'>Нет</label>
						<input type='radio' id='endless_yes' name='endless' value="1"><label for='endless_yes'>Да</label>
					</aside>
				</div>
				<button class='create_coupons'>Создать купон</button>
			</section>
		</section>
	</section>
</section>
</template>

<template id="coupons_list" type="text/html">
{{#data}}
<article class='clearfix'>
	<div style="width: 90px;">
		<p>{{discount}} {{format}}</p>
	</div>
	<div style="width: 70px;">
		<p>{{active}}</p>
	</div>
	<div style="width: 100px;">
		<p>{{code}}</p>
	</div>
	<div style="width: 110px;">
		<p>{{apply_date}}</p>
	</div>
	<div style="width: 110px;">
		<p>{{endless}}</p>
	</div>
	<div style="width: 110px;">
		<p>{{expire_date}}</p>
	</div>
</article>
{{/data}}
</template>