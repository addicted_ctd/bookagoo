<template id='general' type='text/html'>
{{#data}}
<section id='general'>
	<section class='one clearfix'>
		<h1 class='h'>Книга</h1>
		<div>
			<p class='op'>Стоимость карточки</p>
			<p><edit contenteditable='true' data-assign="card_price">{{settings.card_price}}</edit> руб</p>
		</div>
		<div>
			<p class='op'>Стоимость коробки</p>
			<p><edit contenteditable='true' data-assign="box_price">{{settings.box_price}}</edit> руб</p>
		</div>
	</section>
	<section class='two clearfix'>
		<h1 class='h'>Социальные сети</h1>
		<div>
			<p class='op'>Ссылка  vk</p>
			<p><edit contenteditable='true' data-assign="vk_link">{{settings.vk_link}}</edit></p>
		</div>
		<div>
			<p class='op'>Ссылка  facebook</p>
			<p><edit contenteditable='true' data-assign="fb_link">{{settings.fb_link}}</edit></p>
		</div>
	</section>
	<section class='three clearfix'>
		<div class='clearfix'>
			<h1 class='h'>Доставка</h1>
			<input type='text' class='street' placeholder='Улица самовывоза' data-assign="delivery_street" value="{{settings.delivery_street}}">
			<input type='text' class='house' placeholder='Дом' data-assign="delivery_house" value="{{settings.delivery_house}}">
			<input type='text' class='appartment' placeholder='Кв' data-assign="delivery_appartment" value="{{settings.delivery_appartment}}">
		</div>
		<div class='clearfix'>
			<h1 class='h'>Пользователи</h1>
			<button class='invite_trigger' onclick='$("body").addClass("ov");'>Пригласить</button>
			<ul data-items="{{settings.user_mails}}"></ul>
		</div>
	</section>
	<section class='four clearfix'>
		<h1 class='h'>Оплата</h1>
		<input type='text' class='merchant_id' data-assign="merchant_id" placeholder='Merchant ID' value="{{settings.merchant_id}}">
		<input type='text' class='client_id'  data-assign="client_id" placeholder='Client ID' value="{{settings.client_id}}">
	</section>
	<section class='five clearfix'>
		<h1 class='h'>Email</h1>
		<p class='op'>Уведомления о заказах</p>
		<input type='text' class='notification_email' placeholder='Новый email'>
		<button class='add_email'>+</button>
		<ul data-items="{{settings.notification_emails}}"></ul>
	</section>
	<section id="overlay">
		<section class="ov_content">
			<section class='cards popup'>
				<div class='back clearfix'><a onclick='$("body").removeClass("ov");'></a></div>
				<input type='text' class='invite_email' placeholder="Укажите Email">
				<button onclick="General.invite();">Пригласить</button>
			</section>
		</section>
	</section>
</section>
{{/data}}
</template>