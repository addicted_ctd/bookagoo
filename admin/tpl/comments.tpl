<template id="comments" type="text/html">
<section id="comments">
	<header class="clearfix page_header">
		<h1 class="hover" onclick="router.navigate('comments/add', {trigger: true});"><span class="entypo-plus"></span>Add</h1>
	</header>
	<section class="content">
		<section id="list">

		</section>
	</section>
</section>
</template>

<template id="comments_list">
<ul>
	{{#data}}
	<li class="clearfix">
		<section class="news" style="background-image: url({{post.content.thumb}});">
			<h1>{{post.meta.title}}</h1>
		</section>
		<section class="comments clearfix">
			{{#thread}}
			<div data-comment_id="{{id}}" class="comment">
				<p contenteditable="true">{{content}}</p>
			</div>
			{{/thread}}
		</section>
	</li>
	{{/data}}
</ul>
</template>

<template id="add_comment">
<section id="add_comment_wrapper">
	<form mod="comments" action="add" class="callback_ajax" id="add_comment">
		<textarea name="content"></textarea>
		<input type="text" name="email">
		<input type="text" name="parent_id" value="{{parent_id}}">
		<input type="submit" value="add">
	</form>
</section>
</template>