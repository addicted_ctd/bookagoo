<template id="news" type="text/html">
	<section id="news">
		<header class="clearfix">
			<ul>
				<li onclick="router.navigate('editor/new', {trigger: true});"><span class='entypo-plus'></span>Add</li>
				<li class='after_select'>Hide</li>
				<li class='after_select'>Show</li>
				<li class='after_select'>Delete</li>
			</ul>
			<aside>
				<input type="text" placeholder="search query" id="news_search">
			</aside>
		</header>
		<section class="list">

		</section>
	</section>
</template>

<template id="news_list_item" type="text/html">
	{{#data}}
	<article class="clearfix" data-id="{{id}}">
		<div>
			<radio_button ui_target="#news .list"></radio_button>
		</div>
		<p>{{date}}</p>
		{{#thumb}}
			<figure style="background-image: url({{thumb}});" onclick="router.navigate('posts/{{id}}', {trigger: true});" style='cursor: pointer;'></figure>
		{{/thumb}}
		{{^thumb}}
			<figure style="background-color: #eeeeee" onclick="router.navigate('posts/{{id}}', {trigger: true});" style='cursor: pointer;'><span class="entypo-picture"></span></figure>
		{{/thumb}}
		<h1 onclick="router.navigate('posts/{{id}}', {trigger: true});" style='cursor: pointer;'>{{{title}}}</h1>
		<aside>
			<ul class="actions">
				<li onclick="router.navigate('posts/{{id}}', {trigger: true});"><span>edit</span></li>
				{{#visible}}
					<li data-action="hide" onclick="News.toggle_status(0, {{id}}, true);"><span>hide</span></li>
				{{/visible}}
				{{^visible}}
					<li data-action="show" onclick="News.toggle_status(1, {{id}}, true);"><span>show</span></li>
				{{/visible}}
				<li onclick="News.delete_post({{id}});"><span>delete</span></li>
			</ul>
		</aside>
	</article>
	{{/data}}
</template>

<template id="news_meta" type="text/html">
	<section id="news_meta" class="clearfix">
		<header class="clearfix">
			<h1 class="section_title">
				<span class="active">Post info</span>
				<span onclick="router.navigate('editor/' + News.Meta.id, {trigger: true});">Content</span>
			</h1>
			<ul>
				<li class="hover" onclick="router.navigate('posts', {trigger: true});" style="opacity: 1;"><span class="entypo-left-open" style="margin-right: 15px;"></span>Back</li>
				<li id="save">Save</li>
				{{#data.visible}}
				<li id="toggle_status" data-status="0">Hide</li>
				{{/data.visible}}
				{{^data.visible}}
				<li id="toggle_status" data-status="1">Show</li>
				{{/data.visible}}
				
				<li id="delete" onclick="News.delete_post({{data.id}});" style="color: #bb4036; opacity: 1;">Delete</li>
			</ul>
		</header>
		<section class="meta_list clearfix">
			<h1><span class="entypo-clipboard"></span>{{data.title}}</h1>
			<section>
				<div>
					<h5>Date add:<span>e.g. 03.11.2014</span></h5>
					<aside>
						<input type="text" class="date" value="{{data.date}}">
					</aside>
				</div>
				<div>
					<h5>Short description:</h5>
					<aside>
						<textarea rows="5" class="short">{{data.description}}</textarea>
					</aside>
				</div>
				<div>
					<h5>Lead:</h5>
					<aside>
						<textarea rows="3" class="lead">{{data.lead}}</textarea>
					</aside>
				</div>
			</section>
			<section>
				<div class="category_box">
					
				</div>
				<div class="tags_box">
					
				</div>
				<div class="rubrics_box">
					
				</div>
				
			</section>
		</section>
		<section class="meta_actions">
		
		</section>
	</section>
</template>

<template id="rubrics" type="text/html">
	<h5 class="top" style="padding-top: 5px;">Rubric:</h5>
	<aside class="clearfix" style="padding-top: 40px;">
		<custom_select value="null" class="rubric">
			<li selected value="null">-</li>
			{{#data}}
			<li value="{{id}}">{{name}}</li>
			{{/data}}
		</custom_select>
		<div class="clearfix"></div>
		<section class="custom_box">
		</section>
	</aside>
	<script>
		if (News.Meta.data.rubric !== "0" && News.Meta.data.rubric !== "") {
			$(".rubric").find("li[value='"+News.Meta.data.rubric+"']").click();
			$(".rubric").click();
		}
	</script>
</template>

<template id="category" type="text/html">
	<h5>Categories:<span>* autocomplete</span></h5>
	<aside>
		<input type="text">
		<p class="tip"></p>
		<section class="categories">
		</section>
	</aside>
</template>

<template id="tags" type="text/html">
	<h5>Tags:<span>* autocomplete</span></h5>
	<aside>
		<input type="text">
		<p class="tip"></p>
		<section class="tags">
		</section>
	</aside>
</template>

<template id="custom_fields">
	<h4><span class="entypo-brush"></span>Custom fields</h4>
	<ul class="custom_fields">
		{{#data}}
		<li data-id={{id}}>
			<p>{{title}}</p>
			{{#text_input}}
				<input type="text" value="{{default_val}}" name="{{name}}">
			{{/text_input}}
			{{#number_input}}
				<input type="number" value="{{default_val}}" name="{{name}}">
			{{/number_input}}
			{{#textarea}}
				<textarea  value="{{default_val}}" name="{{name}}" rows="5"></textarea>
			{{/textarea}}
			{{#file}}
				<div>
					<input type="text" value="{{default_val}}" name="{{name}}" class="path_preview">
					<button class="entypo-upload upload_default hover"></button>
					<form enctype="multipart/form-data" method="post" action="php/router.php">
					    <input type="file" name="photo" class="field_value">
					    <input type="hidden" name="mod" value="root.upload_image">
					</form>
				</div>
			{{/file}}
		</li>
		{{/data}}
	</ul>
</template>