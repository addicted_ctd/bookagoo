<template id="orders" type="text/html">
<section id="orders">
	<header class="clearfix">
		<aside>
			<input type="text" class="search" placeholder="Начните искать...">
		</aside>
	</header>
	<section class="all">
		<ul class="clearfix">
			<li style='width: 100px;'>Дата</li>
			<li style='width: 100px;'>ID заказа</li>
			<li style='width: 150px;'>Сумма заказа</li>
			<li style='width: 200px;'>Имя</li>
			<li style='width: 200px;'>Телефон</li>
			<li style='width: 150px;'>Доставка</li>
			<li style='width: 150px;'>Статус заказа</li>
		</ul>
		<section class='scroll_wrapper'>
			<section class='scroll_content'>
			</section>
		</section>
		<section class='pagination clearfix'>
			<div class='clearfix'>
				<p>1</p>
				<p>2</p>
				<p>3</p>
				<p class='sep'></p>
				<p>12</p>
				<a class="hover">Следующая страница<span class="entypo-right-open"></span></a>
			</div>
		</section>
	</section>
</section>
</template>

<template id="orders_list">
{{#data}}
<article class="clearfix" onclick="router.navigate('/orders/person/{{uid}}/order/{{id}}', {trigger: true});">
	<div style='width: 100px;'><p>{{date}}</p></div>
	<div style='width: 100px;'><p># {{id}}</p></div>
	<div style='width: 150px;'><p>{{total}}</p></div>
	<div style='width: 200px;'><p>{{name}}</p></div>
	<div style='width: 200px;'><p>{{phone}}</p></div>
	<div style='width: 150px;'><p>Курьером</p></div>
	<div style='width: 150px;' data-status="{{status}}"><p>{{status}}</p></div>
</article>
{{/data}}
</template>

<template id="person_orders" type="text/html">
{{#data}}
<section id="person_orders">
	<section class='person_info'>
		<section class='title clearfix'>
			<h1>{{#info}}{{1}} {{2}}{{/info}}</h1>
			<h2>{{#orders_meta}}{{phone}}{{/orders_meta}}</h2>
			<a class='email'>{{#info}}{{0}}{{/info}}</a>
			<span class="burger"></span>
			<span class="entypo-up-open hover"></span>
		</section>
		<section class='info clearfix'>
			<section>
				<p>Общая сумма заказов</p>
				<h1>{{#orders_meta}}{{sum(total)}}{{/orders_meta}}</h1>
				<p>Последний заказ</p>
				<h1>{{#orders_meta}}{{total}}{{/orders_meta}} <span>₽</span></h1>
				<textarea placeholder='Введите комментарий о клиенте..'></textarea>
			</section>
			<div>
				<p>
					Дата последнего заказа
					<span>{{#orders_meta}}{{max(date)}}{{/orders_meta}}</span>
				</p>
				<p>
					Ребенок
					<span>{{#info}}{{3}}{{/info}}</span>
				</p>
				<p>
					День рождения ребенка
					<span>{{#info}}{{4}}{{/info}}</span>
				</p>
				<p>
					Ссылка Vkontakte
					<span>{{#social}}{{#vk_id}}<a href='http://vk.com/id{{vk_id}}' target='_blank'>Перейти</a>{{/vk_id}}{{^vk_id}}Нет{{/vk_id}}{{/social}}</span>
				</p>
				<p>
					Ссылка Facebook
					<span>{{#social}}{{#fb_id}}<a href='http://facebook.com/{{fb_id}}' target='_blank'>Перейти</a>{{/fb_id}}{{^fb_id}}Нет{{/fb_id}}{{/social}}</span>
				</p>
			</div>
		</section>
	</section>
	{{#orders}}
	<section class='order' data-id='{{id}}'>
		<section class='order_title clearfix'>
			<h1>Заказ #{{id}}</h1>
			<span class="entypo-down-open hover"></span>
		</section>
		<section class='details'>
			<section class='four clearfix'>
				<a class='pdf_dl' href='pack_pdf.php?order_id={{id}}' target="_blank">Скачать PDF</a>
				<section class='status clearfix'>
					<select data-selected="{{status}}" class='order_status'>
						<option selected="true" disabled="disabled">Статус заказа</option>
						<option value="0">Заказ принят</option>
						<option value="1">Заказ ожидает оплаты</option>
						<option value="2">Заказ оплачен</option>
						<option value="3">Заказ передан в производство</option>
						<option value="4">Передан в службу доставки</option>
						<option value="5">Готов для самовывоза</option>
						<option value="6">Заказ завершен</option>
						<option value="7">Заказ анулирован</option>
					</select>
				</section>
			</section>
			<section class='one clearfix'>
				<div>
					<p>Дата заказа</p>
					<span>{{date}}</span>
				</div>
				<div>
					<p>Доставка</p>
					<select data-selected="{{delivery}}" class='delivery_type'>
						<option value="0">Самовывоз</option>
						<option value="1">Курьером</option>
					</select>
				</div>
				<div>
					<p>Адрес доставки</p>
					<span class='inline_update delivery_address' contenteditable='true'>{{delivery_address}}</span>
				</div>
			</section>
			<section class='two clearfix'>
				<article class='clearfix cards'>
					<div>
						<p>Карточки</p>
					</div>
					<div>
						<p>{{cards_quantity}} шт.</p>
					</div>
					<div>
						<p><a class='cards_total'>{{cards_total}}</a> руб</p>
					</div>
				</article>
				<article class='clearfix'>
					<div>
						<p>Коробка</p>
					</div>
					<div>
						<p><span class='num_only box_quantity inline_update' contenteditable='true'>{{box_quantity}}</span> шт.</p>
					</div>
					<div>
						<p><a class='box_total'>{{box_total}}</a> руб</p>
					</div>
				</article>
				<article class='clearfix'>
					<div>
						<p>Доставка</p>
					</div>
					<div>
						<p></p>
					</div>
					<div>
						<p><span contenteditable='true' class="num_only delivery_price inline_update">{{delivery_total}}</span> руб</p>
					</div>
				</article>
				<article class='clearfix'>
					<div>
						<p>Скидка купона</p>
					</div>
					<div>
						<p><span class='coupon_code inline_update' contenteditable='true' style='text-transform: uppercase;'>{{#coupon}}{{coupon}}{{/coupon}}{{^coupon}}–{{/coupon}}</span></p>
					</div>
					<div>
						<p><a class='discount_total'>{{#coupon}}{{discount_total}}{{/coupon}}{{^coupon}}0{{/coupon}}</a> руб</p>
					</div>
				</article>
			</section>
			<section class='three clearfix'>
				<h2><a class='total'>{{total}}</a> <span>руб</span></h2>
			</section>
		</section>
	</section>
	{{/orders}}
	<section id="overlay">
		<section class="ov_content">
			<section class='cards popup'>
				<div class='back'>Назад к заказу</div>
				<div class='title clearfix'>
					<p>Карточки</p>
					<span>К-во</span>
				</div>
				<div class='ul_wrapper'>
					<ul>
						<li class='clearfix' data-item='1'>
							<p>Обложка</p>
							<aside>
								<p><num>2</num>
									<span>-</span>
									<span>+</span>
								</p>
							</aside>
						</li>
						<li class='clearfix' data-item='2'>
							<p>Привет, а вот и я</p>
							<aside>
								<p><num>2</num>
									<span>-</span>
									<span>+</span>
								</p>
							</aside>
						</li>
						<li class='clearfix' data-item='3'>
							<p>Моя семья</p>
							<aside>
								<p><num>2</num>
									<span>-</span>
									<span>+</span>
								</p>
							</aside>
						</li>
						<li class='clearfix' data-item='4'>
							<p>Моя ручка и ножка</p>
							<aside>
								<p><num>2</num>
									<span>-</span>
									<span>+</span>
								</p>
							</aside>
						</li>
						<li class='clearfix' data-item='5'>
							<p>Я расту</p>
							<aside>
								<p><num>2</num>
									<span>-</span>
									<span>+</span>
								</p>
							</aside>
						</li>
						<li class='clearfix' data-item='6'>
							<p>Любимое</p>
							<aside>
								<p><num>2</num>
									<span>-</span>
									<span>+</span>
								</p>
							</aside>
						</li>
						<li class='clearfix' data-item='7'>
							<p>Путешествую</p>
							<aside>
								<p><num>2</num>
									<span>-</span>
									<span>+</span>
								</p>
							</aside>
						</li>
						<li class='clearfix' data-item='8'>
							<p>Вот и год прошел</p>
							<aside>
								<p><num>2</num>
									<span>-</span>
									<span>+</span>
								</p>
							</aside>
						</li>
					</ul>
				</div>
			</section>
		</section>
	</section>
</section>
{{/data}}

</template>