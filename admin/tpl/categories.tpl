<template id="categories" type="text/html">
	<section id="categories">
		<header class="clearfix">
			<h2>Category list</h2>
			<h1 class="section_title">
				<span class="active">Manage</span>
				<span>Options</span>
			</h1>
		</header>
		<section class="clearfix">
			<section class="list_wrapper">
				<section class="list">
				
				</section>
				<aside>
					<button id="add_category"><span class="entypo-list-add"></span>Add</button>
				</aside>
			</section>
			<section class="details">
				
			</section>
		</section>
	</section>
</template>

<template id="category_editor" type="text/html">
	{{#data}}
	<h1><span class="entypo-pencil"></span>Category Editor</h1>
	<div class="clearfix">
		<p>Name:</p>
		<input type="text" value="{{#info}}{{name}}{{/info}}" class="name">
	</div>
	<div class="clearfix">
		<p>Image:</p>
		{{#info}}
			{{#image}}
			<figure data-image="{{image}}" style="background-image: url({{image}})" class="image"></figure>
			{{/image}}
			{{^image}}
			<figure class="image" data-image=""><span class="entypo-picture"></span></figure>
			{{/image}}
		{{/info}}
		
		<aside><button class="entypo-upload upload"></button>
			<form enctype="multipart/form-data" method="post" action="php/router.php">
                <input type="file" name="photo" class="load_image">
                <input type="hidden" name="mod" value="root.upload_image">
            </form>
		</aside>
	</div>
	<div class="clearfix">
		<p>Description:</p>
		<textarea rows="5" class="description">{{#info}}{{description}}{{/info}}</textarea>
	</div>
	<div class="clearfix">
		<p>Parent:</p>
		<aside>
			<custom_select value="{{#info}}{{parent}}{{/info}}" class="parent">
			<li selected value="0">-</li>
			{{#list}}
			<li value="{{id}}">{{name}}</li>
			{{/list}}
			</custom_select>
		</aside>
	</div>
	<aside>
		<span id="save_category">Save</span>
		<span id="discard_changes">Close</span>
	</aside>
	{{/data}}
</template>

<template id="category_add" type="text/html">
	{{#data}}
	<h1><span class="entypo-list-add"></span>Add category</h1>
	<div class="clearfix">
		<p>Name:</p>
		<input type="text" class="name">
	</div>
	<div class="clearfix">
		<p>Image:</p>
		<figure class="image" data-image="null"><span class="entypo-picture"></span></figure>
		<aside><button class="entypo-upload upload"></button>
			<form enctype="multipart/form-data" method="post" action="php/router.php">
                <input type="file" name="photo" class="load_image">
                <input type="hidden" name="mod" value="root.upload_image">
            </form>
		</aside>
	</div>
	<div class="clearfix">
		<p>Description:</p>
		<textarea rows="5" class="description"></textarea>
	</div>
	<div class="clearfix">
		<p>Parent:</p>
		<aside>
			<custom_select value="0" class="parent">
			<li selected value="0">-</li>
			{{#list}}
			<li value="{{id}}">{{name}}</li>
			{{/list}}
			</custom_select>
		</aside>
	</div>
	<aside>
		<span id="save_category">Save</span>
		<span id="discard_changes">Close</span>
	</aside>
	{{/data}}
</template>