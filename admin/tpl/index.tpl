<template id="index" type="text/html">
	<menu>
	    <logo></logo>
	    <ul>
	        <li data-route="posts"><span class="entypo-newspaper"></span>Новости</li>
	        <!--<li data-route="rubrics"><span class="entypo-flow-cascade"></span>Рубрики</li>-->
	        <!--<li data-route="categories"><span class="entypo-list"></span>Категории</li>-->
	        <!--<li data-route="tags"><span class="entypo-tag"></span>Теги</li>-->
	        <li data-route="users"><span class="entypo-vcard"></span>Пользователи</li>
	        <li data-route="coupons"><span class="entypo-bookmark" style="padding-left: 8px;"></span>Купоны</li>
	        <li data-route="orders"><span class="entypo-basket"></span>Заказы</li>
	        <li data-route="general"><span class="entypo-info-circled"></span>Общее</li>
	        <li data-route="api"><span class="entypo-code"></span>API</li>
	        <li data-route="logout"><span class="entypo-logout"></span>Log out</li>
	    </ul>
	</menu>

	<main>

	</main>

	<script src="js/plugins.js"></script>
	<script src="js/vendor/jquery.maskedinput.js"></script>
	<script src="js/routes/users.js"></script>
	<script src="js/routes/news.js"></script>
	<script src="js/routes/categories.js"></script>
	<script src="js/routes/tags.js"></script>
	<script src="js/routes/rubrics.js"></script>
	<script src="js/routes/orders.js"></script>
	<script src="js/routes/coupons.js"></script>
	<script src="js/routes/comments.js"></script>
	<script src="js/routes/general.js"></script>
	<script src="js/editor.js" type="text/javascript"></script>
	<script src="js/editor/grid.js" type="text/javascript"></script>
	<script src="js/editor/resize.js" type="text/javascript"></script>
	<script src="js/editor/drag.js" type="text/javascript"></script>
	<script src="js/editor/photo.js" type="text/javascript"></script>
	<script src="js/editor/select.js" type="text/javascript"></script>
	<script src="js/editor/video.js" type="text/javascript"></script>
	<script src="js/editor/gallery.js" type="text/javascript"></script>
	<script src="js/editor/side_tooltip.js" type="text/javascript"></script>
	<script src="js/editor/save.js" type="text/javascript"></script>
	<script src="js/editor/date.js" type="text/javascript"></script>
	<script src="js/router.js"></script>
</template>

<template id="login" type="text/html">
	<section id="login">
		<article>
			<div>
				<p>BOOKAGOO</p>
				<section>
					<input type="text" id="username" placeholder="Логин" autocomplete="off">
				</section>
				<section>
					<input type="password" id="password" placeholder="Пароль" autocomplete="off">
				</section>
				<button id="submit">Войти в админпанель</button>
			</div>
		</article>
	</section>
</template>