<template id="ecommerce" type="text/html">
<section id="ecommerce">
	<header class="clearfix">
		<ul>
			<li class="active hover"><span class="entypo-archive"></span>Catalog</li>
			<li class="hover"><span class="entypo-briefcase"></span>Orders</li>
			<li class="hover"><span class="entypo-chart-line"></span>Statistics</li>
		</ul>
		<h1 class="hover entypo-cog"><span>Product Settings</span></h1>
	</header>
	<section class="content catalog">
		<ul>
			<li style="width: 125px;">Article</li>
			<li style="width: 145px;">Image</li>
			<li style="width: 245px;">Title</li>
			<li style="width: 125px;">In stock</li>
			<li style="width: 105px;">Price</li>
			<li style="width: 120px;">Quantity</li>
		</ul>
		<section class="clearfix" id="list">
		</section>
	</section>
</section>
</template>

<template id="catalog_list" type="text/html">
	<article>
		<div class="article_wrapper">
			<span>#</span>
			<input type="text" class="article" placeholder="New article">
		</div>
		<div>
			<figure style="background-image: url();" class="image new_image" data-image="null"><span class="entypo-picture"></span>
				<a class="upload"></a>
				<form enctype="multipart/form-data" method="post" action="php/router.php">
	                <input type="file" name="photo" class="load_image">
	                <input type="hidden" name="mod" value="root.upload_image">
	            </form>
			</figure>
		</div>
		<div>
			<input type="text" class="name" placeholder="New title">
		</div>
		<div>
			<custom_select value="1" class="in_stock">
				<li value="1" selected>Yes</li>
				<li value="0">No</li>
			</custom_select>
		</div>
		<div class="price_wrapper">
			<span>₴</span>
			<input type="text" class="price" placeholder="00.00">
		</div>
		<div>
			<input type="number" min="0" class="quantity" placeholder="0">
		</div>
		<div>
			<span class="entypo-plus hover" id="add_item"><a>add</a></span>
		</div>
	</article>
	{{#data}}
	<article data-id="{{id}}">
		<div class="article_wrapper">
			<span>#</span>
			<input type="text" class="article" value="{{article}}" data-update="article">
		</div>
		<div>
			{{^image}}
				<figure class="image" data-image="null"><span class="entypo-picture"></span>
					<a class="upload"></a>
					<form enctype="multipart/form-data" method="post" action="php/router.php">
		                <input type="file" name="photo" class="load_image">
		                <input type="hidden" name="mod" value="root.upload_image">
		            </form>
				</figure>
			{{/image}}
			{{#image}}
				<figure style="background-image: url(uploads/{{image}});" class="image" data-image="{{image}}">
					<a class="upload"></a>
					<form enctype="multipart/form-data" method="post" action="php/router.php">
		                <input type="file" name="photo" class="load_image">
		                <input type="hidden" name="mod" value="root.upload_image">
		            </form>
				</figure>
			{{/image}}
		</div>
		<div>
			<input type="text" class="name" value="{{name}}" data-update="name">
		</div>
		<div>
			<custom_select value="1" class="in_stock block_update" data-update="in_stock">
				<li value="1" selected>Yes</li>
				<li value="0">No</li>
			</custom_select>
			<script>
				$("#list").find("article[data-id={{id}}]").find("custom_select").find("[value={{in_stock}}]").click();
				$("#list").find("article[data-id={{id}}]").find("custom_select").removeClass("block_update").click();
			</script>
		</div>
		<div class="price_wrapper">
			<span>₴</span>
			<input type="text" class="price" value="{{price}}" data-update="price">
		</div>
		<div>
			<input type="number" min="0" class="quantity" value="{{quantity}}" data-update="quantity">
		</div>
		<div>
			<span class="entypo-feather hover edit_item" onclick="router.navigate('ecommerce/editor/{{id}}', {trigger: true});"><a style="top: -3px;">edit</a></span>
		</div>
		<div>
			<h4>Updated</h4>
		</div>
	</article>
	{{/data}}
</template>

<template id="catalog_editor" type="text/html">
<section id="catalog_editor">
	{{#data}}
	<header class="clearfix">
		<ul>
			<li class="hover" onclick="router.navigate('ecommerce', {trigger: true});" style="opacity: 1;"><span class="entypo-left-open" style="margin-right: 15px;"></span>Back</li>
			<li id="save" class="hover" onclick="Ecommerce.Editor.save();">Save</li>
			<li id="delete" onclick="" class="hover" style="color: #bb4036; opacity: 1;">Delete</li>
		</ul>
	</header>
	<section class="clearfix content">
		<section>
			<h1 class="entypo-clipboard">
				Product name: <input type="text" class="name" value="{{name}}">
			</h1>
			<div>
				<h5>Date add:<span>e.g. 03.11.2014</span></h5>
				<aside>
					<input type="text" class="date" value="{{date_add}}">
				</aside>
			</div>
			<div>
				<h5>Image:</h5>
				<aside>
					{{^image}}
					<figure class="image" data-image="null"><span class="entypo-picture"></span></figure>
					{{/image}}
					{{#image}}
					<figure class="image" data-image="{{image}}" style="background-image: url({{image}});"></figure>
					{{/image}}
				</aside>
				<aside class="upload_wrapper">
					<button class="entypo-upload upload hover"></button>
					<form enctype="multipart/form-data" method="post" action="php/router.php">
		                <input type="file" name="photo" class="load_image">
		                <input type="hidden" name="mod" value="root.upload_image">
		            </form>
				</aside>
			</div>
			<div>
				<h5>Short<br>description:</h5>
				<aside>
					<textarea rows="5" class="short">{{data.description}}</textarea>
				</aside>
			</div>
			<div class="category_box">
				
			</div>
			<div class="tags_box">
				
			</div>
		</section>
		<section>
			<h1 class="entypo-database">
				<span>#</span>Article: <input type="text" class="article" value="{{article}}" style="width: 80px; padding-left: 12px;">
			</h1>
			<div class="price_wrapper">
				<h5>Price:</h5>
				<span>₴</span>
				<aside>
					<input type="text" class="price" value="{{price}}" style="width: 80px; padding-left: 12px;">
				</aside>
			</div>
			<div>
				<h5>In stock</h5>
				<aside>
					<custom_select value="1" class="in_stock" data-update="in_stock">
						<li value="1" selected>Yes</li>
						<li value="0">No</li>
					</custom_select>
					<script>
						$("#catalog_editor").find("custom_select.in_stock").find("[value={{in_stock}}]").click();
						$("#catalog_editor").find("custom_select.in_stock").click();
					</script>
				</aside>
			</div>
			<div class="price_wrapper">
				<h5>Quantity:</h5>
				<aside>
					<input type="number" min="1" class="quantity" value="{{quantity}}" style="width: 50px;">
				</aside>
			</div>
			<section class="custom_box"></section>
		</section>
	</section>
	{{/data}}
</section>
</template>

<template id="custom_fields">
	<h1 class="entypo-brush">
		Custom fields
	</h1>
	<ul class="custom_fields">
		{{#data}}
		<li data-id={{id}}>
			<p>{{title}}</p>
			{{#text_input}}
				<input type="text" value="{{default_val}}" name="{{name}}">
			{{/text_input}}
			{{#number_input}}
				<input type="number" value="{{default_val}}" name="{{name}}">
			{{/number_input}}
			{{#textarea}}
				<textarea  value="{{default_val}}" name="{{name}}" rows="5"></textarea>
			{{/textarea}}
			{{#file}}
				<div>
					<input type="text" value="{{default_val}}" name="{{name}}" class="path_preview">
					<button class="entypo-upload upload_default hover"></button>
					<form enctype="multipart/form-data" method="post" action="php/router.php">
					    <input type="file" name="photo" class="field_value">
					    <input type="hidden" name="mod" value="root.upload_image">
					</form>
				</div>
			{{/file}}
		</li>
		{{/data}}
	</ul>
	<aside class="add_custom_wrapper">
		<input type="text" class="name" placeholder="Name">
		<input type="text" class="title" placeholder="Title">
		<input type="text" class="default" placeholder="Default value">
		<button class="entypo-upload upload_default hover"></button>
		<form enctype="multipart/form-data" method="post" action="php/router.php">
		    <input type="file" name="photo" class="default_file">
		    <input type="hidden" name="mod" value="root.upload_image">
		</form>
		<custom_select value="0" class="type">
			<li value="0" selected>Short text</li>
			<li value="1">Text</li>
			<li value="2">Number</li>
			<li value="3">File</li>
		</custom_select>
		<button class="hover" id="add_custom_field"><span class="entypo-plus-circled"></span>Add</button>
	</aside>
</template>