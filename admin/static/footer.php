<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.8.0.min.js"><\/script>')</script>
        <script src="js/jquery.form.js" type="text/javascript"></script>
        <script src="js/main.js"></script>
        <script src="js/global.js"></script>  
        <script src="js/vendor/underscore.js/underscore.js" type="text/javascript"></script>
        <script src="js/vendor/backbone.js/backbone.js" type="text/javascript"></script>
        <script src="js/vendor/mustache.js" type="text/javascript"></script>
        <script src="js/vendor/jquery.mustache.js" type="text/javascript"></script>
        <script src="js/ui.js"></script>
        <script src="js/vendor/css3finalize/jquery.css3finalize.min.js" type="text/javascript"></script>
        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            var _gaq = [['_setAccount', 'UA-XXXXX-X'], ['_trackPageview']];
            (function(d, t) {
                var g = d.createElement(t), s = d.getElementsByTagName(t)[0];
                g.src = '//www.google-analytics.com/ga.js';
                s.parentNode.insertBefore(g, s)
            }(document, 'script'));
        </script>
    </body>
</html>