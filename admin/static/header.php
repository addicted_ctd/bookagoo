<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <?php include('php/ctd.php');?>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title><?$ctd::display($content['title'], "ctd");?></title>
        <meta name="description" content="<?$ctd::display($content['meta_description'], 'DGFICC');?>">
        <meta name="keywords" content="<?$ctd::display($content['keywords'], 'DGFICC,DGF,кондитерские ингридиенты,шоколад,мастер-класс,школа,кондитерское искусство');?>">
        <meta name="viewport" content="width=device-width">
        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/construction_sight.css">
        <script src="js/vendor/modernizr-2.6.2.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 7]>
            <script src="js/vendor/html5shiv/html5shiv.js" type="text/javascript"></script>
            <script src="js/vendor/css3pie/PIE.js" type="text/javascript"></script>
        <![endif]-->