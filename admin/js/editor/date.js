var Date_selector = Editor.extend({

	item: '#date',
	init: function() {
		this.create_lists();
	},

	create_lists: function() {
		var self = this;
		var date_parts = $(this.item).find('span'), current, new_date = '';
		$('header.top_header').find('dropdown').remove();
		$(this.item).after("<dropdown class='clearfix helper'></dropdown>");
		this.dropdown = $("dropdown");
		var month = ['Января', 'Февраля', 'Марта', 'Апреля', 'Мая', 'Июня', 'Июля', 'Августа', 'Сентября', 'Октября', 'Ноября', 'Декабря'];
		var year = (new Date()).getFullYear() - 2;
		for (var i = 0; i < 3; i++) {
			this.dropdown.append('<select>');
		}
		var selects = this.dropdown.find('select');
		for (var i = 1; i <= 31; i++) {
			selects.eq(0).append('<option>'+i+'</option>');
		}
		for (var i = 0; i < month.length; i++) {
			selects.eq(1).append('<option>'+month[i].toLowerCase()+'</option>');
		}
		for (var i = year; i < year + 5; i++) {
			selects.eq(2).append('<option>'+i+'</option>');
		}
		this.track_select(selects, date_parts);
		for (var i = 0; i < 3; i++) {
			current = date_parts.eq(selects.eq(i).index()).text();
			selects.eq(i).find('option').filter(function() {
			    return $(this).text() === current; 
			}).prop('selected', true);
			selects.eq(i).width(self.ghost_width(selects.eq(i).val()));
			i !== 2 ? new_date += selects.eq(i).val() + ' ' : new_date += selects.eq(i).val();
		}
		$(this.item).attr('full_date', new_date);
	},

	track_select: function(select, date_parts) {
		var self = this;
		var date_h4 = $(this.item);
		var new_date;
		select.change(function() {
			new_date = '';
			for (var i = 0; i < 3; i++) {
				i !== 2 ? new_date += select.eq(i).val() + ' ' : new_date += select.eq(i).val();
			}
			date_h4.attr('full_date', new_date)
			$(this).width(self.ghost_width($(this).val()));
			date_parts.eq($(this).index()).text($(this).val());
		});
	},

	ghost_width: function(value) {
		this.dropdown.append('<span>'+value+'</span>');
		var ghost = this.dropdown.children().filter('span');
		var w = ghost.width();
		ghost.remove();
		return w;
	}

});

var date = new Date_selector();