var Select = Editor.extend({
    tooltip: "#selection_tooltip",
    tooltip_active: false,
    selection: "",
    tooltip_mode: "add",
    init: function() {
        var all_p = $("#premade_box").find("p");
        var self = this, action_p, selection;
        all_p.live("mousedown", function(e) {
            action_p = $(this);
            $(window).mouseup(function() {
                $(window).off("mouseup");
                console.log('smth');
                selection = self.return_selection();
                if (action_p.text().toLowerCase().indexOf(selection.text.toLowerCase()) !== -1) {
                    self.selection = selection;
                    self.init_tooltip(selection);
                }
            });
        });
        all_p.live("click", function(e) {
            e.stopPropagation();
        });
        self.edit_highlighted();
        self.prevent_divs();
        self.prevent_empty();
        self.header_processor();
    },
    header_processor: function() {
        var top_text = $('header.top_header').find('p.fixed_child');
        var top_h1 = $('header.top_header').find('h1');
        var header_height = $('header.top_header').height();
        top_h1.on('input', function() {
            align();
        });

        function align() {
            top_text.height(header_height - top_h1.height() - 42);
            top_text.parent().height(header_height - top_h1.height() - 42);
        }

        align();
    },
    prevent_divs: function() {
        var self = this;
        $('p.fixed_child, h1#title').die('keydown').live('keydown', function(e) {
            if (e.keyCode === 13) {
                document.execCommand('insertHTML', false, '<br>');
                return false;
            }
        });
        $('p.fixed_child').die('keyup').live('keyup', function(e) {
            self.clean_paste($(this));
        });
    },

    clean_paste: function(item) {
        var c = item.children();
        var c_l = c.length;
        console.log(c_l);
        var stop = 10;
        while (c_l !== 0) {
            stop--;
            for (var i = 0; i < c_l; i++) {
                console.log(c.eq(i), c.eq(i).text(), c.eq(i)[0].toString(), c.eq(i).hasClass("highlighted"));
                if (c.eq(i)[0].nodeName !== "BR" && !c.eq(i).hasClass("highlighted")) {
                    c.eq(i).replaceWith(c.eq(i)[0].childNodes);
                }
            }
            c = item.children();
            c_l = this.recount_children(c);
            console.log(c_l);
            if (!stop) break;
        }
    },

    recount_children: function(c) {
        var total = 0;
        for (var k = 0; k < c.length; k++) {
            if (!c.eq(k).hasClass("highlighted")) {
                total++;
            }
        }
        return total;
    },

    prevent_empty: function() {
        var give_a_chance, oops, self = this;
        $("h1#title").die("keyup").live("keyup", function(e) {
            self.clean_paste($(this));
            if ($(this).text() === "") {
                oops = $(this);
                give_a_chance = setTimeout(function() {
                    oops.html("Don't kill me<br> no more, ok?");
                    setTimeout(function() {
                        oops.html("НАЗВАНИЕ<br>СТАТЬИ");
                    }, 4000);
                }, 5000);
            } else {
                clearTimeout(give_a_chance);
            }
        });
    },
    return_selection: function() {
        var text_data, range;
        if (window.getSelection) {
            text_data = window.getSelection();
            range = text_data.getRangeAt(0);
        }
        text_data = {
            'text': text_data.toString(),
            'rect': range.getBoundingClientRect(),
            'range': range,
            'parent': range.commonAncestorContainer.parentNode
        };
        return text_data;
    },
    init_tooltip: function(s_data) {
        var self = this;
        if ($(this.tooltip).length === 0) {
            $.Mustache.load(router.tpl_path + '/editor.tpl', function() {
                $("#premade_box").mustache('selection_tooltip', {});
                self.bind_close();
                self.bind_click();
                self.tooltip_active = true;
                self.position_tooltip(s_data.rect, s_data.parent);
            });
        } else {
            self.bind_close();
            self.bind_click();
            self.tooltip_active = true;
            self.position_tooltip(s_data.rect, s_data.parent);
        }
    },
    position_tooltip: function(rect, parent_node) {
        parent_node = $(parent_node).closest(".fixed");
        console.log(rect);
        var tooltip = $(this.tooltip);
        setTimeout(function() {
            tooltip.css({
                'left': (rect.left + rect.width / 2 - parent_node.offset().left + parent_node.position().left - tooltip.width() / 2) + 'px',
                'top': (rect.top - tooltip.height() - parent_node.offset().top + parent_node.position().top - 15) + 'px',
                'display': 'block',
                'visibility': 'visible'
            });
        }, 200);
    },
    bind_close: function() {
        console.log("bind close");
        var self = this;
        $(document).click(function() {
            if (self.tooltip_active) {
                self.hide_tooltip();
            }
        });
    },
    hide_tooltip: function() {
        var self = this;
        var tt = $(self.tooltip);
        tt.attr("class", "").width(tt.data("init_w")).hide();
        tt.find("input").each(function() {
            $(this).val($(this).data("hint"));
        });
        self.tooltip_mode = 'add';
    },
    bind_click: function() {
        console.log('bind click');
        var self = this, new_node;
        $(this.tooltip).find("span").off("click").click(function(e) {
            switch (self.tooltip_mode) {
                case 'add':
                    self.build_wrapper($(this).data("apply"), function(wrapper) {
                        wrapper.appendChild(self.selection.range.extractContents());
                        self.selection.range.insertNode(wrapper);
                        self.hide_tooltip();
                    });
                    break;
                case 'remove':
                    self.build_wrapper($(this).data("apply"), function(wrapper) {
                    });
                    self.unwrap_me.contents().unwrap();
                    self.hide_tooltip();
                    break;
                default:
                    break;
            }
        });
        $(this.tooltip).children().click(function(e) {
            e.stopPropagation();
        });
    },
    edit_highlighted: function() {
        var self = this, clicked;
        $("#premade_box").find(".highlighted").live("click", function() {
            clicked = $(this);
            self.tooltip_active = true;
            self.position_tooltip({
                'left': clicked.offset().left,
                'top': clicked.offset().top + 5,
                'width': clicked.width(),
                'height': clicked.height()
            }, clicked.closest('.fixed'));
            self.tooltip_mode = 'remove';
            self.unwrap_me = $(this);
        });
    },
    build_wrapper: function(style, callback) {
        var self = this;
        console.log(style);
        switch (style) {
            case 'bold':
                var span = document.createElement("span");
                span.className = "bold highlighted";
                callback(span);
                break;
            case 'italic':
                var span = document.createElement("span");
                span.className = "italic highlighted";
                callback(span);
                break;
            case 'link':
                self.show_level(2, 'add_link');
                self.listen_input_submit('add_link', function(val) {
                    var a = document.createElement("a");
                    a.href = val;
                    a.target = "_blank";
                    a.className = "link highlighted";
                    callback(a);
                });
                break;
            default:
                break;
        }
    },
    listen_input_submit: function(node, callback) {
        node = $(this.tooltip).find(node);
        var input = node.find("input");
        var button = node.find("button");
        button.off('click').click(function() {
            callback(input.val());
        });
    },
    show_level: function(level, node) {
        var tt = $(this.tooltip);
        node = tt.find(node);
        tt.attr("class", "").addClass("show_" + level);
        tt.delay(400).animate({
            'left': '+=' + (tt.width() - node.width()) / 2 + 'px',
            'width': node.width() + 'px'
        }, 300);
    }

});
var select = new Select();