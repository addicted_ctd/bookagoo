var Drag = Editor.extend({
    box: "#premade_box",
    in_sight: false,
    ghost: false,
    ghost_block: ".ghost",
    fixed: ".fixed",
    active: false,
    fixed_child: ".fixed_child",
    edit: false,
    init: function() {
        var self = this;
        this.drag_block = $(".premade.drag_me");
        this.enable_drag(self.drag_block);
        this.enable_drag($(self.fixed));
        this.edit_child();
    },
    edit_child: function() {
        var self = this;
        $(document).on('focus', self.fixed_child, function(e) {
            self.edit = true;
        });
        $(document).on('blur', self.fixed_child, function(e) {
            self.edit = false;
        });
    },
    enable_drag: function(drag_me) {
        var tmp_block, offset;
        var self = this;
        var item;
        drag_me.off("mousedown");
        drag_me.on("mousedown", function() {
            item = $(this);
            if (!item.hasClass("block_drag")) {
                offset = item.offset();
                self.kill_track(function() {});
                self.active = setTimeout(function() {
                    if (!resize.active && !self.edit && !photo.inner_drag) {
                        self.unwrap_fixed();
                        $(".tmp_drag").remove();
                        if (item.hasClass("fixed")) {
                            tmp_block = item.removeClass('fixed').addClass("tmp_drag").appendTo("body");
                        } else {
                            tmp_block = item.clone().addClass("tmp_drag").appendTo("body");
                        }
                        tmp_block.css({
                            'top': offset.top + 'px',
                            'left': offset.left + 'px'
                        });
                        self.track(tmp_block);
                        self.track_hover(offset.left, offset.top, tmp_block);
                    }
                }, 200);
            }
        });
        drag_me.off('dblclick').on('dblclick', function() {
            if (!$(this).hasClass('fixed')) {
                new_fixed = $(this).clone().removeClass("tmp_drag drag_me ghost").addClass('fixed').addClass('for_tt');
                new_fixed.appendTo($(self.box).find(".content"));
                new_fixed.children().filter("p").addClass("fixed_child").attr("contenteditable", true);
                new_fixed.find(".remove_in_fixed").remove();
                self.justify();
                self.refresh_actions();
            }
        });
    },
    unwrap_fixed: function() {
        var item = $(".fixed");
        var len = item.length;
        for (var i = 0; i < len; i++) {
            if (item.eq(i).parent().is("wrapper"))
                item.eq(i).unwrap();
        }
        side_tt.remove_tt();
    },
    track: function(block) {
        var self = this;
        var w = (block.width() + parseInt(block.css("padding-left")) + parseInt(block.css("padding-right"))) / 2;
        var h = (block.height() + parseInt(block.css("padding-left")) + parseInt(block.css("padding-right"))) / 4;
        self.area = self.get_area();
        $(window).off("mousemove");
        $(window).mousemove(function(e) {
            block.css({
                'top': e.pageY - h + 'px',
                'left': e.pageX - w + 'px'
            });
            self.track_hover(e.pageX, e.pageY, block);
        });
        this.kill_track(function() {
            self.check_hover(block);
        });
    },
    check_hover: function(block, place) {
        var self = this;
        var new_fixed;
        block.remove();
        if (self.in_sight) {
            new_fixed = $(".ghost").removeClass("tmp_drag drag_me ghost").addClass('fixed').addClass('for_tt');
            new_fixed.children().filter("p").addClass("fixed_child").attr("contenteditable", true);
            new_fixed.find(".remove_in_fixed").remove();
            self.justify();
            self.refresh_css(block);
            self.refresh_actions();
        }
    },
    h_h: function(in_line) {
        var len = in_line.length;
        var max = 0;
        for (var i = 0; i < len; i++) {
            if (in_line[i].outerHeight() > max) {
                max = in_line[i].outerHeight()
            }
        }
        return max;
    },
    justify: function() {
        var self = this;
        var lines = [];
        var fixed = $('#premade_box').find('.premade');
        var len = fixed.length;
        if (len !== 0) {
            var c_l = [fixed.eq(0)], c_l_t = fixed.eq(0).position().top, h_h;
            for (var i = 1; i < len; i++) {
                if (fixed.eq(i).position().top === c_l_t) {
                    c_l.push(fixed.eq(i));
                } else {
                    c_l_t = fixed.eq(i).position().top;
                    lines.push(c_l);
                    c_l = [fixed.eq(i)];
                }
            }
            lines.push(c_l);
            len = lines.length;
            for (var l = 0; l < len; l++) {
                h_h = self.h_h(lines[l]);
                for (var k = 0; k < lines[l].length; k++) {
                    lines[l][k].outerHeight(h_h);
                }
            }
        }
    },
    process_placing: function(block, place) {
        var self = this;
        console.log(place);
        if (typeof place === "undefined") {
            block.appendTo($(self.box).find(".content"));
        } else {
            if (!place[0].hasClass("block_drag"))
                place[1] === "before" ? place[0].before(block) : place[0].after(block);
            $('.ghost').outerHeight(place[0].outerHeight());
        }
    },
    kill_track: function(callback) {
        var self = this;
        $(window).off("mouseup");
        $(window).on("mouseup", function() {
            clearTimeout(self.active);
            $(this).off("mousemove");
            callback();
        });
    },
    refresh_actions: function() {
        var self = this;
        self.in_sight = true;
        self.ghost = false;
        self.enable_drag($(self.fixed));
    },
    track_hover: function(x, y, block) {
        var self = this;
        var save_place, place;
        var hover_x = (x >= self.area.left && x <= self.area.right);
        var hover_y = (y >= self.area.top && y <= self.area.bottom);
        if (hover_x && hover_y) {
            self.in_sight = true;
            place = self.check_over_fixed(block, x, y);
            if (typeof place !== "undefined" && save_place !== place) {
                $(".ghost").remove();
                self.ghost = false;
                save_place = place;
                self.ghost_append(block, place);
            }
            if (!self.ghost) {
                self.ghost_append(block);
            }
        } else {
            self.in_sight = false;
            $(".ghost").remove();
            self.ghost = false;
        }
    },
    check_over_fixed: function(block, x, y) {
        var self = this;
        self.matrix = self.get_fixed_matrix();
        var matrix_len = self.matrix.length, over_el, hover_x, hover_y, item_pos, method;
        for (var i = 0; i < matrix_len; i++) {
            item_pos = self.matrix[i];
            hover_x = (x >= item_pos.left && x <= item_pos.right);
            hover_y = (y >= item_pos.top && y <= item_pos.bottom);
            if (hover_x && hover_y && item_pos) {
                over_el = $(self.fixed).eq(i);
                if (y <= item_pos.center_y) {
                    (Math.abs(x - item_pos.center_x) * item_pos.height / item_pos.width) <= Math.abs(y - item_pos.center_y) ? method = 'before' : method = 'after';
                } else {
                    (Math.abs(x - item_pos.center_x) * item_pos.height / item_pos.width) >= Math.abs(y - item_pos.center_y) ? method = 'before' : method = 'after';
                }
                return [over_el, method];
            }
        }
    },
    ghost_append: function(block, place) {
        var self = this;
        if (!self.ghost) {
            $(".ghost").remove();
            var new_ghost = block.clone().removeClass('tmp_drag').addClass("ghost");
            self.process_placing(new_ghost, place);
            self.refresh_css(new_ghost);
            self.ghost = true;
        }
    },
    refresh_css: function(item) {
        item.css({
            'left': 'auto',
            'top': 'auto'
        });
    },
    get_fixed_matrix: function() {
        var fixed = $(".fixed");
        var len = fixed.length;
        var matrix = [], tmp, pos, item, width, height;
        for (var i = 0; i < len; i++) {
            item = fixed.eq(i);
            width = item.width() + parseInt(item.css("padding-left")) + parseInt(item.css("padding-right"));
            height = item.height() + parseInt(item.css("padding-top")) + parseInt(item.css("padding-bottom"));
            pos = item.offset();
            tmp = {
                'center_x': pos.left + width / 2,
                'center_y': pos.top + height / 2,
                'bottom': pos.top + height,
                'left': pos.left,
                'top': pos.top,
                'right': pos.left + width,
                'width': width,
                'height': height
            };
            matrix.push(tmp);
        }
        return matrix;
    },
    get_area: function() {
        var self = this;
        var area = $(self.box);
        var pos = area.offset();
        var area = {
            'left': pos.left,
            'right': pos.left + area.width(),
            'top': pos.top,
            'bottom': pos.top + area.height()
        };
        return area;
    }

});

var drag = new Drag();