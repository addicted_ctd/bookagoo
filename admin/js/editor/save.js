var Save = Editor.extend({
    save_button: "#save",
    publish_button: "#publish",
    delete_button: "#delete",
    init: function(id) {
        this.id = id;
        this.bind_click();
    },
    bind_click: function() {
        var self = this;
        $(this.save_button).click(function() {
            self.save_action(0);
        });
        $(this.publish_button).click(function() {
            self.save_action(1);
        });
        $(this.delete_button).click(function() {
            if (confirm('А может не надо?'))
                self.delete_action(self.id);
        });
    },

    save_action: function(status) {
        var sight = $(".construction_sight");
        var self = this;
        var post_data = {
            'full_date': sight.find('#date').attr('full_date'),
            'dev_tpl': sight.html(),
            'tpl': self.form_clean_tpl(sight),
            'id': self.id,
            'title': sight.find("#title").text(),
            'thumb': sight.find("#top_pic").css("background-image").replace('url(','').replace(')',''),
            'lead': sight.find('.lead').text(),
            'status': status
        };
        console.log(post_data);
        Global.ajax({
            mod: "posts.save_dev",
            data: post_data
        }, function(data) {
            console.log(data);
            if(data == null) return;
            if (self.id === "new") {
                Global.log("Created post #" + data.data);
            } else {
                Global.log("Updated content of post #" + self.id);
            }
            alert('Сохранено');
            if (data.data !== "updated") {
                window.location.hash = "editor/" + parseInt(data.data);
            }
            if (status !== 0)
                sight.closest(".panel").find(".close").click();
        });
    },

    delete_action: function(id) {
        var self = this;
        Global.ajax({
            mod: "posts.delete_post",
            data: {
                id: id
            }
        }, function(data) {
            if(data == null) return;
            Global.log("Deleted post #" + id);
            router.navigate("posts", {trigger: true});
        });
    },

    form_clean_tpl: function(node) {
        var clean_node = node.clone();
        clean_node.find(".helper").remove();
        clean_node.find("#selection_tooltip").remove();
        clean_node.find("p.fixed_child").removeAttr("contenteditable");
        clean_node.find("h1").removeAttr("contenteditable");
        return clean_node.html();
    }

});

var save = new Save();