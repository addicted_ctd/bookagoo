var Video = Editor.extend({
    
    item: ".video.fixed",
    init: function() {
        this.listen_url();
    },
    
    listen_url: function() {
        var self = this;
        $(this.item).find("button").live("click", function() {
            console.log("click click");
            var url = $(this).parent().find("input").val();
            if (url !== "") {
                self.append_iframe(url, $(this).closest(".fixed"));
            }
        });
    },
    
    append_iframe: function(url, parent) {
        $.Mustache.load(router.tpl_path + 'editor.tpl', function() {
            parent.find("iframe").remove();
            parent.mustache('video_frame', {url: url}, {method: 'append'});
        });
    }
    
});

var video = new Video();