var Grid = Editor.extend({
    box: "#premade_box",
    w: 842,
    e_w: 800,
    step: {
        'x': 40,
        'y': 20
    },

    init: function() {
        this.sizes = [this.w/4, Math.floor(this.w/3), this.w/2, Math.floor(this.w/3*2), this.w/4*3, this.w];
    },
    
    snap: function(el, width, callback) {
        var len = this.sizes.length;
        var diff = 1000, closest = 0;
        var e_trigger = this.e_w - (this.e_w - this.w) / 2;
        console.log(width, e_trigger);
        if (width > e_trigger) {
            el.addClass('extended');
        } else {
            for (var i = 0; i < len; i++) {
                if (Math.abs(width - this.sizes[i]) < diff) {
                    closest = i;
                    diff = Math.abs(width - this.sizes[i]);
                }
            }
            el.removeClass('extended').outerWidth(this.sizes[closest]);
        }
        callback();
    }

});

var grid = new Grid();