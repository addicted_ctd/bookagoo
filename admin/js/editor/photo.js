/**/

var Photo = Editor.extend({
    inner_drag: false,
    init: function() {
        this.trigger_upload();
        this.listen_change();
        this.allow_drag();
        this.drop.check();
    },
    drop: {
        check: function() {
            if (typeof (window.FileReader) === 'undefined') {
                $(".photo").find("hint").html("");
            } else {
                $(".photo").find("hint").html("drag and drop");
                this.init();
            }
        },
        init: function() {
            this.listen();
            //this.block_unwanted();
        },
        block_unwanted: function() {
            $(document).on("drop", function(e) {
                e.preventDefault();
            });
            $(document).on("dragover", function(e) {
                e.preventDefault();
            });
        },
        listen: function() {
            var self = this;
            var photos = $(".construction_sight").find(".photo.fixed");
            photos.live("drop", function(e) {
                e.preventDefault();
                e.stopPropagation();
                var file = e.originalEvent.dataTransfer.files[0];
                self.upload_file(file, $(this), $(this).find("figure").css("background-image"));
            });
            photos.live("dragover", function(e) {
                e.preventDefault();
                e.stopPropagation();
                $(this).addClass("file_over");
            });
            photos.live("dragleave", function(e) {
                e.preventDefault();
                e.stopPropagation();
                $(this).removeClass("file_over");
            });
        },
        upload_file: function(file, fixed, fixed_bg) {
            var xhr = new XMLHttpRequest();
            var fd = new FormData();
            fd.append('photo', file);
            fd.append('request', 'upload_image');
            fd.append('mod', 'root.upload_image');
            xhr.onreadystatechange = stateChange;
            xhr.open('POST', 'php/router.php');
            xhr.send(fd);

            function stateChange(e) {
                if (e.target.readyState === 4 && e.target.status === 200) {
                    photo.upload_callback(xhr.responseText, fixed, fixed_bg);
                }
            }
        }
    },
    listen_change: function() {
        var self = this;
        $(".load_image").live("change", function() {
            self.change_action($(this));
        });
    },
    change_action: function(up) {
        var self = this;
        var fixed = up.closest(".fixed");
        var fixed_bg = fixed.find("figure").css("background-image");
        fixed.find("form").ajaxSubmit({
            success: function(data) {
                self.upload_callback(data, fixed, fixed_bg);
            }
        });
    },
    upload_callback: function(data, fixed, fixed_bg) {
        console.log(data);
        data = JSON.parse(data);
        console.log(data);
        data = data.data;
        if (typeof data.error === "undefined") {
            if (data === "") {
                fixed.find("figure").css("background-image", fixed_bg);
                fixed.find("img").attr("src", fixed_bg);
            } else {
                fixed.removeClass("file_over").find("figure").css({
                    "background-image": "url(uploads/" + data.file + ")",
                    "width": data.size.width + 'px',
                    "height": data.size.height + 'px'
                }).attr({
                    "init_w": data.size.width,
                    "init_h": data.size.height
                });
                var fixed_img = fixed.find("img");
                fixed_img.attr({
                    "src": "uploads/" + data.file,
                });
                if (fixed.parent().prop("tagName").toLowerCase() === "gallery") {
                    gallery.upload_action(fixed);
                }
            }
        } else {
            alert(data.error);
        }
    },
    trigger_upload: function() {
        $(".upload_photo").die("click").live("click", function() {
            $(this).closest(".fixed").find(".load_image").click();
        });
    },
    allow_drag: function() {
        var closest, self = this, figure;
        $(".photo_image").die("dblclick").live("dblclick", function() {
            closest = $(this).closest(".fixed");
            figure = $(this);
            if (!self.inner_drag) {
                closest.addClass("inner_drag");
                self.drag_on($(this));
                self.zoom.on($(this));
            } else {
                closest.removeClass("inner_drag");
                self.zoom.off();
            }
            self.inner_drag = !self.inner_drag;
        });
    },
    drag_on: function(block) {
        var init_pos, init_click, max_shift, pos;
        var self = this;
        block.on("mousedown", function(e) {
            pos = {};
            init_pos = block.position();
            init_click = [e.pageX, e.pageY];
            max_shift = self.drag_limits($(this));
            $(window).on("mousemove", function(e) {
                if (self.inner_drag) {
                    pos.left = init_pos.left - init_click[0] + e.pageX;
                    pos.top = init_pos.top - init_click[1] + e.pageY;
                    pos = self.snap_to_limits(pos, max_shift);
                    block.css({
                        'left': pos.left + 'px',
                        'top': pos.top + 'px'
                    });
                }
            });
        });

        $(window).on("mouseup", function() {
            $(this).off("mousemove");
        });
    },
    drag_limits: function(figure) {
        var parent = figure.closest(".fixed");
        return {
            'left': figure.width() > parent.width() ? Math.abs((parent.width() - figure.width() + 10)) : 0,
            'top': figure.height() > parent.height() ? Math.abs((parent.height() - figure.height() + 10)) : 0
        };
    },
    snap_to_limits: function(pos, limits) {
        if (pos.left < 0 && pos.left < -limits.left) {
            pos.left = -limits.left;
        }
        if (pos.left > 0) {
            pos.left = 0;
        }
        if (pos.top < 0 && pos.top < -limits.top) {
            pos.top = -limits.top;
        }
        if (pos.top > 0) {
            pos.top = 0;
        }
        return pos;
    },
    zoom: {
        _tpl: '<input type="range" name="scale" min="-5" max="20" class="zoom_rate helper">',
        step: 0.1,
        center: 13,
        on: function(block) {
            var self = this;
            var fixed = block.closest(".fixed");
            fixed.append($(this._tpl));
            $(".zoom_rate").val(fixed.attr('zoom'));
            this.listen_zoom(block, fixed);
        },
        listen_zoom: function(figure, parent) {
            var range = parent.find(".zoom_rate");
            var current = range.attr("max") - range.val();
            var zoom_limits = this.get_limits(figure, parent);
            var self = this;
            range.on('change mousemove', function() {
                if ($(this).attr("max") - $(this).val() !== current) {
                    current = $(this).attr("max") - $(this).val();
                    console.log(current);
                    self.apply_zoom(current, figure);
                }
            });
        },
        apply_zoom: function(rate, figure) {
            var set_rate = $(".zoom_rate").attr("max") - rate;
            rate = 1 + (rate - this.center) * this.step;
            console.log(figure);
            var pos = photo.snap_to_limits({
                'left': parseInt(figure.css("left")) * rate,
                'top': parseInt(figure.css("top")) * rate
            }, photo.drag_limits(figure));
            figure.css({
                'width': figure.attr("init_w") * rate + 'px',
                'height': figure.attr("init_h") * rate + 'px',
                'left': pos.left + 'px',
                'top': pos.top + 'px'
            }).attr('zoom', set_rate);

        },
        off: function() {
            var fixed = $(".fixed");
            fixed.find(".zoom_rate").remove();
        },
        get_limits: function(figure, parent) {
            return true;
        }

    }

});

var photo = new Photo();
