var Side_tooltip = Editor.extend({
    
    tt: "#side_tooltip",
    action_to: "",
    init: function() {
        var self = this;
        $(this.tt).remove();
        $.Mustache.load(router.tpl_path + '/editor.tpl', function() {
            $("#premade_box").mustache('side_tooltip', {});
            self.listen_hovers();
            self.listen_clicks();
        });
    },
    listen_hovers: function() {
        var self = this;
        $(".main_content").find('.for_tt').live({
            mouseenter: function(e) {
                if (!resize.active) {
                    e.stopPropagation();
                    /*if (!$(this).parent().is("wrapper"))
                        $(this).wrap('<wrapper class="clearfix">');*/
                    self.action_to = $(this);
                    self.show_tt($(this));
                }
            }
        });
    },
    
    show_tt: function(block) {
        //$(this.tt).appendTo(block.closest('wrapper'));
        this.position_tt(block);
        $(this.tt).fadeIn();
    },
    
    remove_tt: function() {
        $(this.tt).hide();
    },

    position_tt: function(block) {
        var diff = 0;
        if (block.width() > 850) {
            diff = $(this.tt).outerWidth();
        }
        $(this.tt).css({
            left: (block.offset().left - $("#premade_box").offset().left + block.outerWidth() - diff) + 'px',
            top: block.position().top + 'px'
        });
    },
    
    listen_clicks: function() {
        var self = this;
        $(this.tt).find("span[data-action]").click(function() {
            $(self.tt).hide();
            switch ($(this).data("action")) {
                case "remove": 
                    self.remove_action();
                    break;
                case "settings":
                    break;
                default:
                    break;
            }
        });
    },
    
    remove_action: function() {
        this.action_to.remove();
    }

});

var side_tt = new Side_tooltip();