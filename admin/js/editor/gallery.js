var Gallery = Editor.extend({

	thumbs: 'thumbs',
	thumb: '<thumb class="helper"></thumb>',
	t_margin: 10,
	t_w: 20,
	init: function() {
		this.let_thumbs_live();
	},

	upload_action: function(fixed) {
		console.log('in gallery');
		fixed.removeClass('helper');
		this.add_slide(fixed.parent());
	},

	resize_action: function(block) {
		console.log('gallery item resize');
		this.recalculate(block.find('gallery'));
	},

	add_slide: function(g) {
		var self = this;
		var thumbs;
		var slides = g.find('figure.photo_image');
		var not_empty = 0;
		var len = slides.length;
		for (var i = 0; i < len; i++) {
			if (slides.eq(i).css('background-image') !== 'none')
				not_empty++;
		}
		if (not_empty === len) {
			$.Mustache.load(router.tpl_path + '/editor.tpl', function() {
	            g.mustache('gallery_item', {});
	            thumbs = g.closest(".fixed").find(self.thumbs);
	            thumbs.find('thumb').eq(-1).removeClass('helper');
	            thumbs.append(self.thumb);
	            console.log(thumbs.length);
	            self.thumbs_recalc(g.closest('.fixed'));
	            self.recalculate(g);
	       	});
		}
	},

	thumbs_recalc: function(fixed) {
		var thumbs = fixed.find(this.thumbs);
		var c = thumbs.find('thumb');
		var len = c.length;
		var self = this;
		console.log(len);
		thumbs.css({
			'width': len * self.t_w + (len - 1) * self.t_margin + 'px',
			'margin-left': - ((len - 1) * self.t_w + (len - 2) * self.t_margin) / 2 + 'px'
		});
	},

	recalculate: function(g) {
		var w = 0;
		var slides = g.children();
		console.log(slides);
		slides.css({
			'max-width': g.parent().outerWidth() + 2 + 'px',
			'min-width': g.parent().outerWidth() + 2 + 'px',
		});
		var len = slides.length;
		for (var i = 0; i < len; i++) {
			w += slides.eq(i).outerWidth();
		}
		console.log(w);
		g.innerWidth(w + 2);
	},

	let_thumbs_live: function() {
		var g, step, eq;
		$('thumb').die('click').live('click', function() {
			console.log('click');
			eq = $(this).index();
			g = $(this).closest('.fixed').find('gallery_wrapper');
			console.log(eq);
			step = g.outerWidth();
			g.animate({
				'scrollLeft': eq * step + 'px'
			}, 800);
		});
	}

});

var gallery = new Gallery();