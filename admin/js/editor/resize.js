var Resize = Editor.extend({
    box: "#resize_box",
    resizer: ".resizer",
    active: false,
    load: function() {
        var self = this;
        this.listen_hovers();
        this.listen_drag();
    },
    kill: function() {
        $(this.box).remove();
    },
    listen_hovers: function() {
        var self = this, entered;
        $(document).on('mouseenter', drag.fixed, function(e) {
            e.stopPropagation();
            if (!self.active) {
                if (!$(this).hasClass('block_resize')) {
                    if ($(self.box).length === 0) {
                        entered = $(this);
                        $.Mustache.load(router.tpl_path + '/editor.tpl', function() {
                            $("body").mustache('resize_box', {});
                            self.process_show(entered);
                        });
                    } else {
                        self.process_show($(this));
                    }
                } else {
                    $(this).parent().trigger('mouseenter');
                }
            }
        });
        $(document).on('mouseleave', drag.fixed, function() {
            if (!self.active && !$(this).hasClass('block_resize')) {
                self.process_hide();
                //$(this).parent().trigger('mouseenter');
            }
        });
    },
    process_show: function(block) {
        $(this.box).appendTo(block).show();
    },
    process_hide: function() {
        $(this.box).appendTo("body").hide();
    },
    listen_drag: function() {
        var self = this;
        $(document).on('mousedown', self.resizer, function(e) {
            drag.unwrap_fixed();
            self.active = true;
            self.track_resize($(this), e.pageX, e.pageY);
            self.kill_track($(this));
        });
    },
    track_resize: function(block, start_x, start_y) {
        var self = this;
        var fixed = block.closest(".fixed");
        var step_x, step_y, sign, in_line, in_line_q;
        $(window).off("mousemove").on("mousemove", function(e) {
            side_tt.remove_tt();
            step_x = e.pageX - start_x;
            step_y = e.pageY - start_y;
            if (Math.abs(step_x) >= grid.step.x) {
                if (!fixed.hasClass('block_drag') || fixed.hasClass('exception')) {
                    grid.snap(fixed, fixed.width() + step_x, function() {
                        start_x = block.offset().left + 4;
                    });
                }
            }
            if (Math.abs(step_y) >= grid.step.y) {
                sign = Math.abs(step_y) / step_y;
                in_line = self.get_in_line_fixed(fixed);
                in_line_q = in_line.length;
                fixed.outerHeight('+=' + grid.step.y * sign);
                for (var i = 0; i < in_line_q; i++) {
                    in_line[i].outerHeight(fixed.outerHeight());
                }
                start_y = block.offset().top + 4;
            }
        });
    },

    get_in_line_fixed: function(fixed) {
        var in_line = [];
        var lines_pos = fixed.offset().top;
        var all_fixed = $(".fixed");
        var len = all_fixed.length;
        for (var i = 0; i < len; i++) {
            if (all_fixed.eq(i).offset().top === lines_pos) {
                in_line.push(all_fixed.eq(i));
            }
        }
        return in_line;
    },

    kill_track: function(block) {
        var self = this;
        $(document).on("mouseup", function() {
            $(window).off("mousemove");
            self.active = false;
            drag.justify();
            console.log(block.closest('.fixed'));
            if (block.closest('.fixed').hasClass('gallery')) {
                gallery.resize_action(block.closest('.fixed'));
            }
        });
    }

});

var resize = new Resize();