var PageRouter = Backbone.Router.extend({

    tpl_path: 'tpl',
    main: 'main',

    routes: {
        '': function() {
            
        },

        'posts': function() {
        	Menu.highlight('posts');
            News.init();
        },

        'posts/:id': function(id) {
            Menu.highlight('posts');
            News.Meta.init(id);
        },

        'comments': function() {
            Menu.highlight('comments');
            Comments.init();
        },

        'comments/add': function() {
            Menu.highlight('comments');
            Comments.Add.init();
        },

        'categories': function() {
            Menu.highlight('categories');
            Categories.init();
        },

        'tags': function() {
            Menu.highlight('tags');
            Tags.init();
        },

        'api': function() {
            Menu.highlight('api');
        },

        'editor/:id': function(id) {
            console.log(id);
            Menu.highlight('posts');
            editor.init(id);
        },

        'rubrics': function() {
            Menu.highlight('rubrics');
            Rubrics.init();
        },

        'rubrics/editor/:id': function(id) {
            Menu.highlight('rubrics');
            Rubrics.editor_init(id);
        },

        'logout': function() {
            Login.logout();
        },

        'users': function() {
            Menu.highlight('users');
            Users.init();
        },

        'users/:id': function(id) {
            Menu.highlight('users');
            Users.Editor.init(id);
        },

        'ecommerce': function() {
            Menu.highlight('ecommerce');
            Ecommerce.Catalog.init();
        },

        'ecommerce/editor/:id': function(id) {
            Menu.highlight('ecommerce');
            Ecommerce.Editor.init(id);
        },

        'orders': function() {
            Menu.highlight('orders');
            Orders.init();
        },

        'orders/person/:uid': function(uid) {
            console.log(uid);
            Menu.highlight('orders');
            Orders.Person.init(uid);
        },

        'orders/person/:uid/order/:id': function(uid, id) {
            console.log(uid, id);
            Menu.highlight('orders');
            Orders.Person.init(uid, id);
        },

        'coupons': function() {
            Menu.highlight('coupons');
            Coupons.init();
        },

        'general': function() {
            Menu.highlight('general');
            General.init();
        }

    },
});


var router = new PageRouter();
if (!Backbone.History.started)
    Backbone.history.start();


