var register = {

	reg_mail: '',

	init: function() {
		this.check_hash();
	},

	check_hash: function() {
		var self = this;
		Global.ajax({
			mod: 'user.check_reg_hash',
			data: {
				reg_hash: window.location.hash.replace('#', '')
			}
		}, function(data) {
			if (typeof data.data.error !== 'undefined')
				window.location.href = 'http://google.com';
			self.bind();
		});
	},

	bind: function() {

		var user = $('#username');
		var pass_1 = $('#password');
		var pass_2 = $('#pass_repeat');

		$("#submit").click(function() {
			if (pass_1.val() !== pass_2.val()) return Global.input_highlight(pass_2);
			if (!Global.check_inputs([user, pass_1, pass_2])) return;
			Global.ajax({
				mod: 'user.register',
				data: {
					reg_hash: window.location.hash.replace('#', ''),
					pass: pass_2.val(),
					user: user.val()
				}
			}, function(data) {
				if (typeof data.data.error !== 'undefined') {
					return alert('Пользователь с таким именем уже существует');
				}
				window.location.href = 'index.php';
				console.log(data);
			});
 		});
	}
};

register.init();