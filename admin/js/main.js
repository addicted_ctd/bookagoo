$(document).ready(function() {

	check_auth();
	Menu.init();
	Global.init();

});

function check_auth() {
	Global.ajax({
		mod: "user.check_auth"
	}, function(data) {
		data = data.data;
		if (data) {
			$.Mustache.load('tpl/index.tpl', function() {
	            $("content").mustache('index', {}, {method: 'html'});
	        });
		} else {
			$.Mustache.load('tpl/index.tpl', function() {
	            $("content").mustache('login', {}, {method: 'html'});
	            Login.init();
	        });
		}
	});
}

var Menu = {

	el: "menu",

	highlight: function(route) {
		$(this.el).find(".active").removeClass("active");
		$(this.el).find("[data-route="+route+"]").addClass("active");
	},

	init: function() {
		this.bind_click();
	},

	bind_click: function() {
		$(document).on("click", this.el + " li" ,function() {
			router.navigate($(this).data("route"), {trigger: true});
		});
	}

};	

var Login = {

	init: function() {
		this.bind_click();
	},

	bind_click: function() {

		var self = this;
		var user = $("#username");
		var pass = $("#password");

		$("#submit").click(function() {
			if (typeof $(this).attr("disabled") !== "undefined") return;
			if (self.check_inputs([user, pass])) {
				$(this).prop("disabled", true);
				Global.ajax({
					mod: "user.login",
					data: {
						name: user.val(),
						password: pass.val()
					}
				}, function(data) {
					if (data.data) {
						check_auth();
					} else {
						$("#submit").removeAttr("disabled");
						self.show_alert();
					}
				});
			}
		});

		$("#password, #username").on("keyup", function(e) {
			if (e.keyCode === 13) {
				$("#submit").click();
			}
		});
	},

	check_inputs: function(req_arr) {
		var len = req_arr.length;
        var check = true;
        for (var i = 0; i < len; i++) {
            if (req_arr[i].val() === "") {
                check = false;
                this.show_alert();
            }
        }
        return check;
	},

	show_alert: function() {
		alert('Неправильные данные');
	},

	logout: function() {
		Global.ajax({
			mod: "user.logout"
		}, function(data) {
			router.navigate("", {trigger: true});
			check_auth();
		});	
	}

};