var Global = {

    init: function() {
        this.bind_events();
    },

    pagination: function(num_pages) {
        var self = this;
        var pagination = $('.pagination').find('div');
        pagination.attr('total_pages', num_pages);
        pagination.find('p').remove();
        for (var i = num_pages; i > 0; i--) {
            pagination.prepend('<p>'+i+'</p>');
        }
        this.to_page(1);
    },

    to_page: function(page_num) {
        var pagination = $('.pagination').find('div');
        var total = pagination.attr('total_pages');
        var pages = pagination.find('p');
        pages.removeClass('active');
        pages.filter(":contains('"+page_num+"')").addClass('active');
        console.log(total);
    },

    bind_events: function() {
        var self = this;
        $(document).on("submit", ".callback_ajax", function(e) {
            e.preventDefault();
            var names = $(this).find("[name]");
            var len = names.length;
            var data = {};
            var form = $(this);
            for (var i = 0; i < len; i++) {
                data[names.eq(i).attr("name")] = names.eq(i).val();
            }
            self.ajax({
                mod: form.attr("mod") + "." + form.attr("action"),
                data: data
            }, function(cb_data) {
                self.trigger_event("proceed", form.get(0), cb_data.data);
            });
        });
    },

    trigger_event: function(event_name, target, vars) {
        if (!target) target = document.body;
        var event;
        if (document.createEvent) {
            event = document.createEvent("HTMLEvents");
            set_vars();
            event.initEvent(event_name, true, true);
        } else {
            event = document.createEventObject();
            set_vars();
            event.eventType = event_name;
        }

        event.eventName = event_name;

        if (document.createEvent) {
            target.dispatchEvent(event);
        } else {
            target.fireEvent("on" + event.eventType, event);
        }

        function set_vars() {
            event.data = {};
            for (var key in vars) {
                event.data[key] = vars[key];
            }
        }
    },

    ajax: function(params, callback) {
        if (!params.hasOwnProperty('data')) params.data = {};
        $.ajax({type:'POST',url:'php/router.php',dataType:'JSON',data:{mod:params.mod,data:JSON.stringify(params.data)},success:function(data){console.log(data);if(data === null) alert("Insuficient privileges");if (typeof callback !== "undefined") callback(data);}});
    },

    test_ajax: function(params, callback) {
        if (!params.hasOwnProperty('data')) params.data = {};
        $.ajax({type:'POST',url:'php/router.php',data:{mod:params.mod,data:JSON.stringify(params.data)},success:function(data){if(typeof callback!= "undefined")callback(data);}});
    },

    log: function(event) {
        console.log("log: " + event);
        $.ajax({
            type: 'POST',
            url: 'php/router.php',
            dataType: 'JSON',
            data: {
                mod: "user.log",
                data: JSON.stringify({event: event})
            },
            success: function(data) {
                console.log(data);
                if (data.data) {
                    console.log("Log successful");
                }
            }
        });
    },

    check_inputs: function(req_arr) {
        var len = req_arr.length;
        var check = true;
        for (var i = 0; i < len; i++) {
            if (req_arr[i].val() === "") {
                check = false;
                this.input_highlight(req_arr[i]);
            }
        }
        return check;
    },

    input_highlight: function(block) {
        block.addClass('wrong');
        setTimeout(function() {
            block.removeClass('wrong');
        }, 600);
    }

}