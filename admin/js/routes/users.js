var RouteUsers = Backbone.View.extend({
    
	tpl: 'users.tpl',
    page: 1,
    list: {},
    sort_criteria: 0,
    sort_direction: 'down',
    search_query: '',

    init: function() {
    	this.render();
        this.bind_sort();
    },

    render: function(data) {
    	var self = this;
    	$.Mustache.load(router.tpl_path + '/' + this.tpl, function() {
            $(router.main).mustache('users', {}, {method: 'html'});
            self.pagination();
            self.refresh_list();
            self.bind_search();
        });
    },

    bind_search: function() {
        var self = this;
        var refresh_tm;
        $('#users').find('.search').on('keyup', function() {
            self.search_query = $(this).val();
            clearTimeout(refresh_tm);
            refresh_tm = setTimeout(function() {
                self.pagination();
                self.refresh_list();
            }, 500);
        });
    },

    refresh_list: function() {
    	var self = this;
    	this.fetch(function(data) {
            console.log(data);
    		self.render_list(data.data);
    	});
    },

    bind_sort: function() {

        var criteria;
        var self = this;

        $(document).on('click', '#users .all li', function() {
            criteria = $(this).index();
            $(this).closest('.all').find('li').removeClass('sort_criteria');
            if (criteria === self.sort_criteria) {
                self.sort_direction = self.sort_direction === 'up' ? 'down' : 'up';
            }
            $(this).addClass('sort_criteria').attr('sort_direction', self.sort_direction);
            self.sort_criteria = criteria;
            console.log(self.sort_criteria, self.sort_direction);
            self.sort_list();
        });
    },

    sort_list: function() {
        var self = this;
        var list_items = $('.scroll_content').find('article').get();
        var list = $('.scroll_content');
        list_items.sort(function(a, b) {
            if (self.sort_criteria > 2) {
                return parseInt($(a).find('div').eq(self.sort_criteria).text()) > parseInt($(b).find('div').eq(self.sort_criteria).text());
            } else {
                return $(a).find('div').eq(self.sort_criteria).text().toUpperCase().localeCompare($(b).find('div').eq(self.sort_criteria).text().toUpperCase());
            }
        });
        if (self.sort_direction === 'up')
            list_items.reverse();
        list.empty();
        $.each(list_items, function(idx, itm) {list.append(itm);});
    },

    pagination: function() {

        var self = this;

        Global.ajax({
            mod: 'user.calculate_pages',
            data: {
                search_query: self.search_query
            }
        }, function(data) {
            Global.pagination(data.data);
        });

        $(document).on('click', '#users .pagination p', function() {
            if (!$(this).hasClass('active')) {
                Global.to_page($(this).text());
                self.page = parseInt($(this).text());
                self.refresh_list();
            }
        });

        var current_page;
        var pagination = $('.pagination');

        pagination.find('a').click(function() {
            current_page = parseInt(pagination.find('.active').text());
            if (current_page !== parseInt(pagination.find('div').attr('total_pages'))) {
                current_page++;
                Global.to_page(current_page);
                self.page = current_page;
                self.refresh_list();
            }
        });
    },

    render_list: function(data) {
    	var self = this;
    	$.Mustache.load(router.tpl_path + '/' + this.tpl, function() {
            $('#users').find('.scroll_content').mustache('user_list', {data}, {method: 'html'});
            self.sort_list();
        });
    },

    fetch: function(callback) {
        var self = this;
    	Global.ajax({
            mod: "user.get_list",
            data: {
                page: self.page,
                search_query: self.search_query
            }
        }, function(data) {
            callback(data);
        });
    }

});

var Users = new RouteUsers();