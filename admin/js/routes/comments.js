var RouteComments = Backbone.View.extend({
    
	tpl: 'comments.tpl',

    init: function() {
    	this.render();
    },

    render: function(data) {
    	var self = this;
    	$.Mustache.load(router.tpl_path + '/' + this.tpl, function() {
            $(router.main).mustache('comments', {}, {method: 'html'});
            self.render_list();
        });
    },

    render_list: function() {
        var self = this;
        this.get_list(function(data) {
            $.Mustache.load(router.tpl_path + '/' + self.tpl, function() {
                $(router.main).find("#list").mustache('comments_list', data, {method: 'html'});
            });
        });
    },

    get_list: function(cb) {
        Global.ajax({
            mod: "comments.render_list"
        }, function(data) {
            try {
                cb(data);
            } catch (e) {
                console.log(e);
            }
        });
    },

    Add: {

        tpl: 'comments.tpl',

        init: function() {
            this.render();
        },

        render: function() {
            var self = this;
            $.Mustache.load(router.tpl_path + '/' + this.tpl, function() {
                $(router.main).mustache('add_comment', {}, {method: 'html'});
                self.bind_events();
            });
        },

        bind_events: function() {
            $("#add_comment").on("proceed", function(e) {
                console.log("proceed");
                console.log(e.originalEvent.data);
            });
        },

        exec: function() {
            Global.ajax(data, function(data) {
                console.log(data);
            });
        }
    }

});

var Comments = new RouteComments();