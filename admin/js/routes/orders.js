var RouteOrders = Backbone.View.extend({
    
	tpl: 'orders.tpl',
    page: 1,
    sort_criteria: 0,
    sort_direction: 'down',
    search_query: '',

    init: function() {
        this.render();
        this.bind_sort();
    },

    render: function(data) {
        var self = this;
        $.Mustache.load(router.tpl_path + '/' + this.tpl, function() {
            $(router.main).mustache('orders', {}, {method: 'html'});
            self.render_list();
            self.pagination();
            self.bind_search();
        });
    },

    bind_search: function() {
        var self = this;
        var refresh_tm;
        $('#orders').find('.search').on('keyup', function() {
            self.search_query = $(this).val();
            clearTimeout(refresh_tm);
            refresh_tm = setTimeout(function() {
                self.render_list();
                self.pagination();
            }, 500);
        });
    },

    bind_sort: function() {

        var criteria;
        var self = this;

        $(document).on('click', '#orders .all li', function() {
            criteria = $(this).index();
            $(this).closest('.all').find('li').removeClass('sort_criteria');
            if (criteria === self.sort_criteria) {
                self.sort_direction = self.sort_direction === 'up' ? 'down' : 'up';
            }
            $(this).addClass('sort_criteria').attr('sort_direction', self.sort_direction);
            self.sort_criteria = criteria;
            self.sort_list();
        });
    },

    sort_list: function() {
        var self = this;
        var list_items = $('.scroll_content').find('article').get();
        var list = $('.scroll_content');
        list_items.sort(function(a, b) {
            if (self.sort_criteria === 2) {
                return parseInt($(a).find('div').eq(self.sort_criteria).text()) > parseInt($(b).find('div').eq(self.sort_criteria).text());
            } else {
                return $(a).find('div').eq(self.sort_criteria).text().toUpperCase().localeCompare($(b).find('div').eq(self.sort_criteria).text().toUpperCase());
            }
        });
        if (self.sort_direction === 'up')
            list_items.reverse();
        list.empty();
        $.each(list_items, function(idx, itm) {list.append(itm);});
    },

    render_list: function() {
        var self = this;
        Global.ajax({
            mod: 'orders.fetch_list',
            data: {
                page: self.page,
                search_query: self.search_query
            }
        }, function(data) {
            $.Mustache.load(router.tpl_path + '/' + self.tpl, function() {
                $('#orders').find('.scroll_content').mustache('orders_list', {data: data.data}, {method: 'html'});
                self.sort_list();
            });
        });
    },

    pagination: function() {

        var self = this;

        Global.ajax({
            mod: 'orders.calculate_pages',
            data: {
                search_query: self.search_query
            }
        }, function(data) {
            Global.pagination(data.data);
        });

        $(document).on('click', '#orders .pagination p', function() {
            if (!$(this).hasClass('active')) {
                Global.to_page($(this).text());
                self.page = parseInt($(this).text());
                self.render_list();
            }
        });

        var current_page;
        var pagination = $('.pagination');

        pagination.find('a').click(function() {
            current_page = parseInt(pagination.find('.active').text());
            if (current_page !== parseInt(pagination.find('div').attr('total_pages'))) {
                current_page++;
                Global.to_page(current_page);
                self.page = current_page;
                self.render_list();
            }
        });
    },
 
    Person: {
        init: function(uid, id) {
            var self = this;
            if (typeof id === 'undefined') {
                self.render_cb = self.show_profile;
            } else {
                self.init_show_id = id;
                self.render_cb = self.show_order;
            }
            Global.ajax({
                mod: 'orders.person_orders',
                data: {
                    uid: uid
                }
            }, function(data) {
                self.render(data);
                console.log(data);
            });
        },

        show_profile: function() {
            $('.person_info').addClass('opened');
        },

        show_order: function() {
            $('.order[data-id="'+this.init_show_id+'"]').addClass('opened');
        },

        render: function(data) {
            var self = this;
            $.Mustache.load(router.tpl_path + '/' + Orders.tpl, function() {
                $(router.main).mustache('person_orders', {data: data.data}, {method: 'html'});
                self.bind();
                self.render_cb();
                self.bind_popup();
            });
        },

        bind: function() {

            $("select[data-selected]").each(function() {
                $(this).find('[value="' + $(this).data('selected') + '"]').attr('selected', true);
            });

            var delivery_type, delivery_address, order_status, update_tm_3, coupon_code;
            var order_id, box_quantity, update_tm, update_tm_2, delivery_price;

            $('select.order_status').on('change', function() {
                order_status = $(this).val();
                order_id = $(this).closest('.order').data('id');
                Global.test_ajax({
                    mod: 'orders.update_order_status',
                    data: {
                        order_status: order_status,
                        order_id: order_id
                    }
                }, function(data) {
                    console.log(data);
                });
            });

            $('select.delivery_type').on('change', function() {
                delivery_type = $(this).val();
                order_id = $(this).closest('.order').data('id');
                Global.ajax({
                    mod: 'orders.update_delivery_type',
                    data: {
                        delivery_type: delivery_type,
                        order_id: order_id
                    }
                }, function(data) {
                    console.log(data);
                });
            });

            $('.order_title').click(function() {
                $(this).closest('.order').toggleClass('opened');
            });

            $('.person_info').find('.title').find('span').click(function() {
                $(this).closest('.person_info').toggleClass('opened');
            });

            $('.delivery_address').on('keyup', function(e) {
                clearInterval(update_tm);
                order_id = $(this).closest('.order').data('id');
                delivery_address = $(this).text().trim();
                update_tm = setTimeout(function() {
                    Global.ajax({
                        mod: 'orders.update_delivery_address',
                        data: {
                            delivery_address: delivery_address,
                            order_id: order_id
                        }
                    }, function(data) {
                        console.log(data);
                    });
                }, 1000);
            });

            $('.box_quantity, .delivery_price').on('keydown', function(e) {
                if (!(/^\d+$/.test(String.fromCharCode(e.keyCode))) 
                    && e.keyCode !== 8
                    && e.keyCode !== 37
                    && e.keyCode !== 38
                    && e.keyCode !== 39
                    && e.keyCode !== 40) return e.preventDefault();
            });

            $('.box_quantity').on('keyup', function(e) {
                clearInterval(update_tm);
                order_id = $(this).closest('.order').data('id');
                box_quantity = $(this).text().trim();
                if (box_quantity.length === 0) return;
                update_tm = setTimeout(function() {
                    Global.ajax({
                        mod: 'orders.update_box_quantity',
                        data: {
                            box_quantity: box_quantity,
                            order_id: order_id
                        }
                    }, function(data) {
                        $('.box_total').html(data.data.box_total);
                        $('[data-id='+order_id+']').find('.total').html(data.data.total);
                    });
                }, 1000);
            });

            $('.delivery_price').on('keyup', function(e) {
                clearInterval(update_tm_2);
                order_id = $(this).closest('.order').data('id');
                delivery_price = $(this).text().trim();
                if (delivery_price.length === 0) return;
                update_tm_2 = setTimeout(function() {
                    Global.ajax({
                        mod: 'orders.update_delivery_price',
                        data: {
                            delivery_total: delivery_price,
                            order_id: order_id
                        }
                    }, function(data) {
                        $('[data-id='+order_id+']').find('.total').html(data.data.total);
                    });
                }, 1000);
            });

            $('.coupon_code').on('keyup', function(e) {
                clearInterval(update_tm_3);
                order_id = $(this).closest('.order').data('id');
                coupon_code = $(this).text().trim();
                if (coupon_code.length === 0) return;
                update_tm_3 = setTimeout(function() {
                    Global.ajax({
                        mod: 'orders.set_coupon',
                        data: {
                            coupon_code: coupon_code,
                            order_id: order_id
                        }
                    }, function(data) {
                        console.log(data);
                        data = data.data;
                        if (data) {
                            if (data.hasOwnproperty('error')) {
                                alert('Купон недействителен');
                            } else {
                                $('[data-id='+order_id+']').find('.total').html(data.total);
                                $('[data-id='+order_id+']').find('.discount_total').html(data.discount_total);
                            }
                        }
                    });
                }, 1000);
            });

        },

        bind_popup: function() {

            var self = this;
            var order_id;

            $('article.cards').click(function() {
                order_id = $(this).closest('.order').data('id');
                Global.ajax({
                    mod: 'orders.popup_data',
                    data: {
                        order_id: order_id
                    }
                }, function(data) {
                    self.add_aditional(data.data);
                    console.log(data);
                });
                $('body').addClass('ov');
            });

            $("#overlay").find('.back').click(function() {
                $('body').removeClass('ov');
            });

            var num, val, dir, card_id;

            $("#overlay").find('.cards').find('li').find('span').click(function() {
                num = $(this).parent().find('num');
                val = parseInt(num.text());
                card_id = $(this).closest('li').data('item');
                dir = $(this).index() === 1 ? -1 : 1;
                if (val > 0) {
                    num.text(val + dir);
                } else {
                    if (dir > 0) {
                        num.text(val + dir);
                    }
                }
                val = parseInt(num.text());
                Global.ajax({
                    mod: 'orders.update_details',
                    data: {
                        order_id: order_id,
                        card_id: card_id,
                        quantity: val
                    }
                }, function(data) {
                    $('[data-id='+order_id+']').find('.cards_total').html(data.data.cards_total);
                    $('[data-id='+order_id+']').find('.total').html(data.data.total);
                });
            });

        },

        add_aditional: function(data) {
            var cards = $('#overlay').find('.cards');
            cards.find('li').find('aside').find('num').text(0);
            cards.find('.additional').remove();
            var list_items = cards.find('li');
            for (var key in data.additional) {
                cards.find('ul').append("<li class='clearfix additional' data-item='"+key+"'><p>"+data.additional[key].title+"</p><aside><p><num>0</num><span>-</span><span>+</span></p></aside></li>");
            }
            for (var i = 0; i < data.details.length; i++) {
                cards.find('[data-item="'+data.details[i].card_id+'"]').find('num').text(data.details[i].quantity);
            }
        },

    }

});

var Orders = new RouteOrders();