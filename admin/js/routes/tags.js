var RouteTags = Backbone.View.extend({
    
	tpl: 'tags.tpl',
    tag_in_process: null,

    init: function() {
    	this.render();
    },

    render: function(data) {
    	var self = this;
    	$.Mustache.load(router.tpl_path + '/' + this.tpl, function() {
            $(router.main).mustache('tags', {}, {method: 'html'});
            self.refresh_list();
            self.secondary_init();
        });
    },

    refresh_list: function() {
    	var self = this;
    	this.fetch(function(data) {
    		self.render_list(data);
    	});
    },

    render_list: function(data) {
    	var self = this;
    	$.Mustache.load(router.tpl_path + '/' + this.tpl, function() {
            $("#tags").find(".content").mustache('tags_list', {data: data.data}, {method: 'html'});
        });
    },

    secondary_init: function() {
        var self = this;

        $("#add_tag").click(function() {
            self.tag_in_process = 'new';
            self.render_tag_editor({new: true});
        });

        $("#tags").on("click", ".tags_list li span", function() {
            self.tag_in_process = $(this).closest("li").data("id");
            self.fetch_tag(self.tag_in_process, function(data) {
                console.log(data);
                self.render_tag_editor(data.data);
            });
        });

        $("#tags").on("click", "ul.related li span", function() {
            $(this).parent().remove();
        });

        $("#tags").on("click", ".upload", function() {
            $(this).parent().find("input[type='file']").click();
        });

        $("#tags").on("change", ".load_image", function() {
            var figure = $(this).closest("div").find("figure");
            $(this).closest("form").ajaxSubmit({
                success: function(data) {
                    data = JSON.parse(data).data;
                    figure.css("background-image", "url("+data.file+")").data("image", data.file);
                    figure.find("span").remove();
                }
            });
        });

        $("#tags").on("click", "#discard_changes", function() {
            $("#tags").find(".tag_editor").remove();
        });

        $("#tags").on("click", "#save_tag", function() {
            var name = $(".tag_editor").find(".name");
            var image = $(".tag_editor").find(".image");
            var description = $(".tag_editor").find(".description");
            if (Global.check_inputs([name, description])) {
                console.log(self.tag_in_process, name.val(), image.data("image"), description.val(), self.collect_related());
                Global.ajax({
                    mod: "tags.save",
                    data: {
                        id: self.tag_in_process,
                        name: name.val(),
                        image: image.data("image"),
                        description: description.val(),
                        related: self.collect_related()
                    }
                }, function(data) {
                    if(data == null) return;
                    alert("saved");
                    Global.log(self.tag_in_process === "new" ? ("Created tag called " + name.val()) : ("Updated tag #" + self.tag_in_process));
                });
            }
        });

        self.init_tag_drag();
    },

    init_tag_drag: function() {
        var self = this;

        var drag_active = false;
        var hover_target = false;
        var rel_tag, id, name, allow_click, copy, c_w, c_h, drop_area = {};

        $("#tags").on("mousedown", ".tags_list li span", function() {
            clearTimeout(allow_click);
            rel_tag = $(this);
            id = rel_tag.parent().data("id");
            name = rel_tag.text();
            allow_click = setTimeout(function() {
                if (!$(".tag_editor").length)
                    return;
                if (id === self.tag_in_process) {
                    $("#tag_copy").remove();
                    return;
                }
                drag_active = true;
                create_copy();
                $(".tag_editor").find(".related").addClass("waiting");
            }, 300);
        });

        $(document).on("mouseup", function() {
            if ($(".tag_editor").length) {
                clearTimeout(allow_click);
                if (drag_active) {
                    drag_active = false;
                    destroy_copy();
                }
                if (hover_target) {
                    hover_target = false;
                    append_tag();
                }
            }
        });

        $(document).on("mousemove", function(e) {
            if (drag_active) {
                var left = e.pageX - c_w;
                var top = e.pageY - c_h;
                $("#tag_copy").css({
                    'left': left + 'px',
                    'top': top + 'px',
                    'display': 'block'
                });
                if (left >= drop_area.left && left <= drop_area.right && top >= drop_area.top && top <= drop_area.bottom) {
                    hover_target = true;
                    $(".tag_editor").find(".related").addClass("accept");
                } else {
                    hover_target = false;
                    $(".tag_editor").find(".related").removeClass("accept");
                }
            }
        });

        function create_copy() {
            locate_area();
            copy = $("<li data-id='"+id+"' id='tag_copy' style='display: none;'>"+name+"<span class='entypo-cancel hover'></span></li>");
            copy.appendTo($("body"));
            c_w = $("#tag_copy").outerWidth() / 2;
            c_h = $("#tag_copy").outerHeight() / 2;
        }

        function locate_area() {
            var area = $(".tag_editor").find(".related");
            drop_area.left = area.offset().left - 50;
            drop_area.top = area.offset().top - 50;
            drop_area.right = area.offset().left + area.outerWidth() + 10;
            drop_area.bottom = area.offset().top + area.outerHeight() + 10;
        }

        function append_tag() {
            var check = $(".related").find("li[data-id='"+id+"']")
            if (check.length) {
                highlight_present(check);
                return;
            }
            copy = copy.clone();
            copy.removeAttr("id").removeAttr("style");
            $(".tag_editor").find(".related").append(copy);
        }

        function highlight_present(item) {
            item.addClass("notice_me");
            setTimeout(function() {
                item.removeClass("notice_me");
                setTimeout(function() {
                    item.addClass("notice_me");
                    setTimeout(function() {
                        item.removeClass("notice_me");
                    }, 400);
                }, 300);
            }, 400);
        }   

        function destroy_copy() {
            $(".tag_editor").find(".related").removeClass("waiting");
            $("#tag_copy").remove();
        }
    },

    collect_related: function() {
        var ul = $(".tag_editor").find(".related");
        var li = ul.find("li");
        var len = li.length;
        var to_return = "";
        for (var i = 0; i < len; i++) {
            if (i !== 0)
                to_return += ",";
            to_return += li.eq(i).data("id");
        }
        console.log(to_return);
        return to_return;
    },

    fetch: function(callback) {
    	Global.ajax({
            mod: "tags.fetch_list"
        }, function(data) {
            callback(data);
        });
    },

    fetch_tag: function(id, callback) {
        Global.ajax({
            mod: "tags.fetch_tag",
            data: {
                id: id
            }
        }, function(data) {
            callback(data);
        });
    },

    render_tag_editor: function(data) {
        $(".tag_editor").remove();
        $.Mustache.load(router.tpl_path + '/' + this.tpl, function() {
            $("#tags").find(".content").mustache('tag_editor', {data: data}, {method: 'append'});

        });
    }
});

var Tags = new RouteTags();