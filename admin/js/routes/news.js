var RouteNews = Backbone.View.extend({
    
	tpl: 'news.tpl',

    init: function() {
    	this.render();
    },

    render: function(data) {
    	var self = this;
    	$.Mustache.load(router.tpl_path + '/' + this.tpl, function() {
            $(router.main).mustache('news', {}, {method: 'html'});
            self.refresh_list();
            self.secondary_init();
        });
    },

    refresh_list: function() {
    	var self = this;
    	this.fetch(function(data) {
    		self.render_list(data);
    	});
    },

    render_list: function(data) {
    	var self = this;
    	$.Mustache.load(router.tpl_path + '/' + this.tpl, function() {
            $("#news").find(".list").mustache('news_list_item', {data: data.data}, {method: 'html'});
        });
    },

    secondary_init: function() {
    	this.init_search();
    	this.listen_select();
    	this.listen_actions();
    },

    listen_actions: function() {
    	
    },

    delete_post: function(id) {
        var self = this;
        if (confirm("Are you sure?")) {
            Global.ajax({
                mod: "posts.delete_post",
                data: {
                    id: id
                }
            }, function(data) {
                Global.log("Deleted post #"+id);
                router.navigate("posts", {trigger: true});
                self.refresh_list();
            });
        }
    },

    toggle_status: function(status, id, refresh) {
        var self = this;
        Global.ajax({
            mod: "posts.update_status",
            data: {
                id: id,
                status: status
            }
        }, function(data) {
            Global.log("Changed status of post #" + id + " to " + status);
            if (refresh)
                self.refresh_list();
        });
    },

    init_search: function() {
    	var search_el = $("#news_search");
    	search_el.on("focus", function() {
    		$(this).parent().addClass("focus");
    	});
    	search_el.on("blur", function() {
    		$(this).parent().removeClass("focus");
    	});
    },

    listen_select: function() {
    	$("#news").find(".list").on("ctd", function(e) {
    		console.log(e);
    	});
    },

    fetch: function(callback) {
    	Global.ajax({
            mod: "posts.get_posts"
        }, function(data) {
            callback(data);
        });
    },

    update_status: function(status, id) {
        var self = this;
        Global.ajax({
            mod: "posts.update_status",
            data: {
                status: status,
                id: id
            }
        }, function(data) {
            Global.log("Changed status of post #" + id + " to " + status);
        	self.refresh_list();
        });
    },

    Meta: {

    	id: null,
        data: null,

    	init: function(id) {
    		this.id = id;
    		this.render();
    	},

    	render: function() {
    		var self = this;
            this.fetch_data(this.id, function(data) {
                $.Mustache.load(router.tpl_path + '/' + News.tpl, function() {
                    $(router.main).mustache('news_meta', {data: data.data}, {method: 'html'});
                    self.render_rubric();
                    self.render_categories();
                    self.render_tags();
                    self.secondary_init();
                });
            });
    	},

        secondary_init: function() {

            var self = this;

            var meta_list = $(".meta_list");

            $("#save").click(function() {
                var date_add = meta_list.find(".date");
                var description = meta_list.find(".short");
                var lead = meta_list.find(".lead");
                var rubric = meta_list.find(".rubric");
                if (Global.check_inputs([date_add])) {
                    console.log(date_add.val(), rubric.attr("value"), description.val(), lead.val(), self.collect_custom(), self.collect_box($(".tags")), self.collect_box($(".categories")));
                    Global.ajax({
                        mod: "posts.update_meta",
                        data: {
                            id: self.id,
                            date_add: date_add.val(),
                            description: description.val(),
                            lead: lead.val(),
                            rubric: rubric.attr("value"),
                            custom_fields: self.collect_custom(),
                            tags: self.collect_box($(".tags")),
                            categories: self.collect_box($(".categories"))
                        }
                    }, function(data) {
                        if(data !== null) {
                            Global.log("Update meta of post #" + self.id);
                            alert("saved");
                        }
                    });
                }
            });

            $("#news_meta").on("ctd", ".rubric", function(e) {
                Rubrics.fetch_custom_fields(e.val, function(data) {
                    console.log(data);
                    self.render_custom(data.data);
                });
            });

            $("#toggle_status").click(function() {
                News.toggle_status($(this).data("status"), self.id, false);
                $(this).data("status", !parseInt($(this).data("status"))).text($(this).text().toLowerCase() === "show" ? "Hide" : "Show");
            }); 

            $("#news_meta").on("change", ".field_value", function() {
                var default_box = $(this).closest("div").find(".path_preview");
                $(this).closest("div").find("form").ajaxSubmit({
                    success: function(data) {
                        data = JSON.parse(data).data;
                        default_box.val(data.file);
                    }
                });
            });

            $("#news_meta").on("click", ".upload_default", function() {
                $(this).parent().find("input[type='file']").click();
            });

        },

        render_custom: function(data) {
            var self = this;
            console.log(data);
            if (!data.length) {
                $(".custom_box").empty();
            } else {
                $.Mustache.load(router.tpl_path + '/' + News.tpl, function() {
                    $("#news_meta").find(".custom_box").mustache('custom_fields', {data: data}, {method: 'html'});
                    self.replace_values(data);
                });
            }
        },

        replace_values: function() {
            var custom_values = this.data.custom_fields;
            var box = $(".custom_fields");
            for (var key in custom_values) {
                box.find("[name='"+key+"']").val(custom_values[key]);
            }
        },

        collect_custom: function() {
            var box = $(".custom_fields");
            var c = box.children();
            var len = c.length;
            var res = "";
            for (var i = 0; i < len; i++) {
                if (i > 0) {
                    res += "###";
                }
                res += c.eq(i).find("[name]").attr("name") + "##" + c.eq(i).find("[name]").val();
            }
            return res;
        },

        collect_box: function(box) {
            var list = "";
            var c = box.children();
            var len = c.length;
            for (var i = 0; i < len; i++) {
                if (i) {
                    list += ","
                }
                list += c.eq(i).data("id");
            }
            return list;
        },

        render_rubric: function() {
            Rubrics.fetch(function(data) {
                $.Mustache.load(router.tpl_path + '/' + News.tpl, function() {
                    $("#news_meta").find(".rubrics_box").mustache('rubrics', {data: data.data}, {method: 'html'});
                });
            });
        },

        render_categories: function() {
            var self = this;
            Categories.fetch(function(data) {
                $.Mustache.load(router.tpl_path + '/' + News.tpl, function() {
                    $("#news_meta").find(".category_box").mustache('category', {data: data.data}, {method: 'html'});
                    self.init_autocomplete($(".category_box").find("input"), $(".category_box").find(".categories"), $(".category_box").find(".tip"), data.data);
                    self.fill_box($(".category_box").find(".categories"), data.data, self.data.category);
                });
            });
        },

        render_tags: function() {
            var self = this;
            Tags.fetch(function(data) {
                $.Mustache.load(router.tpl_path + '/' + News.tpl, function() {
                    $("#news_meta").find(".tags_box").mustache('tags', {data: data.data}, {method: 'html'});
                    self.init_autocomplete($(".tags_box").find("input"), $(".tags_box").find(".tags"), $(".tags_box").find(".tip"), data.data);
                    self.fill_box($(".tags_box").find(".tags"), data.data, self.data.tags);
                });
            });
        },

        fill_box: function(box, data, values) {
            console.log(data);
            var len_1 = data.length;
            var len_2 = values.length;
            for (var i = 0; i < len_2; i++) {
                for (var k = 0; k < len_1; k++) {
                    if (values[i] === data[k].id) {
                        box.append('<span data-id='+data[k].id+'>'+data[k].name+'<a class="entypo-cancel" onclick="$(this).parent().remove();"></a></span>');
                        break
                    }
                }
            }
        },

        fetch_data: function(id, callback) {
            var self = this;
            Global.ajax({
                mod: "posts.get_meta",
                data: {
                    id: self.id
                }
            }, function(data) {
                self.data = data.data;
                callback(data);
            });
        },

        init_autocomplete: function(input, box, tip, data) {
            console.log(data);
            var val, len = data.length, match;
            data.sort(function(a, b) {
                return a.name.length - b.name.length;
            });
            input.on("keydown", function(e) {
                if (e.keyCode === 9) {
                    e.preventDefault();
                }
            });
            input.on("keyup", function(e) {
                val = $(this).val();
                if (e.keyCode === 13) {
                    find_specific();
                    if (match) {
                        if (!box.find("[data-id="+match.id+"]").length) {
                            box.append('<span data-id='+match.id+'>'+match.name+'<a class="entypo-cancel" onclick="$(this).parent().remove();"></a></span>');
                            input.val("");
                            tip.html("");
                            match = false;
                        } else {
                            highlight_present(box.find("[data-id="+match.id+"]"));
                        }
                    }
                } else if (e.keyCode === 9) {
                    input.val(match.name);
                    tip.html("");
                } else {
                    find_match();
                }
                console.log(val);
                console.log(e);
            });

            function find_specific() {
                for (var i = 0; i < len; i++) {
                    if (data[i].name === val) {
                        match = data[i];
                        break;
                    }
                }
            }

            function find_match() {
                match = false;
                for (var i = 0; i < len; i++) {
                    if (data[i].name.indexOf(val) >= 0) {
                        tip.html(data[i].name);
                        match = data[i];
                        break;
                    }
                }
                if (!match) {
                    tip.html("");
                }
            }

            function highlight_present(item) {
                item.addClass("notice_me");
                setTimeout(function() {
                    item.removeClass("notice_me");
                    setTimeout(function() {
                        item.addClass("notice_me");
                        setTimeout(function() {
                            item.removeClass("notice_me");
                        }, 400);
                    }, 300);
                }, 400);
            }

        },

    }
    
});

var News = new RouteNews();