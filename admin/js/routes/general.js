var RouteGeneral = Backbone.View.extend({
    
	tpl: 'general.tpl',

    init: function() {
        this.fetch();
    },

    fetch: function() {
        var self = this;
        Global.ajax({
            mod: 'general.get_vars'
        }, function(data) {
            console.log(data);
            self.render(data.data);
        });
    },

    render: function(data) {
        var self = this;
        $.Mustache.load(router.tpl_path + '/' + this.tpl, function() {
            $(router.main).mustache('general', {data: data}, {method: 'html'});
            self.bind();
            self.list_items();
        });
    },

    list_items: function() {
        var items;
        $('[data-items]').each(function() {
            items = $(this).data('items').split(';');
            for (var i = 0; i < items.length; i++) {
                $(this).append('<li>'+items[i]+'</li>');
            }
        });
    },

    invite: function() {
        var email = $('.invite_email');
        if (!Global.check_inputs([email])) return;
        Global.test_ajax({
            mod: 'general.invite',
            data: {
                email: email.val()
            }
        }, function(data) {
            alert('Приглашение отправлено');
            $('body').removeClass('ov');
        });
    },

    bind: function() {
        var assign, val, update_tm;
        $('[data-assign]').on('keyup', function() {
            clearTimeout(update_tm);
            assign = $(this).data('assign');
            val = $(this).text() || $(this).val();
            update_tm = setTimeout(function() {
                Global.ajax({
                    mod: 'general.update_val',
                    data: {
                        assign: assign,
                        val: val
                    }
                }, function(data) {
                    console.log(data);
                });
            }, 1000);
        });

        var notification_email = $('.notification_email');
        var add_email = $('.add_email');
        var ul = $('section.five').find('ul');

        add_email.click(function() {
            if (!Global.check_inputs([notification_email])) return;
            Global.ajax({
                mod: 'general.add_notification_email',
                data: {
                    notification_email: notification_email.val()
                }
            }, function(data) {
                ul.append('<li>'+notification_email.val()+'</li>');
            });
        });
    }

});

var General = new RouteGeneral();