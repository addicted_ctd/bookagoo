var RouteEcommerce = Backbone.View.extend({
    
	tpl: 'ecommerce.tpl',

    Catalog: {
        init: function() {
            this.render();
        },

        render: function(data) {
            var self = this;
            $.Mustache.load(router.tpl_path + '/' + Ecommerce.tpl, function() {
                $(router.main).mustache('ecommerce', {}, {method: 'html'});
                self.refresh_list();
                self.secondary_init();
            });
        },

        refresh_list: function() {
            var self = this;
            this.fetch(function(data) {
                self.render_list(data);
            });
        },

        render_list: function(data) {
            var self = this;
            $.Mustache.load(router.tpl_path + '/' + Ecommerce.tpl, function() {
                $("#list").mustache('catalog_list', {data: data}, {method: 'html'});
            });
        },

        fetch: function(callback) {
            Global.ajax({
                mod: "ecommerce.fetch_list"
            }, function(data) {
                callback(data.data);
            });
        },

        secondary_init: function() {

            var self = this;

            $("#list").on("click", "#add_item", function() {
                var parent = $(this).closest("article");
                var article = parent.find(".article"),
                    image = parent.find(".image"),
                    name = parent.find(".name"),
                    in_stock = parent.find(".in_stock"),
                    price = parent.find(".price"),
                    quantity = parent.find(".quantity");
                if (Global.check_inputs([article, name])) {
                    console.log(article.val(), image.data("image"), name.val(), in_stock.attr("value"), price.val(), quantity.val());
                    Global.ajax({
                        mod: "ecommerce.add_item",
                        data: {
                            article: article.val(),
                            image: image.data("image"),
                            name: name.val(),
                            in_stock: in_stock.attr("value"),
                            price: price.val(),
                            quantity: quantity.val()
                        }
                    }, function(data) {
                        self.refresh_list();
                    });
                }
            });

            $("#list").on("change", ".load_image", function() {
                console.log("image preloaded");
                var figure = $(this).closest("article").find("figure");
                $(this).closest("form").ajaxSubmit({
                    success: function(data) {
                        data = JSON.parse(data).data;
                        figure.css("background-image", "url(uploads/"+data.file+")").data("image", data.file);
                        figure.find("span").remove();
                        if (typeof figure.closest("article").data("id") !== "undefined") {
                            self.inline_update("image", data.file, figure.closest("article").data("id"));
                        }
                    }
                });
            });

            $("#list").on("click", ".upload", function() {
                $(this).parent().find("input[type='file']").click();
            });

            var old_val, new_val;

            $("#list").on("focus", "[data-update]", function() {
                console.log("focus");
                old_val = $(this).val();
            });

            $("#list").on("blur", "[data-update]", function() {
                new_val = $(this).val();
                if (new_val !== old_val) {
                    self.inline_update($(this).data("update"), new_val, $(this).closest("[data-id]").data("id"));
                }
            });

            $("#list").on("ctd", "custom_select", function(e) {
                if (!$(this).hasClass("block_update"))
                    self.inline_update("in_stock", e.val, $(this).closest("[data-id]").data("id"));
            });

        },

        inline_update: function(to_update, new_val, id) {
            Global.ajax({
                mod: "ecommerce.quick_update_item",
                data: {
                    to_update: to_update,
                    new_val: new_val,
                    id: id
                }
            }, function(data) {
                $("[data-id="+id+"]").addClass("updated");
                setTimeout(function() {
                    $("[data-id="+id+"]").removeClass("updated");
                }, 1500);
            });
        },

        fetch_product: function(id, callback) {
            Global.ajax({
                mod: "ecommerce.get_item",
                data: {
                    id: id
                }
            }, function(data) {
                console.log(data);
                callback(data);
            });
        }
    },

    Editor: {

        id: null,
        data: {},

        init: function(id) {
            console.log(id);
            this.id = id;
            this.render();
        },

        render: function(data) {
            var self = this;
            console.log(this.id);
            Ecommerce.Catalog.fetch_product(this.id, function(data) {
                self.data = data.data;
                $.Mustache.load(router.tpl_path + '/' + Ecommerce.tpl, function() {
                    $(router.main).mustache('catalog_editor', {data: data.data}, {method: 'html'});
                    self.secondary_init();
                });
            });
        },

        replace_values: function() {
            var custom_values = this.data.custom_fields;
            var box = $(".custom_fields");
            for (var key in custom_values) {
                box.find("[name='"+key+"']").val(custom_values[key]);
            }
        },

        collect_custom: function() {
            var box = $(".custom_fields");
            var c = box.children();
            var len = c.length;
            var res = "";
            for (var i = 0; i < len; i++) {
                if (i > 0) {
                    res += "###";
                }
                res += c.eq(i).find("[name]").attr("name") + "##" + c.eq(i).find("[name]").val();
            }
            return res;
        },

        secondary_init: function() {
            this.render_categories();
            this.render_tags();
            this.render_custom_fields();
            var self = this;

            $("#catalog_editor").on("change", ".load_image", function() {
                console.log("image preloaded");
                var figure = $(this).closest("div").find("figure");
                $(this).closest("form").ajaxSubmit({
                    success: function(data) {
                        data = JSON.parse(data).data;
                        figure.css("background-image", "url(uploads/"+data.file+")").data("image", data.file);
                        figure.find("span").remove();
                    }
                });
            });

            $("#catalog_editor").on("click", ".upload", function() {
                $(this).parent().find("input[type='file']").click();
            });

            $("#catalog_editor").on("click", "#add_custom_field", function() {
                var li = $(this).closest("aside");
                var name_input = li.find(".name");
                var title_input = li.find(".title");
                var default_input = li.find(".default");
                var type = li.find(".type");
                if (Global.check_inputs([name_input, title_input])) {
                    Global.ajax({
                        mod: "ecommerce.save_custom_field",
                        data: {
                            name: name_input.val(),
                            title: title_input.val(),
                            default_val: default_input.val(),
                            type: type.attr("value")
                        }
                    }, function(data) {
                        self.render_custom_fields();
                    });
                }
                console.log("hello");
            });

            $("#catalog_editor").on("change", ".default_file", function() {
                var default_box = $(this).closest("aside").find(".default");
                $(this).closest("aside").find("form").ajaxSubmit({
                    success: function(data) {
                        data = JSON.parse(data).data;
                        default_box.val(data.file);
                    }
                });
            });

            $("#catalog_editor").on("change", ".field_value", function() {
                var default_box = $(this).closest("div").find(".path_preview");
                $(this).closest("div").find("form").ajaxSubmit({
                    success: function(data) {
                        data = JSON.parse(data).data;
                        default_box.val(data.file);
                    }
                });
            });

            $("#catalog_editor").on("click", ".upload_default", function() {
                $(this).parent().find("input[type='file']").click();
            });

            $("#catalog_editor").on("ctd", ".add_custom_wrapper .type", function(e) {
                if (e.val === 3) {
                    $(".upload_default").fadeIn();
                } else {
                    $(".upload_default").hide();
                }
            });
        },

        render_custom_fields: function() {
            var self = this;
            this.fetch_custom_fields(this.id, function(data) {
                $.Mustache.load(router.tpl_path + '/' + Ecommerce.tpl, function() {
                    $(".custom_box").mustache('custom_fields', {data: data.data}, {method: 'html'});
                    self.replace_values();
                });
            });
        },

        fetch_custom_fields: function(id, callback) {
            Global.ajax({
                mod: "ecommerce.fetch_custom_fields",
                data: {
                    id: id
                }
            }, function(data) {
                callback(data);
            });
        },

        render_categories: function() {
            var self = this;
            Categories.fetch(function(data) {
                $.Mustache.load(router.tpl_path + '/' + News.tpl, function() {
                    $(".category_box").mustache('category', {data: data.data}, {method: 'html'});
                    News.Meta.init_autocomplete($(".category_box").find("input"), $(".category_box").find(".categories"), $(".category_box").find(".tip"), data.data);
                    News.Meta.fill_box($(".category_box").find(".categories"), data.data, self.data.category);
                });
            });
        },

        render_tags: function() {
            var self = this;
            Tags.fetch(function(data) {
                $.Mustache.load(router.tpl_path + '/' + News.tpl, function() {
                    $(".tags_box").mustache('tags', {data: data.data}, {method: 'html'});
                    News.Meta.init_autocomplete($(".tags_box").find("input"), $(".tags_box").find(".tags"), $(".tags_box").find(".tip"), data.data);
                    News.Meta.fill_box($(".tags_box").find(".tags"), data.data, self.data.tags);
                });
            });
        },

        save: function() {
            var self = this;
            var box = $("#catalog_editor");
            var name = box.find(".name"),
                article = box.find(".article"),
                image = box.find(".image"),
                in_stock = box.find(".in_stock"),
                price = box.find(".price"),
                quantity = box.find(".quantity"),
                tags = News.Meta.collect_box($(".tags")),
                date_add = box.find(".date"),
                description = box.find(".short"),
                categories = News.Meta.collect_box($(".categories")),
                custom_fields = self.collect_custom();
            console.log(custom_fields);
            if (Global.check_inputs([name, article])) {
                Global.ajax({
                    mod: "ecommerce.update",
                    data: {
                        id: self.id,
                        name: name.val(),
                        article: article.val(),
                        image: image.data("image"),
                        in_stock: in_stock.attr("value"),
                        price: price.val(),
                        quantity: quantity.val(),
                        tags: tags,
                        date_add: date_add.val(),
                        description: description.val(),
                        categories: categories,
                        custom_fields: custom_fields
                    }
                }, function(data) {
                    if (data.data) {
                        alert("saved");
                    }
                });
            }
        }

    }

});

var Ecommerce = new RouteEcommerce();