var RouteRubrics = Backbone.View.extend({
    
	tpl: 'rubrics.tpl',
    id_in_process: null,

    init: function() {
    	this.render();
    },

    editor_init: function(id) {
        console.log(id);
        this.id_in_process = id;
        this.render_editor();
    },

    render: function(data) {
    	var self = this;
    	$.Mustache.load(router.tpl_path + '/' + this.tpl, function() {
            $(router.main).mustache('rubrics', {}, {method: 'html'});
            self.refresh_list();
            self.secondary_init();
        });
    },

    refresh_list: function() {
    	var self = this;
    	this.fetch(function(data) {
    		self.render_list(data);
    	}, true);
    },

    render_list: function(data) {
    	var self = this;
    	$.Mustache.load(router.tpl_path + '/' + this.tpl, function() {
            $("#rubrics").find(".content").mustache('rubrics_list', {data: data.data}, {method: 'html'});
        });
    },

    secondary_init: function() {
        var self = this;

    },

    fetch: function(callback, more_info) {
    	Global.ajax({
            mod: "rubrics.fetch_list",
            data: {
                more_info: typeof more_info === "undefined" ? false : more_info
            }
        }, function(data) {
            callback(data);
        });
    },

    fetch_rubric: function(id, callback) {
        Global.ajax({
            mod: "rubrics.fetch",
            data: {
                id: id
            }
        }, function(data) {
            callback(data);
        });
    },

    render_editor: function() {
        var self = this;
        this.fetch_rubric(self.id_in_process, function(data) {
            $.Mustache.load(router.tpl_path + '/' + self.tpl, function() {
                $(router.main).mustache('rubrics_editor', {data: data.data}, {method: 'html'});
                self.render_custom_fields();
                self.editor_secondary_init();
            });
        });
    },

    fetch_custom_fields: function(id, callback) {
        Global.ajax({
            mod: "rubrics.fetch_custom_fields",
            data: {
                id: id
            }
        }, function(data) {
            callback(data);
        });
    },

    render_custom_fields: function() {
        var self = this;
        this.fetch_custom_fields(this.id_in_process, function(data) {
            $.Mustache.load(router.tpl_path + '/' + self.tpl, function() {
                $(".custom_fields").find("ul").mustache('custom_fields', {data: data.data}, {method: 'html'});
                self.bind_fields();
            });
        })
    },

    bind_fields: function() {
        var self = this;

        $("#add_custom_field").click(function() {
            var li = $(this).closest("aside");
            var name_input = li.find(".name");
            var title_input = li.find(".title");
            var default_input = li.find(".default");
            var type = li.find(".type");
            if (Global.check_inputs([name_input, title_input])) {
                console.log(name_input.val(), title_input.val(), default_input.val(), self.id_in_process);
                Global.ajax({
                    mod: "rubrics.save_custom_field",
                    data: {
                        rubric_id: self.id_in_process,
                        id: 'new',
                        name: name_input.val(),
                        title: title_input.val(),
                        default_val: default_input.val(),
                        type: type.attr("value")
                    }
                }, function(data) {
                    Global.log("Created custom field "+name_input.val()+" in rubric #" + self.id_in_process);
                    self.render_custom_fields();
                });
            }
            console.log("hello");
        });
    },

    loop_save_fields: function() {

        var self = this;

        var ul = $(".custom_fields");
        var li = ul.find("aside[data-id]");
        var len = li.length;
        var name_input, title_input, default_input;
        for (var i = 0; i < len; i++) {
            name_input = li.eq(i).find(".name");
            title_input = li.eq(i).find(".title");
            default_input = li.eq(i).find(".default");
            type = li.eq(i).find(".type");
            if (Global.check_inputs([name_input, title_input])) {
                console.log(name_input.val(), title_input.val(), default_input.val(), self.id_in_process);
                Global.ajax({
                    mod: "rubrics.save_custom_field",
                    data: {
                        rubric_id: self.id_in_process,
                        id: li.eq(i).data("id"),
                        name: name_input.val(),
                        title: title_input.val(),
                        default_val: default_input.val(),
                        type: type.attr("value")
                    }
                }, function(data) {
                    Global.log("Saved custom field "+name_input.val()+" in rubric #" + self.id_in_process);
                    self.render_custom_fields();
                });
            }
        }
    },

    remove_custom_field: function(id, name) {
        var self = this;
        Global.ajax({
            mod: "rubrics.remove_custom_field",
            data: {
                id: id
            }
        }, function(data) {
            Global.log("Removed custom field "+name+" in rubric #" + self.id_in_process);
            self.render_custom_fields();
        });
    },

    editor_secondary_init: function() {
        var self = this;

        $("#save").click(function() {
            var name = $(".rubric_editor").find(".info").find(".name");
            var image = $(".rubric_editor").find(".info").find(".image");
            console.log(image);
            var description = $(".rubric_editor").find(".info").find(".description");
            if (Global.check_inputs([name, description])) {
                console.log(self.id_in_process, name.val(), image.data("image"), description.val());
                Global.ajax({
                    mod: "rubrics.save",
                    data: {
                        id: self.id_in_process,
                        name: name.val(),
                        image: image.data("image"),
                        description: description.val(),
                    }
                }, function(data) {
                    if(data == null) return;
                    self.loop_save_fields();
                    if (self.id_in_process === "new") {
                        Global.log("Created rubric "+name.val()+"");
                    } else {
                        Global.log("Updated info of rubric #" + self.id_in_process);
                    }
                    alert("saved");
                    router.navigate('rubrics/editor/' + data.data, {trigger: true});
                });
            }
        });

        $("#delete").click(function() {
            if (confirm("R U sure?")) {
                Global.ajax({
                    mod: "rubrics.remove_rubric",
                    data: {
                        id: self.id_in_process
                    }
                }, function(data) {
                    if(data == null) return;
                    Global.log("Deleted rubric #" + self.id_in_process);
                    router.navigate('rubrics', {trigger: true});
                });
            }
        });

        $("#rubrics").on("click", ".upload", function() {
            $(this).parent().find("input[type='file']").click();
        });

        $("#rubrics").on("change", ".load_image", function() {
            var figure = $(this).closest("div").find("figure");
            $(this).closest("form").ajaxSubmit({
                success: function(data) {
                    data = JSON.parse(data).data;
                    figure.css("background-image", "url("+data.file+")").data("image", data.file);
                    figure.find("span").remove();
                }
            });
        });

        $("#rubrics").on("change", ".default_file", function() {
            var default_box = $(this).closest("aside").find(".default");
            $(this).closest("aside").find("form").ajaxSubmit({
                success: function(data) {
                    data = JSON.parse(data).data;
                    default_box.val(data.file);
                }
            });
        });

        $("#rubrics").on("change", ".default_file", function() {
            var default_box = $(this).closest("aside").find(".default");
            $(this).closest("aside").find("form").ajaxSubmit({
                success: function(data) {
                    data = JSON.parse(data).data;
                    default_box.val(data.file);
                }
            });
        });

        $("#rubrics").on("click", ".upload_default", function() {
            $(this).parent().find("input[type='file']").click();
        });

        $("#rubrics").on("ctd", ".type", function(e) {
            if (e.val === 3) {
                $(this).closest("aside").find(".upload_default").fadeIn();
            } else {
                $(this).closest("aside").find(".upload_default").hide();
            }
        });


    }

});

var Rubrics = new RouteRubrics();