var RouteCoupons = Backbone.View.extend({
    
	tpl: 'coupons.tpl',
    page: 1,

    init: function() {
        this.render();
    },

    render: function(data) {
        var self = this;
        $.Mustache.load(router.tpl_path + '/' + this.tpl, function() {
            $(router.main).mustache('coupons', {}, {method: 'html'});
            self.refresh_list();
            self.bind();
            self.pagination();
        });
    },

    pagination: function() {

        var self = this;

        Global.ajax({
            mod: 'coupons.calculate_pages'
        }, function(data) {
            Global.pagination(data.data);
        });

        $(document).on('click', '#coupons .pagination p', function() {
            if (!$(this).hasClass('active')) {
                Global.to_page($(this).text());
                self.page = parseInt($(this).text());
                self.refresh_list();
            }
        });

        var current_page;
        var pagination = $('.pagination');

        pagination.find('a').click(function() {
            current_page = parseInt(pagination.find('.active').text());
            if (current_page !== parseInt(pagination.find('div').attr('total_pages'))) {
                current_page++;
                Global.to_page(current_page);
                self.page = current_page;
                self.refresh_list();
            }
        });
    },

    refresh_list: function() {
        var self = this;
        Global.ajax({
            mod: 'coupons.list_coupons',
            data: {
                page: self.page
            }
        }, function(data) {
            self.render_list(data.data);
        });
    },

    render_list: function(data) {
        $.Mustache.load(router.tpl_path + '/' + this.tpl, function() {
            $('#coupons').find('.scroll_content').mustache('coupons_list', {data: data}, {method: 'html'});
        });
    },

    bind: function() {

        var self = this;

        $('.back').find('a').click(function() {
            $('body').removeClass('ov');
        });

        $('#overlay').find('.popup').click(function(e) {
            e.stopPropagation();
        });

        $('#overlay').click(function() {
            $('body').removeClass('ov');
        });

        $('header').find('button').click(function() {
            $('body').addClass('ov');
        });

        $('.expire_date').mask('99.99.9999');

        var ov_coupon_type = $('#overlay').find('[data-type]');
        ov_coupon_type.click(function() {
            ov_coupon_type.removeClass('selected');
            $(this).addClass('selected');
        });

        var discount_amount = $('.discount_amount');
        var quantity = $('.coupon_quantity');

        $("#overlay").find('.create_coupons').click(function() {
            if (!Global.check_inputs([discount_amount, quantity])) return;
            Global.test_ajax({
                mod: 'coupons.create_coupons',
                data: {
                    quantity: quantity.val(),
                    discount: discount_amount.val(),
                    type: ov_coupon_type.filter('.selected').data('type'),
                    endless: $('[name="endless"]:checked').val(),
                    expire_date: $('.expire_date').val()
                }
            }, function(data) {
                alert('Готово');
                $('body').removeClass('ov');
                self.refresh_list();
            });
        });
    }

});

var Coupons = new RouteCoupons();