var RouteCategories = Backbone.View.extend({
    
	tpl: 'categories.tpl',
    cat_in_process: null,
    list: {},

    init: function() {
    	this.render();
    },

    render: function(data) {
    	var self = this;
    	$.Mustache.load(router.tpl_path + '/' + this.tpl, function() {
            $(router.main).mustache('categories', {}, {method: 'html'});
            self.refresh_list();
            self.secondary_init();
        });
    },

    refresh_list: function() {
    	var self = this;
    	this.fetch(function(data) {
    		self.render_list(data);
    	});
    },

    render_list: function(data) {
    	var self = this;
    	$(".list").html(self.build_list(data));
    },

    build_list: function(data) {
        console.log(data);
        var self = this;
        data = data.data;
        var list = $("<ul></ul>");
        self.list = {0: []}, l = data.length;
        for (var i = 0; i < l; i++) {
            if (!self.list.hasOwnProperty(data[i].id))
                self.list[data[i].id] = [];
            if (!self.list.hasOwnProperty(data[i].parent)) 
                self.list[data[i].parent] = [];
            self.list[data[i].parent].push({
                id: data[i].id,
                name: data[i].name
            });
        }

        function recursive_build(id, parent_node) {
            var loop_node;
            for (var i = 0; i < self.list[id].length; i++) {
                if (self.list[self.list[id][i].id].length > 0) {
                    loop_node = $("<li data-id="+self.list[id][i].id+"><span>"+self.list[id][i].name+"</span><ul></ul></li>");
                    recursive_build(self.list[id][i].id, loop_node.find("ul"));
                } else {
                    loop_node = $("<li data-id="+self.list[id][i].id+"><span>"+self.list[id][i].name+"</span></li>");
                }
                parent_node.append(loop_node);
            }
            console.log(list.html());
        }

        recursive_build(0, list);
        console.log(self.list);
        return list;
    },

    secondary_init: function() {
        var self = this;
    	$("#categories").find("#add_category").click(function() {
            self.cat_in_process = "new";
            Global.ajax({
                mod: "categories.fetch_cat_info",
                data: {
                    id: self.cat_in_process
                }
            }, function(data) {
                $.Mustache.load(router.tpl_path + '/' + self.tpl, function() {
                    $("#categories").find(".details").mustache('category_add', {data: data.data}, {method: 'html'});
                });
            });
        });

        $("#categories").on("click", ".list li span", function() {
            self.cat_in_process = $(this).parent().data("id");
            Global.ajax({
                mod: "categories.fetch_cat_info",
                data: {
                    id: self.cat_in_process
                }
            }, function(data) {
                $.Mustache.load(router.tpl_path + '/' + self.tpl, function() {
                    $("#categories").find(".details").mustache('category_editor', {data: data.data}, {method: 'html'});
                    self.prevent_looping(data.data);
                });
            });
        });

        $("#categories").on("click", ".upload", function() {
            $(this).parent().find("input[type='file']").click();
        });

        $("#categories").on("change", ".load_image", function() {
            console.log("image preloaded");
            var figure = $(this).closest("div").find("figure");
            $(this).closest("form").ajaxSubmit({
                success: function(data) {
                    data = JSON.parse(data).data;
                    figure.css("background-image", "url("+data.file+")").data("image", data.file);
                    figure.find("span").remove();
                }
            });
        });

        $("#categories").on("click", "#discard_changes", function() {
            $("#categories").find(".details").empty();
        });

        $("#categories").on("click", "#save_category", function() {
            var name = $(".details").find(".name");
            var image = $(".details").find(".image");
            var description = $(".details").find(".description");
            var parent = $(".details").find(".parent");
            if (Global.check_inputs([name, description])) {
                console.log(self.cat_in_process, name.val(), image.data("image"), description.val(), parent.attr("value"));
                Global.ajax({
                    mod: "categories.save",
                    data: {
                        id: self.cat_in_process,
                        name: name.val(),
                        image: image.data("image"),
                        description: description.val(),
                        parent: parent.attr("value")
                    }
                }, function(data) {
                    if (data === null) return;
                    if (self.cat_in_process === "new") {
                        Global.log("Created category " + name.val());
                    } else {
                        Global.log("Updated category #" + self.cat_in_process);
                    }
                    alert("saved");
                    self.refresh_list();
                });
            }
        });
    },

    prevent_looping: function(data) {
        var self = this;
        var the_item = $(".list").find("li[data-id='"+self.cat_in_process+"']");
        var inner = the_item.find("li");
        var select = $(".details").find("custom_select");
        select.find("li[value='"+self.cat_in_process+"'").remove();
        inner.each(function() {
            console.log($(this).data("id"));
            select.find("li[value='"+$(this).data("id")+"'").remove();
        });
        if (typeof data.info[0].parent !== "undefined" && data.info[0].parent !== "") {
            select.find("li[value='"+data.info[0].parent+"']").click();
            select.click();
        }
        console.log(the_item);
    },

    fetch: function(callback) {
    	Global.ajax({
            mod: "categories.fetch_list"
        }, function(data) {
            callback(data);
        });
    }    
});

var Categories = new RouteCategories();