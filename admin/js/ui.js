var UI = {

	elements: [
		"radio_button",
		"custom_select",
		"custom_select li"
	],

	init: function() {

		// -- radio button

		$(document).on('click', this.elements[0], function() {
			var target = $(this).attr("ui_target");
			if (typeof target !== "undefined") {
				target = $(target);
				var e = jQuery.Event("ctd");
				e.stack = typeof target.data("stack") === "undefined" ? 0 : target.data("stack");
				if ($(this).hasAttr('checked')) {
					$(this).removeAttr('checked');
					e.stack--;
				} else {
					$(this).attr('checked', true);
					e.stack++;
				}
				target.data("stack", e.stack).trigger(e);
			} else {
				$(this).hasAttr('checked') ? $(this).removeAttr('checked') : $(this).attr('checked', true);
			}
		});

		// -- select

		$(document).on('click', this.elements[1], function() {
			var h = 0;
			if (!$(this).hasAttr('ext_h')) {
				var c = $(this).children();
				var l = c.length;
				for (var i = 0; i < l; i++) {
					h += c.eq(i).outerHeight();
				}
				$(this).attr("ext_h", h);
				$(this).attr("init_h", $(this).outerHeight());
			} else {
				h = $(this).attr('ext_h');
			}
			$(this).height($(this).hasClass("opened") ? $(this).attr("init_h") : h).toggleClass("opened");
			console.log(h);
		});

		$(document).on('click', this.elements[2], function() {
			var parent = $(this).parent();
			if ($(this).index() > 0) {
				parent.find("li").removeAttr("selected");
				parent.attr("value", $(this).attr("value"));
				$(this).attr("selected", true).prependTo(parent);
				var e = jQuery.Event("ctd");
				e.val = $(this).attr("value");
				parent.trigger(e);
			}
			
		});

	},


}

$.fn.hasAttr = function(name) {  
   return this.attr(name) !== undefined && this.attr(name);
};

UI.init();