
var Editor = Backbone.View.extend({
    init: function(id) {
        var self = this;
        self.id = id;
        if (id !== "new") {
            $.get("dev/dev_"+id+".html", function(data) {
                data = {
                    'dev_file': data
                };
                self.render(id, data);
            });
        } else {
            self.render(id);
        }
        resize.load();
    },
    render: function(id, data) {
        var self = this;
        $.Mustache.load(router.tpl_path + '/editor.tpl?v=212121', function() {
            $(router.main).mustache('editor', {tpl: data}, {method: 'html'});
            self.box = $("#editor");
            grid.init();
            save.init(id);
            drag.init();
            photo.init();
            select.init();
            video.init();
            gallery.init();
            side_tt.init();
            date.init();
        });
    }
});
var editor = new Editor;


/**/