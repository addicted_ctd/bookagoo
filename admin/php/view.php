<?php
error_reporting(0);
ini_set('display_errors', '0');
define(TPL_ROOT, $_SERVER['DOCUMENT_ROOT'] . '/umtrava/static/');

class View {

	private static $file;
	private $data = array();
	private $ArrayValues = array();

	private function __construct() {
		register_shutdown_function(array(& $this, 'complete'));
	}

	public function complete() {
		$count = 0;
		while (list($key, $val) = @each($this->ArrayValues[$count])) {
			$$key = $val;
			$count++;
		}
		ob_start();
		include_once (TPL_ROOT . self::$file . '.php');
		return ob_get_clean();
	}

	public static function factory($file) {
		self::$file = $file;
		if (!file_exists(TPL_ROOT . $file . '.php'))
			die('Файл вида, под названием: <b><i>' . $file . '.php</i></b> не найден!');
		return new View();
	}

	public function bind($key, & $value) {
		$this->data[$key] = & $value;
		$this->ArrayComposer($this->data);
		unset($this->data);
		return $this;
	}

	private function ArrayComposer($data) {
		$this->ArrayValues[] = $data;
	}

	public function __toString() {
		return $this->complete();
	}

}

?>