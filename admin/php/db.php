<?php

	Class db {
		public function execute($query_string, $return_id = false) {
			global $settings;
			$con = self::get_connection($settings->db);
			mysqli_query($con, "SET NAMES utf8");
	        $res = mysqli_query($con, $query_string);
	        if ($return_id) {
	        	$id = mysqli_insert_id($con);
	        	mysqli_close($con);
	        	return $id;
	        } else {
	        	if (gettype($res) != "boolean") {
		        	$to_return = array();
			        while($row = mysqli_fetch_array($res)) {
			        	array_push($to_return, $row);
			        }
			        return $to_return;
		        } else {
		        	return $res;
		        }
	        }
		}

		private function get_connection($db_s) {
	        return mysqli_connect($db_s->host, $db_s->user, $db_s->password, $db_s->db_name);
	    }
	}

?>