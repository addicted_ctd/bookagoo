<?php

	Class Mod_coupons_ajax {

		static function create_coupons($data) {
			global $db;
			for ($i = 0; $i < $data->quantity; $i++) {
				$code = self::generate_code();
				$data->expire_date = explode('.', $data->expire_date);
				$data->expire_date = $data->expire_date[2] . '-' . $data->expire_date[1] . '-' . $data->expire_date[0];
				$db->execute('insert into coupons(type, code, discount, active, endless, expire_date) values('.$data->type.', "'.$code.'", "'.$data->discount.'", 1, '.$data->endless.', "'.$data->expire_date.'")');
			}
			return true;
		}

		static function generate_code() {
			global $db;
			do {
				$code = strtoupper(substr(md5(microtime()),rand(0, 26), 5));
				$exists = $db->execute('select code from coupons where code = ' . $code . ' limit 1');
			} while ($exists['code'] !== NULL);
			return $code;
		}

		static function list_coupons($data) {
			global $db;
			$page = $data->page - 1;
			$l = 10;
			$limit = 'limit ' . $l;
			$offset = 'offset ' . $page * $l;
			$res = $db->execute('select * from coupons order by id desc ' . $limit . ' ' . $offset);
			$ret = array();
			foreach ($res as $row) {
				array_push($ret,
					array(
						'id' => $row['id'],
						'discount' => $row['discount'],
						'code' => $row['code'],
						'apply_date' => $row['apply_date'] !== '0000-00-00 00:00:00' ? date('d.m.Y h:m', strtotime($row['apply_date'])) : '–',
						'active' => $row['active'] === "1" ? "Да" : "Нет",
						'format' => $row['type'] === "1" ? "руб" : "%",
						'expire_date' => date('d.m.Y', strtotime($row['expire_date'])),
						'endless' => $row['endless'] ? 'Да' : "Нет"
					)
				);
			}
			return $ret;
		}

		static function calculate_pages($data) {
			global $db;
			$res = $db->execute('select count(*) from coupons');
			$l = 10;
			$rows = $res[0][0];
			$pages = ceil($rows / $l);
			return $pages;
		}

	}

?>