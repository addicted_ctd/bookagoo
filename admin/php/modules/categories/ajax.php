<?php

	Class Mod_categories_ajax {

		static function fetch_list() {
			global $db;
			$cats = $db->execute("select * from categories");
			return $cats;
		}

		static function get_children($data) {
			global $db;
			$cats = $db->execute("select categories.id, categories.name, categories_meta.image from categories inner join categories_meta on categories_meta.id = categories.id where parent = " . $data->id);
			$stack = array();
			$i = 0;
			foreach ($cats as $cat) {
				array_push($stack, array(
					'id' => $cat['id'],
					'name' => $cat['name'],
					'image' => $cat['image'],
					'children' => array()
				));
				if (isset($data->inner) && $data->inner) {
					$stack[$i]['children'] = self::recursive_stack_children($cat['id'], $stack[$i]['children']);
				}
				$i++;
			}
			return $stack;
		}

		static function recursive_stack_children($id, $stack) {
			global $db;
			$cats = $db->execute("select categories.id, categories.name, categories_meta.image from categories inner join categories_meta on categories_meta.id = categories.id where parent = " . $id);
			foreach ($cats as $cat) {
				array_push($stack, array(
					'id' => $cat['id'],
					'name' => $cat['name'],
					'image' => $cat['image'],
				));
				$stack = self::recursive_stack_children($cat['id'], $stack);
			}
			return $stack;
		}

		static function fetch_cat_info($data) {
			global $db;
			$list = self::fetch_list();
			if ($data->id == 'new') {
				$info = array();
			} else {
				$info = $db->execute('select categories.id, categories.name, categories.parent, categories_meta.description, categories_meta.image from categories inner join categories_meta on categories_meta.id = categories.id where categories.id = ' . $data->id);
			}
			return array(
				'info' => $info,
				'list' => $list
			);
		}

		static function save($data) {
			global $db, $user;
			$user->allowed(3);
			$id = $data->id;
			if ($id == "new") {
				$q = "insert into categories(name, parent) values ('".$data->name."', '".$data->parent."')";
				$id = $db->execute($q, true);
				$q = "insert into categories_meta(id, image, description) values ('".$id."', '".$data->image."', '".$data->description."')";
			} else {
				$q = "update categories set parent = '".$data->parent."', name = '".$data->name."' where id = ".$data->id;
				$db->execute($q);
				$q = "update categories_meta set image = '".$data->image."', description = '".$data->description."' where id = ".$data->id;
			}
			$db->execute($q);
			return true;
		}

	}

?>