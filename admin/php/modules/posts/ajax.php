<?php

define("DEV", "dev_");
define("PROD", "production_");

	Class Mod_posts_ajax {

		static function get_posts($data) {
			global $db;
			$where = "";
			$offset = "";
			if (isset($data->rubric)) {
				$where == " where rubric = " . $data->rubric;
			}
			if (isset($data->category)) {
				$where == "" ? $where = " where category like '%".$data->category."%'" : $where .= " and category like '%".$data->category."%'";
			}
			if (isset($data->page)) {
				$offset = " " . $data->page * 10;
			}
	        $res = $db->execute("SELECT * FROM posts" . $where . $offset);
	        $to_return = array();
	        if (!$res) return $res;
	        foreach($res as $row) {
	            $row_data = array(
	                'thumb' => $row['thumb'] == 'none' ? NULL : $row['thumb'],
	                'id' => $row['id'],
	                'title' => $row['title'],
	                'visible' => $row['status'] == 0 ? NULL : true,
	                'date' => date("d.m.Y", strtotime($row['date'])),
	                'full_date' => self::full_date(strtotime($row['date'])),
	                'time' => self::get_time(strtotime($row['date']))
	            );
	            array_push($to_return, $row_data);
	        }
	        return $to_return;
	    }

	    static function full_date($date) {
	    	$months = array("Января", "Февраля", "Марта", "Апреля", "Мая", "Июня", "Июля", "Августа", "Сентября", "Октября", "Ноября", "Декабря");
	    	return date("d", $date) . "&nbsp" . $months[((int) date("m", $date)) - 1];
	    }

	    static function get_time($date) {
	    	return date("H:i", $date);
	    }

	    static function get_list($data) {
	    	global $db;
	        $res = $db->execute("select posts.id, posts.date, posts.title, posts.thumb, posts.rubric, posts_meta.description, posts_meta.lead from posts inner join posts_meta on posts.id = posts_meta.id where status != 0 and rubric = " . $data->rubric);
	        $to_return = array();
	        foreach($res as $row) {
	            $row_data = array(
	                'thumb' => $row['thumb'] == 'none' ? NULL : $row['thumb'],
	                'id' => $row['id'],
	                'title' => $row['title'],
	                'date' => date("d.m.Y", strtotime($row['date'])),
	                'rubric' => $row['rubric'],
	                'description' => $row['description'],
	                'lead' => $row['lead']
	            );
	            array_push($to_return, $row_data);
	        }
	        return $to_return;
	    }

	    static function get_post($data) {
	    	global $settings, $db;
	    	$folder = $_SERVER["DOCUMENT_ROOT"] . $settings->production_dir . '/';
	        $res = isset($data->slug) ? $db->execute("SELECT * FROM posts WHERE slug='" . $data->slug . "'") : $db->execute("SELECT * FROM posts WHERE id=" . $data->id);
	        foreach ($res as $row) {
	            $row_data = array(
	                'thumb' => $row['thumb'],
	                'id' => $row['id'],
	                'title' => $row['title'],
	                'status' => $row['status'],
	                'tpl' => file_get_contents($folder . PROD . $row['id'] . ".html"),
	                'meta' => self::get_seo((object) array("id" => $row['id']))
	            );
	        }
	        return $row_data;
	    }

	    static function update_meta($data) {
	    	global $db, $user;
	    	$user->allowed(3);
	    	$db->execute("update posts_meta set lead = '".$data->lead."', description = '".$data->description."', custom_fields = '".$data->custom_fields."' where id = " . $data->id);
	    	$db->execute("update posts set date = STR_TO_DATE('".$data->date_add."', '%d.%m.%Y'), rubric = '".$data->rubric."', tags = '".$data->tags."', category = '".$data->categories."' where id = " . $data->id);
	    	return $data->custom_fields;
	    }

	    static function get_meta($data) {
	    	global $db;
	    	$q = "select posts.id, posts.date, posts.title, posts.status, posts.category, posts.rubric, posts.tags, posts_meta.lead, posts_meta.description, posts_meta.custom_fields from posts inner join posts_meta on posts.id = posts_meta.id where posts.id = " . $data->id;
	    	$res = $db->execute($q);
	    	foreach ($res as $row) {
	    		$info = array(
	    			'id' => $row['id'],
	    			'date' => date("d.m.Y", strtotime($row['date'])),
	    			'title' => $row['title'],
	    			'visible' => $row['status'] == 0 ? NULL : true,
	    			'category' => $row['category'] != "" ? explode(",", $row['category']) : array(),
	    			'rubric' => $row['rubric'],
	    			'tags' => $row['tags'] != "" ? explode(",", $row['tags']) : array(),
	    			'lead' => $row['lead'],
	    			'description' => $row['description'],
	    			'custom_fields' => self::decode_custom_fields($row['custom_fields'])
	    		);
	    	}
	    	return $info;
	    }

	    static function get_seo($data) {
	    	global $db;
	    	$q = "select posts.id, posts.date, posts.title, posts.status, posts.category, posts.rubric, posts.tags, posts_meta.lead, posts_meta.description, posts_meta.custom_fields from posts inner join posts_meta on posts.id = posts_meta.id where posts.id = " . $data->id;
	    	$res = $db->execute($q);
	    	foreach ($res as $row) {
	    		$info = array(
	    			'title' => $row['title'],
	    			'category' => $row['category'] != "" ? explode(",", $row['category']) : array(),
	    			'rubric' => $row['rubric'],
	    			'tags' => self::get_tags($row['tags']),
	    			'lead' => $row['lead'],
	    			'description' => $row['description']
	    		);
	    	}
	    	return $info;
	    }

	    static function get_tags($id_str) {
	    	$ret = array();
	    	$seo_array = array();
	    	$seo_string = "";
	    	if ($id_str == "")
	    		return array(
		    		"data" => $ret,
		    		"seo" => $seo_string
		    	);
	    	global $db;
	    	$id_array = explode(",", $id_str);
	    	$q = "select tags.name, tags_meta.image, tags_meta.description from tags inner join tags_meta on tags.id = tags_meta.id where ";
	    	$len = sizeof($id_array);
	    	for ($i = 0; $i < $len; $i++) {
	    		if ($i != 0)
	    			$q .= " or ";
	    		$q .= "tags.id = " . $id_array[$i];
	    	}
	    	$res = $db->execute($q);
	    	foreach ($res as $row) {
	    		array_push($ret, array(
	    				"name" => $row['name'],
	    				"image" => $row['image'],
	    				"description" => $row['description']	
	    			)
	    		);
	    		array_push($seo_array, $row['name']);
	    	}
	    	$seo_string = implode(",", $seo_array);
	    	return array(
	    		"data" => $ret,
	    		"seo" => $seo_string
	    	);
	    }

	    static function decode_custom_fields($str) {
	    	$res = array();
	    	$field = explode("###", $str);
	    	for ($i = 0; $i < sizeof($field); $i++) {
	    		$values = explode("##", $field[$i]);
	    		if (sizeof($values) > 1) {
	    			$res[$values[0]] = $values[1];
	    		}
	    	}
	    	return $res;
	    }

	    static function save_dev($post_data) {
	    	global $settings, $db, $user, $util;
	    	$user->allowed(3);
	    	$dev_dir = $_SERVER["DOCUMENT_ROOT"] . $settings->dev_dir . '/';
        	$prod_dir = $_SERVER["DOCUMENT_ROOT"] . $settings->production_dir . '/';
	        $exist = $post_data->id != 'new';
	        $post_data->slug = $util->translit($post_data->title);
	        if (!$exist) {
	            $q = "INSERT INTO posts(title, thumb, status) VALUES('".$post_data->title."', '".$post_data->thumb."', '".$post_data->status."')";
	            $id = $db->execute($q, true);
	            $db->execute("INSERT INTO posts_meta(id, description, lead, custom_fields) VALUES('".$id."', '', '".$post_data->lead."', '')");
	            $name = $id . ".html";
	        } else {
	        	$db->execute("INSERT INTO posts_meta(id, description, lead, custom_fields) VALUES('".$post_data->id."', '', '".$post_data->lead."', '') on duplicate key update lead = '".$post_data->lead."'");
	            $q = "UPDATE posts SET title='".$post_data->title."', thumb='".$post_data->thumb."', status='".$post_data->status."', slug='".$post_data->slug."' WHERE id='".$post_data->id."'";
	            $db->execute($q);
	            $name = $post_data->id . ".html";
	        }
	        self::save_html($dev_dir, DEV . $name, $post_data->dev_tpl);
	        self::save_html($prod_dir, PROD . $name, $post_data->tpl);
	        return isset($id) ? $id : "updated";
	    }

	    private static function save_html($dir, $name, $html) {
	        file_put_contents($dir . $name, $html);
	    }

	    static function delete_post($data) {
	        global $db, $user;
	        $user->allowed(3);
	    	$db->execute("delete from posts where id = " . $data->id);
	    	$db->execute("delete from posts_meta where id = " . $data->id);
	    	return true;
	    }

	    static function update_status($data) {
	    	global $db, $user;
	    	$user->allowed(3);
	        return $db->execute("UPDATE posts SET status='".$data->status."' WHERE id='".$data->id."'");
	        return true;
	    }

	    static function get_fb() {    
	    	$profile_id = "377690729022083";
	    	//App Info, needed for Auth
	    	$app_id = "1685387005022680";
	    	$app_secret = "b79717eb897104ad8295d9785ffa93dd";
	    	//Retrieve auth token
	    	$authToken = self::fetchUrl("https://graph.facebook.com/oauth/access_token?grant_type=client_credentials&client_id={$app_id}&client_secret={$app_secret}");
	    	$json_object = self::fetchUrl("https://graph.facebook.com/{$profile_id}/feed?{$authToken}&limit=100");
	    	return json_decode($json_object);
		}

		static function fetchUrl($url){
	        $ch = curl_init();
	        curl_setopt($ch, CURLOPT_URL, $url);
	        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
	        $feedData = curl_exec($ch);
	        curl_close($ch); 
	        return $feedData;
	    }

	}

?>