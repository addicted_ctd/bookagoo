<?php
error_reporting(0);

	Class Mod_tags_ajax {

		static function fetch_list() {
			global $db;
			$res = $db->execute("select * from tags");
			return $res;
		}

		static function fetch_related($id) {
			global $db;
			$res = $db->execute("select related from tags_meta where id = " . $id);
			foreach($res as $row) {
				$id_array = explode(",", $row['related']);
			}
			$related = array();
			for ($i = 0; $i < sizeof($id_array); $i++) {
				if ($id_array[$i] != "") {
					$rel_tag = $db->execute("select name from tags where id = " . $id_array[$i]);
					foreach($rel_tag as $row) {
						$tag_info = array(
							'id' => $id_array[$i],
							'name' => $row['name']
						);
					}
					array_push($related, $tag_info);
				}
			}
			return $related;
		}

		static function fetch_tag($data) {
			global $db;
			$res = $db->execute("select tags.id, tags.name, tags_meta.description, tags_meta.image from tags inner join tags_meta on tags.id = tags_meta.id where tags.id = " . $data->id);
			foreach($res as $row) {
				$info = array(
					'id' => $row['id'],
					'name' => $row['name'],
					'description' => $row['description'],
					'image' => $row['image'],
					'related' => self::fetch_related($row['id'])
				);
	        }
			return $info;
		}

		static function save($data) {
			global $db, $user;
			$user->allowed(3);
			$id = $data->id;
			if ($id == "new") {
				$q = "insert into tags(name) values ('".$data->name."')";
				$id = $db->execute($q, true);
				$q = "insert into tags_meta(id, image, description, related) values ('".$id."', '".$data->image."', '".$data->description."', '".$data->related."')";
			} else {
				$q = "update tags set name = '".$data->name."' where id = ".$data->id;
				$db->execute($q);
				$q = "update tags_meta set image = '".$data->image."', description = '".$data->description."', related = '".$data->related."' where id = ".$data->id;
			}
			$db->execute($q);
			return true;
		}

	}

?>