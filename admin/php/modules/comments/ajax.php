<?php

	Class Mod_comments_ajax {

		static function add($data) {
			global $db;
			if (!strcmp("", $data->parent_id) || !isset($data->parent_id))
				return array(
						"error" => 0
					);
			if (!strcmp("", $data->content) || !isset($data->content))
				return array(
						"error" => 1
					);
			$data->thread = self::thread($data);
			$db->execute("insert into comments_data(thread, content) values(".$data->thread.", '".$data->content."')");
			return array(
					"ok" => true
				);
		}

		static function thread($data) {
			global $db;
			$thread = $db->execute("select thread from comments where parent_id = " . $data->parent_id);
			if (!$thread) {
				return $db->execute("insert into comments(parent_id) values(".$data->parent_id.")", true);
			}
			return $thread[0][0];
		}

		static function get_list() {
			global $db;
			$ret = array();
			$res = $db->execute("select * from comments");
			foreach ($res as $row) {
				array_push($ret,
					array(
						"parent_id" => $row['parent_id'],
						"thread" => self::get_thread($row['thread'])
					)
				);
			}
			return $ret;
		}

		static function render_list() {
			global $db, $ctd;
			$list = self::get_list();
			for ($i = 0; $i < sizeof($list); $i++) {
				$list[$i]['post'] = array(
					"content" => $ctd::exec("posts.get_post", (object) array("id" => $list[$i]['parent_id'])),
					"meta" => $ctd::exec("posts.get_meta", (object) array("id" => $list[$i]['parent_id']))
				);
			}
			return $list;
		}

		static function get_thread($thread) {
			global $db;
			$ret = array();
			$res = $db->execute("select * from comments_data where thread = " . $thread . " order by date desc");
			foreach ($res as $row) {
				array_push($ret, array(
						"id" => $row['id'],
						"date" => $row['date'],
						"content" => $row['content'],
						"thread" => $row['thread']
					)
				);
			}
			return $ret;
		}

	}

?>