<?php

define("API_KEY", "2C9D8Y_8FEdZBlv2M3Zf3A");
require('/home/quantum/webpoetry.org/bookagoo/docs/app/php/Mandrill.php');

	Class Mod_mailer_ajax {

		static function send($data) {
			if (!isset($data->global_merge_vars)) {
				$data->global_merge_vars = array();
			}
			try {
			    $mandrill = new Mandrill(API_KEY);
			    $template_name = $data->template_name;
			    $template_content = $data->template_content;
			    $message = array(
			        'to' => $data->to,
			        'headers' => array('Reply-To' => 'message.reply@example.com'),
			        'important' => false,
			        'track_opens' => null,
			        'track_clicks' => null,
			        'auto_text' => null,
			        'auto_html' => null,
			        'inline_css' => null,
			        'url_strip_qs' => null,
			        'preserve_recipients' => null,
			        'view_content_link' => null,
			        'tracking_domain' => null,
			        'signing_domain' => null,
			        'return_path_domain' => null,
			        'merge' => true,
			        'merge_language' => 'mailchimp',
			        'global_merge_vars' => $data->global_merge_vars,
			    );
			    $async = false;
			    $ip_pool = 'Main Pool';
			    $result = $mandrill->messages->sendTemplate($template_name, $template_content, $message, $async, $ip_pool);
			    return $result;
			} catch(Mandrill_Error $e) {
			    // Mandrill errors are thrown as exceptions
			    echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
			    // A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'
			    throw $e;
			}
		}

		static function order_0($order_id) {
			global $db;
			$order = $db->execute('select uid, total, box_quantity from site_orders where id = ' . $order_id);
			$order = $order[0];
			$order_details = $db->execute('select sum(quantity) from site_orders_details where order_id = ' . $order_id . ' and card_id != 0');
			$order_details = $order_details[0][0];
			$user = $db->execute('select email, f_name from site_users where id = ' . $order['uid']);
			$user = $user[0];
			$order_content = "Карточки - ".$order_details."шт.";
			if ($order['box_quantity'] > 0) {
				$order_content .= " + Коробка " . $order['box_quantity'] . "шт.";
			}
			$data = array();
			$data['template_name'] = 'order_0';
			$data['template_content'] = array(
			    array(
			        'name' => 'username',
			        'content' => $user['f_name']
			    ),
			    array(
			    	'name' => 'order_id',
			    	'content' => $order_id
			    ),
			    array(
			    	'name' => 'order_content',
			    	'content' => $order_content
			    ),
			    array(
			    	'name' => 'total_price',
			    	'content' => $order['total']
			    )
			);
			$data['to'] = array(
			    array(
			        'email' => $user['email'],
			        'name' => $user['f_name'],
			        'type' => 'to'
			    )
			);
			return self::send((object) $data);
		}

		static function order_1($order_id) {
			global $db;
			$order = $db->execute('select uid, total, box_quantity from site_orders where id = ' . $order_id);
			$order = $order[0];
			$order_details = $db->execute('select sum(quantity) from site_orders_details where order_id = ' . $order_id . ' and card_id != 0');
			$order_details = $order_details[0][0];
			$user = $db->execute('select email, f_name from site_users where id = ' . $order['uid']);
			$user = $user[0];
			$order_content = "Карточки - ".$order_details."шт.";
			if ($order['box_quantity'] > 0) {
				$order_content .= " + Коробка " . $order['box_quantity'] . "шт.";
			}
			$data = array();
			$data['template_name'] = 'order_1';
			$data['template_content'] = array(
			    array(
			        'name' => 'username',
			        'content' => $user['f_name']
			    ),
			    array(
			    	'name' => 'order_id',
			    	'content' => $order_id
			    ),
			    array(
			    	'name' => 'order_content',
			    	'content' => $order_content
			    ),
			    array(
			    	'name' => 'order_total',
			    	'content' => $order['total']
			    )
			);
			$data['to'] = array(
			    array(
			        'email' => $user['email'],
			        'name' => $user['f_name'],
			        'type' => 'to'
			    )
			);
			$data['global_merge_vars'] = array(
				array(
					'name' => 'order_id',
					'content' => $order_id
				)
			);
			return self::send((object) $data);
		}

		static function order_4($order_id) {
			global $db;
			$order = $db->execute('select uid from site_orders where id = ' . $order_id);
			$order = $order[0];
			$user = $db->execute('select email, f_name from site_users where id = ' . $order['uid']);
			$user = $user[0];
			$data = array();
			$data['template_name'] = 'order_4';
			$data['template_content'] = array();
			$data['to'] = array(
			    array(
			        'email' => $user['email'],
			        'name' => $user['f_name'],
			        'type' => 'to'
			    )
			);
			$data['global_merge_vars'] = array(
				array(
					'name' => 'order_id',
					'content' => $order_id
				),
				array(
					'name' => 'username',
					'content' => $user['f_name']
				)
			);
			return self::send((object) $data);
		}

		static function order_5($order_id) {
			global $db;
			$order = $db->execute('select uid from site_orders where id = ' . $order_id);
			$order = $order[0];
			$user = $db->execute('select email, f_name from site_users where id = ' . $order['uid']);
			$user = $user[0];
			$general = $db->execute('select delivery_street, delivery_house, delivery_appartment from general');
			$general = $general[0];
			$data = array();
			$data['template_name'] = 'order_5';
			$data['template_content'] = array();
			$data['to'] = array(
			    array(
			        'email' => $user['email'],
			        'name' => $user['f_name'],
			        'type' => 'to'
			    )
			);
			$data['global_merge_vars'] = array(
				array(
					'name' => 'order_id',
					'content' => $order_id
				),
				array(
					'name' => 'username',
					'content' => $user['f_name']
				),
				array(
					'name' => 'carry_address',
					'content' => 'Ул. '.$general['delivery_street'].' дом '.$general['delivery_house'].', оф. '.$general['delivery_appartment']
				)
			);
			return self::send((object) $data);
		}

		static function order_6($order_id) {
			global $db;
			$order = $db->execute('select uid from site_orders where id = ' . $order_id);
			$order = $order[0];
			$user = $db->execute('select email, f_name from site_users where id = ' . $order['uid']);
			$user = $user[0];
			$data = array();
			$data['template_name'] = 'order_6';
			$data['template_content'] = array();
			$data['to'] = array(
			    array(
			        'email' => $user['email'],
			        'name' => $user['f_name'],
			        'type' => 'to'
			    )
			);
			$data['global_merge_vars'] = array(
				array(
					'name' => 'order_id',
					'content' => $order_id
				),
				array(
					'name' => 'username',
					'content' => $user['f_name']
				)
			);
			return self::send((object) $data);
		}

		static function order_7($order_id) {
			global $db;
			$order = $db->execute('select uid from site_orders where id = ' . $order_id);
			$order = $order[0];
			$user = $db->execute('select email, f_name from site_users where id = ' . $order['uid']);
			$user = $user[0];
			$data = array();
			$data['template_name'] = 'order_7';
			$data['template_content'] = array();
			$data['to'] = array(
			    array(
			        'email' => $user['email'],
			        'name' => $user['f_name'],
			        'type' => 'to'
			    )
			);
			$data['global_merge_vars'] = array(
				array(
					'name' => 'order_id',
					'content' => $order_id
				),
				array(
					'name' => 'username',
					'content' => $user['f_name']
				)
			);
			return self::send((object) $data);
		}

	}

?>