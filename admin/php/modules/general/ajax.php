<?php

	Class Mod_general_ajax {

		static function update_val($data) {
			global $db;
			$db->execute('update general set '.$data->assign.' = "'.$data->val.'"');
			return true;
		}

		static function get_vars() {
			global $db;
			$settings = $db->execute('select * from general');
			$users = $db->execute('select email from users');
			$settings = $settings[0];
			$users = $users;
			$user_mails = '';
			foreach ($users as $row) {
				$user_mails .= $row['email'] . ";";
			}
			$user_mails = rtrim($user_mails, ";");
			$settings['user_mails'] = $user_mails;
			return array(
				'settings' => $settings
			);
		}

		static function add_notification_email($data) {
			global $db;
			$db->execute('update general set notification_emails = concat(notification_emails, ";", "'.$data->notification_email.'")');
			return true;
		}

		static function invite($data) {
			global $db;
			$invite_hash = md5($data->email);
			$db->execute('insert into users(email, reg_hash, access_level) values("'.$data->email.'","'.$invite_hash.'", 3)');
			$message = "Для регистрации в админ панели Bookagoo пройдите по ссылке <a href='http://bookagoo.webpoetry.org/admin/reg.html#".$invite_hash."'>http://bookagoo.webpoetry.org/admin/reg.html#".$invite_hash."</a>";
			$subject = "Bookagoo - приглашение";
		    $from = "Bookagoo<info@webpoetry.org>";
		    $headers = 'Content-type: text/html; charset=utf-8' . "\r\n";
		    $headers .= "From:" . $from;
		    mail($data->email, $subject, $message, $headers);
			return true;
		}

	}

?>