<?php

	Class Mod_orders_ajax {

		static function get_prices() {
			global $db;
			$res = $db->execute('select card_price, box_price from general');
			$res = $res[0];
			return array(
				'box' => $res['box_price'],
				'card' => $res['card_price']
			);
		}

		static function set_coupon($data) {
			global $db;
			$res = $db->execute('select * from coupons where code = "'.$data->coupon_code.'"');
			if (sizeof($res) != 0) {
				$res = $res[0];
				if ($res['active'] == '1' && $res['expire_date'] > date("Y-m-d")) {
					if ($res['type'] == '0') {
						$db->execute('update site_orders set discount_total = "'.(-$res['discount']).'", coupon = "'.$data->coupon_code.'", total = cards_total + box_total + discount_total where id = ' . $data->order_id);
					} else {
						$db->execute('update site_orders set discount_total = -cards_total * '.($res['discount'] / 100).', coupon = "'.$data->coupon_code.'", total = cards_total + box_total + discount_total where id = ' . $data->order_id);
					}
					$ret = $db->execute('select discount_total, total from site_orders where id = ' . $data->order_id);
					if (!$res['endless'])
						$db->execute('update coupons set active = 0 where code = "'.$data->coupon_code.'"');
					return $ret[0];
				} else {
					return array('error' => '1');
				}
			}
			return false;
		}

		static function update_box_quantity($data) {
			global $db;
			$prices = self::get_prices();
			$db->execute('update site_orders set box_total = ' . $data->box_quantity * $prices['box'] . ', box_quantity = ' . $data->box_quantity . ', total = cards_total + box_total + discount_total + delivery_total where id = ' . $data->order_id);
			$total = $db->execute('select box_total, total from site_orders where id = ' . $data->order_id);
			return $total[0];
		}

		static function update_delivery_price($data) {
			global $db;
			$db->execute('update site_orders set delivery_total = ' . $data->delivery_total . ', total = cards_total + box_total + discount_total + delivery_total where id = ' . $data->order_id);
			$total = $db->execute('select total from site_orders where id = ' . $data->order_id);
			return $total[0];
		}

		static function update_delivery_type($data) {
			global $db;
			$db->execute('update site_orders set delivery = '.$data->delivery_type.' where id = ' . $data->order_id);
			return true;
		}

		static function update_order_status($data) {
			global $db, $ctd;
			$db->execute('update site_orders set status = '.$data->order_status.' where id = ' . $data->order_id);
			switch ($data->order_status) {
				case 0:
					return $ctd::exec('mailer.order_0', $data->order_id);
					break;
				case 1:
					return $ctd::exec('mailer.order_1', $data->order_id);
					break;
				case 4:
					return $ctd::exec('mailer.order_4', $data->order_id);
					break;
				case 5:
					return $ctd::exec('mailer.order_5', $data->order_id);
					break;
				case 6:
					return $ctd::exec('mailer.order_6', $data->order_id);
					break;
				case 7:
					return $ctd::exec('mailer.order_7', $data->order_id);
					break;
				default:
					break;
			}
			return true;
		}

		static function update_delivery_address($data) {
			global $db;
			$db->execute('update site_orders set delivery_address = "'.$data->delivery_address.'" where id = ' . $data->order_id);
			return true;
		}

		static function person_orders($data) {
			global $db;
			$orders = $db->execute('select * from site_orders where uid = ' . $data->uid . ' order by id desc');
			$info = $db->execute('select site_users.email, site_users.f_name, site_users.l_name, site_child_info.f_name, site_child_info.birth_date from site_users inner join site_child_info on site_users.id = site_child_info.uid where site_users.id = ' . $data->uid);
			$social = $db->execute('select vk_id, fb_id from site_social_networks where uid = ' . $data->uid);
			$orders_meta = $db->execute('select max(date), sum(total), total, phone from site_orders where uid = ' . $data->uid . ' order by date asc limit 1');
			return array(
				'orders' => $orders,
				'info' => $info,
				'social' => $social,
				'orders_meta' => $orders_meta
			);
		}

		static function popup_data($data) {
			return array(
				'additional' => self::get_additional($data),
				'details' => self::get_details($data)
			);
		}

		static function update_details($data) {
			global $db;
			$db->execute('update site_orders_details set quantity = ' . $data->quantity . ' where order_id = ' . $data->order_id . ' and card_id = ' . $data->card_id);
			$cards_total = 0;
			$details = $db->execute('select quantity, card_id from site_orders_details where order_id = ' . $data->order_id);
			$price = self::get_prices();
			foreach ($details as $row) {
				if ($row['card_id'] !== '0')
					$cards_total += $price['card'] * $row['quantity'];
			}
			$db->execute('update site_orders set cards_total = "' . $cards_total . '", total = cards_total + box_total + discount_total + delivery_total where id = ' . $data->order_id);
			$ret = $db->execute('select cards_total, total from site_orders where id = ' . $data->order_id);
			return $ret[0];
		}

		static function get_additional($data) {
			global $ctd, $db;
			if (!isset($data->uid)) {
				$uid = $ctd::exec('user.uid');
			} else {
				$uid = $data->uid;
			}
			$additional = $db->execute('select card_id, var_value, var_id from site_constructor_values where card_id > 8 and uid = ' . $uid . ' order by card_id asc');
	
			$ret = array();
			$i = 0;
			foreach ($additional as $row) {
				$ret[$row['card_id']] = array();
				if (!isset($ret[$row['card_id']])) {
					$ret[$row['card_id']]['title'] = 'Название раздела';
				}
				if ($row['var_id'] === '0') {
					$ret[$row['card_id']]['title'] = $row['var_value'];
				}
			}
			return $ret;
		}

		static function get_details($data) {
			global $db;
			$details = $db->execute('select card_id, quantity from site_orders_details where order_id = ' . $data->order_id);
			return $details;
		}

		static function calculate_pages($data) {
			global $db;
			$res = $db->execute('select count(*) from site_orders where name like "%'.$data->search_query.'%"');
			$l = 10;
			$rows = $res[0][0];
			$pages = ceil($rows / $l);
			return $pages;
		}

		static function fetch_list($data) {
			global $db;
			$page = $data->page - 1;
			$l = 10;
			$limit = 'limit ' . $l;
			$offset = 'offset ' . $page * $l;
			$res = $db->execute('select * from site_orders where name like "%'.$data->search_query.'%" order by id desc ' . $limit . ' ' . $offset);
			$ret = array();
			foreach ($res as $row) {
				array_push($ret,
					array(
						'id' => $row['id'],
						'uid' => $row['uid'],
						'date' => date('d.m', strtotime($row['date'])),
						'total' => $row['total'],
						'name' => $row['name'],
						'phone' => $row['phone'],
						'status' => self::status_name($row['status'])
					)
				);
			}
			return $ret;
		}

		static function status_name($status_id) {
			global $db;
			$res = $db->execute('select name from status where id = ' . $status_id);
			if (sizeof($res) > 0) {
				return $res[0][0];
			} else {
				return '';
			}
		}

	}

?>