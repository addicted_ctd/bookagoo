<?php

	Class Mod_ecommerce_ajax {

		static function add_item($data) {
			global $db, $user;
			$user->allowed(3);
			$id = $db->execute("insert into catalog(name) values('".$data->name."')", true);
			$db->execute("insert into catalog_meta(id, article, in_stock, quantity, image, price) values(".$id.", '".$data->article."', '".$data->in_stock."', '".$data->quantity."', '".$data->image."', '".$data->price."')");
			return true;
		}

		static function fetch_list($data) {
			global $db;
			$res = $db->execute("select catalog.id, catalog.name, catalog_meta.in_stock, catalog_meta.article, catalog_meta.quantity, catalog_meta.image, catalog_meta.price from catalog inner join catalog_meta on catalog.id = catalog_meta.id");
			return $res;
		}

		static function quick_update_item($data) {
			global $db, $user;
			$user->allowed(3);
			$db->execute("update catalog_meta set " . $data->to_update . " = '" . $data->new_val . "' where id = " . $data->id);
			$db->execute("update catalog set " . $data->to_update . " = '" . $data->new_val . "' where id = " . $data->id);
			return true;
		}

		static function update($data) {
			global $db, $user;
			$user->allowed(3);
			$db->execute("update catalog set name = '" . $data->name . "', category = '".$data->categories."', tags = '".$data->tags."' where id = " . $data->id);
			$db->execute("update catalog_meta set article = '".$data->article."', price = '".$data->price."', quantity = ".$data->quantity.", in_stock = ".$data->in_stock.", description = '".$data->description."', image = '".$data->image."', date_add = STR_TO_DATE('".$data->date_add."', '%d.%m.%Y'), custom_fields = '".$data->custom_fields."' where id = " . $data->id);
			return true;
		}

		static function save_custom_field($data) {
			global $db, $user;
			$user->allowed(3);
			$db->execute("insert into catalog_custom_fields(name, title, default_val, type) values('".$data->name."', '".$data->title."', '".$data->default_val."', '".$data->type."')");
			return true;
		}

		static function fetch_custom_fields($data) {
			global $db;
			$res = $db->execute("select id, name, title, default_val, type from catalog_custom_fields");
			$to_return = array();
			foreach ($res as $row) {
				$field = array(
					"id" => $row['id'],
					"name" => $row['name'],
					"title" => $row['title'],
					"default_val" => $row['default_val'],
					"textarea" => $row['type'] == 1 ? true : NULL,
					"text_input" => $row['type'] == 0 ? true : NULL,
					"number_input" => $row['type'] == 2 ? true : NULL,
					"file" => $row['type'] == 3 ? true : NULL
				);
				array_push($to_return, $field);
			}
			return $to_return;
		}

		static function get_item($data) {
			global $db;
			$res = $db->execute("select catalog.*, catalog_meta.* from catalog inner join catalog_meta on catalog.id = catalog_meta.id where catalog.id = " . $data->id);
			foreach ($res as $row) {
				$to_return = array(
					'id' => $row['id'],
					'name' => $row['name'],
					'image' => $row['image'],
					'price' => $row['price'],
					'in_stock' => $row['in_stock'],
					'description' => $row['description'],
					'article' => $row['article'],
					'quantity' => $row['quantity'],
					'date_add' => date("d.m.Y", strtotime($row['date_add'])),
					'category' => $row['category'],
					'tags' => $row['tags'],
					'custom_fields' => self::decode_custom_fields($row['custom_fields'])
				);
			}
			return $to_return;
		}

		static function decode_custom_fields($str) {
	    	$res = array();
	    	$field = explode("###", $str);
	    	for ($i = 0; $i < sizeof($field); $i++) {
	    		$values = explode("##", $field[$i]);
	    		if (sizeof($values) > 1) {
	    			$res[$values[0]] = $values[1];
	    		}
	    	}
	    	return $res;
	    }

	}

?>