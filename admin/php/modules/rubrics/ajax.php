<?php
error_reporting(0);

	Class Mod_rubrics_ajax {

		static function fetch_list($data) {
			global $db;
			$res = $db->execute("select * from rubrics");
			if ($data->more_info) {
				$to_return = array();
				$res = $db->execute("select rubrics.id, rubrics.name, rubrics_meta.image from rubrics inner join rubrics_meta on rubrics.id = rubrics_meta.id");
				foreach ($res as $row) {
					$info = array(
						'id' => $row['id'],
						'name' => $row['name'],
						'children' => sizeof(self::select_children($row['id'])),
						'image' => $row['image']
					);
					array_push($to_return, $info);
				}
				return $to_return;
			}
			return $res;
		}

		static function select_children($id) {
			global $db;
			$res = $db->execute("select id from posts where rubric = " . $id);
			foreach ($res as $row) {
				$info = array(
					'id' => $row['id']
				);
			}
			return $info;
		}

		static function fetch($data) {
			global $db;
			$res = $db->execute("select rubrics.id, rubrics.name, rubrics_meta.description, rubrics_meta.image from rubrics inner join rubrics_meta on rubrics.id = rubrics_meta.id where rubrics.id = " . $data->id);
			foreach ($res as $row) {
				$info = array(
					'id' => $row['id'],
					'name' => $row['name'],
					'description' => $row['description'],
					'image' => $row['image']
				);
			}
			return $info;
		}

		static function remove_rubric($data) {
			global $db, $user;
			$user->allowed(3);
			$db->execute('delete from rubrics where id = ' . $data->id);
			$db->execute('delete from rubrics_meta where id = ' . $data->id);
			$db->execute('delete from rubrics_fields where rubric_id = '. $data->rubric_id);
			return true;
		}

		static function remove_custom_field($data) {
			global $db, $user;
			$user->allowed(3);
			$db->execute('delete from rubrics_fields where id = ' . $data->id);
			return true;
		}

		static function save_custom_field($data) {
			global $db, $user;
			$user->allowed(3);
			if ($data->id == 'new') {
				$db->execute("insert into rubrics_fields(rubric_id, name, title, default_val, type) values('".$data->rubric_id."', '".$data->name."', '".$data->title."', '".$data->default_val."', '".$data->type."')");
			} else {
				$db->execute("update rubrics_fields set type = '".$data->type."', name = '".$data->name."', title = '".$data->title."', default_val = '".$data->default_val."' where id = " . $data->id);
			}
			return true;
		}

		static function fetch_custom_fields($data) {
			global $db;
			$res = $db->execute("select id, name, title, default_val, type from rubrics_fields where rubric_id = " . $data->id);
			$to_return = array();
			foreach ($res as $row) {
				$field = array(
					"id" => $row['id'],
					"name" => $row['name'],
					"title" => $row['title'],
					"default_val" => $row['default_val'],
					"textarea" => $row['type'] == 1 ? true : NULL,
					"text_input" => $row['type'] == 0 ? true : NULL,
					"number_input" => $row['type'] == 2 ? true : NULL,
					"file" => $row['type'] == 3 ? true : NULL,
					"type" => $row['type']
				);
				array_push($to_return, $field);
			}
			return $to_return;
		}

		static function save($data) {
			global $db, $user;
			$user->allowed(3);
			$id = $data->id;
			if ($id == "new") {
				$q = "insert into rubrics(name) values ('".$data->name."')";
				$id = $db->execute($q, true);
				$q = "insert into rubrics_meta(id, image, description) values ('".$id."', '".$data->image."', '".$data->description."')";
			} else {
				$q = "update rubrics set name = '".$data->name."' where id = ".$data->id;
				$db->execute($q);
				$q = "update rubrics_meta set image = '".$data->image."', description = '".$data->description."' where id = ".$data->id;
			}
			$db->execute($q);
			return $id;
		}

	}

?>