<?php
	Class Mod_user_ajax {

		static function check_auth() {
			session_start();
			if (isset($_SESSION['token'])) {
				$session_token = $_SESSION['token'];
			} else {
				session_write_close();
				return false;
			}
			session_write_close();
			if (isset($session_token) && isset($_COOKIE['token']) && $session_token == $_COOKIE['token']) {
				return true;
			} else {
				return false;
			}
		}

		static function check_reg_hash($data) {
			global $db;
			$res = $db->execute('select email, name from users where reg_hash = "'.$data->reg_hash.'"');
			if (sizeof($res) === 0) {
				return array('error' => 1);
			} else {
				$res = $res[0];
				if ($res['name'] !== '') return array('error' => 1);
			}
			return $res['email'];
		}

		static function register($data) {
			global $db;
			$name_exists = $db->execute('select email from users where name = "'.$data->user.'"');
			if (sizeof($name_exists) > 0) {
				return array('error' => 1);
			}
			$db->execute('update users set name = "'.$data->user.'", pass = "'.md5($data->pass).'" where reg_hash = "'.$data->reg_hash.'"');
			return true;
		}

		static function get_user($data) {
			global $db;
			if ($data->id == "me") {
				$uid = self::uid();
				$res = $db->execute("select * from users_meta where id = " . $uid);
			} else {
				$res = $db->execute("select * from users_meta where id = " . $data->id);
			}
			return $res;
		}

		static function log($data) {
			global $db;
			$uid = self::uid();
			$res = $db->execute("insert into users_log(uid, event) values(".$uid.", '".$data->event."')");
			return $res;
		}

		static function update($data) {
			global $db;
			$uid = self::uid();
			$db->execute("update users_meta set name = '".$data->name."', phone = '".$data->phone."', role = '".$data->role."', image = '".$data->image."', email = '".$data->email."'");
			return true;
		}

		static function uid() {
			session_start();
			$uid = $_SESSION['uid'];
			session_write_close();
			return $uid;
		}

		static function get_log() {
			global $db;
			$uid = self::uid();
			$res = $db->execute("select * from users_log where uid = " . $uid . " order by time desc");
			return $res;
		}

		static function login($data) {
			global $db;
			$res = $db->execute("select id, pass, access_level from users where name = '".$data->name."'");
			foreach ($res as $row) {
				$pass = $row['pass'];
				$uid = $row['id'];
				$access_level = $row['access_level'];
			}
			if (isset($pass) && $pass != "") {
				if (md5($data->password) == $pass) {
					session_start();
					$token = md5(time() . $data->name);
					$_SESSION['token'] = $token;
					$_SESSION['uid'] = $uid;
					$_SESSION['access_level'] = $access_level;
					setcookie("token", $token, strtotime('+3 days'));
					session_write_close();
					return true;
				}
			}
			return false;
		}

		static function logout() {
			session_start();
		    session_unset();
		    session_destroy();
		    session_write_close();
		    return true;
		}

		static function allowed($level_needed) {
			session_start();
			$access_level = $_SESSION['access_level'];
			session_write_close();
			if (!(self::check_auth() && $access_level >= $level_needed)) {
				exit;
			}
		}

		static function get_list($data) {
			global $db;
			$page = $data->page - 1;
			$l = 10;
			$limit = 'limit ' . $l;
			$offset = 'offset ' . $page * $l;
			$info = $db->execute('select id, f_name, l_name, email, reg_date, last_login from site_users where f_name like "%'.$data->search_query.'%" or email like "%'.$data->search_query.'%" or l_name like "%'.$data->search_query.'%" or m_name like "%'.$data->search_query.'%" ' . $limit . ' ' . $offset);
			$ret = array();
			foreach ($info as $row) {
				$order_info = $db->execute('select count(*), sum(total) from site_orders where uid = ' . $row['id'] . ' limit 1');
				array_push($ret, array(
						'id' => $row['id'],
						'f_name' => $row['f_name'],
						'l_name' => $row['l_name'],
						'email' => $row['email'],
						'reg_date' => date('d.m.y', strtotime($row['reg_date'])),
						'last_login' => date('d.m.y', strtotime($row['last_login'])),
						'orders' => $order_info[0][0] === 0 ? NULL : $order_info[0][0],
						'total' => $order_info[0][1]
					)
				);
			}
			return $ret;
		}

		static function calculate_pages($data) {
			global $db;
			$res = $db->execute('select count(*) from site_users where f_name like "%'.$data->search_query.'%" or email like "%'.$data->search_query.'%" or l_name like "%'.$data->search_query.'%" or m_name like "%'.$data->search_query.'%"');
			$l = 10;
			$rows = $res[0][0];
			$pages = ceil($rows / $l);
			return $pages;
		}

	}

?>