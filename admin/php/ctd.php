<?php
include('settings.php');
include('util.php');
include('db.php');
include('view.php');

global $db, $util;
$db = new db();
$util = new util();

Class Ctd {
	static function exec($request, $data = array()) {
		global $util;
		$request = explode(".", $request);
		if (sizeof($request) === 2) {
			$util->sanitize($data);
			if (gettype($data) == "array") {
				$data = (object)$data;
			}
			$module = $request[0];
			$method = $request[1];
			$mod_class = "Mod_" . $module . "_ajax";
			if (is_file(__DIR__ . "/modules/" . $module . "/ajax.php")) {
				require_once(__DIR__ . "/modules/" . $module . "/ajax.php");
				$mod_class = new $mod_class;
				return $mod_class::$method($data);
			} else {
				return "error";
			}
		}
		return false;
	}

	static function display($var, $last_chance = "") {
		if (isset($var)) {
			echo $var;
		} else {
			echo $last_chance;
		}
	}
}

$ctd = new Ctd();