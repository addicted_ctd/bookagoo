<?php
error_reporting(0);
header("Cache-Control: no-cache, must-revalidate");
include('settings.php');
include('util.php');
include('db.php');
include('modules/user/ajax.php');

global $db, $util, $ctd, $user;
$db = new db();
$util = new util();
$user = new Mod_user_ajax();

Class Ctd {
	static function exec($request, $data = array()) {
		$request = explode(".", $request);
		if (sizeof($request) === 2) {
			if (gettype($data) == "array") {
				$data = (object)$data;
			}
			$module = $request[0];
			$method = $request[1];
			$mod_class = "Mod_" . $module . "_ajax";
			if (is_file(__DIR__ . "/modules/" . $module . "/ajax.php")) {
				require_once(__DIR__ . "/modules/" . $module . "/ajax.php");
				$mod_class = new $mod_class;
				return $mod_class::$method($data);
			} else {
				return "error";
			}
		}
		return false;
	}
}

$ctd = new Ctd();

$request = $_POST['mod'];
$request = explode(".", $request);
if (isset($_POST['data'])) {
	$data = json_decode($_POST['data']);
} else {
	$data = array();
}

if (sizeof($request) === 2) {
	$module = $request[0];
	$method = $request[1];
	$mod_class = "Mod_" . $module . "_ajax";
	if (is_file("modules/" . $module . "/ajax.php")) {
		require_once("modules/" . $module . "/ajax.php");
		$mod_class = new $mod_class;
		print_r(json_encode(array("data" => $mod_class::$method($data))));
	} else {
		echo "no such module";
	}
}