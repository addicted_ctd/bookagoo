<?php

	Class Mod_constructor_ajax {

		static function get_child_info($data) {
			global $ctd, $db, $util;
			if (!isset($data->uid)) {
				$uid = $ctd::exec('user.uid');
			} else {
				$uid = $data->uid;
			}
			$child = $db->execute('select gender, f_name, birth_date from site_child_info where uid = ' . $uid);
			$child[0]['birth_date'] = $util->reverse_mask_date_format($child[0]['birth_date']);
			return $child;
		}

		static function get_vars($data) {
			global $ctd, $db;
			if (!isset($data->uid)) {
				$uid = $ctd::exec('user.uid');
			} else {
				$uid = $data->uid;
			}
			$res = $db->execute('select * from site_constructor_values where uid = ' . $uid . ' and card_id = ' . $data->card_id);
			$ret = array();
			foreach ($res as $row) {
				$ret[$row['var_id']] = $row['var_value'];
			}
			return $ret;
		}

		static function write($data) {
			global $ctd, $db;
			$uid = $ctd::exec('user.uid');
			$exist = $db->execute('select id from site_constructor_values where uid = ' . $uid . ' and card_id = ' . $data->card_id . ' and var_id = ' . $data->var_id);
			if (sizeof($exist) > 0) {
				$db->execute('update site_constructor_values set var_value = "'.addslashes($data->val).'", updated = now() where id = ' . $exist[0]['id']);
			} else {
				$query = $db->insert_query('site_constructor_values', 
					(object) array(
						'uid' => $uid,
						'var_id' => $data->var_id,
						'card_id' => $data->card_id,
						'var_value' => addslashes($data->val),
						'side' => $data->side
					)
				);
				$db->execute($query);
			}
			return true;
		}

		static function get_additional($data) {
			global $ctd, $db;
			if (!isset($data->uid)) {
				$uid = $ctd::exec('user.uid');
			} else {
				$uid = $data->uid;
			}
			$additional = $db->execute('select card_id, var_value, var_id from site_constructor_values where card_id > 8 and uid = ' . $uid . ' order by card_id asc');
			$ret = array();
			foreach ($additional as $row) {
				if (!isset($ret[$row['card_id']])) {
					$ret[$row['card_id']] = 'Название раздела';
				}
				if ($row['var_id'] === '0') {
					$ret[$row['card_id']] = $row['var_value'];
				}
			}
			return $ret;
		}

	}