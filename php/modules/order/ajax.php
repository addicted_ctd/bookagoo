<?php

	Class Mod_order_ajax {

		static function get_prices() {
			global $db;
			$res = $db->execute('select card_price, box_price from general');
			$res = $res[0];
			return array(
				'box' => $res['box_price'],
				'card' => $res['card_price']
			);
		}

		static function get_regions() {
			global $db;
			$res = $db->execute('select * from region where country_id = 3159 order by sort_pos desc');
			return $res;
		}

		static function list_cities($data) {
			global $db;
			$res = $db->execute('select name from city where region_id = "'.$data->region_id.'" order by sort_pos desc');
			return $res;
		}

		static function place_order($data) {
			global $db, $ctd;

			$uid = $ctd::exec('user.uid');
			if (isset($data->coupon->code)) {
				$data->coupon = $data->coupon->code;
				$db->execute('update coupons set apply_date = now() where code = "' . $data->coupon . '"');
				$db->execute('update coupons set active = 0 where code = "' . $data->coupon . '" and endless != 1');
			} else {
				$data->coupon = '';
			}
			$order_id = $db->execute('insert into site_orders(uid, total, cards_total, box_total, discount_total, coupon, purchase_ids, name, phone, cards_quantity, box_quantity, delivery, delivery_address, comments) values('.$uid.', "'.$data->total->all.'", "'.$data->total->cards.'", "'.$data->total->box.'", "'.$data->total->discount.'", "'.$data->coupon.'", "'.implode(',', $data->cards).'","'.$data->name.'", "'.$data->phone.'", '.$data->cards_quantity.', '.$data->box_quantity.', '.$data->delivery.', "'.$data->delivery_address.'", "'.$data->comments.'")', true);


			for ($i = 0; $i < sizeof($data->cards); $i++) {
				if ($data->cards[$i] !== 0) {
					$db->execute('insert into site_orders_details(order_id, card_id, quantity) values('.$order_id.', '.$data->cards[$i].', '.$data->cards_quantity_details->{$data->cards[$i]}.')');
				} else {
					$db->execute('insert into site_orders_details(order_id, card_id, quantity) values('.$order_id.', '.$data->cards[$i].', '.$data->box_quantity.')');
				}
			}

			$pdf_dir = $_SERVER['DOCUMENT_ROOT'] . '/admin/order/' . $order_id . '/';
			if (!file_exists($pdf_dir)) {
			    mkdir($pdf_dir, 0777, true);
			}

			$sides = array('front', 'back');
			for ($i = 0; $i < sizeof($data->cards); $i++) {
				if ($data->cards[$i] !== 0) {
					for ($k = 0; $k < sizeof($sides); $k++) {
						if (!($data->cards[$i] === 1 && $sides[$k] === 'back')) {
							$filename = 'card_' . $data->cards[$i] . '_' . $sides[$k] . '_' . $uid . '_' . time();
							$make_pdf = 'wkhtmltopdf -d 300 -T 3 -L 3 -R 3 -B 3 --page-width 154 --zoom 0.902 --javascript-delay 5000 --no-stop-slow-scripts --enable-javascript --debug-javascript "http://bookagoo.webpoetry.org/print/?card='.$data->cards[$i].'&side='.$sides[$k].'&uid='.$uid.'" '.$pdf_dir.$filename.'.pdf';
							shell_exec($make_pdf);
							$to_cmyk = 'gs -dSAFER -dBATCH -dNOPAUSE -dNOCACHE -sDEVICE=pdfwrite -sColorConversionStrategy=CMYK -dProcessColorModel=/DeviceCMYK -sOutputFile='.$pdf_dir.$filename.'_cmyk.pdf '.$pdf_dir.$filename.'.pdf';
							shell_exec($to_cmyk);
							$rm_rgb = 'rm ' . $pdf_dir.$filename . '.pdf';
							shell_exec($rm_rgb);
						}
					}
				}
			}
			return $data;
		}

		static function get_orders() {
			global $db, $ctd;

			$uid = $ctd::exec('user.uid');

			$res = $db->execute('select * from site_orders where uid = ' . $uid . ' order by date desc');

			$ret = array();
			foreach ($res as $row) {
				array_push($ret,
					array(
						'id' => $row['id'],
						'purchase_ids' => $row['purchase_ids'],
						'date' => $row['date'],
						'delivery_address' => $row['delivery_address'],
						'total' => $row['total'],
						'cards_total' => $row['cards_total'],
						'box_total' => $row['box_total'],
						'cards_quantity' => $row['cards_quantity'],
						'box_quantity' => $row['box_quantity'],
						'status_id' => $row['status'],
						'status' => self::status_name($row['status'])
					)
				);
			}

			return $ret;
		}

		static function status_name($status_id) {
			global $db;
			$res = $db->execute('select name from status where id = ' . $status_id);
			if (sizeof($res) > 0) {
				return $res[0][0];
			} else {
				return '';
			}
		}

		static function get_coupon($data) {
			global $db;
			$res = $db->execute('select * from coupons where active != 0 and code = "'.strtoupper($data->code).'" and expire_date > now() limit 1');
			if (sizeof($res) == 0) {
				return array(
					'error' => 1
				);
			}
			$ret = array();
			foreach ($res as $row) {
				$ret['discount'] = $row['discount'];
				$ret['type'] = $row['type'];
			}
			return $ret;
		}

		static function generate_preview($data) {
			global $db, $ctd;

			$uid = $ctd::exec('user.uid');
			$new_file_needed = false;
			$filename = 'card_' . $data->card_id . '_' . $data->side . '_' . $uid . '_' . time();
			$preview_dir = $_SERVER['DOCUMENT_ROOT'] . 'preview/';
			$pdf_dir = $_SERVER['DOCUMENT_ROOT'] . 'pdf/';

			$pdfs = $db->execute('select filename, updated from site_pdf_preview where uid = ' . $uid . ' and card_id = ' . $data->card_id . ' and side = "' . $data->side . '" limit 1');
			$updated_vars = $db->execute('select max(updated) from site_constructor_values where uid = ' . $uid . ' and card_id = ' . $data->card_id . ' and side = "' . $data->side . '"');
			if (sizeof($pdfs) === 0) {
				if ($updated_vars[0][0] === NULL) {
					$filename = 'default_' . $data->card_id . '_' . $data->side;
				} else {
					$new_file_needed = true;
					$db->execute('insert into site_pdf_preview(uid, card_id, side, filename) values('.$uid.', '.$data->card_id.', "'.$data->side.'", "'.$filename.'")');
				}
			} else {
				foreach ($pdfs as $pdf) {
					$filename = $pdf['filename'];
					$filedate = $pdf['updated'];
				}
				if ($updated_vars[0][0] !== NULL && $updated_vars[0][0] > $filedate) {
					$new_file_needed = true;
				}
			}

			if ($new_file_needed) {
				$make_jpg = 'wkhtmltoimage --width 969 "http://bookagoo.webpoetry.org/print/?card='.$data->card_id.'&side='.$data->side.'&uid='.$uid.'" '.$preview_dir.$filename.'.jpg';
				shell_exec($make_jpg);
				//$to_jpg = 'convert -verbose -density 150 -trim '.$pdf_dir.$filename.'.pdf -quality 60 -sharpen 0x1.0 '.$preview_dir.$filename.'.jpg';
				//shell_exec($to_jpg);
				//$to_cmyk = 'gs -dSAFER -dBATCH -dNOPAUSE -dNOCACHE -sDEVICE=pdfwrite -sColorConversionStrategy=CMYK -dProcessColorModel=/DeviceCMYK -sOutputFile='.$pdf_dir.$filename.'_cmyk.pdf '.$pdf_dir.$filename.'.pdf';
				//shell_exec($to_cmyk);
				$db->execute('update site_pdf_preview set updated = now() where uid = ' . $uid . ' and card_id = ' . $data->card_id . ' and side = "' . $data->side . '"');
			}
			return array(
				'filename' => $filename . '.jpg',
				'card_id' => $data->card_id,
				'side' => $data->side,
				'file_needed' => $new_file_needed
			);
		}

	}