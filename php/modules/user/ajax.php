<?php

	define("VK_APP_ID", '5097470');
	define("VK_APP_SECRET", 'ttXxUbA8ZGy970rvHhVA');

	Class Mod_user_ajax {

		static function register($data) {
			global $db, $util, $ctd;

			//check if email exists

			if (self::check_mail((object)array('email' => $data->email)))
				return array("error" => 1);

			//generate password md5 hash

			$pwd = self::generate_pwd($data->password);

			//write new user

			$query = $db->insert_query('site_users', 
				(object) array(
					'email' => $data->email,
					'password' => $pwd,
					'reg_date' => date("Y-m-d H:i:s")
				)
			);
			$data->id = $db->execute($query, true);

			//write newsletter subscription

			self::update_subscription((object) array(
					'uid' => $data->id,
					'subscription' => $data->subscription
				)
			);

			//write child info

			$data->kid_birth_date = $util->mask_date_format($data->kid_birth_date);

			self::write_child_info((object) array(
					'uid' => $data->id,
					'gender' => $data->kid_gender,
					'f_name' => $data->kid_name,
					'birth_date' => $data->kid_birth_date
				)
			);

			//first login cookie

			setcookie("first_login", true, strtotime('+10 days'), '/');

			//authorize user

			self::login((object) array(
					'email' => $data->email,
					'password' => $data->password
				)
			);

			return true;
		}

		static function generate_pwd($pwd) {
			return md5($pwd);
		}

		static function update_subscription($data) {
			global $db;
			$query = $db->insert_query('site_newsletter_subscription', $data);
			$query .= ' on duplicate key update subscription = ' . $data->subscription;
			return $db->execute($query);
		}

		static function write_child_info($data) {
			global $db;
			$query = $db->insert_query('site_child_info', $data);
			return $db->execute($query);
		}

		static function check_mail($data) {
			global $db;
			$res = $db->execute("select id from site_users where email = '".$data->email."'");
			return $res;
		}

		static function delete_account() {
			global $db;
			$uid = self::uid();
			self::logout();
			$db->execute('delete from site_users where id = ' . $uid);
			$db->execute('delete from site_social_networks where uid = ' . $uid);
			$db->execute('delete from site_orders where uid = ' . $uid);
			$db->execute('delete from site_newsletter_subscription where uid ' . $uid);
			return true;
		}

		static function login($data) {
			global $db;
			$res = $db->execute("select id, password, email from site_users where email = '".$data->email."'");
			if (sizeof($res) == 0)
				return array("error" => 1);
			foreach ($res as $row) {
				$pass = $row['password'];
				$uid = $row['id'];
			}
			if (isset($pass) && $pass != "") {
				if (md5($data->password) == $pass) {
					session_start();
					$token = md5(time() . $data->email);
					$_SESSION['token'] = $token;
					$_SESSION['uid'] = $uid;
					setcookie("token", $token, strtotime('+10 days'), '/');
					session_write_close();
					$db->execute('update site_users set last_login = now() where id = ' . $uid);
					return array("error" => false);
				}
			}
			return array("error" => 2);
		}

		static function logout() {
			session_start();
		    session_unset();
		    session_destroy();
		    session_write_close();
		    if (isset($_COOKIE['token'])) {
			    unset($_COOKIE['token']);
			    setcookie('token', null, -1, '/');
			}
		    return true;
		}

		static function check_auth() {
			session_start();
			if (isset($_SESSION['token'])) {
				$session_token = $_SESSION['token'];
			} else {
				session_write_close();
				return false;
			}
			session_write_close();
			if (isset($session_token) && isset($_COOKIE['token']) && $session_token == $_COOKIE['token']) {
				return true;
			} else {
				return false;
			}
		}

		static function uid() {
			session_start();
			$uid = $_SESSION['uid'];
			session_write_close();
			return $uid;
		}

		static function check_social_exist($data) {
			global $db;
			$query = "select uid from site_social_networks where " . $data->network . " = '" . $data->social_id . "'";
			$res = $db->execute($query);
			return (sizeof($res) !== 0) ? $res[0]['uid'] : false;
		}

		static function social_disconnect($data) {
			global $db;
			$uid = self::uid();
			$query = "update site_social_networks set " . $data->network . "_id = '' where uid = " . $uid;
			return $db->execute($query);
		}

		static function vk_login($data) {
			global $db;
			$signature_ok = self::check_vk_signature($data);
			if (!$signature_ok) return array("error" => 1);
			$data = (object) array(
				'vk_id' => $data->session->user->id,
				'f_name' => $data->session->user->first_name,
				'l_name' => $data->session->user->last_name
			);

			$exist = self::check_social_exist((object) array('network' => 'vk_id', 'social_id' => $data->vk_id));
			$auth = self::check_auth();

			if (!$exist) {

				if ($auth) {
					$uid = self::uid();
					$query = $db->insert_query('site_social_networks',
						(object) array(
							'vk_id' => $data->vk_id,
							'uid' => $uid,
							'vk_f_name' => $data->f_name,
							'vk_l_name' => $data->l_name
						)
					);
					$query .= 'on duplicate key update vk_id = "'.$data->vk_id.'", vk_f_name = "'.$data->f_name.'", vk_l_name = "'.$data->l_name.'"';
					$db->execute($query);
					return true;
				}

				$query = $db->insert_query('site_users', 
					(object) array(
						'email' => '',
						'f_name' => $data->f_name,
						'l_name' => $data->l_name,
						'password' => md5(time() . $data->vk_id),
						'reg_date' => date("Y-m-d H:i:s")
					)
				);
				$data->id = $db->execute($query, true);

				self::update_subscription((object) array(
						'uid' => $data->id,
						'subscription' => 0
					)
				);

				self::write_child_info((object) array(
						'uid' => $data->id
					)
				);

				$query = $db->insert_query('site_social_networks',
					(object) array(
						'vk_id' => $data->vk_id,
						'uid' => $data->id,
						'vk_f_name' => $data->f_name,
						'vk_l_name' => $data->l_name
					)
				);

				$db->execute($query);
			}
			if (!$auth && !$exist) {
				//first login cookie
				setcookie("first_login", true, strtotime('+10 days'), '/');
			}
			if (!$auth) {

				self::authorize_by_social((object) array(
						'network' => 'vk_id',
						'social_id' => $data->vk_id
					)
				);
			}
			return $exist;
		}

		static function authorize_by_social($data) {
			global $db;
			$query = "select uid from site_social_networks where " . $data->network . " = '" . $data->social_id . "'";
			$res = $db->execute($query);
			if (sizeof($res) == 0)
				return array("error" => 1);
			$uid = $res[0]['uid'];
			session_start();
			$token = md5(time() . $data->social_id);
			$_SESSION['token'] = $token;
			$_SESSION['uid'] = $uid;
			setcookie("token", $token, strtotime('+10 days'), '/');
			session_write_close();
			$db->execute('update site_users set last_login = now() where id = ' . $uid);
			return array("error" => false);
		}

		static function check_vk_signature($data) {
  			$member = false; 
  			$sign = 'expire='.$data->session->expire.'mid='.$data->session->mid.'secret='.$data->session->secret.'sid='.$data->session->sid;
			$sign .= VK_APP_SECRET; 
			$sign = md5($sign); 
			if ($data->session->sig == $sign) { 
			  	$member = true;
			} 
  			return $member;
		}

		static function fb_login($data) {
			global $db;
			$signature_ok = self::check_fb_signature($data->signature);
			if (!$signature_ok) return array("error" => 1);
			
			$exist = self::check_social_exist((object) array('network' => 'fb_id', 'social_id' => $data->fb_id));
			$auth = self::check_auth();

			if (!$exist) {

				if ($auth) {
					$uid = self::uid();
					$query = $db->insert_query('site_social_networks',
						(object) array(
							'fb_id' => $data->fb_id,
							'uid' => $uid,
							'fb_f_name' => $data->f_name,
							'fb_l_name' => $data->l_name
						)
					);
					$query .= 'on duplicate key update fb_id = "'.$data->fb_id.'", fb_f_name = "'.$data->f_name.'", fb_l_name = "'.$data->l_name.'"';
					$db->execute($query);
					return true;
				}

				$query = $db->insert_query('site_users', 
					(object) array(
						'email' => '',
						'f_name' => $data->f_name,
						'l_name' => $data->l_name,
						'password' => md5(time() . $data->fb_id),
						'reg_date' => date("Y-m-d H:i:s")
					)
				);
				$data->id = $db->execute($query, true);

				self::update_subscription((object) array(
						'uid' => $data->id,
						'subscription' => 0
					)
				);

				self::write_child_info((object) array(
						'uid' => $data->id
					)
				);

				$query = $db->insert_query('site_social_networks',
					(object) array(
						'fb_id' => $data->fb_id,
						'uid' => $data->id,
						'fb_f_name' => $data->f_name,
						'fb_l_name' => $data->l_name
					)
				);

				$db->execute($query);
			}

			if (!$auth) {
				self::authorize_by_social((object) array(
						'network' => 'fb_id',
						'social_id' => $data->fb_id
					)
				);
			}
			return $signature_ok;
		}

		static function check_fb_signature($signed_request) {
		  	list($encoded_sig, $payload) = explode('.', $signed_request, 2); 

		  	$secret = "a8de29e10b74e0fc75992c5d885a6fae";

		  	// decode the data
		  	$sig = self::base64_url_decode($encoded_sig);
		  	$data = json_decode(self::base64_url_decode($payload), true);

		  	// confirm the signature
		  	$expected_sig = hash_hmac('sha256', $payload, $secret, $raw = true);
		  	if ($sig !== $expected_sig) {
		    	return false;
		  	}

		  	return $data;
		}

		static function base64_url_decode($input) {
		  	return base64_decode(strtr($input, '-_', '+/'));
		}

		static function update_profile($data) {
			global $db, $util, $ctd;
			$uid = self::uid();

			//check if email is used by another user
			$email_exists = $db->execute('select email from site_users where id != ' . $uid . ' and email = "'.$data->email.'"');
			if (sizeof($email_exists) > 0) return array('error' => 1);

			//check if password change request is made
			if ($data->new_pass !== '') {
				// check old pass match
				$current_pass = $db->execute('select password from site_users where id = ' . $uid);
				if (md5($data->old_pass) !== $current_pass[0]['password']) {
					return array('error' => 2);
				} else {
					//update password
					$db->execute('update site_users set password = "'.md5($data->new_pass).'" where id = ' . $uid);
				}
			}

			//check if first info update made

			$first = $db->execute('select f_name, email, m_name, l_name from site_users where id = ' . $uid);
			$first = $first[0];
			if ($first['f_name'] == '' || $first['l_name'] == '' || $first['m_name'] == '' || $first['email'] == '') {
				$ctd::exec('mailer.send_reg_mail', $data);
			}

			//update user info
			$db->execute('update site_users set email = "'.$data->email.'", f_name = "'.$data->f_name.'", l_name = "'.$data->l_name.'", m_name = "'.$data->m_name.'" where id = ' . $uid);

			//update child info
			$data->child_birth = $util->mask_date_format($data->child_birth);
			$db->execute('update site_child_info set gender = "'.$data->child_gender.'", f_name = "'.$data->child_name.'", birth_date = "'.$data->child_birth.'" where uid = ' . $uid);

			//update newsletter info
			$db->execute('update site_newsletter_subscription set subscription = ' . $data->newsletter . ' where uid = ' . $uid);

			return true;

		}

		static function get_profile() {
			global $db, $util;
			$uid = self::uid();
			$me = $db->execute('select email, f_name, m_name, l_name from site_users where id = ' . $uid);
			$child = $db->execute('select gender, f_name, birth_date from site_child_info where uid = ' . $uid);
			$child[0]['birth_date'] = $util->reverse_mask_date_format($child[0]['birth_date']);
			$newsletter = $db->execute('select subscription from site_newsletter_subscription where uid = ' . $uid);
			$social = $db->execute('select * from site_social_networks where uid = ' . $uid);
			return array(
				'me' => $me[0],
				'child' => $child[0],
				'newsletter' => $newsletter[0],
				'social' => sizeof($social) > 0 ? $social[0] : null
			);
		}

		static function feedback($data) {
			$message = 'Имя: ' . $data->name . '<br>';
			$message .= 'Email: ' . $data->email . '<br>';
			$message .= 'Тема сообщения: ' . $data->topic . '<br><br>';
			$message .= $data->msg;  
			$subject = "Обратная связь";
		    $from = "Bookagoo<info@webpoetry.org>";
		    $headers = 'Content-type: text/html; charset=utf-8' . "\r\n";
		    $headers .= "From:" . $from;
		    mail('kisselalex@gmail.com', $subject, $message, $headers);
		    return true;
		}

	}