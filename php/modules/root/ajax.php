<?php

define("MAX_SIZE", "20");
ini_set('display_errors', 1);
error_reporting(E_ALL); 

	Class Mod_root_ajax {

		static function read_content($dir) {
			$dir = $_SERVER["DOCUMENT_ROOT"] . "/" . $dir;
	        $files = array_diff(scandir($dir, 1), array('..', '.'));
	        $files_content = array();
	        for ($i = 0; $i < sizeof($files); $i++) {
	            $file = $dir . "/" . $files[$i];
	            if (is_file($file)) {
	                $file_content = file_get_contents($file);
	                array_push($files_content, $file_content);
	            }
	        }
	        return $files_content;
		}

		static function get_general($data) {
			global $db;
			$res = $db->execute('select * from general');
			return $res[0];
		}

		static function download_image($data) {
			global $settings;
			$upload_dir = $_SERVER["DOCUMENT_ROOT"] . $settings->upload_dir . '/';
			$name = md5($data->remote_img) . '.jpg';
			$ch = curl_init($data->remote_img);
			$fp = fopen($upload_dir . $name, 'wb');
			curl_setopt($ch, CURLOPT_FILE, $fp);
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
			curl_exec($ch);
			curl_close($ch);
			fclose($fp);
			list($w, $h) = getimagesize($upload_dir . $name);
	        return array(
	            'file' => $name,
	            'size' => array(
	                'width' => $w,
	                'height' => $h
	            )
	        );
		}

		static function upload_image() {
			global $settings;
			if (!isset($_FILES['photo'])) {
	            return 'No file given!';
	        } else {
	            $img_file = $_FILES['photo'];
	            $upload_dir = $_SERVER["DOCUMENT_ROOT"] . $settings->upload_dir . '/';
	            $output_dir = '/' . $settings->upload_dir . '/';
	        }
	        $valid_formats = array("jpg", "png", "gif", "bmp", "jpeg");
	        if (is_uploaded_file($img_file['tmp_name'])) {
	            $filename = stripslashes($img_file['name']);
	            $size = filesize($img_file['tmp_name']);
	            $ext = strtolower(self::get_ext($filename));
	            if (in_array($ext, $valid_formats)) {
	                if ($size < (MAX_SIZE * 1024 * 1024)) {
	                    $new_name = md5_file($img_file['tmp_name']) . "." . $ext;
	                    if (file_exists($upload_dir . $new_name)) {
	                        list($w, $h) = getimagesize($upload_dir . $new_name);
	                        return array(
	                            'file' => $new_name,
	                            'size' => array(
	                                'width' => $w,
	                                'height' => $h
	                            )
	                        );
	                    } else {
	                        if (move_uploaded_file($img_file['tmp_name'], $upload_dir . $new_name)) {
	                            list($w, $h) = getimagesize($upload_dir . $new_name);
	                            return array(
	                                'file' => $new_name,
	                                'size' => array(
	                                    'width' => $w,
	                                    'height' => $h
	                                )
	                            );
	                        }
	                    }
	                } else {
	                    return array('error' => 'Picture size exceeded!');
	                }
	            } else {
	                return array('error' => 'Unsupported format!');
	            }
	        } else {
	            return array('error' => 'Error loading file!');
	        }
	    }

	    static function get_ext($file_name) {
	        return strtolower(pathinfo($file_name, PATHINFO_EXTENSION));
	    }

	}

?>