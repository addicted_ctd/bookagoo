<?php

define("DEV", "dev_");
define("PROD", "production_");

	Class Mod_posts_ajax {

	    static function full_date($date) {
	    	$months = array("Января", "Февраля", "Марта", "Апреля", "Мая", "Июня", "Июля", "Августа", "Сентября", "Октября", "Ноября", "Декабря");
	    	return date("d", $date) . "&nbsp" . $months[((int) date("m", $date)) - 1];
	    }

	    static function get_time($date) {
	    	return date("H:i", $date);
	    }

	    static function get_list($data) {
	    	global $db;
	        $res = $db->execute("select posts.id, posts.slug, posts.date, posts.title, posts.thumb, posts.rubric, posts_meta.description, posts_meta.lead from posts inner join posts_meta on posts.id = posts_meta.id where status != 0");
	        $to_return = array();
	        foreach($res as $row) {
	            $row_data = array(
	                'thumb' => $row['thumb'] == 'none' ? NULL : $row['thumb'],
	                'id' => $row['id'],
	                'title' => $row['title'],
	                'date' => date("d.m.Y", strtotime($row['date'])),
	                'rubric' => $row['rubric'],
	                'description' => $row['description'],
	                'lead' => $row['lead'],
	                'slug' => $row['slug']
	            );
	            array_push($to_return, $row_data);
	        }
	        return $to_return;
	    }

	    static function get_post($data) {
	    	global $settings, $db;
	    	$folder = $_SERVER["DOCUMENT_ROOT"] . $settings->production_dir . '/';
	        $res = isset($data->slug) ? $db->execute("SELECT * FROM posts WHERE slug='" . $data->slug . "'") : $db->execute("SELECT * FROM posts WHERE id=" . $data->id);
	        foreach ($res as $row) {
	            $row_data = array(
	                'thumb' => $row['thumb'],
	                'id' => $row['id'],
	                'title' => $row['title'],
	                'status' => $row['status'],
	                'tpl' => file_get_contents($folder . PROD . $row['id'] . ".html"),
	                'meta' => self::get_seo((object) array("id" => $row['id']))
	            );
	        }
	        return $row_data;
	    }

	    static function get_meta($data) {
	    	global $db;
	    	$q = "select posts.id, posts.date, posts.title, posts.status, posts.category, posts.rubric, posts.tags, posts_meta.lead, posts_meta.description, posts_meta.custom_fields from posts inner join posts_meta on posts.id = posts_meta.id where posts.id = " . $data->id;
	    	$res = $db->execute($q);
	    	foreach ($res as $row) {
	    		$info = array(
	    			'id' => $row['id'],
	    			'date' => date("d.m.Y", strtotime($row['date'])),
	    			'title' => $row['title'],
	    			'visible' => $row['status'] == 0 ? NULL : true,
	    			'category' => $row['category'] != "" ? explode(",", $row['category']) : array(),
	    			'rubric' => $row['rubric'],
	    			'tags' => $row['tags'] != "" ? explode(",", $row['tags']) : array(),
	    			'lead' => $row['lead'],
	    			'description' => $row['description'],
	    			'custom_fields' => self::decode_custom_fields($row['custom_fields'])
	    		);
	    	}
	    	return $info;
	    }

	    static function get_seo($data) {
	    	global $db;
	    	$q = "select posts.id, posts.date, posts.title, posts.status, posts.category, posts.rubric, posts.tags, posts_meta.lead, posts_meta.description, posts_meta.custom_fields from posts inner join posts_meta on posts.id = posts_meta.id where posts.id = " . $data->id;
	    	$res = $db->execute($q);
	    	foreach ($res as $row) {
	    		$info = array(
	    			'title' => $row['title'],
	    			'lead' => $row['lead'],
	    			'description' => $row['description']
	    		);
	    	}
	    	return $info;
	    }

	}

?>